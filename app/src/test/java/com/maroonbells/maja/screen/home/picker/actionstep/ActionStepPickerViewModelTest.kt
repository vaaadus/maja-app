package com.maroonbells.maja.screen.home.picker.actionstep

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.mockk.verifyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class ActionStepPickerViewModelTest {

    @RelaxedMockK
    private lateinit var pinoutManager: PinoutManager

    @RelaxedMockK
    private lateinit var pinsObserver: Observer<List<NamedPin>>

    @RelaxedMockK
    private lateinit var selectedPinObserver: Observer<NamedPin>

    private lateinit var viewModel: ActionStepPickerViewModel

    @BeforeEach
    fun setUp() {
        viewModel = ActionStepPickerViewModel(pinoutManager)
        viewModel.pins.observeForever(pinsObserver)
        viewModel.selectedPin.observeForever(selectedPinObserver)
    }

    @Test
    fun onCustomizedPinoutChanged_updatesPins() {
        val pin = CustomizedPin(
            id = PinId(1),
            customName = "Hello",
            defaultName = "1",
            value = PinValue.High,
            type = Pin.Type.Digital,
            icon = Icon.Laptop,
            accessibleFromHome = true)

        viewModel.onCustomizedPinoutChanged(listOf(pin))

        verify { pinsObserver.onChanged(listOf(NamedPin(pin, null))) }
    }

    @Test
    fun onConfigureDefaultPin_updatesSelectedPin() {
        val pin = CustomizedPin(
            id = PinId(1),
            customName = "Test",
            defaultName = "A1",
            value = PinValue.Low,
            type = Pin.Type.Digital,
            icon = Icon.Blinds,
            accessibleFromHome = true)

        every { pinoutManager.pins } returns listOf(pin)

        viewModel.onConfigureDefaultPin(pin.id, pin.value)

        verifyOrder {
            pinsObserver.onChanged(listOf(NamedPin(pin, pin.value)))
            selectedPinObserver.onChanged(NamedPin(pin, pin.value))
        }
    }
}