package com.maroonbells.maja.screen.home.accessiblepins

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class AccessiblePinsViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: AccessiblePinsNavigation

    @RelaxedMockK
    private lateinit var pinoutManager: PinoutManager

    @RelaxedMockK
    private lateinit var pinsObserver: Observer<List<CustomizedPin>>

    private lateinit var viewModel: AccessiblePinsViewModel

    @BeforeEach
    fun setUp() {
        viewModel = AccessiblePinsViewModel(navigation, pinoutManager)
        viewModel.pins.observeForever(pinsObserver)
    }

    @Test
    fun onCustomizedPinoutChanged_refreshesPins() {
        val pin = CustomizedPin(
            id = PinId(0),
            customName = "Holly",
            defaultName = "0",
            value = PinValue(0),
            type = Pin.Type.Digital,
            icon = Icon.Battery,
            accessibleFromHome = true)

        viewModel.onStart()
        viewModel.onCustomizedPinoutChanged(listOf(pin))

        verify { pinsObserver.onChanged(listOf(pin)) }
    }

    @Test
    fun onClose_navigates() {
        viewModel.onClose()

        verify { navigation.onCloseAccessiblePins() }
    }

    @Test
    fun onPinAccessibilityEdited_savesIt() {
        val pinId = PinId(0)
        val accessible = true

        viewModel.onPinAccessibilityEdited(pinId, accessible)

        verify { pinoutManager.setAccessibleFromHome(pinId, accessible) }
    }
}