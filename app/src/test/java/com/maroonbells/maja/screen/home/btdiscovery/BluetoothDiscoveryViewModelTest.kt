package com.maroonbells.maja.screen.home.btdiscovery

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class BluetoothDiscoveryViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: BluetoothDiscoveryNavigation

    @RelaxedMockK
    private lateinit var bluetoothManager: BluetoothManager

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var devicesObserver: Observer<List<BluetoothDevice>>

    @RelaxedMockK
    private lateinit var discoveringObserver: Observer<Boolean>

    private lateinit var viewModel: BluetoothDiscoveryViewModel

    @BeforeEach
    fun setUp() {
        viewModel = BluetoothDiscoveryViewModel(navigation, bluetoothManager, sessionManager)
        viewModel.discoveredDevices.observeForever(devicesObserver)
        viewModel.discovering.observeForever(discoveringObserver)
        clearMocks(devicesObserver, discoveringObserver)
    }

    @Test
    fun onStart_listsPairedDevices() {
        val devices = listOf(BluetoothDevice(BluetoothAddress("ABC"), "Maja", true))

        every { bluetoothManager.pairedDevices } returns devices

        viewModel.onStart()

        verify { devicesObserver.onChanged(devices) }
    }

    @Test
    fun onStop_stopsDiscovery() {
        viewModel.onStop()

        verify {
            bluetoothManager.stopDiscovery()
            discoveringObserver.onChanged(false)
        }
    }

    @Test
    fun onBluetoothDiscoveryStarted_postsValue() {
        viewModel.onBluetoothDiscoveryStarted()

        verify { discoveringObserver.onChanged(true) }
    }

    @Test
    fun onBluetoothDiscoveryFinished_postsValue() {
        viewModel.onBluetoothDiscoveryFinished()

        verify { discoveringObserver.onChanged(false) }
    }

    @Test
    fun onDiscoveryStart_startsIt() {
        val devices = emptyList<BluetoothDevice>()

        every { bluetoothManager.pairedDevices } returns devices

        viewModel.onDiscoveryStart()

        verify {
            bluetoothManager.startDiscovery(viewModel)
            discoveringObserver.onChanged(true)
            devicesObserver.onChanged(devices)
        }
    }

    @Test
    fun onDiscoveryStop_stopsIt() {
        viewModel.onDiscoveryStop()

        verify {
            bluetoothManager.stopDiscovery()
            discoveringObserver.onChanged(false)
        }
    }

    @Test
    fun onSelectDevice_configuresBoardManager() {
        val device = BluetoothDevice(BluetoothAddress("842342"), "name", false)

        viewModel.onSelectDevice(device)

        verify { sessionManager.configure(device.address) }
    }

    @Test
    fun onClose_navigates() {
        viewModel.onClose()

        verify { navigation.onCloseBluetoothDiscovery() }
    }
}