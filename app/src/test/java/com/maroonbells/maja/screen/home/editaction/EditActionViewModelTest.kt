package com.maroonbells.maja.screen.home.editaction

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.ActionManager
import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.board.value.Pin.Type.Digital
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.mockk.verifyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class EditActionViewModelTest {

    companion object {

        private val step = Action.Step(
            pinId = PinId(12),
            pinValue = PinValue.High)

        private val customizedStep = CustomizedActionStep(
            pinName = "Red LED",
            pinType = Digital,
            pinId = step.pinId,
            pinValue = step.pinValue)

        private fun editableStep(step: CustomizedActionStep, isRemoving: Boolean) = EditableStep(
            pinId = step.pinId,
            pinName = step.pinName,
            pinValue = step.pinValue,
            pinType = step.pinType,
            isRemoving = isRemoving
        )

        private val action = CustomizedAction(
            id = ActionId(10),
            steps = listOf(customizedStep),
            name = "???",
            customName = "Custom name",
            icon = Icon.GamePad,
            color = Color.Brown,
            availableAsScene = true
        )
    }

    @RelaxedMockK
    private lateinit var navigation: EditActionNavigation

    @RelaxedMockK
    private lateinit var actionManager: ActionManager

    @RelaxedMockK
    private lateinit var titleObserver: Observer<Title>

    @RelaxedMockK
    private lateinit var nameObserver: Observer<String>

    @RelaxedMockK
    private lateinit var stepsObserver: Observer<List<EditableStep>>

    @RelaxedMockK
    private lateinit var isRemovingStepsObserver: Observer<Boolean>

    @RelaxedMockK
    private lateinit var canAddStepsObserver: Observer<Boolean>

    @RelaxedMockK
    private lateinit var iconsObserver: Observer<List<SelectableIcon>>

    @RelaxedMockK
    private lateinit var colorsObserver: Observer<List<SelectableColor>>

    @RelaxedMockK
    private lateinit var isSceneObserver: Observer<Boolean>

    private lateinit var viewModel: EditActionViewModel

    @BeforeEach
    fun setUp() {
        every { actionManager[null] } returns null

        viewModel = EditActionViewModel(navigation, actionManager)
        viewModel.title.observeForever(titleObserver)
        viewModel.name.observeForever(nameObserver)
        viewModel.steps.observeForever(stepsObserver)
        viewModel.isRemovingSteps.observeForever(isRemovingStepsObserver)
        viewModel.canAddSteps.observeForever(canAddStepsObserver)
        viewModel.icons.observeForever(iconsObserver)
        viewModel.colors.observeForever(colorsObserver)
        viewModel.isScene.observeForever(isSceneObserver)
    }

    @Test
    fun onCustomizedActionsChanged_refreshesAll_whenConfigured() {
        every { actionManager[action.id] } returns action
        every { actionManager.list } returns listOf(action)
        every { actionManager.maxStepsPerAction } returns action.steps.size
        every { actionManager.availableIcons } returns listOf(Icon.Battery, Icon.GamePad)
        every { actionManager.availableColors } returns listOf(Color.Red, Color.Brown)

        viewModel.configureActionId(action.id)
        viewModel.onCustomizedActionsChanged(listOf(action))

        verify {
            titleObserver.onChanged(Title.EditAction)
            nameObserver.onChanged(action.customName)
            stepsObserver.onChanged(listOf(editableStep(step = customizedStep, isRemoving = false)))
            isRemovingStepsObserver.onChanged(null)
            canAddStepsObserver.onChanged(false)
            iconsObserver.onChanged(
                listOf(
                    SelectableIcon(Icon.Battery, false),
                    SelectableIcon(Icon.GamePad, true)
                ))
            colorsObserver.onChanged(
                listOf(
                    SelectableColor(Color.Red, false),
                    SelectableColor(Color.Brown, true)
                ))
            isSceneObserver.onChanged(action.availableAsScene)
        }
    }

    @Test
    fun onCustomizedActionsChanged_refreshesAll_whenNotConfigured() {
        every { actionManager[action.id] } returns action
        every { actionManager.list } returns listOf(action)
        every { actionManager.maxStepsPerAction } returns action.steps.size * 2
        every { actionManager.availableIcons } returns listOf(Icon.Battery, Icon.GamePad)
        every { actionManager.availableColors } returns listOf(Color.Red, Color.Brown)
        every { actionManager.defaultIcon } returns Icon.Blinds
        every { actionManager.defaultColor } returns Color.Red

        viewModel.onCustomizedActionsChanged(listOf(action))

        verify {
            titleObserver.onChanged(Title.NewAction)
            nameObserver.onChanged("")
            stepsObserver.onChanged(emptyList())
            isRemovingStepsObserver.onChanged(null)
            canAddStepsObserver.onChanged(true)
            iconsObserver.onChanged(
                listOf(
                    SelectableIcon(Icon.Blinds, true),
                    SelectableIcon(Icon.Battery, false),
                    SelectableIcon(Icon.GamePad, false)
                ))
            colorsObserver.onChanged(
                listOf(
                    SelectableColor(Color.Red, true),
                    SelectableColor(Color.Brown, false)
                ))
            isSceneObserver.onChanged(null)
        }
    }

    @Test
    fun onNameEdited_updatesName() {
        viewModel.onNameEdited("Hello")
        verify { nameObserver.onChanged("Hello") }
    }

    @Test
    fun onRemoveSteps_updatesIsRemoving() {
        viewModel.onRemoveSteps() // enable
        viewModel.onRemoveSteps() // disable

        verifyOrder {
            isRemovingStepsObserver.onChanged(true)
            isRemovingStepsObserver.onChanged(false)
        }
    }

    @Test
    fun onStepAdded_updatesSteps() {
        every { actionManager.asCustomizedActionStep(step) } returns customizedStep

        viewModel.onStepAdded(step)

        verify { stepsObserver.onChanged(listOf(editableStep(customizedStep, false))) }
    }

    @Test
    fun onStepEdited_updatesSteps() {
        val newStep = Action.Step(PinId(20), PinValue(30))
        val newCustomizedStep = CustomizedActionStep(PinId(20), PinValue(30), "new", Digital)

        every { actionManager.asCustomizedActionStep(step) } returns customizedStep
        every { actionManager.asCustomizedActionStep(newStep) } returns newCustomizedStep

        viewModel.onStepAdded(step)
        viewModel.onStepAdded(step)
        viewModel.onStepEdited(step, newStep)

        val step1 = editableStep(customizedStep, false)
        val step2 = editableStep(newCustomizedStep, false)

        verifyOrder {
            stepsObserver.onChanged(listOf(step1))
            stepsObserver.onChanged(listOf(step1, step1))
            stepsObserver.onChanged(listOf(step2, step1))
        }
    }

    @Test
    fun onStepRemoved_updatesSteps() {
        every { actionManager.asCustomizedActionStep(step) } returns customizedStep

        viewModel.onStepAdded(step)
        viewModel.onRemoveSteps()
        viewModel.onStepRemoved(step)

        verifyOrder {
            stepsObserver.onChanged(listOf(editableStep(customizedStep, true)))
            stepsObserver.onChanged(emptyList())
        }
    }

    @Test
    fun onIconSelected_updatesPresentIcon() {
        every { actionManager.availableIcons } returns listOf(Icon.Laptop, Icon.Light, Icon.Computer)

        viewModel.onIconSelected(Icon.Light)

        verify {
            iconsObserver.onChanged(
                listOf(
                    SelectableIcon(Icon.Laptop, false),
                    SelectableIcon(Icon.Light, true),
                    SelectableIcon(Icon.Computer, false)
                ))
        }
    }

    @Test
    fun onIconSelected_updatesIcons() {
        every { actionManager.availableIcons } returns listOf(Icon.Computer)

        viewModel.onIconSelected(Icon.Blinds)

        verify {
            iconsObserver.onChanged(
                listOf(
                    SelectableIcon(Icon.Blinds, true),
                    SelectableIcon(Icon.Computer, false)
                ))
        }
    }

    @Test
    fun onColorSelected_updatesPresentColor() {
        every { actionManager.availableColors } returns listOf(Color.Red, Color.Yellow, Color.Brown)

        viewModel.onColorSelected(Color.Yellow)

        verify {
            colorsObserver.onChanged(
                listOf(
                    SelectableColor(Color.Red, false),
                    SelectableColor(Color.Yellow, true),
                    SelectableColor(Color.Brown, false)
                ))
        }
    }

    @Test
    fun onColorSelected_updatesColor() {
        every { actionManager.availableColors } returns listOf(Color.Green)

        viewModel.onColorSelected(Color.Amber)

        verify {
            colorsObserver.onChanged(
                listOf(
                    SelectableColor(Color.Amber, true),
                    SelectableColor(Color.Green, false)
                ))
        }
    }

    @Test
    fun onSceneSwitchChanged_updatesIsScene() {
        viewModel.onSceneSwitchChanged(true)

        verify { isSceneObserver.onChanged(true) }
    }

    @Test
    fun onSave_createsNewAction() {
        every { actionManager.asCustomizedActionStep(step) } returns customizedStep

        val request = UpdateCustomizedActionRequest(
            name = "Hello",
            steps = listOf(step),
            icon = Icon.Laptop,
            color = Color.Cyan,
            availableAsScene = false)

        viewModel.onNameEdited("Hello")
        viewModel.onStepAdded(step)
        viewModel.onIconSelected(Icon.Laptop)
        viewModel.onColorSelected(Color.Cyan)
        viewModel.onSceneSwitchChanged(false)

        viewModel.onSave()

        verify { actionManager.create(request) }
    }

    @Test
    fun onSave_updatesAction() {
        every { actionManager[action.id] } returns action
        every { actionManager.asCustomizedActionStep(step) } returns customizedStep

        val request = UpdateCustomizedActionRequest(
            name = "Hello",
            steps = emptyList(),
            icon = Icon.Laptop,
            color = Color.Cyan,
            availableAsScene = true)

        viewModel.configureActionId(action.id)
        viewModel.onNameEdited("Hello")
        viewModel.onStepRemoved(step)
        viewModel.onIconSelected(Icon.Laptop)
        viewModel.onColorSelected(Color.Cyan)
        viewModel.onSceneSwitchChanged(true)

        viewModel.onSave()

        verify { actionManager.update(action.id, request) }
    }
}