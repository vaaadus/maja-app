package com.maroonbells.maja.screen.home.manualcontrol

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class ManualControlViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: ManualControlNavigation

    @RelaxedMockK
    private lateinit var pinoutManager: PinoutManager

    @RelaxedMockK
    private lateinit var pinsObserver: Observer<List<CustomizedPin>>

    private lateinit var viewModel: ManualControlViewModel

    @BeforeEach
    fun setUp() {
        viewModel = ManualControlViewModel(navigation, pinoutManager)
        viewModel.pins.observeForever(pinsObserver)
    }

    @Test
    fun onCustomizedPinoutChanged_refreshesPins() {
        val pin = CustomizedPin(
            id = PinId(0),
            defaultName = "0",
            customName = "Narnia",
            value = PinValue(0),
            icon = Icon.Battery,
            type = Pin.Type.Digital,
            accessibleFromHome = false)

        viewModel.onStart()
        viewModel.onCustomizedPinoutChanged(listOf(pin))

        verify { pinsObserver.onChanged(listOf(pin)) }
    }

    @Test
    fun onClose_navigates() {
        viewModel.onClose()

        verify { navigation.onCloseManualControl() }
    }

    @Test
    fun onManagePinsAvailableFromHome_navigates() {
        viewModel.onManagePinsAvailableFromHome()

        verify { navigation.onManagePinsAvailableFromHome() }
    }

    @Test
    fun onCustomizePins_navigates() {
        viewModel.onCustomizePins()

        verify { navigation.onPersonalizePinout() }
    }

    @Test
    fun onPinValueEdited_savesPinValue() {
        val pinId = PinId(0)
        val pinValue = PinValue(10)

        viewModel.onPinValueEdited(pinId, pinValue)

        verify { pinoutManager.setState(pinId, pinValue) }
    }
}