package com.maroonbells.maja.screen.home.manageactions

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.ActionManager
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.CustomizedAction
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class ManageActionsViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: ManageActionsNavigation

    @RelaxedMockK
    private lateinit var actionManager: ActionManager

    @RelaxedMockK
    private lateinit var actionsObserver: Observer<List<ManagedAction>>

    @RelaxedMockK
    private lateinit var removingObserver: Observer<Boolean>

    private lateinit var viewModel: ManageActionsViewModel

    @BeforeEach
    fun setUp() {
        viewModel = ManageActionsViewModel(navigation, actionManager)
        viewModel.actions.observeForever(actionsObserver)
        viewModel.isRemoving.observeForever(removingObserver)
    }

    @Test
    fun onCustomizedActionsChanged_refreshesActions() {
        val action = CustomizedAction(
            id = ActionId(10),
            steps = emptyList(),
            name = "Hogwart",
            customName = "Hogwart",
            icon = Icon.Computer,
            color = Color.Blue,
            availableAsScene = false)

        val expected = ManagedAction(action.id, action.name, action.icon, action.color, false)

        viewModel.onStart()
        viewModel.onCustomizedActionsChanged(listOf(action))

        verify { actionsObserver.onChanged(listOf(expected)) }
    }

    @Test
    fun onRemove_refreshesActions() {
        val action = CustomizedAction(
            id = ActionId(10),
            steps = emptyList(),
            name = "Godric's Valley",
            customName = "Godric's Valley",
            icon = Icon.Blinds,
            color = Color.Red,
            availableAsScene = false)

        val expected = ManagedAction(action.id, action.name, action.icon, action.color, true)

        every { actionManager.list } returns listOf(action)

        viewModel.onStart()
        viewModel.onRemove()

        verify { actionsObserver.onChanged(listOf(expected)) }
        verify { removingObserver.onChanged(true) }
    }

    @Test
    fun onClose_navigates() {
        viewModel.onClose()

        verify { navigation.onCloseManageActions() }
    }

    @Test
    fun onAddAction_navigates() {
        viewModel.onAddAction()

        verify { navigation.onAddAction() }
    }

    @Test
    fun onEditAction_navigates() {
        val id = ActionId(15)
        viewModel.onEditAction(id)

        verify { navigation.onEditAction(id) }
    }

    @Test
    fun onRemoveAction_removesIt() {
        val id = ActionId(1)
        viewModel.onRemoveAction(id)

        verify { actionManager.remove(id) }
    }
}