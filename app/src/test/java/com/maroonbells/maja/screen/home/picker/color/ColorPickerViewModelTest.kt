package com.maroonbells.maja.screen.home.picker.color

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class ColorPickerViewModelTest {

    @RelaxedMockK
    private lateinit var customizationManager: CustomizationManager

    @RelaxedMockK
    private lateinit var observer: Observer<List<Color>>

    private lateinit var viewModel: ColorPickerViewModel

    @BeforeEach
    fun setUp() {
        viewModel = ColorPickerViewModel(customizationManager)
        viewModel.availableColors.observeForever(observer)
    }

    @Test
    fun onStart_updatesColors() {
        every { customizationManager.availableColors } returns Color.values().toList()

        viewModel.onStart()

        verify { observer.onChanged(Color.values().toList()) }
    }
}