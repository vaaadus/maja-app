package com.maroonbells.maja.screen.home.realtimeclock

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.RealTimeClockManager
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import com.maroonbells.maja.screen.home.realtimeclock.RealTimeClockViewModel.Companion.DATE_FORMAT
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkConstructor
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit.SECONDS

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class RealTimeClockViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: RealTimeClockNavigation

    @RelaxedMockK
    private lateinit var realTimeClockManager: RealTimeClockManager

    @RelaxedMockK
    private lateinit var smartphoneTime: Observer<String>

    @RelaxedMockK
    private lateinit var boardTime: Observer<String>

    private lateinit var viewModel: RealTimeClockViewModel

    @BeforeEach
    fun setUp() {
        viewModel = RealTimeClockViewModel(navigation, realTimeClockManager)
        viewModel.smartphoneDatetime.observeForever(smartphoneTime)
        viewModel.boardDatetime.observeForever(boardTime)
    }

    @Test
    fun onDatetimeChanged_updatesTimes() {
        val date = Date()
        val boardDate = Date(date.time + SECONDS.toMillis(1))
        val dateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

        mockkConstructor(Date::class)
        every { anyConstructed<Date>().time } returns date.time
        every { realTimeClockManager.datetime } returns boardDate

        viewModel.onDatetimeChanged(boardDate)

        verify {
            smartphoneTime.onChanged(dateFormat.format(date))
            boardTime.onChanged(dateFormat.format(boardDate))
        }
    }

    @Test
    fun onClose_navigates() {
        viewModel.onClose()

        verify { navigation.onCloseRealTimeClock() }
    }

    @Test
    fun onSynchronize_setsDatetime() {
        viewModel.onSynchronize()

        verify { realTimeClockManager.synchronize() }
    }
}