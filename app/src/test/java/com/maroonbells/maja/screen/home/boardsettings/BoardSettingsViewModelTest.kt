package com.maroonbells.maja.screen.home.boardsettings

import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class BoardSettingsViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: BoardSettingsNavigation

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    private lateinit var viewModel: BoardSettingsViewModel

    @BeforeEach
    fun setUp() {
        viewModel = BoardSettingsViewModel(navigation, sessionManager)
    }

    @Test
    fun onDisconnect_destroysConfig() {
        viewModel.onDisconnect()

        verify { sessionManager.destroyConfiguration() }
    }

    @Test
    fun onRenameBoard_navigates() {
        viewModel.onRenameBoard()

        verify { navigation.onRenameBoard() }
    }

    @Test
    fun onManageActions_navigates() {
        viewModel.onManageActions()

        verify { navigation.onManageActions() }
    }

    @Test
    fun onManageManualControl_navigates() {
        viewModel.onManageManualControl()

        verify { navigation.onManualControl() }
    }

    @Test
    fun onCustomizePins_navigates() {
        viewModel.onCustomizePins()

        verify { navigation.onPersonalizePinout() }
    }

    @Test
    fun onAdjustDatetime_navigates() {
        viewModel.onAdjustDatetime()

        verify { navigation.onAdjustDatetime() }
    }
}