package com.maroonbells.maja.screen.home

import com.maroonbells.maja.di.startKoinWithAppComponent
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.check.checkModules

class HomeModuleTest : KoinTest {

    @Test
    fun allDependenciesCanBeResolved() {
        startKoinWithAppComponent {
            modules(homeModule)
        }.checkModules()
    }

    @AfterEach
    fun tearDown() {
        stopKoin()
    }
}