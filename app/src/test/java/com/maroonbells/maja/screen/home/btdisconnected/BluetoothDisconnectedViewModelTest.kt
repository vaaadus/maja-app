package com.maroonbells.maja.screen.home.btdisconnected

import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class BluetoothDisconnectedViewModelTest {

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var boardManager: BoardManager

    private lateinit var viewModel: BluetoothDisconnectedViewModel

    @BeforeEach
    fun setUp() {
        viewModel = BluetoothDisconnectedViewModel(sessionManager, boardManager)
    }

    @Test
    fun tryAgain_reconnects() {
        viewModel.tryAgain()

        verify { boardManager.reconnect() }
    }

    @Test
    fun onClose_destroysConfig() {
        viewModel.onClose()

        verify { sessionManager.destroyConfiguration() }
    }
}