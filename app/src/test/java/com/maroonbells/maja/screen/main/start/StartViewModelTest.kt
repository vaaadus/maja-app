package com.maroonbells.maja.screen.main.start

import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class StartViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: StartNavigation

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    private lateinit var viewModel: StartViewModel

    @BeforeEach
    fun setUp() {
        viewModel = StartViewModel(navigation, sessionManager)
    }

    @Test
    fun onConnect_showsHomeScreen() {
        viewModel.onConnect()

        verify { navigation.showHomeScreen() }
    }

    @Test
    fun onTryDemo_configuresDemoBoard() {
        viewModel.onTryDemo()

        verify { sessionManager.configure(DemoAddress()) }
    }
}