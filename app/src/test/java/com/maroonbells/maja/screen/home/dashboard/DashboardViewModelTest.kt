package com.maroonbells.maja.screen.home.dashboard

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.BoardNameManager
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class DashboardViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: DashboardNavigation

    @RelaxedMockK
    private lateinit var boardNameManager: BoardNameManager

    @RelaxedMockK
    private lateinit var pinoutManager: PinoutManager

    @RelaxedMockK
    private lateinit var boardNameObserver: Observer<String>

    @RelaxedMockK
    private lateinit var pinsObserver: Observer<List<CustomizedPin>>

    private lateinit var viewModel: DashboardViewModel

    @BeforeEach
    fun setUp() {
        viewModel = DashboardViewModel(navigation, boardNameManager, pinoutManager)
        viewModel.boardName.observeForever(boardNameObserver)
        viewModel.pins.observeForever(pinsObserver)
    }

    @Test
    fun onCustomizedPinoutChanged_refreshesData() {
        val pin1 = mockk<CustomizedPin> { every { accessibleFromHome } returns true }
        val pin2 = mockk<CustomizedPin> { every { accessibleFromHome } returns false }

        viewModel.onCustomizedPinoutChanged(listOf(pin1, pin2))

        verify { pinsObserver.onChanged(listOf(pin1)) }
    }

    @Test
    fun onBoardNameChanged_refreshesData() {
        val name = "Hello #2"

        viewModel.onBoardNameChanged(name)

        verify { boardNameObserver.onChanged(name) }
    }

    @Test
    fun onRenameBoard_navigates() {
        viewModel.onRenameBoard()

        verify { navigation.onRenameBoard() }
    }

    @Test
    fun onManageManualControl_navigates() {
        viewModel.onManageManualControl()

        verify { navigation.onManualControl() }
    }

    @Test
    fun onPinValueEdited_savesPinValue() {
        val pinId = PinId(0)
        val pinValue = PinValue(10)

        viewModel.onPinValueEdited(pinId, pinValue)

        verify { pinoutManager.setState(pinId, pinValue) }
    }
}