package com.maroonbells.maja.screen.home.btconnecting

import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class BluetoothConnectingViewModelTest {

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    private lateinit var viewModel: BluetoothConnectingViewModel

    @BeforeEach
    fun setUp() {
        viewModel = BluetoothConnectingViewModel(sessionManager)
    }

    @Test
    fun onClose_destroysConfiguration() {
        viewModel.onClose()

        verify { sessionManager.destroyConfiguration() }
    }
}