package com.maroonbells.maja.screen.home.picker.icon

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class IconPickerViewModelTest {

    @RelaxedMockK
    private lateinit var customizationManager: CustomizationManager

    @RelaxedMockK
    private lateinit var observer: Observer<List<Icon>>

    private lateinit var viewModel: IconPickerViewModel

    @BeforeEach
    fun setUp() {
        viewModel = IconPickerViewModel(customizationManager)
        viewModel.availableIcons.observeForever(observer)
    }

    @Test
    fun onStart_updatesIcons() {
        every { customizationManager.availableIcons } returns Icon.values().toList()

        viewModel.onStart()

        verify { observer.onChanged(Icon.values().toList()) }
    }
}