package com.maroonbells.maja.screen.home.boardname

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.BoardNameManager
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class BoardNameViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: BoardNameNavigation

    @RelaxedMockK
    private lateinit var boardNameManager: BoardNameManager

    @RelaxedMockK
    private lateinit var nameObserver: Observer<String?>

    private lateinit var viewModel: BoardNameViewModel

    @BeforeEach
    fun setUp() {
        viewModel = BoardNameViewModel(navigation, boardNameManager)
        viewModel.name.observeForever(nameObserver)
    }

    @Test
    fun onBoardNameChanged_refreshesName() {
        val customName = "Maja-board-123"

        viewModel.onBoardNameChanged(customName)

        verify { nameObserver.onChanged(customName) }
    }

    @Test
    fun onSave_savesName() {
        val newCustomName = "Changed name"

        viewModel.onSave(newCustomName)

        verify {
            boardNameManager.setBoardName(newCustomName)
            navigation.onCloseBoardName()
        }
    }
}