package com.maroonbells.maja.screen.main

import com.maroonbells.maja.di.startKoinWithAppComponent
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.check.checkModules

class MainModuleTest : KoinTest {

    @Test
    fun allDependenciesCanBeResolved() {
        startKoinWithAppComponent {
            modules(mainModule)
        }.checkModules()
    }

    @AfterEach
    fun tearDown() {
        stopKoin()
    }
}