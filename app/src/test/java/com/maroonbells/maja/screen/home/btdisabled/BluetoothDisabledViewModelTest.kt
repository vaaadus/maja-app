package com.maroonbells.maja.screen.home.btdisabled

import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class BluetoothDisabledViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: BluetoothDisabledNavigation

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var bluetoothManager: BluetoothManager

    private lateinit var viewModel: BluetoothDisabledViewModel

    @BeforeEach
    fun setUp() {
        viewModel = BluetoothDisabledViewModel(navigation, sessionManager, bluetoothManager)
    }

    @Test
    fun onEnable_callsManager() {
        viewModel.onEnable()

        verify { bluetoothManager.enable() }
    }

    @Test
    fun onClose_destroysConfig_andNavigates() {
        viewModel.onClose()

        verify {
            sessionManager.destroyConfiguration()
            navigation.onCloseDisabledBluetooth()
        }
    }
}