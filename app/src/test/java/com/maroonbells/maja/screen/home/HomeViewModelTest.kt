package com.maroonbells.maja.screen.home

import com.maroonbells.maja.domain.connection.ConnectionManager
import com.maroonbells.maja.domain.connection.ConnectionManager.Status
import io.mockk.clearMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.mockk.verifyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class HomeViewModelTest {

    @RelaxedMockK
    private lateinit var navigator: HomeNavigator

    @RelaxedMockK
    private lateinit var connectionManager: ConnectionManager

    private lateinit var viewModel: HomeViewModel

    @BeforeEach
    fun setUp() {
        viewModel = HomeViewModel(navigator, connectionManager)
    }

    @Test
    fun onStart_addsListener_forConnectionStatus() {
        viewModel.onStart()

        verify { connectionManager.addConnectionStatusListener(viewModel) }
    }

    @Test
    fun onStop_removesListener_forConnectionStatus() {
        viewModel.onStop()

        verify { connectionManager.removeConnectionStatusListener(viewModel) }
    }

    @Test
    fun onConnectionStatusChanged_bluetoothDisabled_navigates() {
        viewModel.onConnectionStatusChanged(Status.BluetoothDisabled)

        verifyOrder {
            navigator.clearBackStack()
            navigator.showBluetoothDisabled()
        }
    }

    @Test
    fun onConnectionStatusChanged_bluetoothEnabled_navigates() {
        viewModel.onConnectionStatusChanged(Status.BluetoothEnabled)

        verifyOrder {
            navigator.clearBackStack()
            navigator.showBluetoothDiscovery()
        }
    }

    @Test
    fun onConnectionStatusChanged_connecting_navigates() {
        viewModel.onConnectionStatusChanged(Status.Connecting)

        verifyOrder {
            navigator.clearBackStack()
            navigator.showBluetoothConnecting()
        }
    }

    @Test
    fun onConnectionStatusChanged_disconnected_navigates() {
        viewModel.onConnectionStatusChanged(Status.Disconnected)

        verifyOrder {
            navigator.clearBackStack()
            navigator.showBluetoothDisconnected()
        }
    }

    @Test
    fun onConnectionStatusChanged_connected_navigates() {
        viewModel.onConnectionStatusChanged(Status.Connected)

        verifyOrder {
            navigator.clearBackStack()
            navigator.showDashboard()
        }
    }

    @Test
    fun onConnectionStatusChanged_twiceConnected_navigatesOnce() {
        viewModel.onConnectionStatusChanged(Status.Connected)
        verify { navigator.showDashboard() }

        clearMocks(navigator)

        viewModel.onConnectionStatusChanged(Status.Connected)
        verify(exactly = 0) { navigator.showDashboard() }
    }

    @Test
    fun onConnectionStatusChanged_twiceConnected_after_onCleared_navigatesTwice() {
        viewModel.onConnectionStatusChanged(Status.Connected)
        verify { navigator.showDashboard() }

        clearMocks(navigator)
        viewModel.onCleared()

        viewModel.onConnectionStatusChanged(Status.Connected)
        verify { navigator.showDashboard() }
    }

    @Test
    fun onDashboardSelected_navigates() {
        viewModel.onDashboardSelected()

        verify { navigator.showDashboard() }
    }

    @Test
    fun onBoardSettingsSelected_navigates() {
        viewModel.onBoardSettingsSelected()

        verify { navigator.showBoardSettings() }
    }

    @Test
    fun onProfileSelected_navigates() {
        viewModel.onProfileSelected()

        verify { navigator.showProfile() }
    }

    @Test
    fun onPremiumSelected_navigates() {
        viewModel.onPremiumSelected()

        verify { navigator.showPremium() }
    }
}