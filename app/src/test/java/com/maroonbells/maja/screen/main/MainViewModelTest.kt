package com.maroonbells.maja.screen.main

import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MainViewModelTest {

    @RelaxedMockK
    private lateinit var navigator: MainNavigator

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    private lateinit var viewModel: MainViewModel

    @BeforeEach
    fun setUp() {
        viewModel = MainViewModel(navigator, sessionManager)
    }

    @Test
    fun onBoardSessionCreated_navigates() {
        viewModel.onBoardSessionCreated(DemoAddress())

        verify { navigator.showHomeScreen() }
    }
}