package com.maroonbells.maja.screen.home.personalize

import androidx.lifecycle.Observer
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class PersonalizePinoutViewModelTest {

    @RelaxedMockK
    private lateinit var navigation: PersonalizePinoutNavigation

    @RelaxedMockK
    private lateinit var pinoutManager: PinoutManager

    @RelaxedMockK
    private lateinit var pinsObserver: Observer<List<CustomizedPin>>

    private lateinit var viewModel: PersonalizePinoutViewModel

    @BeforeEach
    fun setUp() {
        viewModel = PersonalizePinoutViewModel(navigation, pinoutManager)
        viewModel.pins.observeForever(pinsObserver)
    }

    @Test
    fun onCustomizedPinoutChanged_refreshesPins() {
        val pin = CustomizedPin(
            id = PinId(0),
            type = Pin.Type.DigitalPwm,
            defaultName = "0",
            customName = "Bell",
            value = PinValue(0),
            icon = Icon.Battery,
            accessibleFromHome = false)

        viewModel.onStart()
        viewModel.onCustomizedPinoutChanged(listOf(pin))

        verify { pinsObserver.onChanged(listOf(pin)) }
    }

    @Test
    fun onClose_navigates() {
        viewModel.onClose()

        verify { navigation.onClosePersonalizePinout() }
    }

    @Test
    fun onPinNameEdited_savesPinName() {
        val pinId = PinId(0)
        val name = "Hello, world!"

        viewModel.onPinNameEdited(pinId, name)

        pinoutManager.setName(pinId, name)
    }

    @Test
    fun onPinIconEdited_savesPinIcon() {
        val pinId = PinId(0)
        val icon = Icon.Blinds

        viewModel.onPinIconEdited(pinId, icon)

        pinoutManager.setIcon(pinId, icon)
    }
}