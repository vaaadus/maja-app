package com.maroonbells.maja.service.resources

import android.content.res.Resources
import com.maroonbells.maja.R
import com.maroonbells.maja.domain.board.value.Pin.Type.Digital
import com.maroonbells.maja.domain.board.value.Pin.Type.DigitalPwm
import com.maroonbells.maja.domain.board.value.PinValue
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class ResourceLocatorTest {

    companion object {
        private const val ON = "on"
        private const val OFF = "off"
    }

    @RelaxedMockK
    private lateinit var resources: Resources

    @BeforeEach
    fun setUp() {
        every { resources.getString(R.string.common_on) } returns ON
        every { resources.getString(R.string.common_off) } returns OFF
    }

    @Test
    fun formatPinValue() {
        assertThat(PinValue.Low.format(Digital, resources)).isEqualTo(OFF)
        assertThat(PinValue.Low.format(DigitalPwm, resources)).isEqualTo(OFF)

        assertThat(PinValue.High.format(Digital, resources)).isEqualTo(ON)
        assertThat(PinValue.High.format(DigitalPwm, resources)).isEqualTo("0%")

        assertThat(PinValue.fromDutyCycle(50).format(DigitalPwm, resources)).isEqualTo("49%")
        assertThat(PinValue(PinValue.MAX_VALUE).format(DigitalPwm, resources)).isEqualTo("100%")
    }

    @Test
    fun formatAsPercentage() {
        assertThat(0.formatAsPercentage()).isEqualTo("0%")
        assertThat(30.formatAsPercentage()).isEqualTo("30%")
        assertThat(99.formatAsPercentage()).isEqualTo("99%")
        assertThat(100.formatAsPercentage()).isEqualTo("100%")
    }
}