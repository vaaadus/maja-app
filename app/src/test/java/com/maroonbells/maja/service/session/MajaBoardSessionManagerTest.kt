package com.maroonbells.maja.service.session

import com.maroonbells.maja.di.getOrNull
import com.maroonbells.maja.di.startKoinWithAppComponent
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.logic.ActionManager
import com.maroonbells.maja.domain.board.logic.BoardNameManager
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.configuration.repository.Configuration
import com.maroonbells.maja.domain.configuration.repository.InMemoryConfiguration
import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener
import com.maroonbells.maja.service.session.MajaBoardSessionManager.Companion.KEY_BOARD_ADDRESS
import com.maroonbells.maja.service.session.MajaBoardSessionManager.Companion.KEY_BOARD_TYPE
import io.mockk.clearMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.spyk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.get

@ExtendWith(MockKExtension::class)
class MajaBoardSessionManagerTest : KoinTest {

    private lateinit var configuration: Configuration

    @RelaxedMockK
    private lateinit var listener: BoardSessionListener

    private lateinit var manager: MajaBoardSessionManager

    @BeforeEach
    fun setUp() {
        startKoinWithAppComponent()

        configuration = spyk(InMemoryConfiguration())

        manager = MajaBoardSessionManager(configuration)
        manager.addBoardSessionListener(listener)
        clearMocks(listener)

        loadKoinModules(
            module {
                single<BoardSessionManager>(override = true) { manager }
            }
        )
    }

    @AfterEach
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun configure_bluetoothAddress() {
        val address = BluetoothAddress("11:22:33:44:55")

        manager.configure(address)

        verify {
            configuration.putString(KEY_BOARD_ADDRESS, address.raw)
            configuration.putInt(KEY_BOARD_TYPE, BoardType.Bluetooth.ordinal)
            listener.onBoardSessionCreated(address)
        }

        assertThat(manager.boardAddress).isEqualTo(address)
        assertThat(manager.boardManager).isNotNull
        assertThat(manager.boardNameManager).isNotNull
        assertThat(manager.pinoutManager).isNotNull
        assertThat(manager.actionManager).isNotNull

        assertThat(get<BoardManager>()).isNotNull
        assertThat(get<BoardNameManager>()).isNotNull
        assertThat(get<PinoutManager>()).isNotNull
        assertThat(get<ActionManager>()).isNotNull
    }

    @Test
    fun configure_andDestroy_bluetoothAddress() {
        val address = BluetoothAddress("11:22:33:44:55")

        manager.configure(address)
        assertDependenciesCreated(address)
        clearMocks(configuration, listener)

        manager.destroyConfiguration()

        verify {
            configuration.clear(KEY_BOARD_ADDRESS)
            configuration.clear(KEY_BOARD_TYPE)
            listener.onBoardSessionDestroyed()
        }

        assertDependenciesDestroyed()
    }

    @Test
    fun configure_demoAddress() {
        val address = DemoAddress()

        manager.configure(address)

        verify {
            configuration.putString(KEY_BOARD_ADDRESS, address.raw)
            configuration.putInt(KEY_BOARD_TYPE, BoardType.Demo.ordinal)
            listener.onBoardSessionCreated(address)
        }

        assertDependenciesCreated(address)
    }

    @Test
    fun configure_andDestroy_demoAddress() {
        val address = DemoAddress()

        manager.configure(address)
        assertDependenciesCreated(address)
        clearMocks(configuration, listener)

        manager.destroyConfiguration()

        verify {
            configuration.clear(KEY_BOARD_ADDRESS)
            configuration.clear(KEY_BOARD_TYPE)
            listener.onBoardSessionDestroyed()
        }

        assertDependenciesDestroyed()
    }

    private fun assertDependenciesCreated(address: BoardAddress) {
        assertThat(manager.boardAddress).isEqualTo(address)
        assertThat(manager.boardManager).isNotNull
        assertThat(manager.boardNameManager).isNotNull
        assertThat(manager.pinoutManager).isNotNull
        assertThat(manager.actionManager).isNotNull

        assertThat(get<BoardManager>()).isNotNull
        assertThat(get<BoardNameManager>()).isNotNull
        assertThat(get<PinoutManager>()).isNotNull
        assertThat(get<ActionManager>()).isNotNull
    }

    private fun assertDependenciesDestroyed() {
        assertThat(manager.boardAddress).isNull()
        assertThat(manager.boardManager).isNull()
        assertThat(manager.boardNameManager).isNull()
        assertThat(manager.pinoutManager).isNull()
        assertThat(manager.actionManager).isNull()

        assertThat(getOrNull<BoardManager>()).isNull()
        assertThat(getOrNull<BoardNameManager>()).isNull()
        assertThat(getOrNull<PinoutManager>()).isNull()
        assertThat(getOrNull<ActionManager>()).isNull()
    }
}