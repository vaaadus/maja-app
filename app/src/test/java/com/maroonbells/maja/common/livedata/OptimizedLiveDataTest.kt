package com.maroonbells.maja.common.livedata

import androidx.lifecycle.Observer
import com.maroonbells.maja.framework.InstantTaskExecutionExtension
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.mockk.verifyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class, InstantTaskExecutionExtension::class)
class OptimizedLiveDataTest {

    @RelaxedMockK
    private lateinit var observer: Observer<Boolean>

    private lateinit var liveData: OptimizedLiveData<Boolean>

    @BeforeEach
    fun setUp() {
        liveData = OptimizedLiveData()
        liveData.observeForever(observer)
    }

    @Test
    fun setValue() {
        liveData.setValue(true)
        verify { observer.onChanged(true) }
    }

    @Test
    fun setValue_setValue() {
        liveData.setValue(true)
        liveData.setValue(true)
        verify(exactly = 1) { observer.onChanged(true) }
    }

    @Test
    fun setValue_setOtherValue() {
        liveData.setValue(true)
        liveData.setValue(false)

        verifyOrder {
            observer.onChanged(true)
            observer.onChanged(false)
        }
    }
}