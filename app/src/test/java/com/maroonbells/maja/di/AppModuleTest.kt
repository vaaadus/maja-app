package com.maroonbells.maja.di

import android.bluetooth.BluetoothAdapter
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.Test
import org.junit.jupiter.api.AfterEach
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.check.checkModules


class AppModuleTest : KoinTest {

    @Test
    fun allDependenciesCanBeResolved() {
        startKoinWithAppComponent().checkModules()
    }

    @AfterEach
    fun tearDown() {
        stopKoin()
    }
}

/**
 * Starts a koin DI using app component as a base.
 * Optionally you can provide more modules in the [moduleDescriptor] lambda.
 */
fun startKoinWithAppComponent(moduleDescriptor: (KoinApplication.() -> Unit)? = null): KoinApplication {
    mockBluetoothAdapter()

    return startKoin {
        androidContext(mockk(relaxed = true))
        modules(majaAppComponent)
        moduleDescriptor?.invoke(this)
    }
}

private fun mockBluetoothAdapter() {
    mockkStatic(BluetoothAdapter::class)
    every { BluetoothAdapter.getDefaultAdapter() } returns mockk(relaxed = true)
}