package com.maroonbells.maja.common

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 * A base view model to be extended by all view
 * models which are interested in the lifecycle.
 */
abstract class LifecycleViewModel : AppViewModel(), LifecycleObserver {

    /**
     * Called on ON_START lifecycle event.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {

    }

    /**
     * Called on ON_STOP lifecycle event.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {

    }
}