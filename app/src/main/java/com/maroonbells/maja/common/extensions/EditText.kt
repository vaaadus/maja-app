package com.maroonbells.maja.common.extensions

import android.widget.EditText

fun EditText.moveCursorToEnd() {
    setSelection(text.toString().length)
}