package com.maroonbells.maja.common

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment


/**
 * Base fragment to be extended by any fragment in the app.
 */
abstract class AppFragment : Fragment() {

    /**
     * Dismisses attached dialog fragment by a tag.
     * Can be called after onStop() safely.
     */
    fun dismissDialogFragment(tag: String?) {
        val fragment = fragmentManager?.findFragmentByTag(tag)
        if (fragment is DialogFragment) {
            fragment.dismissAllowingStateLoss()
        }
    }

    /**
     * A present or newly created arguments.
     */
    fun obtainArguments(): Bundle {
        val args = arguments ?: Bundle()
        arguments = args
        return args
    }
}