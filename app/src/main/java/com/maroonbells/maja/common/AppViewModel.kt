package com.maroonbells.maja.common

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel

/**
 * Base view model to be extended by all view models.
 */
abstract class AppViewModel : ViewModel() {

    @CallSuper
    override fun onCleared() {
        super.onCleared()
    }
}