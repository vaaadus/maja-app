package com.maroonbells.maja.common

import android.os.Bundle
import androidx.annotation.CallSuper

/**
 * Base activity to be extended by any activity
 * in the app which is interested in the lifecycle.
 */
abstract class LifecycleAwareActivity<T : LifecycleViewModel> : AppActivity() {

    abstract val viewModel: T

    @CallSuper
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }
}