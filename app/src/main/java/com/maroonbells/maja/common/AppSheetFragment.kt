package com.maroonbells.maja.common

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import androidx.annotation.CallSuper
import androidx.annotation.DrawableRes
import com.google.android.material.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Base bottom sheet dialog fragment to be extended by any fragment in the app.
 *
 * Receives dialog background drawable resource ID in the constructor.
 */
abstract class AppSheetFragment(@DrawableRes private val backgroundId: Int) : BottomSheetDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener { setupBottomSheetBackground(it) }
        return dialog
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                expandDialogToFullHeight(requireDialog())
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }

    /**
     * A present or newly created arguments.
     */
    fun obtainArguments(): Bundle {
        val args = arguments ?: Bundle()
        arguments = args
        return args
    }

    /**
     * Lookup for a listener which is set as a target fragment or a parent fragment.
     */
    protected inline fun <reified T> findListener(): T? {
        if (targetFragment is T) return targetFragment as T
        if (parentFragment is T) return parentFragment as T
        return null
    }

    private fun setupBottomSheetBackground(dialogInterface: DialogInterface) {
        val dialog = dialogInterface as BottomSheetDialog
        val bottomSheet: View = dialog.findViewById(R.id.design_bottom_sheet) ?: return
        bottomSheet.setBackgroundResource(backgroundId)
    }

    private fun expandDialogToFullHeight(dialog: Dialog) {
        val bottomSheet = dialog.findViewById<View>(R.id.design_bottom_sheet)
        BottomSheetBehavior.from(bottomSheet).apply {
            state = BottomSheetBehavior.STATE_EXPANDED
            peekHeight = 0
        }
    }
}