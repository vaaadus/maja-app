package com.maroonbells.maja.common

import androidx.navigation.NavController

/**
 * Base navigator to be extended by all navigators.
 */
abstract class AppNavigator {

    private var attachedActivity: AppActivity? = null

    protected val activity: AppActivity?
        get() = attachedActivity

    protected val navController: NavController?
        get() = attachedActivity?.findNavController()

    /**
     * Attach an activity to this navigator. Remember to detach it when its destroyed.
     */
    fun onAttach(activity: AppActivity) {
        attachedActivity = activity
    }

    /**
     * Detach the activity from the navigator.
     */
    fun onDetach(activity: AppActivity) {
        if (attachedActivity == activity) {
            attachedActivity = null
        }
    }
}