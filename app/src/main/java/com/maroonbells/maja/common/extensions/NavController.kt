@file:Suppress("SameParameterValue")

package com.maroonbells.maja.common.extensions

import android.os.Bundle
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.maroonbells.maja.R

fun NavController.clearBackStack() {
    while (popBackStack()) {
        // popping...
    }
}

/**
 * Navigate to a destination with fade animation.
 */
fun NavController.fadeTo(@IdRes destinationId: Int, args: Bundle? = null) {
    navigate(destinationId, args, buildFadeNavOptions())
}

/**
 * Navigate to a destination without any animation but clearing the top of the stack from duplicates.
 */
fun NavController.navigateToSingleTop(@IdRes destinationId: Int, args: Bundle? = null) {
    navigate(destinationId, args, buildNavOptions(launchSingleTop = true))
}

/**
 * Navigate to a destination with fade animation with clearing the top of the stack from duplicates.
 */
fun NavController.fadeToSingleTop(@IdRes destinationId: Int, args: Bundle? = null) {
    navigate(destinationId, args, buildFadeNavOptions(launchSingleTop = true))
}

/**
 * Navigate to a destination with slide animation.
 */
fun NavController.slideTo(@IdRes destinationId: Int, args: Bundle? = null) {
    navigate(destinationId, args, buildSlideNavOptions())
}

private fun buildFadeNavOptions(
    launchSingleTop: Boolean = false,
    @AnimRes enterAnim: Int = R.anim.fade_in,
    @AnimRes exitAnim: Int = R.anim.fade_out,
    @AnimRes popEnterAnim: Int = R.anim.fade_in,
    @AnimRes popExitAnim: Int = R.anim.fade_out): NavOptions {

    return NavOptions.Builder()
        .setLaunchSingleTop(launchSingleTop)
        .setEnterAnim(enterAnim)
        .setExitAnim(exitAnim)
        .setPopEnterAnim(popEnterAnim)
        .setPopExitAnim(popExitAnim)
        .build()
}

private fun buildSlideNavOptions(
    launchSingleTop: Boolean = false,
    @AnimRes enterAnim: Int = R.anim.slide_in_left,
    @AnimRes exitAnim: Int = R.anim.slide_out_left,
    @AnimRes popEnterAnim: Int = R.anim.slide_in_right,
    @AnimRes popExitAnim: Int = R.anim.slide_out_right): NavOptions {

    return NavOptions.Builder()
        .setLaunchSingleTop(launchSingleTop)
        .setEnterAnim(enterAnim)
        .setExitAnim(exitAnim)
        .setPopEnterAnim(popEnterAnim)
        .setPopExitAnim(popExitAnim)
        .build()
}

private fun buildNavOptions(launchSingleTop: Boolean = false): NavOptions {
    return NavOptions.Builder().setLaunchSingleTop(launchSingleTop).build()
}