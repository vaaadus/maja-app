package com.maroonbells.maja.common

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper

/**
 * Base fragment to be extended by any fragment
 * in the app which is interested in the lifecycle.
 */
abstract class LifecycleAwareFragment<T : LifecycleViewModel> : AppFragment() {

    abstract val viewModel: T

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycle.addObserver(viewModel)
    }
}