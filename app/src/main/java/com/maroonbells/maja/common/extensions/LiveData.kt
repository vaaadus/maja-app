package com.maroonbells.maja.common.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Toggles a boolean value in the mutable live data.
 */
fun MutableLiveData<Boolean>.toggleValue() {
    value = !(value ?: false)
}

/**
 * Returns a boolean value from live data or false if not present.
 */
val LiveData<Boolean>.boolValue: Boolean
    get() = value ?: false

/**
 * Returns a list value from live data or empty list if not present.
 */
fun <T> LiveData<List<T>>.listValue(): List<T> {
    return value ?: emptyList()
}