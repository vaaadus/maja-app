package com.maroonbells.maja.common.extensions

import androidx.recyclerview.widget.RecyclerView

/**
 * Removes and adds again all item decoration to enforce redecoration.
 */
fun RecyclerView.redecorate() {
    val decorations = Array(itemDecorationCount) { getItemDecorationAt(it) }
    for (decoration in decorations) removeItemDecoration(decoration)
    for (decoration in decorations) addItemDecoration(decoration)
}