package com.maroonbells.maja.common.livedata

import androidx.lifecycle.MutableLiveData

/**
 * A mutable live data which doesn't send events if the value didn't change.
 *
 * Required custom implementation of equals for the generic type.
 */
class OptimizedLiveData<T>(value: T?) : MutableLiveData<T>(value) {

    constructor() : this(null)

    override fun setValue(value: T) {
        if (this.value != value) {
            super.setValue(value)
        }
    }
}