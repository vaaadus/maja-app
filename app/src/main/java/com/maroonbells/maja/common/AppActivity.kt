package com.maroonbells.maja.common

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.maroonbells.maja.R

/**
 * Base activity to be extended by any activity in the app.
 */
abstract class AppActivity : AppCompatActivity() {

    fun findNavController(): NavController = findNavController(R.id.navHostFragment)
}