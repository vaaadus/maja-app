package com.maroonbells.maja.common.extensions

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.widget.ImageView
import androidx.annotation.ColorInt

fun ImageView.setIconTint(@ColorInt color: Int) {
    colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)
}