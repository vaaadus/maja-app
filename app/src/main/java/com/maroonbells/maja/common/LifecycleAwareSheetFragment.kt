package com.maroonbells.maja.common

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.DrawableRes

/**
 * Base bottom sheet dialog fragment to be extended by any
 * fragment in the app which is interested in the lifecycle.
 *
 * Receives dialog background drawable resource ID in the constructor.
 */
abstract class LifecycleAwareSheetFragment<T : LifecycleViewModel>(@DrawableRes backgroundId: Int)
    : AppSheetFragment(backgroundId) {

    abstract val viewModel: T

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycle.addObserver(viewModel)
    }
}