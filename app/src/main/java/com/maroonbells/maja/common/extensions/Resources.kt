package com.maroonbells.maja.common.extensions

import android.content.res.Resources
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.graphics.drawable.DrawableCompat

/**
 * Indicates no resource.
 */
const val NO_RESOURCE_ID = 0

/**
 * Lookup for a drawable, then tint it with a color.
 */
fun Resources.obtainTintedDrawable(@DrawableRes resId: Int, @ColorInt tint: Int): Drawable {
    val drawable = DrawableCompat.wrap(getDrawable(resId, null))
    drawable.mutate()
    DrawableCompat.setTint(drawable, tint)
    return drawable
}