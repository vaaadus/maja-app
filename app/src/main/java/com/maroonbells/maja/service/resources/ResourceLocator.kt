package com.maroonbells.maja.service.resources

import android.content.res.Resources
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.maroonbells.maja.R
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon

@DrawableRes
fun Icon.toDrawableId(): Int = when (this) {
    Icon.Ventilation -> R.drawable.ic_ventilator_black
    Icon.Light -> R.drawable.ic_light_bulb_black
    Icon.Heating -> R.drawable.ic_heating_black
    Icon.Pan -> R.drawable.ic_pan_black
    Icon.Computer -> R.drawable.ic_pc_black
    Icon.Laptop -> R.drawable.ic_laptop_black
    Icon.Smartphone -> R.drawable.ic_smartphone_black
    Icon.GamePad -> R.drawable.ic_gamepad_black
    Icon.TV -> R.drawable.ic_desktop_monitor_black
    Icon.Battery -> R.drawable.ic_battery_black
    Icon.Lock -> R.drawable.ic_lock_black
    Icon.Blinds -> R.drawable.ic_eye_hide_black
    Icon.Door -> R.drawable.ic_door_black
    Icon.Bell -> R.drawable.ic_bell_black
    Icon.Alarm -> R.drawable.ic_alarm_clock_black
}

@ColorRes
fun Color.toColorId(): Int = when (this) {
    Color.Red -> R.color.material_red
    Color.Pink -> R.color.material_pink
    Color.Purple -> R.color.material_purple
    Color.DeepPurple -> R.color.material_deepPurple
    Color.Indigo -> R.color.material_indigo
    Color.Blue -> R.color.material_blue
    Color.LightBlue -> R.color.material_lightBlue
    Color.Cyan -> R.color.material_cyan
    Color.Teal -> R.color.material_teal
    Color.Green -> R.color.material_green
    Color.LightGreen -> R.color.material_lightGreen
    Color.Lime -> R.color.material_lime
    Color.Yellow -> R.color.material_yellow
    Color.Amber -> R.color.material_amber
    Color.Orange -> R.color.material_orange
    Color.DeepOrange -> R.color.material_deepOrange
    Color.Brown -> R.color.material_brown
    Color.Grey -> R.color.material_grey
    Color.BlueGrey -> R.color.material_blueGrey
}

/**
 * Formats the pin value according to the rules:
 * ```
 * * Digital pins: On or Off
 * * Digital PWM pins: Off (0%) - 100%
 * ```
 */
fun PinValue.format(type: Pin.Type, resources: Resources): String {
    return if (type == Pin.Type.Digital) {
        if (isOn) resources.getString(R.string.common_on)
        else resources.getString(R.string.common_off)
    } else {
        if (isOff) resources.getString(R.string.common_off)
        else "%d%%".format(dutyCycle)
    }
}

/**
 * Formats int as percentage (0 - 0%, 100 - 100%).
 */
fun Int.formatAsPercentage(): String {
    return "%d%%".format(this)
}