package com.maroonbells.maja.service.session

import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.bluetooth.BluetoothBoardManager
import com.maroonbells.maja.domain.board.bluetooth.MajaBluetoothBoardManager
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.MajaFeatureManager
import com.maroonbells.maja.domain.board.feature.repository.FeatureChannel
import com.maroonbells.maja.domain.board.logic.*
import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.framework.board.feature.repository.bluetooth.BluetoothFeatureChannel
import com.maroonbells.maja.framework.board.feature.repository.demo.DemoFeatureChannel
import org.koin.dsl.module


val bluetoothBoardSessionModule = module {
    single<FeatureChannel<BluetoothAddress>> { BluetoothFeatureChannel(get()) }
    single<FeatureManager<BluetoothAddress>> { MajaFeatureManager(get()) }
    single<BluetoothBoardManager> { MajaBluetoothBoardManager(get(), get(), get(), get()) }
    single<BoardManager> { get<BluetoothBoardManager>() }
    single<BoardNameManager> { MajaBoardNameManager(get(), get(), get()) }
    single<PinoutManager> { MajaPinoutManager(get(), get(), get()) }
    single<ActionManager> { MajaActionManager(get(), get(), get()) }
    single<RealTimeClockManager> { MajaRealTimeClockManager(get()) }
}

val demoBoardSessionModule = module {
    single<FeatureChannel<DemoAddress>> { DemoFeatureChannel() }
    single<FeatureManager<DemoAddress>> { MajaFeatureManager(get()) }
    single<BluetoothBoardManager> { MajaBluetoothBoardManager(get(), get(), get(), get()) }
    single<BoardManager> { get<BluetoothBoardManager>() }
    single<BoardNameManager> { MajaBoardNameManager(get(), get(), get()) }
    single<PinoutManager> { MajaPinoutManager(get(), get(), get()) }
    single<ActionManager> { MajaActionManager(get(), get(), get()) }
    single<RealTimeClockManager> { MajaRealTimeClockManager(get()) }
}