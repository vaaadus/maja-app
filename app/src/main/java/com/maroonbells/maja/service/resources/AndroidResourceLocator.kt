package com.maroonbells.maja.service.resources

import android.content.Context
import com.maroonbells.maja.R
import com.maroonbells.maja.domain.resources.ResourceLocator
import com.maroonbells.maja.domain.resources.ResourceLocator.Text

class AndroidResourceLocator(private val context: Context) : ResourceLocator {

    override fun getString(text: Text): String = when (text) {
        Text.BoardName -> context.getString(R.string.common_yourBoard)
        Text.UnknownActionName -> "???"
    }
}