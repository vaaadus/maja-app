package com.maroonbells.maja.service.session

import com.maroonbells.maja.di.getOrNull
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.logic.ActionManager
import com.maroonbells.maja.domain.board.logic.BoardNameManager
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.configuration.repository.Configuration
import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener
import org.koin.core.KoinComponent
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import java.util.concurrent.CopyOnWriteArrayList

class MajaBoardSessionManager(private val configuration: Configuration) : BoardSessionManager, KoinComponent {

    companion object {
        const val KEY_BOARD_ADDRESS = "key_board_address"
        const val KEY_BOARD_TYPE = "key_board_type"
    }

    private val listeners = CopyOnWriteArrayList<BoardSessionListener>()

    init {
        reconfigure()
    }

    override val boardAddress: BoardAddress?
        get() {
            val raw = configuration.getString(KEY_BOARD_ADDRESS)
            val ordinal = configuration.getInt(KEY_BOARD_TYPE)

            if (raw == null || ordinal == null) return null

            return when (BoardType.forOrdinal(ordinal)) {
                BoardType.Bluetooth -> BluetoothAddress(raw)
                BoardType.Demo -> DemoAddress(raw)
            }
        }

    override val boardManager: BoardManager?
        get() = getOrNull()

    override val boardNameManager: BoardNameManager?
        get() = getOrNull()

    override val pinoutManager: PinoutManager?
        get() = getOrNull()

    override val actionManager: ActionManager?
        get() = getOrNull()

    override fun addBoardSessionListener(listener: BoardSessionListener) {
        if (listeners.addIfAbsent(listener)) {
            notifyListener(listener)
        }
    }

    override fun removeBoardSessionListener(listener: BoardSessionListener?) {
        listeners.remove(listener)
    }

    override fun configure(address: BoardAddress) {
        destroyConfiguration()

        when (address) {
            is BluetoothAddress -> {
                configuration.putString(KEY_BOARD_ADDRESS, address.raw)
                configuration.putInt(KEY_BOARD_TYPE, BoardType.Bluetooth.ordinal)
            }
            is DemoAddress -> {
                configuration.putString(KEY_BOARD_ADDRESS, address.raw)
                configuration.putInt(KEY_BOARD_TYPE, BoardType.Demo.ordinal)
            }
            else -> {
                configuration.clear(KEY_BOARD_ADDRESS)
                configuration.clear(KEY_BOARD_TYPE)
            }
        }


        reconfigure()
        notifyListeners()
    }

    override fun destroyConfiguration() {
        configuration.clear(KEY_BOARD_ADDRESS)
        configuration.clear(KEY_BOARD_TYPE)

        notifyListeners()
        reconfigure()
    }

    private fun reconfigure() = when (boardAddress) {
        is BluetoothAddress -> loadKoinModules(bluetoothBoardSessionModule)
        is DemoAddress -> loadKoinModules(demoBoardSessionModule)
        else -> unloadKoinModules(listOf(bluetoothBoardSessionModule, demoBoardSessionModule))
    }

    private fun notifyListeners() {
        for (listener in listeners) {
            notifyListener(listener)
        }
    }

    private fun notifyListener(listener: BoardSessionListener) {
        val address = boardAddress
        if (address != null) listener.onBoardSessionCreated(address)
        else listener.onBoardSessionDestroyed()
    }
}

enum class BoardType {
    Bluetooth, Demo;

    companion object {
        fun forOrdinal(num: Int) = values()[num]
    }
}
