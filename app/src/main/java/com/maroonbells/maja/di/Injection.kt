package com.maroonbells.maja.di

import androidx.lifecycle.ViewModel
import com.maroonbells.maja.common.AppActivity
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.common.AppSheetFragment
import org.koin.androidx.scope.currentScope
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier

/**
 * Inject a view model into [AppActivity] with activity current scope.
 */
inline fun <reified T : ViewModel> AppActivity.injectViewModel(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
): Lazy<T> = lazy { currentScope.getViewModel(this, T::class, qualifier, parameters) }

/**
 * Inject a view model into [AppFragment] with activity current scope.
 */
inline fun <reified T : ViewModel> AppFragment.injectViewModel(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
): Lazy<T> = lazy { requireActivity().currentScope.getViewModel(this, T::class, qualifier, parameters) }

/**
 * Inject a view model into [AppSheetFragment] with activity current scope.
 */
inline fun <reified T : ViewModel> AppSheetFragment.injectViewModel(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
): Lazy<T> = lazy { requireActivity().currentScope.getViewModel(this, T::class, qualifier, parameters) }

/**
 * Inject a dependency with activity current scope.
 */
inline fun <reified T> AppActivity.inject(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
): Lazy<T> = lazy { currentScope.get<T>(qualifier, parameters) }

/**
 * Inject a dependency with activity current scope.
 */
inline fun <reified T> AppFragment.inject(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
): Lazy<T> = lazy { requireActivity().currentScope.get<T>(qualifier, parameters) }

/**
 * Inject a dependency with activity current scope.
 */
inline fun <reified T> AppSheetFragment.inject(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
): Lazy<T> = lazy { requireActivity().currentScope.get<T>(qualifier, parameters) }