package com.maroonbells.maja.di

import android.bluetooth.BluetoothAdapter
import android.content.Context
import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.MajaBluetoothManager
import com.maroonbells.maja.domain.bluetooth.repository.Bluetooth
import com.maroonbells.maja.domain.configuration.repository.Configuration
import com.maroonbells.maja.domain.connection.ConnectionManager
import com.maroonbells.maja.domain.connection.MajaConnectionManager
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.MajaCustomizationManager
import com.maroonbells.maja.domain.lifecycle.LifecycleManager
import com.maroonbells.maja.domain.lifecycle.MajaLifecycleManager
import com.maroonbells.maja.domain.resources.ResourceLocator
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.framework.bluetooth.repository.AndroidBluetooth
import com.maroonbells.maja.framework.configuration.SharedPrefsConfiguration
import com.maroonbells.maja.service.resources.AndroidResourceLocator
import com.maroonbells.maja.service.session.MajaBoardSessionManager
import org.koin.core.qualifier.StringQualifier
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import org.koin.dsl.module

fun Scope.createSharedPrefsConfiguration(qualifier: StringQualifier): SharedPrefsConfiguration {
    val context: Context = get()
    val prefs = context.getSharedPreferences(qualifier.value, Context.MODE_PRIVATE)
    return SharedPrefsConfiguration(prefs)
}

val appModule = module {
    val customization = named("Customization")
    val boardSession = named("BoardSession")

    single<Configuration>(customization) { createSharedPrefsConfiguration(customization) }
    single<Configuration>(boardSession) { createSharedPrefsConfiguration(boardSession) }

    single<ResourceLocator> { AndroidResourceLocator(get()) }
    single<LifecycleManager> { MajaLifecycleManager() }
    single<ConnectionManager> { MajaConnectionManager(get(), get(), get()) }
    single<CustomizationManager> { MajaCustomizationManager(get(customization), get()) }
    single<BoardSessionManager> { MajaBoardSessionManager(get(boardSession)) }
}

val bluetoothModule = module {
    single<BluetoothAdapter> { BluetoothAdapter.getDefaultAdapter() }
    single<Bluetooth> { AndroidBluetooth(get(), get()) }
    single<BluetoothManager> { MajaBluetoothManager(get()) }
}

val majaAppComponent = listOf(appModule, bluetoothModule)