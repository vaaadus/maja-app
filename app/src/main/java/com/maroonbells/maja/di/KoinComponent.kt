package com.maroonbells.maja.di

import org.koin.core.KoinComponent

/**
 * Get the dependency or null if it can't be created or doesn't exist.
 */
inline fun <reified T> KoinComponent.getOrNull(): T? {
    return getKoin().getOrNull()
}