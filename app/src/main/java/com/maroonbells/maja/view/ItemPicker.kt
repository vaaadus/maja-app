package com.maroonbells.maja.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.maroonbells.maja.databinding.ViewScrollablePickerBinding


/**
 * A scrollable picker which allows to pick an item.
 */
class ItemPicker : FrameLayout {

    /**
     * Item to be displayed in the picker. Must provide custom toString() implementation.
     */
    interface Item

    var listener: ((Item) -> Unit)? = null
        set(value) {
            field = value
            getSelectedItem()?.let { value?.invoke(it) }
        }

    private val items = mutableListOf<Item>()
    private lateinit var binding: ViewScrollablePickerBinding

    constructor(context: Context) : super(context) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context)
    }

    @Suppress("UNUSED_ANONYMOUS_PARAMETER")
    private fun initialize(context: Context) {
        binding = ViewScrollablePickerBinding.inflate(LayoutInflater.from(context), this, true)
        binding.picker.setFormatter {
            if (items.lastIndex >= it) items[it].toString()
            else ""
        }
        binding.picker.setOnValueChangedListener { picker, oldVal, newVal ->
            notifyItemSelected()
        }
    }

    fun setItems(newItems: List<Item>) {
        items.clear()
        items.addAll(newItems)
        binding.picker.maxValue = if (newItems.isEmpty()) 0 else newItems.lastIndex
        binding.picker.minValue = 0
        notifyItemSelected()
    }

    fun setSelectedItem(item: Item) {
        val index = items.indexOf(item)
        binding.picker.value = if (index >= 0) index else 0
        notifyItemSelected()
    }

    fun getSelectedItem(): Item? {
        return items.getOrNull(binding.picker.value)
    }

    private fun notifyItemSelected() {
        getSelectedItem()?.let { listener?.invoke(it) }
    }
}