package com.maroonbells.maja.view.recycler

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R
import com.maroonbells.maja.databinding.ItemColorTileBinding
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.service.resources.toColorId


/**
 * A reusable RecyclerView which displays icons.
 */
class ColorsList : RecyclerView {

    var listener: ((ColorHolder) -> Unit)? = null

    private lateinit var adapter: ColorsAdapter

    constructor(context: Context) : super(context) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context)
    }

    private fun initialize(context: Context) {
        isNestedScrollingEnabled = true
        layoutManager = GridLayoutManager(context, context.resources.getInteger(R.integer.tiles_in_row))
        adapter = ColorsAdapter { listener?.invoke(it) }
        setAdapter(adapter)
    }

    fun submitList(items: List<ColorHolder>) {
        adapter.submitList(items)
    }
}

data class ColorHolder(val color: Color, val isSelected: Boolean)

private class ColorsAdapter(
    private val onColorSelected: (ColorHolder) -> Unit
) : ListAdapter<ColorHolder, ColorsAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemColorTileBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(holder: ColorHolder) {
            val context = binding.root.context
            binding.root.setOnClickListener { onColorSelected(holder) }
            binding.tile.setColor(getColor(context, holder.color.toColorId()))
            binding.tile.isSelected = holder.isSelected
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemColorTileBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: ColorHolder, newItem: ColorHolder): Boolean {
        return oldItem.color == newItem.color
    }

    override fun areContentsTheSame(oldItem: ColorHolder, newItem: ColorHolder): Boolean {
        return oldItem == newItem
    }
}