package com.maroonbells.maja.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import com.maroonbells.maja.R
import com.maroonbells.maja.common.extensions.obtainTintedDrawable
import com.maroonbells.maja.databinding.ViewColorTileBinding

/**
 * A reusable custom view which displays a color on a card (tile).
 */
class ColorTile : FrameLayout {

    private lateinit var binding: ViewColorTileBinding

    constructor(context: Context) : super(context) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context)
    }

    private fun initialize(context: Context) {
        binding = ViewColorTileBinding.inflate(LayoutInflater.from(context), this, true)
    }

    fun setColor(@ColorInt color: Int) {
        binding.color.background = resources.obtainTintedDrawable(R.drawable.bg_gray_rounded, color)
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        if (selected) {
            binding.overlay.setBackgroundResource(R.drawable.border_white_3dp)
        } else {
            binding.overlay.background = null
        }
    }
}