package com.maroonbells.maja.view

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.NumberPicker
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import com.maroonbells.maja.R
import com.maroonbells.maja.common.logger.Logger

/**
 * A custom number picker which sets custom text style and allows to set divider color.
 */
class MajaNumberPicker : NumberPicker {

    companion object {
        private val LOG = Logger.forClass(MajaNumberPicker::class.java)
    }

    constructor(context: Context) : super(context) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context)
    }

    private fun initialize(context: Context) {
        setDividerColor(ContextCompat.getColor(context, R.color.gray_70))
    }

    override fun addView(child: View?) {
        super.addView(child)
        updateView(child)
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        super.addView(child, index, params)
        updateView(child)
    }

    override fun addView(child: View?, index: Int) {
        super.addView(child, index)
        updateView(child)
    }

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        super.addView(child, params)
        updateView(child)
    }

    override fun setValue(value: Int) {
        super.setValue(value)

        // Fix disappearing item: https://issuetracker.google.com/issues/36952035
        getChildAt(0)?.visibility = FrameLayout.INVISIBLE
    }

    fun setDividerColor(@ColorInt color: Int) {
        try {
            val field = NumberPicker::class.java.getDeclaredField("mSelectionDivider")
            field.isAccessible = true
            field.set(this, ColorDrawable(color))
        } catch (exception: NoSuchFieldException) {
            LOG.e("No selection divider field: %s", exception)
        }
    }

    private fun updateView(view: View?) {
        if (view is EditText) {
            TextViewCompat.setTextAppearance(view, R.style.Font_Big_Center_Bold_White)
        }
    }
}