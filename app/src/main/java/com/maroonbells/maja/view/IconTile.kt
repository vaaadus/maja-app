package com.maroonbells.maja.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat.getColor
import com.maroonbells.maja.R
import com.maroonbells.maja.common.extensions.setIconTint
import com.maroonbells.maja.databinding.ViewIconTileBinding

/**
 * A reusable custom view which displays an icon on a card (tile).
 */
class IconTile : FrameLayout {

    enum class Style(@ColorRes val iconTint: Int, @DrawableRes val backgroundId: Int) {
        WhiteIconOnGrayBackground(R.color.white, R.drawable.bg_gray_rounded),
        OrangeIconOnDarkBackground(R.color.orange, R.drawable.bg_gray_dark_rounded)
    }

    private lateinit var binding: ViewIconTileBinding

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        binding = ViewIconTileBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(context, attrs)
    }

    private fun applyAttributes(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.IconTile, 0, 0)
        try {
            val enumIndex = array.getInt(R.styleable.IconTile_it_style, 0)
            setStyle(Style.values()[enumIndex])

            if (array.hasValue(R.styleable.IconTile_it_icon)) {
                setIcon(array.getResourceId(R.styleable.IconTile_it_icon, 0))
            }
        } finally {
            array.recycle()
        }
    }

    fun setStyle(style: Style) {
        binding.icon.setIconTint(getColor(context, style.iconTint))
        binding.icon.setBackgroundResource(style.backgroundId)
    }

    fun setIcon(@DrawableRes iconId: Int) {
        binding.icon.setImageResource(iconId)
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        if (selected) {
            binding.overlay.setBackgroundResource(R.drawable.border_white_3dp)
        } else {
            binding.overlay.background = null
        }
    }
}