package com.maroonbells.maja.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.maroonbells.maja.R

/**
 * A divider view.
 */
class Divider : View {

    companion object {
        private const val ALPHA = (0.7 * 255).toInt()
    }

    constructor(context: Context) : super(context) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize()
    }

    private fun initialize() {
        background = resources.getDrawable(R.drawable.bg_divider, null)
        background.alpha = ALPHA
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            getDefaultSize(suggestedMinimumWidth, widthMeasureSpec),
            background.intrinsicHeight)
    }
}