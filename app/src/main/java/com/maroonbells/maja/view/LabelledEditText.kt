package com.maroonbells.maja.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import androidx.annotation.DrawableRes
import androidx.annotation.StyleRes
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.*
import androidx.core.view.isVisible
import androidx.core.widget.TextViewCompat
import androidx.core.widget.addTextChangedListener
import com.maroonbells.maja.R
import com.maroonbells.maja.common.extensions.moveCursorToEnd
import com.maroonbells.maja.databinding.ViewLabelledEdittextBinding

/**
 * An edit text with a label.
 */
class LabelledEditText : FrameLayout {

    enum class ImeOptions(val raw: Int) {
        ActionDone(EditorInfo.IME_ACTION_DONE),
        ActionNext(EditorInfo.IME_ACTION_NEXT),
        ActionGo(EditorInfo.IME_ACTION_GO);

        companion object {

            fun forRaw(raw: Int): ImeOptions? {
                return values().firstOrNull { it.raw == raw }
            }
        }
    }

    enum class LabelPosition {

        /**
         * No label.
         */
        None,

        /**
         * Label is displayed at the start of the view (usually left side).
         */
        Start,

        /**
         * Label is displayed at the end of the view (usually right side).
         */
        End
    }

    var textListener: ((String) -> Unit)? = null

    private lateinit var binding: ViewLabelledEdittextBinding

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        binding = ViewLabelledEdittextBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(context, attrs)
        applyListeners()
    }

    private fun applyAttributes(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.LabelledEditText, 0, 0)
        try {
            setText(array.getString(R.styleable.LabelledEditText_let_text))
            setLabel(array.getString(R.styleable.LabelledEditText_let_labelText))
            setPlaceholder(array.getString(R.styleable.LabelledEditText_let_placeholder))

            val labelPosIndex = array.getInt(R.styleable.LabelledEditText_let_labelPosition, 0)
            setLabelPosition(LabelPosition.values()[labelPosIndex])

            if (array.hasValue(R.styleable.LabelledEditText_let_labelBackground)) {
                setLabelBackground(array.getResourceId(R.styleable.LabelledEditText_let_labelBackground, 0))
            }

            if (array.hasValue(R.styleable.LabelledEditText_let_labelTextStyle)) {
                setLabelTextStyle(array.getResourceId(R.styleable.LabelledEditText_let_labelTextStyle, 0))
            }

            if (array.hasValue(R.styleable.LabelledEditText_let_imeOptions)) {
                val imeIndex = array.getInt(R.styleable.LabelledEditText_let_imeOptions, 0)
                setImeOptions(ImeOptions.values()[imeIndex])
            }

            setEditable(array.getBoolean(R.styleable.LabelledEditText_let_editable, true))
        } finally {
            array.recycle()
        }
    }

    private fun applyListeners() {
        binding.editText.addTextChangedListener {
            textListener?.invoke(getText())
        }
    }

    fun getText(): String {
        return binding.editText.text?.toString() ?: ""
    }

    fun setText(text: String?) {
        binding.editText.setText(text)
        binding.editText.moveCursorToEnd()
    }

    fun setPlaceholder(text: String?) {
        binding.editText.hint = text
    }

    fun setLabel(text: String?) {
        binding.label.text = text
    }

    fun setLabelPosition(position: LabelPosition) {

        binding.label.isVisible = position != LabelPosition.None

        val margin = resources.getDimensionPixelSize(R.dimen.grid_1_5x)
        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.constraintLayout)

        constraintSet.clear(R.id.label, START)
        constraintSet.clear(R.id.label, END)
        constraintSet.connect(R.id.label, TOP, R.id.constraintLayout, TOP)
        constraintSet.connect(R.id.label, BOTTOM, R.id.constraintLayout, BOTTOM)

        constraintSet.clear(R.id.editText, START)
        constraintSet.clear(R.id.editText, END)
        constraintSet.connect(R.id.editText, TOP, R.id.constraintLayout, TOP)
        constraintSet.connect(R.id.editText, BOTTOM, R.id.constraintLayout, BOTTOM)

        when (position) {
            LabelPosition.Start -> {
                constraintSet.connect(R.id.label, START, R.id.constraintLayout, START)
                constraintSet.connect(R.id.editText, START, R.id.label, END, margin)
                constraintSet.connect(R.id.editText, END, R.id.constraintLayout, END, margin)
            }
            LabelPosition.End -> {
                constraintSet.connect(R.id.label, END, R.id.constraintLayout, END)
                constraintSet.connect(R.id.editText, START, R.id.constraintLayout, START, margin)
                constraintSet.connect(R.id.editText, END, R.id.label, START, margin)
            }
            LabelPosition.None -> {
                constraintSet.connect(R.id.editText, START, R.id.constraintLayout, START, margin)
                constraintSet.connect(R.id.editText, END, R.id.constraintLayout, END, margin)
            }
        }

        constraintSet.applyTo(binding.constraintLayout)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(l)
        binding.editText.setOnClickListener(l)
    }

    /**
     * Sets a listener to handle IME options.
     * The listener must return true if event was consumed, false otherwise.
     */
    @Suppress("UNUSED_ANONYMOUS_PARAMETER")
    fun setImeOptionsListener(listener: (ImeOptions, String) -> Boolean) {
        binding.editText.setOnEditorActionListener { v, actionId, event ->
            val options = ImeOptions.forRaw(actionId)
            if (options != null) {
                listener(options, getText())
            } else {
                false
            }
        }
    }

    fun setLabelBackground(@DrawableRes background: Int) {
        binding.label.setBackgroundResource(background)
    }

    fun setLabelTextStyle(@StyleRes style: Int) {
        TextViewCompat.setTextAppearance(binding.label, style)
    }

    fun setEditable(editable: Boolean) {
        binding.editText.isFocusable = editable
        binding.editText.isFocusableInTouchMode = editable
        binding.editText.isClickable = editable
        binding.editText.isCursorVisible = editable
    }

    fun setImeOptions(imeOptions: ImeOptions) {
        binding.editText.imeOptions = imeOptions.raw
    }
}