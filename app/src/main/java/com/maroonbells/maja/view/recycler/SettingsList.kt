package com.maroonbells.maja.view.recycler

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.StyleRes
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R
import com.maroonbells.maja.R.style.Font_Normal_Left_Bold_White
import com.maroonbells.maja.R.style.Font_Small_Left_Bold_Gray
import com.maroonbells.maja.databinding.ItemSettingsBinding


/**
 * A reusable RecyclerView which displays settings items.
 */
class SettingsList : RecyclerView {

    var listener: ((SettingsItem) -> Unit)? = null

    private lateinit var adapter: SettingsAdapter

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        val appearance = readAppearance(context, attrs)
        customize(context, appearance)
    }

    private fun readAppearance(context: Context, attrs: AttributeSet?): ItemAppearance {
        val array = context.obtainStyledAttributes(attrs, R.styleable.SettingsList, 0, 0)
        try {
            val title = array.getResourceId(R.styleable.SettingsList_sl_title_style, Font_Normal_Left_Bold_White)
            val label = array.getResourceId(R.styleable.SettingsList_sl_label_style, Font_Small_Left_Bold_Gray)
            return ItemAppearance(title, label)
        } finally {
            array.recycle()
        }
    }

    private fun customize(context: Context, appearance: ItemAppearance) {
        isNestedScrollingEnabled = true
        layoutManager = LinearLayoutManager(context)
        adapter = SettingsAdapter(appearance) { listener?.invoke(it) }
        setAdapter(adapter)
        addItemDecoration(DividerItemDecoration())
    }

    fun submitList(items: List<SettingsItem>) {
        adapter.submitList(items)
    }
}

/**
 * The data class to be displayed in the list. Must provide custom equals implementation.
 */
interface SettingsItem {
    val title: String
    val label: String
}

private data class ItemAppearance(@StyleRes val title: Int, @StyleRes val label: Int)

private class SettingsAdapter(
    private val appearance: ItemAppearance,
    private val onSettingSelected: (SettingsItem) -> Unit
) : ListAdapter<SettingsItem, SettingsAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemSettingsBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SettingsItem) {
            binding.root.setOnClickListener { onSettingSelected(item) }
            binding.title.text = item.title
            binding.label.text = item.label

            TextViewCompat.setTextAppearance(binding.title, appearance.title)
            TextViewCompat.setTextAppearance(binding.label, appearance.label)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemSettingsBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: SettingsItem, newItem: SettingsItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: SettingsItem, newItem: SettingsItem): Boolean {
        return oldItem == newItem
    }
}