package com.maroonbells.maja.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.maroonbells.maja.R

/**
 * A circular progress bar without an end.
 */
class IndeterminateProgressBar : ProgressBar {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        isIndeterminate = true
        indeterminateDrawable = IndeterminateProgressDrawable(context)
    }
}

class IndeterminateProgressDrawable(private val context: Context) : CircularProgressDrawable(context) {

    private val circlePaint = Paint()

    init {
        val lineThickness = 3f.dpToPx()

        circlePaint.color = ContextCompat.getColor(context, R.color.grayDark)
        circlePaint.strokeWidth = lineThickness
        circlePaint.style = Paint.Style.STROKE

        setColorSchemeColors(ContextCompat.getColor(context, R.color.colorAccent))
        strokeWidth = lineThickness
        strokeCap = Paint.Cap.ROUND
    }

    override fun draw(canvas: Canvas) {
        val rect = RectF(bounds)
        val lineInset = circlePaint.strokeWidth / 2
        rect.inset(lineInset, lineInset)

        val cx = rect.centerX()
        val cy = rect.centerY()
        val r = rect.width() / 2
        canvas.drawCircle(cx, cy, r, circlePaint)

        super.draw(canvas)
    }

    private fun Float.dpToPx(): Float {
        return TypedValue.applyDimension(COMPLEX_UNIT_DIP, this, context.resources.displayMetrics)
    }
}