package com.maroonbells.maja.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.SeekBar
import androidx.core.view.isVisible
import androidx.core.widget.TextViewCompat
import com.maroonbells.maja.R
import com.maroonbells.maja.databinding.ViewSeekbarBinding

/**
 * A custom SeekBar view with bubble over the value.
 */
class SeekBar : FrameLayout, SeekBar.OnSeekBarChangeListener {

    private lateinit var binding: ViewSeekbarBinding

    var progressListener: ((Int) -> Unit)? = null

    var valueFormatter: (Int) -> String = { it.toString() }
        set(value) {
            field = value
            reformat()
        }

    val min: Int = 0

    var max: Int
        get() = binding.seekbar.max
        set(value) {
            binding.seekbar.max = value
            reformat()
        }

    var progress: Int
        get() = binding.seekbar.progress
        set(value) {
            binding.seekbar.progress = value
            reformat()
        }

    var showCurrentValue: Boolean
        get() = binding.currentValue.isVisible
        set(value) {
            binding.currentValue.isVisible = value
        }

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        binding = ViewSeekbarBinding.inflate(LayoutInflater.from(context), this, true)
        binding.seekbar.setOnSeekBarChangeListener(this)
        clipChildren = false
        clipToPadding = false
        applyAttributes(context, attrs)
        reformat()
        repositionBubble()
    }

    private fun applyAttributes(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.SeekBar, 0, 0)
        try {
            max = array.getInt(R.styleable.SeekBar_sb_max, 100)
            showCurrentValue = array.getBoolean(R.styleable.SeekBar_sb_showCurrentValue, true)
        } finally {
            array.recycle()
        }
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        repositionBubble()
        reformat()
        progressListener?.invoke(progress)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
        binding.bubble.isVisible = true
        repositionBubble()
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        binding.bubble.isVisible = false
        repositionBubble()
    }

    private fun reformat() {
        binding.minValue.text = valueFormatter(min)
        binding.currentValue.text = valueFormatter(progress)
        binding.bubble.text = valueFormatter(progress)
        binding.maxValue.text = valueFormatter(max)

        TextViewCompat.setTextAppearance(
            binding.minValue,
            if (progress == min) R.style.Font_Small_Center_Bold_White
            else R.style.Font_Small_Center_Bold_Gray)

        TextViewCompat.setTextAppearance(
            binding.maxValue,
            if (progress == max) R.style.Font_Small_Center_Bold_White
            else R.style.Font_Small_Center_Bold_Gray)
    }

    private fun repositionBubble() {
        val margin = binding.root.resources.getDimensionPixelSize(R.dimen.grid_0_5x)
        val bubble = binding.bubble
        val thumb = binding.seekbar.thumb

        bubble.x = thumb.bounds.centerX() - bubble.measuredWidth / 2f + margin
        bubble.y = thumb.bounds.top.toFloat() - bubble.measuredHeight - margin
    }
}