package com.maroonbells.maja.view.recycler

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

/**
 * Similar to [androidx.recyclerview.widget.ListAdapter] but computes the diff
 * on the main thread in order to fast load items (no need to switch threads).
 */
abstract class ListAdapter<Item : Any, ViewHolder : RecyclerView.ViewHolder>
    : RecyclerView.Adapter<ViewHolder>(), DiffUtilItemCallback<Item> {

    private val items = mutableListOf<Item>()

    override fun getItemCount(): Int = items.size

    fun submitList(newItems: List<Item>) {
        val result = DiffUtil.calculateDiff(ItemDiffCallback(items, newItems, this))

        items.clear()
        items.addAll(newItems)

        result.dispatchUpdatesTo(this)
    }

    fun getItem(position: Int): Item = items[position]
}

private class ItemDiffCallback<Item : Any>(
    private val items: List<Item>,
    private val newItems: List<Item>,
    private val diffUtil: DiffUtilItemCallback<Item>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = items.size
    override fun getNewListSize(): Int = newItems.size

    override fun areItemsTheSame(oldPos: Int, newPos: Int): Boolean {
        return diffUtil.areItemsTheSame(items[oldPos], newItems[newPos])
    }

    override fun areContentsTheSame(oldPos: Int, newPos: Int): Boolean {
        return diffUtil.areContentsTheSame(items[oldPos], newItems[newPos])
    }
}

interface DiffUtilItemCallback<T> {

    fun areItemsTheSame(oldItem: T, newItem: T): Boolean

    fun areContentsTheSame(oldItem: T, newItem: T): Boolean
}