package com.maroonbells.maja.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import com.maroonbells.maja.R
import com.maroonbells.maja.common.extensions.setIconTint
import com.maroonbells.maja.databinding.ViewAppbarTabBinding

/**
 * A single tab in the [Appbar].
 */
class AppbarTab : FrameLayout {

    private lateinit var binding: ViewAppbarTabBinding

    constructor(context: Context) : super(context) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context)
    }

    private fun initialize(context: Context) {
        binding = ViewAppbarTabBinding.inflate(LayoutInflater.from(context), this, true)
    }

    fun setText(text: String?) {
        binding.text.text = text
    }

    fun setIcon(drawable: Drawable?) {
        binding.icon.setImageDrawable(drawable)
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)

        val style: Int
        val color: Int

        if (selected) {
            style = R.style.Font_Tiny_Center_Bold_Orange
            color = ContextCompat.getColor(context, R.color.orange)
        } else {
            style = R.style.Font_Tiny_Center_Bold_Gray
            color = ContextCompat.getColor(context, R.color.gray)
        }

        TextViewCompat.setTextAppearance(binding.text, style)
        binding.icon.setIconTint(color)
    }
}