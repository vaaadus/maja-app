package com.maroonbells.maja.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.maroonbells.maja.R
import com.maroonbells.maja.databinding.ViewAppbarBinding


/**
 * A custom bottom navigation view.
 */
class Appbar : FrameLayout {

    enum class TabIndex {
        First,
        Second,
        Third,
        Fourth
    }

    var tabListener: ((TabIndex) -> Unit)? = null
    var shortcutListener: (() -> Unit)? = null

    var currentIndex: TabIndex = TabIndex.First
        set(value) {
            field = value
            toggleTab(mapToTab(value))
        }

    private lateinit var binding: ViewAppbarBinding

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        binding = ViewAppbarBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(context, attrs)
        customize()
    }

    private fun applyAttributes(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.Appbar)

        try {
            if (array.hasValue(R.styleable.Appbar_appbar_tab1_icon)) {
                setTab(
                    binding.tab1,
                    array.getString(R.styleable.Appbar_appbar_tab1_text),
                    array.getDrawable(R.styleable.Appbar_appbar_tab1_icon))
            }

            if (array.hasValue(R.styleable.Appbar_appbar_tab2_icon)) {
                setTab(
                    binding.tab2,
                    array.getString(R.styleable.Appbar_appbar_tab2_text),
                    array.getDrawable(R.styleable.Appbar_appbar_tab2_icon))
            }

            if (array.hasValue(R.styleable.Appbar_appbar_tab3_icon)) {
                setTab(
                    binding.tab3,
                    array.getString(R.styleable.Appbar_appbar_tab3_text),
                    array.getDrawable(R.styleable.Appbar_appbar_tab3_icon))
            }

            if (array.hasValue(R.styleable.Appbar_appbar_tab4_icon)) {
                setTab(
                    binding.tab4,
                    array.getString(R.styleable.Appbar_appbar_tab4_text),
                    array.getDrawable(R.styleable.Appbar_appbar_tab4_icon))
            }

            val shortcut = array.getBoolean(R.styleable.Appbar_appbar_shortcut, true)
            binding.shortcut.isVisible = shortcut

        } finally {
            array.recycle()
        }
    }

    private fun customize() {
        binding.tab1.setOnClickListener { toggleTab(binding.tab1) }
        binding.tab2.setOnClickListener { toggleTab(binding.tab2) }
        binding.shortcut.setOnClickListener { shortcutListener?.invoke() }
        binding.tab3.setOnClickListener { toggleTab(binding.tab3) }
        binding.tab4.setOnClickListener { toggleTab(binding.tab4) }

        currentIndex = mapToIndex(binding.tab1)
    }

    private fun setTab(tab: AppbarTab, text: String?, drawable: Drawable?) {
        tab.setText(text)
        tab.setIcon(drawable)
    }

    private fun toggleTab(tab: AppbarTab) {
        if (!tab.isSelected) {

            listOf(binding.tab1, binding.tab2, binding.tab3, binding.tab4).forEach {
                it.isSelected = it == tab
            }

            tabListener?.invoke(mapToIndex(tab))
        }
    }

    private fun mapToTab(num: TabIndex): AppbarTab = when (num) {
        TabIndex.First -> binding.tab1
        TabIndex.Second -> binding.tab2
        TabIndex.Third -> binding.tab3
        TabIndex.Fourth -> binding.tab4
    }

    private fun mapToIndex(tab: AppbarTab): TabIndex = when (tab) {
        binding.tab1 -> TabIndex.First
        binding.tab2 -> TabIndex.Second
        binding.tab3 -> TabIndex.Third
        binding.tab4 -> TabIndex.Fourth
        else -> throw IllegalArgumentException("Outside allowed values: $tab")
    }
}