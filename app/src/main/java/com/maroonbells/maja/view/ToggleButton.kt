package com.maroonbells.maja.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.widget.TextViewCompat
import com.maroonbells.maja.R
import com.maroonbells.maja.databinding.ViewToggleButtonBinding

/**
 * A button with two states: on, off.
 */
class ToggleButton : FrameLayout {

    private lateinit var binding: ViewToggleButtonBinding

    var listener: ((Boolean) -> Unit)? = null
        set(value) {
            field = value
            listener?.invoke(isChecked)
        }

    var isChecked: Boolean
        get() = binding.on.background != null
        set(value) {
            val (onDrawable, offDrawable) = when (value) {
                true -> getDrawable(context, R.drawable.bg_orange_rounded_left) to null
                false -> null to getDrawable(context, R.drawable.bg_orange_rounded_right)
            }

            binding.on.background = onDrawable
            binding.off.background = offDrawable

            val (onStyle, offStyle) = when (value) {
                true -> R.style.Font_Normal_Center_Bold_White to R.style.Font_Normal_Center_Bold_Gray
                false -> R.style.Font_Normal_Center_Bold_Gray to R.style.Font_Normal_Center_Bold_White
            }

            TextViewCompat.setTextAppearance(binding.on, onStyle)
            TextViewCompat.setTextAppearance(binding.off, offStyle)

            listener?.invoke(value)
        }

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        binding = ViewToggleButtonBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(context, attrs)
        applyListeners()
    }

    private fun applyAttributes(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.ToggleButton, 0, 0)
        try {
            isChecked = array.getBoolean(R.styleable.ToggleButton_tb_isChecked, false)
        } finally {
            array.recycle()
        }
    }

    private fun applyListeners() {
        binding.on.setOnClickListener { isChecked = true }
        binding.off.setOnClickListener { isChecked = false }
    }
}