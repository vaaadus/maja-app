package com.maroonbells.maja.view.recycler

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R
import com.maroonbells.maja.databinding.ItemIconTileBinding
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.service.resources.toDrawableId
import com.maroonbells.maja.view.IconTile


/**
 * A reusable RecyclerView which displays icons.
 */
class IconsList : RecyclerView {

    var listener: ((IconHolder) -> Unit)? = null

    private lateinit var adapter: IconsAdapter

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        val style = readIconStyle(context, attrs)

        isNestedScrollingEnabled = true
        layoutManager = GridLayoutManager(context, context.resources.getInteger(R.integer.tiles_in_row))
        adapter = IconsAdapter(style) { listener?.invoke(it) }
        setAdapter(adapter)
    }

    private fun readIconStyle(context: Context, attrs: AttributeSet?): IconTile.Style {
        val array = context.obtainStyledAttributes(attrs, R.styleable.IconsList, 0, 0)
        try {
            val styleIndex = array.getInt(R.styleable.IconsList_il_style, 0)
            return IconTile.Style.values()[styleIndex]
        } finally {
            array.recycle()
        }
    }

    fun submitList(items: List<IconHolder>) {
        adapter.submitList(items)
    }
}

data class IconHolder(val icon: Icon, val isSelected: Boolean)

private class IconsAdapter(
    private val style: IconTile.Style,
    private val onIconSelected: (IconHolder) -> Unit
) : ListAdapter<IconHolder, IconsAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemIconTileBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(holder: IconHolder) {
            binding.root.setOnClickListener { onIconSelected(holder) }
            binding.tile.setIcon(holder.icon.toDrawableId())
            binding.tile.setStyle(style)
            binding.tile.isSelected = holder.isSelected
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemIconTileBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: IconHolder, newItem: IconHolder): Boolean {
        return oldItem.icon == newItem.icon
    }

    override fun areContentsTheSame(oldItem: IconHolder, newItem: IconHolder): Boolean {
        return oldItem == newItem
    }
}