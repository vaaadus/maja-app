package com.maroonbells.maja.view.recycler

import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration


/**
 * A custom recycler view item decoration which adds
 * a vertical space between all items (skipping the last one).
 */
class VerticalSpaceItemDecoration(@DimenRes private val spaceHeight: Int) : ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        if (position == RecyclerView.NO_POSITION) return
        if (position != state.itemCount - 1) {
            outRect.bottom = parent.resources.getDimensionPixelSize(spaceHeight)
        }
    }
}