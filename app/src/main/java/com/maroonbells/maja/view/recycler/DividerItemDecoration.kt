package com.maroonbells.maja.view.recycler

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R


/**
 * A custom DividerItemDecoration which skips the last divider after the RecyclerView.
 */
class DividerItemDecoration : RecyclerView.ItemDecoration() {

    private var cachedDrawable: Drawable? = null

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val drawable = obtainDrawable(parent.context)
        val dividerLeft = parent.paddingLeft
        val dividerRight = parent.width - parent.paddingRight

        for (i in 0..parent.childCount - 2) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val dividerTop = child.bottom + params.bottomMargin
            val dividerBottom = dividerTop + drawable.intrinsicHeight

            drawable.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
            drawable.draw(canvas)
        }
    }

    private fun obtainDrawable(context: Context): Drawable {
        val obtainedDrawable = cachedDrawable ?: context.getDrawable(R.drawable.bg_item_divider)
        requireNotNull(obtainedDrawable)
        cachedDrawable = obtainedDrawable
        return obtainedDrawable
    }
}