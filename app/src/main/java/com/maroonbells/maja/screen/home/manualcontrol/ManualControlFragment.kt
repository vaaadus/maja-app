package com.maroonbells.maja.screen.home.manualcontrol

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.databinding.FragmentManualControlBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.screen.home.picker.pinvalue.PinValuePickerDialogFragment
import com.maroonbells.maja.screen.home.picker.pinvalue.PinValuePickerDialogFragment.OnPinValueSelectedListener
import com.maroonbells.maja.view.recycler.VerticalSpaceItemDecoration

class ManualControlFragment : LifecycleAwareFragment<ManualControlViewModel>(), OnPinValueSelectedListener {

    companion object {
        private const val SELECT_VALUE_RC = 4
        private const val SELECT_VALUE_TAG = "select_value"
    }

    override val viewModel: ManualControlViewModel by injectViewModel()
    private val digitalPinsAdapter = ManualControlPinsAdapter(::onPinSelected)
    private val digitalPwmPinsAdapter = ManualControlPinsAdapter(::onPinSelected)

    private var selectedPinId: PinId? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentManualControlBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }
        binding.setupPins.setOnClickListener { viewModel.onManagePinsAvailableFromHome() }
        binding.customizePins.setOnClickListener { viewModel.onCustomizePins() }

        binding.digitalPins.adapter = digitalPinsAdapter
        binding.digitalPwmPins.adapter = digitalPwmPinsAdapter

        binding.digitalPins.itemAnimator = null
        binding.digitalPwmPins.itemAnimator = null

        binding.digitalPins.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_2x))
        binding.digitalPwmPins.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_2x))

        viewModel.pins.observe(viewLifecycleOwner, Observer { pins: List<CustomizedPin>? ->
            val list = pins ?: emptyList()
            digitalPinsAdapter.submitList(list.filter { it.type == Pin.Type.Digital })
            digitalPwmPinsAdapter.submitList(list.filter { it.type == Pin.Type.DigitalPwm })
        })

        return binding.root
    }

    override fun onStop() {
        selectedPinId = null
        dismissDialogFragment(SELECT_VALUE_TAG)
        super.onStop()
    }

    override fun onPinValueSelected(requestCode: Int, value: PinValue) {
        val pinId = selectedPinId ?: return
        if (requestCode == SELECT_VALUE_RC) {
            selectedPinId = null
            viewModel.onPinValueEdited(pinId, value)
        }
    }

    private fun onPinSelected(pin: CustomizedPin) {
        when (pin.type) {
            Pin.Type.Digital -> {
                val negated = if (pin.value.isOn) PinValue.Low else PinValue.High
                viewModel.onPinValueEdited(pin.id, negated)
            }
            Pin.Type.DigitalPwm -> {
                selectedPinId = pin.id

                val dialog = PinValuePickerDialogFragment.newInstance(pin.value)
                dialog.setTargetFragment(this, SELECT_VALUE_RC)
                dialog.show(requireFragmentManager(), SELECT_VALUE_TAG)
            }
        }
    }
}