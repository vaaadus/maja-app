package com.maroonbells.maja.screen.home.personalize

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.databinding.FragmentPersonalizePinoutBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.screen.home.picker.icon.IconPickerDialogFragment
import com.maroonbells.maja.screen.home.picker.icon.IconPickerDialogFragment.OnIconSelectedListener
import com.maroonbells.maja.view.recycler.VerticalSpaceItemDecoration

class PersonalizePinoutFragment : LifecycleAwareFragment<PersonalizePinoutViewModel>(), OnIconSelectedListener {

    companion object {
        private const val SELECT_ICON_RC = 3
        private const val SELECT_ICON_TAG = "select_icon"
    }

    override val viewModel: PersonalizePinoutViewModel by injectViewModel()
    private val digitalPinsAdapter = PersonalizedPinsAdapter(::onPinNameEdited, ::onPinIconClicked)
    private val digitalPwmPinsAdapter = PersonalizedPinsAdapter(::onPinNameEdited, ::onPinIconClicked)

    private var selectedIconPinId: PinId? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPersonalizePinoutBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }

        binding.digitalPins.adapter = digitalPinsAdapter
        binding.digitalPwmPins.adapter = digitalPwmPinsAdapter

        binding.digitalPins.itemAnimator = null
        binding.digitalPwmPins.itemAnimator = null

        binding.digitalPins.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_2x))
        binding.digitalPwmPins.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_2x))

        viewModel.pins.observe(viewLifecycleOwner, Observer { pins: List<CustomizedPin>? ->
            val list = pins ?: emptyList()
            digitalPinsAdapter.submitList(list.filter { it.type == Pin.Type.Digital })
            digitalPwmPinsAdapter.submitList(list.filter { it.type == Pin.Type.DigitalPwm })
        })

        return binding.root
    }

    override fun onStop() {
        selectedIconPinId = null
        dismissDialogFragment(SELECT_ICON_TAG)
        super.onStop()
    }

    override fun onIconSelected(requestCode: Int, icon: Icon) {
        val pinId = selectedIconPinId ?: return
        if (requestCode == SELECT_ICON_RC) {
            selectedIconPinId = null
            viewModel.onPinIconEdited(pinId, icon)
        }
    }

    private fun onPinNameEdited(pinId: PinId, text: String) {
        viewModel.onPinNameEdited(pinId, text)
    }

    private fun onPinIconClicked(pinId: PinId) {
        selectedIconPinId = pinId

        val dialog = IconPickerDialogFragment.newInstance()
        dialog.setTargetFragment(this, SELECT_ICON_RC)
        dialog.show(requireFragmentManager(), SELECT_ICON_TAG)
    }
}