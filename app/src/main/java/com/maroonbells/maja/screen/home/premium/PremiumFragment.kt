package com.maroonbells.maja.screen.home.premium

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.databinding.FragmentPremiumBinding

class PremiumFragment : AppFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPremiumBinding.inflate(inflater, container, false)

        return binding.root
    }
}