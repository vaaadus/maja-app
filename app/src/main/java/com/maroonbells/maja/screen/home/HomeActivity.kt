package com.maroonbells.maja.screen.home

import android.os.Bundle
import android.transition.Fade
import android.transition.TransitionManager
import androidx.core.view.isVisible
import androidx.navigation.NavDestination
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareActivity
import com.maroonbells.maja.databinding.ActivityHomeBinding
import com.maroonbells.maja.di.inject
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.view.Appbar.TabIndex
import org.koin.core.context.loadKoinModules

@Suppress("UNUSED_ANONYMOUS_PARAMETER")
class HomeActivity : LifecycleAwareActivity<HomeViewModel>() {

    override val viewModel: HomeViewModel by injectViewModel()
    private val navigator: HomeNavigator by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator.onAttach(this)

        val binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initAppbarListeners(binding)
        observeCurrentDestination(binding)
    }

    private fun initAppbarListeners(binding: ActivityHomeBinding) {
        binding.appbar.tabListener = { index ->
            when (index) {
                TabIndex.First -> viewModel.onDashboardSelected()
                TabIndex.Second -> viewModel.onBoardSettingsSelected()
                TabIndex.Third -> viewModel.onProfileSelected()
                TabIndex.Fourth -> viewModel.onPremiumSelected()
            }
        }

        binding.appbar.shortcutListener = {
            viewModel.onShortcutSelected()
        }
    }

    private fun observeCurrentDestination(binding: ActivityHomeBinding) {
        findNavController().addOnDestinationChangedListener { controller, destination, arguments ->

            val duration = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
            val animation = Fade().setDuration(duration).addTarget(binding.appbar)
            TransitionManager.beginDelayedTransition(binding.container, animation)

            val tabIndex = mapDestinationToTabIndex(destination)
            binding.appbar.isVisible = tabIndex != null

            if (tabIndex != null) {
                binding.appbar.currentIndex = tabIndex
            }
        }
    }

    private fun mapDestinationToTabIndex(destination: NavDestination): TabIndex? {
        return when (destination.id) {
            R.id.dashboard -> TabIndex.First
            R.id.boardSettings -> TabIndex.Second
            R.id.profile -> TabIndex.Third
            R.id.premium -> TabIndex.Fourth
            else -> null
        }
    }

    override fun onDestroy() {
        navigator.onDetach(this)
        super.onDestroy()
    }

    companion object {
        init {
            loadKoinModules(homeModule)
        }
    }
}