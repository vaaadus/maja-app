package com.maroonbells.maja.screen.home.boardname

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.lifecycle.Observer
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.databinding.FragmentBoardNameBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.view.LabelledEditText.ImeOptions

class BoardNameFragment : LifecycleAwareFragment<BoardNameViewModel>() {

    override val viewModel: BoardNameViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentBoardNameBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener {
            viewModel.onSave(binding.editText.getText())
        }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.onSave(binding.editText.getText())
        }

        binding.editText.setImeOptionsListener { imeOptions, text ->
            if (imeOptions == ImeOptions.ActionGo) {
                viewModel.onSave(text)
                true
            } else {
                false
            }
        }

        viewModel.name.observe(viewLifecycleOwner, Observer { name: String? ->
            binding.editText.setText(name)
        })

        return binding.root
    }
}