package com.maroonbells.maja.screen.home.accessiblepins

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.databinding.ItemAccessiblePinBinding
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.view.recycler.ListAdapter

class AccessiblePinsAdapter(
    private val onPinChanged: (PinId, Boolean) -> Unit
) : ListAdapter<CustomizedPin, AccessiblePinsAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemAccessiblePinBinding) : RecyclerView.ViewHolder(binding.root) {

        @Suppress("UNUSED_ANONYMOUS_PARAMETER")
        fun bind(pin: CustomizedPin) {
            binding.root.setOnClickListener { binding.checkbox.toggle() }
            binding.editText.setOnClickListener { binding.checkbox.toggle() }
            binding.editText.setText(pin.name)
            binding.checkbox.isChecked = pin.accessibleFromHome
            binding.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                onPinChanged(pin.id, isChecked)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemAccessiblePinBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem == newItem
    }
}