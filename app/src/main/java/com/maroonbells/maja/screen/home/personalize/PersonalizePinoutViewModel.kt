package com.maroonbells.maja.screen.home.personalize

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.logic.PinoutManager.CustomizedPinoutListener
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.customization.value.Icon

class PersonalizePinoutViewModel(
    private val navigation: PersonalizePinoutNavigation,
    private val pinoutManager: PinoutManager
) : LifecycleViewModel(), CustomizedPinoutListener {

    private val _pins: MutableLiveData<List<CustomizedPin>> = OptimizedLiveData()

    val pins: LiveData<List<CustomizedPin>> = _pins

    override fun onStart() {
        pinoutManager.addPinoutListener(this)
    }

    override fun onStop() {
        pinoutManager.removePinoutListener(this)
    }

    override fun onCustomizedPinoutChanged(newPins: List<CustomizedPin>) {
        _pins.value = newPins
    }

    fun onClose() {
        navigation.onClosePersonalizePinout()
    }

    fun onPinNameEdited(pinId: PinId, name: String) {
        pinoutManager.setName(pinId, name)
    }

    fun onPinIconEdited(pinId: PinId, icon: Icon) {
        pinoutManager.setIcon(pinId, icon)
    }
}