package com.maroonbells.maja.screen.home.realtimeclock

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.RealTimeClockManager
import java.text.SimpleDateFormat
import java.util.*

class RealTimeClockViewModel(
    private val navigation: RealTimeClockNavigation,
    private val realTimeClockManager: RealTimeClockManager
) : LifecycleViewModel(), RealTimeClockManager.DatetimeListener {

    companion object {
        const val DATE_FORMAT = "EEE, dd MMM yyyy H:mm:ss"
        const val PLACEHOLDER = "--"
    }

    private val dateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
    private val _smartphoneDatetime: MutableLiveData<String> = OptimizedLiveData()
    private val _boardDatetime: MutableLiveData<String> = OptimizedLiveData()

    val smartphoneDatetime: LiveData<String> = _smartphoneDatetime
    val boardDatetime: LiveData<String> = _boardDatetime

    override fun onStart() {
        super.onStart()
        realTimeClockManager.addDatetimeListener(this)
    }

    override fun onStop() {
        realTimeClockManager.removeDatetimeListener(this)
        super.onStop()
    }

    override fun onDatetimeChanged(date: Date?) {
        _smartphoneDatetime.value = dateFormat.format(Date())
        _boardDatetime.value = date?.let { dateFormat.format(date) } ?: PLACEHOLDER
    }

    fun onClose() {
        navigation.onCloseRealTimeClock()
    }

    fun onSynchronize() {
        realTimeClockManager.synchronize()
    }
}