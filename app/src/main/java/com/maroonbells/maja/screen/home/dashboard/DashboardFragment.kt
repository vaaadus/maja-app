package com.maroonbells.maja.screen.home.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.databinding.FragmentDashboardBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.screen.home.picker.pinvalue.PinValuePickerDialogFragment
import com.maroonbells.maja.screen.home.picker.pinvalue.PinValuePickerDialogFragment.OnPinValueSelectedListener

class DashboardFragment : LifecycleAwareFragment<DashboardViewModel>(), OnPinValueSelectedListener {

    companion object {
        private const val SELECT_PIN_VALUE_RC = 8
        private const val SELECT_PIN_VALUE_TAG = "select_pin_value"
    }

    override val viewModel: DashboardViewModel by injectViewModel()
    private val manualControlAdapter = ManualControlAdapter(::onPinSelected)

    private var selectedPinId: PinId? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentDashboardBinding.inflate(inflater, container, false)

        binding.header.setOnClickListener { viewModel.onRenameBoard() }
        binding.manualControlSettings.setOnClickListener { viewModel.onManageManualControl() }

        binding.manualControl.adapter = manualControlAdapter
        binding.manualControl.itemAnimator = null

        viewModel.boardName.observe(viewLifecycleOwner, Observer { name: String? ->
            binding.header.text = name ?: getString(R.string.common_yourBoard)
        })

        viewModel.pins.observe(viewLifecycleOwner, Observer { pins: List<CustomizedPin>? ->
            manualControlAdapter.submitList(pins ?: emptyList())
        })

        return binding.root
    }

    override fun onStop() {
        selectedPinId = null
        dismissDialogFragment(SELECT_PIN_VALUE_TAG)
        super.onStop()
    }

    override fun onPinValueSelected(requestCode: Int, value: PinValue) {
        val pinId = selectedPinId ?: return
        if (requestCode == SELECT_PIN_VALUE_RC) {
            selectedPinId = null
            viewModel.onPinValueEdited(pinId, value)
        }
    }

    private fun onPinSelected(pin: CustomizedPin) {
        when (pin.type) {
            Pin.Type.Digital -> {
                val negated = if (pin.value.isOn) PinValue.Low else PinValue.High
                viewModel.onPinValueEdited(pin.id, negated)
            }
            Pin.Type.DigitalPwm -> {
                selectedPinId = pin.id

                val dialog = PinValuePickerDialogFragment.newInstance(pin.value)
                dialog.setTargetFragment(this, SELECT_PIN_VALUE_RC)
                dialog.show(requireFragmentManager(), SELECT_PIN_VALUE_TAG)
            }
        }
    }
}