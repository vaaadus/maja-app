package com.maroonbells.maja.screen.home.btdisabled

interface BluetoothDisabledNavigation {

    fun onCloseDisabledBluetooth()
}