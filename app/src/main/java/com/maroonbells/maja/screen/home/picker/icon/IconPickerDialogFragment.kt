package com.maroonbells.maja.screen.home.picker.icon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareSheetFragment
import com.maroonbells.maja.databinding.FragmentPickerIconBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.screen.home.picker.icon.IconPickerDialogFragment.OnIconSelectedListener
import com.maroonbells.maja.view.recycler.IconHolder

/**
 * A bottom sheet dialog fragment which allows to pick an icon.
 *
 * Remember to set target fragment which implements [OnIconSelectedListener].
 */
class IconPickerDialogFragment : LifecycleAwareSheetFragment<IconPickerViewModel>
    (R.drawable.bg_bottom_dialog_gray_dark) {

    interface OnIconSelectedListener {

        fun onIconSelected(requestCode: Int, icon: Icon)
    }

    companion object {

        fun newInstance() = IconPickerDialogFragment()
    }

    override val viewModel: IconPickerViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPickerIconBinding.inflate(inflater, container, false)

        binding.closeButton.setOnClickListener { dismiss() }

        binding.icons.listener = { holder ->
            val listener: OnIconSelectedListener? = findListener()
            listener?.onIconSelected(targetRequestCode, holder.icon)
            dismiss()
        }

        viewModel.availableIcons.observe(viewLifecycleOwner, Observer { icons: List<Icon>? ->
            binding.icons.submitList(asIconHolders(icons ?: emptyList()))
        })

        return binding.root
    }

    private fun asIconHolders(icons: List<Icon>): List<IconHolder> {
        return icons.map { IconHolder(it, false) }
    }
}