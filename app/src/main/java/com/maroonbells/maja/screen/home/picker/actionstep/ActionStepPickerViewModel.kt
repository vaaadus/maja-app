package com.maroonbells.maja.screen.home.picker.actionstep

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.logic.PinoutManager.CustomizedPinoutListener
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue

class ActionStepPickerViewModel(
    private val pinoutManager: PinoutManager
) : LifecycleViewModel(), CustomizedPinoutListener {

    private val _pins: MutableLiveData<List<NamedPin>> = OptimizedLiveData()
    private val _selectedPin: MutableLiveData<NamedPin> = OptimizedLiveData()
    private var defaultPinId: PinId? = null
    private var defaultPinValue: PinValue? = null

    val pins: LiveData<List<NamedPin>> = _pins
    val selectedPin: LiveData<NamedPin> = _selectedPin

    override fun onStart() {
        pinoutManager.addPinoutListener(this)
    }

    override fun onStop() {
        pinoutManager.removePinoutListener(this)
    }

    override fun onCustomizedPinoutChanged(newPins: List<CustomizedPin>) {
        refreshPins(newPins)
    }

    fun onConfigureDefaultPin(pinId: PinId?, pinValue: PinValue?) {
        defaultPinId = pinId
        defaultPinValue = pinValue
        refreshPins(pinoutManager.pins)
    }

    private fun refreshPins(pins: List<CustomizedPin>) {
        _pins.value = pins.map {
            NamedPin(it, obtainDefaultPinValue(it.id))
        }

        _selectedPin.value = pins.firstOrNull { it.id == defaultPinId }?.let { pin ->
            NamedPin(pin, obtainDefaultPinValue(pin.id))
        }
    }

    private fun obtainDefaultPinValue(pinId: PinId): PinValue? {
        if (pinId == defaultPinId) {
            return defaultPinValue
        }
        return null
    }
}

data class NamedPin(
    val pinId: PinId,
    val name: String,
    val type: Pin.Type,
    val defaultValue: PinValue?) {

    constructor(pin: CustomizedPin, defaultValue: PinValue?) : this(
        pin.id,
        pin.name,
        pin.type,
        defaultValue)
}
