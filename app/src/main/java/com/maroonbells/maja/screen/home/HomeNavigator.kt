package com.maroonbells.maja.screen.home

import com.maroonbells.maja.R
import com.maroonbells.maja.common.AppNavigator
import com.maroonbells.maja.common.extensions.clearBackStack
import com.maroonbells.maja.common.extensions.fadeTo
import com.maroonbells.maja.common.extensions.fadeToSingleTop
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.screen.home.accessiblepins.AccessiblePinsNavigation
import com.maroonbells.maja.screen.home.boardname.BoardNameNavigation
import com.maroonbells.maja.screen.home.boardsettings.BoardSettingsNavigation
import com.maroonbells.maja.screen.home.btdisabled.BluetoothDisabledNavigation
import com.maroonbells.maja.screen.home.btdiscovery.BluetoothDiscoveryNavigation
import com.maroonbells.maja.screen.home.dashboard.DashboardNavigation
import com.maroonbells.maja.screen.home.editaction.EditActionFragmentArgs
import com.maroonbells.maja.screen.home.editaction.EditActionNavigation
import com.maroonbells.maja.screen.home.manageactions.ManageActionsNavigation
import com.maroonbells.maja.screen.home.manualcontrol.ManualControlNavigation
import com.maroonbells.maja.screen.home.personalize.PersonalizePinoutNavigation
import com.maroonbells.maja.screen.home.realtimeclock.RealTimeClockNavigation


class HomeNavigator : AppNavigator(), BluetoothDisabledNavigation,
    BluetoothDiscoveryNavigation, DashboardNavigation, BoardSettingsNavigation,
    ManualControlNavigation, PersonalizePinoutNavigation, AccessiblePinsNavigation,
    ManageActionsNavigation, EditActionNavigation, BoardNameNavigation,
    RealTimeClockNavigation {

    override fun onCloseDisabledBluetooth() {
        navController?.navigate(R.id.main)
        activity?.finish()
    }

    override fun onCloseBluetoothDiscovery() {
        navController?.navigate(R.id.main)
        activity?.finish()
    }

    override fun onRenameBoard() {
        navController?.fadeTo(R.id.boardName)
    }

    override fun onManageActions() {
        navController?.fadeTo(R.id.manageActions)
    }

    override fun onManualControl() {
        navController?.fadeTo(R.id.manualControl)
    }

    override fun onManagePinsAvailableFromHome() {
        navController?.fadeTo(R.id.accessiblePins)
    }

    override fun onPersonalizePinout() {
        navController?.fadeTo(R.id.personalizePinout)
    }

    override fun onAdjustDatetime() {
        navController?.fadeTo(R.id.datetime)
    }

    override fun onCloseManualControl() {
        navController?.popBackStack()
    }

    override fun onClosePersonalizePinout() {
        navController?.popBackStack()
    }

    override fun onCloseAccessiblePins() {
        navController?.popBackStack()
    }

    override fun onCloseManageActions() {
        navController?.popBackStack()
    }

    override fun onAddAction() {
        navController?.fadeTo(R.id.editAction)
    }

    override fun onEditAction(id: ActionId) {
        val bundle = EditActionFragmentArgs(id.raw).toBundle()
        navController?.fadeTo(R.id.editAction, bundle)
    }

    override fun onCloseEditAction() {
        navController?.popBackStack()
    }

    override fun onCloseBoardName() {
        navController?.popBackStack()
    }

    override fun onCloseRealTimeClock() {
        navController?.popBackStack()
    }

    fun showBluetoothDisabled() {
        navController?.clearBackStack()
        navController?.fadeTo(R.id.bluetoothDisabled)
    }

    fun showBluetoothDiscovery() {
        navController?.clearBackStack()
        navController?.fadeTo(R.id.bluetoothDiscovery)
    }

    fun showBluetoothConnecting() {
        navController?.clearBackStack()
        navController?.fadeTo(R.id.bluetoothConnecting)
    }

    fun showBluetoothDisconnected() {
        navController?.clearBackStack()
        navController?.fadeTo(R.id.bluetoothDisconnected)
    }

    fun showDashboard() {
        navController?.fadeToSingleTop(R.id.dashboard)
    }

    fun showBoardSettings() {
        navController?.fadeToSingleTop(R.id.boardSettings)
    }

    fun showProfile() {
        navController?.fadeToSingleTop(R.id.profile)
    }

    fun showPremium() {
        navController?.fadeToSingleTop(R.id.premium)
    }

    fun clearBackStack() {
        navController?.clearBackStack()
    }
}