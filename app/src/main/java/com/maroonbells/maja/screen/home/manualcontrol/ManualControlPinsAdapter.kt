package com.maroonbells.maja.screen.home.manualcontrol

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R
import com.maroonbells.maja.common.extensions.NO_RESOURCE_ID
import com.maroonbells.maja.databinding.ItemManualControlPinBinding
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.service.resources.format
import com.maroonbells.maja.view.recycler.ListAdapter

class ManualControlPinsAdapter(
    private val onPinClicked: (CustomizedPin) -> Unit
) : ListAdapter<CustomizedPin, ManualControlPinsAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemManualControlPinBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(pin: CustomizedPin) {
            binding.editText.setOnClickListener { onPinClicked(pin) }
            binding.editText.setText(pin.name)

            val label = pin.value.format(pin.type, binding.root.resources)
            binding.editText.setLabel(label)

            val (labelStyle, labelBackground) = when (pin.value.isOn) {
                true -> R.style.Font_Normal_Center_Bold_White to R.drawable.bg_orange_rounded_right
                false -> R.style.Font_Normal_Center_Bold_Gray to NO_RESOURCE_ID
            }

            binding.editText.setLabelTextStyle(labelStyle)
            binding.editText.setLabelBackground(labelBackground)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemManualControlPinBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem == newItem
    }
}