package com.maroonbells.maja.screen.home.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.BoardNameManager
import com.maroonbells.maja.domain.board.logic.BoardNameManager.BoardNameListener
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.logic.PinoutManager.CustomizedPinoutListener
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue

class DashboardViewModel(
    private val navigation: DashboardNavigation,
    private val boardNameManager: BoardNameManager,
    private val pinoutManager: PinoutManager
) : LifecycleViewModel(), BoardNameListener, CustomizedPinoutListener {

    private val _boardName: MutableLiveData<String> = OptimizedLiveData()
    private val _pins: MutableLiveData<List<CustomizedPin>> = OptimizedLiveData()

    val boardName: LiveData<String> = _boardName
    val pins: LiveData<List<CustomizedPin>> = _pins

    override fun onStart() {
        super.onStart()
        boardNameManager.addBoardNameListener(this)
        pinoutManager.addPinoutListener(this)
    }

    override fun onStop() {
        boardNameManager.removeBoardNameListener(this)
        pinoutManager.removePinoutListener(this)
        super.onStop()
    }

    override fun onBoardNameChanged(name: String) {
        _boardName.value = name
    }

    override fun onCustomizedPinoutChanged(newPins: List<CustomizedPin>) {
        _pins.value = newPins.filter { it.accessibleFromHome }
    }

    fun onRenameBoard() {
        navigation.onRenameBoard()
    }

    fun onManageManualControl() {
        navigation.onManualControl()
    }

    fun onPinValueEdited(pinId: PinId, pinValue: PinValue) {
        pinoutManager.setState(pinId, pinValue)
    }
}