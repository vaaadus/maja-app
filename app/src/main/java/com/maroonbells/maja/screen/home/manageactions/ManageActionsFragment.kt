package com.maroonbells.maja.screen.home.manageactions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.common.extensions.redecorate
import com.maroonbells.maja.databinding.FragmentManageActionsBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.view.recycler.VerticalSpaceItemDecoration

class ManageActionsFragment : LifecycleAwareFragment<ManageActionsViewModel>() {

    override val viewModel: ManageActionsViewModel by injectViewModel()
    private val actionsAdapter = ManageActionsAdapter(::onActionClicked, ::onActionRemoved)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentManageActionsBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }
        binding.addButton.setOnClickListener { viewModel.onAddAction() }
        binding.removeButton.setOnClickListener { viewModel.onRemove() }

        binding.actions.adapter = actionsAdapter
        binding.actions.itemAnimator = null
        binding.actions.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_2x))

        viewModel.actions.observe(viewLifecycleOwner, Observer { actions: List<ManagedAction>? ->
            val list = actions ?: emptyList()

            actionsAdapter.submitList(list)
            binding.removeButton.isVisible = list.isNotEmpty()
            binding.actions.redecorate()
        })

        viewModel.canAddActions.observe(viewLifecycleOwner, Observer { canAdd: Boolean? ->
            binding.addButton.isVisible = canAdd == true
        })

        viewModel.isRemoving.observe(viewLifecycleOwner, Observer { removing: Boolean? ->
            binding.removeButton.setImageResource(
                when (removing) {
                    true -> R.drawable.ic_checkmark_gray
                    else -> R.drawable.ic_trash_gray
                })
        })

        return binding.root
    }

    private fun onActionClicked(id: ActionId) {
        viewModel.onEditAction(id)
    }

    private fun onActionRemoved(id: ActionId) {
        viewModel.onRemoveAction(id)
    }
}