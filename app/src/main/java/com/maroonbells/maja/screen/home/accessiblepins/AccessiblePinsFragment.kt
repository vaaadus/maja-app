package com.maroonbells.maja.screen.home.accessiblepins

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.databinding.FragmentAccessiblePinsBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin.Type
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.view.recycler.VerticalSpaceItemDecoration

class AccessiblePinsFragment : LifecycleAwareFragment<AccessiblePinsViewModel>() {

    override val viewModel: AccessiblePinsViewModel by injectViewModel()
    private val digitalPinsAdapter = AccessiblePinsAdapter(::onPinAccessibilityChanged)
    private val digitalPwmPinsAdapter = AccessiblePinsAdapter(::onPinAccessibilityChanged)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentAccessiblePinsBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }

        binding.digitalPins.adapter = digitalPinsAdapter
        binding.digitalPwmPins.adapter = digitalPwmPinsAdapter

        binding.digitalPins.itemAnimator = null
        binding.digitalPwmPins.itemAnimator = null

        binding.digitalPins.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_2x))
        binding.digitalPwmPins.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_2x))

        viewModel.pins.observe(viewLifecycleOwner, Observer { pins: List<CustomizedPin>? ->
            val list = pins ?: emptyList()
            digitalPinsAdapter.submitList(list.filter { it.type == Type.Digital })
            digitalPwmPinsAdapter.submitList(list.filter { it.type == Type.DigitalPwm })
        })

        return binding.root
    }

    private fun onPinAccessibilityChanged(pinId: PinId, accessibleFromHome: Boolean) {
        viewModel.onPinAccessibilityEdited(pinId, accessibleFromHome)
    }
}