package com.maroonbells.maja.screen.home.editaction

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R
import com.maroonbells.maja.databinding.ItemActionStepBinding
import com.maroonbells.maja.service.resources.format
import com.maroonbells.maja.view.recycler.ListAdapter

class EditableStepsAdapter(
    private val onStepSelected: (EditableStep) -> Unit,
    private val onStepRemoved: (EditableStep) -> Unit
) : ListAdapter<EditableStep, EditableStepsAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemActionStepBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(step: EditableStep) {
            val res = binding.root.resources
            val name = step.pinName
            val value = step.pinValue.format(step.pinType, res)

            binding.root.setOnClickListener { onStepSelected(step) }
            binding.text.text = res.getString(R.string.manageActions_step_description, name, value)
            binding.removeButton.isVisible = step.isRemoving
            binding.removeButton.setOnClickListener { onStepRemoved(step) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemActionStepBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: EditableStep, newItem: EditableStep): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: EditableStep, newItem: EditableStep): Boolean {
        return oldItem == newItem
    }
}