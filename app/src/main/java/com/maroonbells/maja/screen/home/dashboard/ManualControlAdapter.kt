package com.maroonbells.maja.screen.home.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat.getColor
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R
import com.maroonbells.maja.common.extensions.obtainTintedDrawable
import com.maroonbells.maja.databinding.ItemManualControlTileBinding
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.service.resources.format
import com.maroonbells.maja.service.resources.toDrawableId
import com.maroonbells.maja.view.recycler.ListAdapter

class ManualControlAdapter(
    private val onPinSelected: (CustomizedPin) -> Unit
) : ListAdapter<CustomizedPin, ManualControlAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemManualControlTileBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(pin: CustomizedPin) {
            val res = binding.root.resources

            binding.root.setOnClickListener { onPinSelected(pin) }
            binding.name.text = pin.name
            binding.value.text = pin.value.format(pin.type, res)

            val (nameAppearance, valueAppearance) = when (pin.value.isOn) {
                true -> R.style.Font_Small_Center_Bold_White to R.style.Font_Normal_Center_Bold_White
                false -> R.style.Font_Small_Center_Bold_Gray to R.style.Font_Normal_Center_Bold_Gray
            }

            TextViewCompat.setTextAppearance(binding.name, nameAppearance)
            TextViewCompat.setTextAppearance(binding.value, valueAppearance)

            val drawableColor = when (pin.value.isOn) {
                true -> R.color.white
                false -> R.color.gray
            }

            binding.icon.setImageDrawable(
                res.obtainTintedDrawable(
                    pin.icon.toDrawableId(),
                    getColor(res, drawableColor, null)))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemManualControlTileBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem == newItem
    }
}