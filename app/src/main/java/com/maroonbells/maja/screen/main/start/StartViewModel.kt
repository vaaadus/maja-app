package com.maroonbells.maja.screen.main.start

import com.maroonbells.maja.common.AppViewModel
import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.domain.session.BoardSessionManager


class StartViewModel(
    private val navigation: StartNavigation,
    private val sessionManager: BoardSessionManager) : AppViewModel() {

    fun onConnect() {
        navigation.showHomeScreen()
    }

    fun onTryDemo() {
        sessionManager.configure(DemoAddress())
    }
}