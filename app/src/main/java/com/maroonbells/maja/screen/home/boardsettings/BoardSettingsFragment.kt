package com.maroonbells.maja.screen.home.boardsettings

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maroonbells.maja.R
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.databinding.FragmentBoardSettingsBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.view.recycler.SettingsItem

class BoardSettingsFragment : AppFragment() {

    private val viewModel: BoardSettingsViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentBoardSettingsBinding.inflate(inflater, container, false)

        binding.disconnectButton.setOnClickListener { viewModel.onDisconnect() }
        binding.settings.submitList(buildSettings(resources))
        binding.settings.listener = { (it as BoardSettingsItem).action() }

        return binding.root
    }

    private fun buildSettings(res: Resources): List<SettingsItem> {
        return listOf(
            BoardSettingsItem(
                title = res.getString(R.string.board_rename_title),
                label = res.getString(R.string.board_rename_label),
                action = viewModel::onRenameBoard
            ),
            BoardSettingsItem(
                title = res.getString(R.string.board_actions_title),
                label = res.getString(R.string.board_actions_label),
                action = viewModel::onManageActions
            ),
            BoardSettingsItem(
                title = res.getString(R.string.board_manualControl_title),
                label = res.getString(R.string.board_manualControl_label),
                action = viewModel::onManageManualControl
            ),
            BoardSettingsItem(
                title = res.getString(R.string.board_customizePins_title),
                label = res.getString(R.string.board_customizePins_label),
                action = viewModel::onCustomizePins
            ),
            BoardSettingsItem(
                title = res.getString(R.string.board_datetime_title),
                label = res.getString(R.string.board_datetime_label),
                action = viewModel::onAdjustDatetime
            )
        )
    }
}

data class BoardSettingsItem(
    override val title: String,
    override val label: String,
    val action: () -> Unit) : SettingsItem