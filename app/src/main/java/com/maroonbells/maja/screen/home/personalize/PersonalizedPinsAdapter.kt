package com.maroonbells.maja.screen.home.personalize

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.databinding.ItemPersonalizePinBinding
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.screen.home.personalize.PersonalizedPinsAdapter.ViewHolder
import com.maroonbells.maja.service.resources.toDrawableId
import com.maroonbells.maja.view.recycler.ListAdapter

class PersonalizedPinsAdapter(
    private val onPinNameEdited: (PinId, String) -> Unit,
    private val onPinIconClicked: (PinId) -> Unit
) : ListAdapter<CustomizedPin, ViewHolder>() {

    inner class ViewHolder(private val binding: ItemPersonalizePinBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(pin: CustomizedPin) {
            binding.icon.setIcon(pin.icon.toDrawableId())
            binding.editText.setText(pin.customName)
            binding.editText.setLabel(pin.defaultName)

            binding.icon.setOnClickListener { onPinIconClicked(pin.id) }
            binding.editText.textListener = { onPinNameEdited(pin.id, it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemPersonalizePinBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CustomizedPin, newItem: CustomizedPin): Boolean {
        return oldItem == newItem
    }
}