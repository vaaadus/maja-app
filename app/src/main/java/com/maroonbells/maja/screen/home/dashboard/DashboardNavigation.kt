package com.maroonbells.maja.screen.home.dashboard

interface DashboardNavigation {

    fun onRenameBoard()

    fun onManualControl()
}