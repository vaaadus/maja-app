package com.maroonbells.maja.screen.home.editaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.ActionManager
import com.maroonbells.maja.domain.board.logic.ActionManager.CustomizedActionsListener
import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.customization.CustomizationManager.CustomizationsListener
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon

class EditActionViewModel(
    private val navigation: EditActionNavigation,
    private val actionManager: ActionManager
) : LifecycleViewModel(), CustomizationsListener, CustomizedActionsListener {

    companion object {
        private const val ICONS_COUNT = 10
        private const val COLORS_COUNT = 10
    }

    private val _title: MutableLiveData<Title> = OptimizedLiveData()
    private val _name: MutableLiveData<String> = OptimizedLiveData()
    private val _steps: MutableLiveData<List<EditableStep>> = OptimizedLiveData()
    private val _isRemovingSteps: MutableLiveData<Boolean> = OptimizedLiveData()
    private val _canAddSteps: MutableLiveData<Boolean> = OptimizedLiveData()
    private val _icons: MutableLiveData<List<SelectableIcon>> = OptimizedLiveData()
    private val _colors: MutableLiveData<List<SelectableColor>> = OptimizedLiveData()
    private val _isScene: MutableLiveData<Boolean> = OptimizedLiveData()

    private var actionId: ActionId? = null
    private var selectedName: String? = null
    private var selectedSteps: List<CustomizedActionStep>? = null
    private var selectedIcon: Icon? = null
    private var selectedColor: Color? = null
    private var selectedIsScene: Boolean? = null

    private var removingStepsEnabled = false

    val title: LiveData<Title> = _title
    val name: LiveData<String> = _name
    val steps: LiveData<List<EditableStep>> = _steps
    val isRemovingSteps: LiveData<Boolean> = _isRemovingSteps
    val canAddSteps: LiveData<Boolean> = _canAddSteps
    val icons: LiveData<List<SelectableIcon>> = _icons
    val colors: LiveData<List<SelectableColor>> = _colors
    val isScene: LiveData<Boolean> = _isScene

    override fun onStart() {
        actionManager.addActionsListener(this)
    }

    override fun onStop() {
        actionManager.removeActionsListener(this)
    }

    override fun onCustomizationsChanged() {
        refreshAll()
    }

    override fun onCustomizedActionsChanged(newActions: List<CustomizedAction>) {
        refreshAll()
    }

    fun configureActionId(id: ActionId?) {
        actionId = id
        refreshAll()
    }

    fun onNameEdited(name: String) {
        selectedName = name
        refreshAll()
    }

    fun onRemoveSteps() {
        removingStepsEnabled = !removingStepsEnabled
        refreshAll()
    }

    fun onStepAdded(step: Action.Step) {
        val customizedStep = actionManager.asCustomizedActionStep(step) ?: return
        selectedSteps = (selectedSteps ?: defaultSteps(actionId)) + customizedStep

        refreshAll()
    }

    fun onStepEdited(previous: Action.Step, next: Action.Step) {
        val customizedStep = actionManager.asCustomizedActionStep(next) ?: return
        val steps = (selectedSteps ?: defaultSteps(actionId)).toMutableList()
        val index = steps.indexOfFirst { it.matches(previous) }

        if (index >= 0) {
            steps[index] = customizedStep
        } else {
            steps += customizedStep
        }

        selectedSteps = steps

        refreshAll()
    }

    fun onStepRemoved(step: Action.Step) {
        selectedSteps = (selectedSteps ?: defaultSteps(actionId)).filterNot { it.matches(step) }

        refreshAll()
    }

    fun onIconSelected(icon: Icon) {
        selectedIcon = icon
        refreshAll()
    }

    fun onColorSelected(color: Color) {
        selectedColor = color
        refreshAll()
    }

    fun onSceneSwitchChanged(availableAsScene: Boolean) {
        selectedIsScene = availableAsScene
        refreshAll()
    }

    fun onSave() {
        val action = actionManager[actionId]

        val actionRequest = UpdateCustomizedActionRequest(
            steps = (selectedSteps ?: defaultSteps(action)).toSteps(),
            name = selectedName ?: defaultName(action),
            icon = selectedIcon ?: defaultIcon(action),
            color = selectedColor ?: defaultColor(action),
            availableAsScene = selectedIsScene ?: defaultIsScene(action))

        if (action != null) {
            actionManager.update(action.id, actionRequest)
        } else {
            actionManager.create(actionRequest)
        }

        navigation.onCloseEditAction()
    }

    fun onClose() {
        navigation.onCloseEditAction()
    }

    private fun refreshAll() {
        val action = actionManager[actionId]

        refreshTitle(action)
        refreshName(action)
        refreshSteps(action)
        refreshIcons(action)
        refreshColors(action)
        refreshIsScene(action)
    }

    private fun refreshTitle(action: CustomizedAction?) {
        _title.value = when (action) {
            null -> Title.NewAction
            else -> Title.EditAction
        }
    }

    private fun refreshName(action: CustomizedAction?) {
        _name.value = selectedName ?: defaultName(action)
    }

    private fun refreshSteps(action: CustomizedAction?) {
        val steps = selectedSteps ?: defaultSteps(action)

        _steps.value = steps.asEditableSteps(removingStepsEnabled)
        _canAddSteps.value = steps.size < actionManager.maxStepsPerAction
        _isRemovingSteps.value = removingStepsEnabled
    }

    private fun refreshIcons(action: CustomizedAction?) {
        val selectedIcon = selectedIcon ?: defaultIcon(action)
        val nextIcons = mutableListOf<SelectableIcon>()
        nextIcons.addAll(actionManager.availableIcons.take(ICONS_COUNT).asSelectableIcons())

        val index = nextIcons.indexOfFirst { it.icon == selectedIcon }
        if (index >= 0) {
            nextIcons[index] = nextIcons[index].copy(isSelected = true)
        } else {
            nextIcons.add(0, SelectableIcon(selectedIcon, true))
        }

        _icons.value = nextIcons.take(ICONS_COUNT)
    }

    private fun refreshColors(action: CustomizedAction?) {
        val selectedColor = selectedColor ?: defaultColor(action)
        val nextColors = mutableListOf<SelectableColor>()
        nextColors.addAll(actionManager.availableColors.take(COLORS_COUNT).asSelectableColors())

        val index = nextColors.indexOfFirst { it.color == selectedColor }
        if (index >= 0) {
            nextColors[index] = nextColors[index].copy(isSelected = true)
        } else {
            nextColors.add(0, SelectableColor(selectedColor, true))
        }

        _colors.value = nextColors.take(COLORS_COUNT)
    }

    private fun refreshIsScene(action: CustomizedAction?) {
        _isScene.value = selectedIsScene ?: defaultIsScene(action)
    }

    private fun defaultName(action: CustomizedAction?): String {
        return action?.customName ?: ""
    }

    private fun defaultSteps(actionId: ActionId?): List<CustomizedActionStep> {
        return defaultSteps(actionManager[actionId])
    }

    private fun defaultSteps(action: CustomizedAction?): List<CustomizedActionStep> {
        return action?.steps ?: emptyList()
    }

    private fun defaultIcon(action: CustomizedAction?): Icon {
        return action?.icon ?: actionManager.defaultIcon
    }

    private fun defaultColor(action: CustomizedAction?): Color {
        return action?.color ?: actionManager.defaultColor
    }

    private fun defaultIsScene(action: CustomizedAction?): Boolean {
        return action?.availableAsScene ?: false
    }

    private fun List<CustomizedActionStep>.asEditableSteps(removingSteps: Boolean): List<EditableStep> {
        return mapNotNull { EditableStep(it, removingSteps) }
    }

    private fun List<Icon>.asSelectableIcons(): List<SelectableIcon> {
        return map { SelectableIcon(it, false) }
    }

    private fun List<Color>.asSelectableColors(): List<SelectableColor> {
        return map { SelectableColor(it, false) }
    }

    private fun List<CustomizedActionStep>.toSteps(): List<Action.Step> {
        return map { Action.Step(it.pinId, it.pinValue) }
    }
}

enum class Title {
    NewAction,
    EditAction
}

data class EditableStep(
    val pinId: PinId,
    val pinName: String,
    val pinValue: PinValue,
    val pinType: Pin.Type,
    val isRemoving: Boolean) {

    constructor(step: CustomizedActionStep, removingSteps: Boolean) : this(
        step.pinId,
        step.pinName,
        step.pinValue,
        step.pinType,
        removingSteps)

    fun asModel() = Action.Step(pinId, pinValue)
}

data class SelectableIcon(val icon: Icon, val isSelected: Boolean)
data class SelectableColor(val color: Color, val isSelected: Boolean)