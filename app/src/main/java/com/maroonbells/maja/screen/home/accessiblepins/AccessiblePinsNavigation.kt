package com.maroonbells.maja.screen.home.accessiblepins

interface AccessiblePinsNavigation {

    fun onCloseAccessiblePins()
}