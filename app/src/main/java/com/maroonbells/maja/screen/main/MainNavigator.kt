package com.maroonbells.maja.screen.main

import com.maroonbells.maja.R
import com.maroonbells.maja.common.AppNavigator
import com.maroonbells.maja.screen.main.start.StartNavigation

class MainNavigator : AppNavigator(), StartNavigation {

    override fun showHomeScreen() {
        navController?.navigate(R.id.home)
        activity?.finish()
    }
}