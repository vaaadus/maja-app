package com.maroonbells.maja.screen.main.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.databinding.FragmentStartBinding
import com.maroonbells.maja.di.injectViewModel

class StartFragment : AppFragment() {

    private val viewModel: StartViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentStartBinding.inflate(inflater)

        binding.connectButton.setOnClickListener { viewModel.onConnect() }
        binding.demoButton.setOnClickListener { viewModel.onTryDemo() }

        return binding.root
    }
}