package com.maroonbells.maja.screen.home.boardsettings

import com.maroonbells.maja.common.AppViewModel
import com.maroonbells.maja.domain.session.BoardSessionManager

class BoardSettingsViewModel(
    private val navigation: BoardSettingsNavigation,
    private val sessionManager: BoardSessionManager) : AppViewModel() {

    fun onDisconnect() {
        sessionManager.destroyConfiguration()
    }

    fun onRenameBoard() {
        navigation.onRenameBoard()
    }

    fun onManageActions() {
        navigation.onManageActions()
    }

    fun onManageManualControl() {
        navigation.onManualControl()
    }

    fun onCustomizePins() {
        navigation.onPersonalizePinout()
    }

    fun onAdjustDatetime() {
        navigation.onAdjustDatetime()
    }
}