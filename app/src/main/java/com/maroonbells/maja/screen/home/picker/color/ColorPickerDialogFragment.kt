package com.maroonbells.maja.screen.home.picker.color

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareSheetFragment
import com.maroonbells.maja.databinding.FragmentPickerColorBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.screen.home.picker.color.ColorPickerDialogFragment.OnColorSelectedListener
import com.maroonbells.maja.view.recycler.ColorHolder

/**
 * A bottom sheet dialog fragment which allows to pick a color.
 *
 * Remember to set target fragment which implements [OnColorSelectedListener].
 */
class ColorPickerDialogFragment : LifecycleAwareSheetFragment<ColorPickerViewModel>
    (R.drawable.bg_bottom_dialog_gray_dark) {

    interface OnColorSelectedListener {

        fun onColorSelected(requestCode: Int, color: Color)
    }

    companion object {

        fun newInstance() = ColorPickerDialogFragment()
    }

    override val viewModel: ColorPickerViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPickerColorBinding.inflate(inflater, container, false)

        binding.closeButton.setOnClickListener { dismiss() }

        binding.colors.listener = { holder ->
            val listener: OnColorSelectedListener? = findListener()
            listener?.onColorSelected(targetRequestCode, holder.color)
            dismiss()
        }

        viewModel.availableColors.observe(viewLifecycleOwner, Observer { colors: List<Color>? ->
            binding.colors.submitList(asColorHolders(colors ?: emptyList()))
        })

        return binding.root
    }

    private fun asColorHolders(colors: List<Color>): List<ColorHolder> {
        return colors.map { ColorHolder(it, false) }
    }
}