package com.maroonbells.maja.screen.home.btdisabled

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.databinding.FragmentBluetoothDisabledBinding
import com.maroonbells.maja.di.injectViewModel

class BluetoothDisabledFragment : AppFragment() {

    private val viewModel: BluetoothDisabledViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentBluetoothDisabledBinding.inflate(inflater, container, false)

        binding.enableButton.setOnClickListener { viewModel.onEnable() }
        binding.backButton.setOnClickListener { viewModel.onClose() }

        return binding.root
    }
}