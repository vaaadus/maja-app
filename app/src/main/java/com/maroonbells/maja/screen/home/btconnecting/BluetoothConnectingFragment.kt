package com.maroonbells.maja.screen.home.btconnecting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.databinding.FragmentBluetoothConnectingBinding
import com.maroonbells.maja.di.injectViewModel

class BluetoothConnectingFragment : AppFragment() {

    private val viewModel: BluetoothConnectingViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentBluetoothConnectingBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.onClose()
        }

        return binding.root
    }
}