package com.maroonbells.maja.screen.home.accessiblepins

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.logic.PinoutManager.CustomizedPinoutListener
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.PinId

class AccessiblePinsViewModel(
    private val navigation: AccessiblePinsNavigation,
    private val pinoutManager: PinoutManager
) : LifecycleViewModel(), CustomizedPinoutListener {

    private val _pins: MutableLiveData<List<CustomizedPin>> = OptimizedLiveData()

    val pins: LiveData<List<CustomizedPin>> = _pins

    override fun onStart() {
        pinoutManager.addPinoutListener(this)
    }

    override fun onStop() {
        pinoutManager.removePinoutListener(this)
    }

    override fun onCustomizedPinoutChanged(newPins: List<CustomizedPin>) {
        refreshPins(newPins)
    }

    fun onClose() {
        navigation.onCloseAccessiblePins()
    }

    fun onPinAccessibilityEdited(pinId: PinId, accessible: Boolean) {
        pinoutManager.setAccessibleFromHome(pinId, accessible)
    }

    private fun refreshPins(pins: List<CustomizedPin>) {
        _pins.value = pins
    }
}