package com.maroonbells.maja.screen.home.btdisabled

import com.maroonbells.maja.common.AppViewModel
import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.session.BoardSessionManager

class BluetoothDisabledViewModel(
    private val navigation: BluetoothDisabledNavigation,
    private val sessionManager: BoardSessionManager,
    private val bluetoothManager: BluetoothManager) : AppViewModel() {

    fun onEnable() {
        bluetoothManager.enable()
    }

    fun onClose() {
        sessionManager.destroyConfiguration()
        navigation.onCloseDisabledBluetooth()
    }
}