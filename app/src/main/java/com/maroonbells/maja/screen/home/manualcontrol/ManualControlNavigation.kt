package com.maroonbells.maja.screen.home.manualcontrol

interface ManualControlNavigation {

    fun onManagePinsAvailableFromHome()

    fun onPersonalizePinout()

    fun onCloseManualControl()
}