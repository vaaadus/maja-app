package com.maroonbells.maja.screen.home.picker.icon

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.value.Icon

class IconPickerViewModel(private val customizationManager: CustomizationManager) : LifecycleViewModel() {

    private val _availableIcons: MutableLiveData<List<Icon>> = OptimizedLiveData()

    val availableIcons: LiveData<List<Icon>> = _availableIcons

    override fun onStart() {
        _availableIcons.value = customizationManager.availableIcons
    }
}