package com.maroonbells.maja.screen.home.boardsettings

interface BoardSettingsNavigation {

    fun onRenameBoard()

    fun onManageActions()

    fun onManualControl()

    fun onPersonalizePinout()

    fun onAdjustDatetime()
}