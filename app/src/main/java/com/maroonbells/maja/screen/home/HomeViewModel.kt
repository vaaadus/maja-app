package com.maroonbells.maja.screen.home

import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.domain.connection.ConnectionManager
import com.maroonbells.maja.domain.connection.ConnectionManager.Status

class HomeViewModel(
    private val navigator: HomeNavigator,
    private val connectionManager: ConnectionManager
) : LifecycleViewModel(), ConnectionManager.StatusListener {

    private var lastConnectionStatus: Status? = null

    override fun onStart() {
        connectionManager.addConnectionStatusListener(this)
    }

    override fun onStop() {
        connectionManager.removeConnectionStatusListener(this)
    }

    override fun onConnectionStatusChanged(status: Status) {
        if (lastConnectionStatus != status) {
            lastConnectionStatus = status

            navigator.clearBackStack()
            when (status) {
                Status.BluetoothDisabled -> navigator.showBluetoothDisabled()
                Status.BluetoothEnabled -> navigator.showBluetoothDiscovery()
                Status.Connecting -> navigator.showBluetoothConnecting()
                Status.Disconnected -> navigator.showBluetoothDisconnected()
                Status.Connected -> navigator.showDashboard()
            }
        }
    }

    public override fun onCleared() {
        lastConnectionStatus = null
        super.onCleared()
    }

    fun onDashboardSelected() {
        navigator.showDashboard()
    }

    fun onBoardSettingsSelected() {
        navigator.showBoardSettings()
    }

    fun onProfileSelected() {
        navigator.showProfile()
    }

    fun onPremiumSelected() {
        navigator.showPremium()
    }

    fun onShortcutSelected() {
        // TODO
    }
}