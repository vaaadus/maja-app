package com.maroonbells.maja.screen.home.editaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.common.extensions.redecorate
import com.maroonbells.maja.databinding.FragmentEditActionBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.screen.home.picker.actionstep.ActionStepPickerSheetFragment
import com.maroonbells.maja.screen.home.picker.actionstep.ActionStepPickerSheetFragment.OnActionStepSelectedListener
import com.maroonbells.maja.screen.home.picker.color.ColorPickerDialogFragment
import com.maroonbells.maja.screen.home.picker.color.ColorPickerDialogFragment.OnColorSelectedListener
import com.maroonbells.maja.screen.home.picker.icon.IconPickerDialogFragment
import com.maroonbells.maja.screen.home.picker.icon.IconPickerDialogFragment.OnIconSelectedListener
import com.maroonbells.maja.view.recycler.ColorHolder
import com.maroonbells.maja.view.recycler.IconHolder
import com.maroonbells.maja.view.recycler.VerticalSpaceItemDecoration

class EditActionFragment : LifecycleAwareFragment<EditActionViewModel>(),
    OnActionStepSelectedListener, OnIconSelectedListener, OnColorSelectedListener {

    companion object {
        private const val NO_ACTION_ID = -1

        private const val ADD_STEP_RC = 1
        private const val ADD_STEP_TAG = "add_step"

        private const val EDIT_STEP_RC = 2
        private const val EDIT_STEP_TAG = "edit_step"

        private const val MORE_ICONS_RC = 3
        private const val MORE_ICONS_TAG = "more_icons"

        private const val MORE_COLORS_RC = 4
        private const val MORE_COLORS_TAG = "more_colors"
    }

    override val viewModel: EditActionViewModel by injectViewModel()
    private val stepsAdapter = EditableStepsAdapter(::onStepSelected, ::onStepRemoved)
    private var editedStep: EditableStep? = null

    @Suppress("UNUSED_ANONYMOUS_PARAMETER")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentEditActionBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }

        binding.saveButton.setOnClickListener {
            viewModel.onSave()
        }

        binding.name.textListener = {
            viewModel.onNameEdited(it)
        }

        binding.steps.adapter = stepsAdapter
        binding.steps.itemAnimator = null
        binding.steps.addItemDecoration(VerticalSpaceItemDecoration(R.dimen.grid_1_5x))

        binding.addSteps.text.setText(R.string.common_add)
        binding.addSteps.text.isAllCaps = true
        binding.addSteps.root.background = getDrawable(requireContext(), R.drawable.bg_gray_dark_rounded)

        binding.addSteps.root.setOnClickListener {
            val dialog = ActionStepPickerSheetFragment.newInstance()
            dialog.setTargetFragment(this, ADD_STEP_RC)
            dialog.show(requireFragmentManager(), ADD_STEP_TAG)
        }

        binding.removeStepsButton.setOnClickListener {
            viewModel.onRemoveSteps()
        }

        binding.icons.listener = { holder ->
            viewModel.onIconSelected(holder.icon)
        }

        binding.colors.listener = { holder ->
            viewModel.onColorSelected(holder.color)
        }

        binding.moreIcons.setOnClickListener {
            val dialog = IconPickerDialogFragment.newInstance()
            dialog.setTargetFragment(this, MORE_ICONS_RC)
            dialog.show(requireFragmentManager(), MORE_ICONS_TAG)
        }

        binding.moreColors.setOnClickListener {
            val dialog = ColorPickerDialogFragment.newInstance()
            dialog.setTargetFragment(this, MORE_COLORS_RC)
            dialog.show(requireFragmentManager(), MORE_COLORS_TAG)
        }

        binding.sceneMessage.setOnClickListener {
            binding.sceneSwitch.toggle()
        }

        binding.sceneSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            viewModel.onSceneSwitchChanged(isChecked)
        }

        viewModel.title.observe(viewLifecycleOwner, Observer { title: Title? ->
            binding.header.text = when (title) {
                Title.NewAction -> getString(R.string.manageActions_new_title)
                Title.EditAction -> getString(R.string.manageActions_edit_title)
                null -> ""
            }
        })

        viewModel.name.observe(viewLifecycleOwner, Observer { name: String? ->
            binding.name.setText(name)
        })

        viewModel.steps.observe(viewLifecycleOwner, Observer { steps: List<EditableStep>? ->
            val list = steps ?: emptyList()

            stepsAdapter.submitList(list)
            binding.steps.isVisible = list.isNotEmpty()
            binding.steps.redecorate()
            binding.removeStepsButton.isVisible = list.isNotEmpty()
        })

        viewModel.isRemovingSteps.observe(viewLifecycleOwner, Observer { isRemoving: Boolean? ->
            binding.removeStepsButton.setImageResource(
                when (isRemoving) {
                    true -> R.drawable.ic_checkmark_gray
                    else -> R.drawable.ic_trash_gray
                })
        })

        viewModel.canAddSteps.observe(viewLifecycleOwner, Observer { canAdd: Boolean? ->
            binding.addSteps.root.isVisible = canAdd == true
        })

        viewModel.icons.observe(viewLifecycleOwner, Observer { icons: List<SelectableIcon>? ->
            val list = icons ?: emptyList()
            binding.icons.submitList(list.map { IconHolder(it.icon, it.isSelected) })
        })

        viewModel.colors.observe(viewLifecycleOwner, Observer { colors: List<SelectableColor>? ->
            val list = colors ?: emptyList()
            binding.colors.submitList(list.map { ColorHolder(it.color, it.isSelected) })
        })

        viewModel.isScene.observe(viewLifecycleOwner, Observer { available: Boolean? ->
            binding.sceneSwitch.isChecked = available == true
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = EditActionFragmentArgs.fromBundle(obtainArguments())
        if (args.actionId != NO_ACTION_ID) {
            viewModel.configureActionId(ActionId(args.actionId))
        } else {
            viewModel.configureActionId(null)
        }
    }

    override fun onStop() {
        editedStep = null
        dismissDialogFragment(ADD_STEP_TAG)
        dismissDialogFragment(EDIT_STEP_TAG)
        dismissDialogFragment(MORE_ICONS_TAG)
        dismissDialogFragment(MORE_COLORS_TAG)
        super.onStop()
    }

    override fun onActionStepSelected(requestCode: Int, step: Action.Step) {
        when (requestCode) {
            ADD_STEP_RC -> {
                viewModel.onStepAdded(step)
            }
            EDIT_STEP_RC -> {
                editedStep?.let {
                    viewModel.onStepEdited(it.asModel(), step)
                    editedStep = null
                }
            }
        }
    }

    override fun onIconSelected(requestCode: Int, icon: Icon) {
        if (requestCode == MORE_ICONS_RC) {
            viewModel.onIconSelected(icon)
        }
    }

    override fun onColorSelected(requestCode: Int, color: Color) {
        if (requestCode == MORE_COLORS_RC) {
            viewModel.onColorSelected(color)
        }
    }

    private fun onStepSelected(step: EditableStep) {
        editedStep = step

        val dialog = ActionStepPickerSheetFragment.newInstance(step.asModel())
        dialog.setTargetFragment(this, EDIT_STEP_RC)
        dialog.show(requireFragmentManager(), EDIT_STEP_TAG)
    }

    private fun onStepRemoved(step: EditableStep) {
        viewModel.onStepRemoved(step.asModel())
    }
}
