package com.maroonbells.maja.screen.home.realtimeclock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.databinding.FragmentRealTimeClockBinding
import com.maroonbells.maja.di.injectViewModel

class RealTimeClockFragment : LifecycleAwareFragment<RealTimeClockViewModel>() {

    override val viewModel: RealTimeClockViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentRealTimeClockBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }
        binding.syncButton.setOnClickListener { viewModel.onSynchronize() }

        viewModel.smartphoneDatetime.observe(viewLifecycleOwner, Observer { text: String? ->
            binding.smartphoneDatetime.text = text
        })

        viewModel.boardDatetime.observe(viewLifecycleOwner, Observer { text: String? ->
            binding.boardDatetime.text = text
        })

        return binding.root
    }
}