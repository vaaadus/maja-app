package com.maroonbells.maja.screen.home.boardname

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.BoardNameManager

class BoardNameViewModel(
    private val navigation: BoardNameNavigation,
    private val boardNameManager: BoardNameManager
) : LifecycleViewModel(), BoardNameManager.BoardNameListener {

    private val _name: MutableLiveData<String> = OptimizedLiveData()

    val name: LiveData<String> = _name

    override fun onStart() {
        super.onStart()
        boardNameManager.addBoardNameListener(this)
    }

    override fun onStop() {
        boardNameManager.removeBoardNameListener(this)
        super.onStop()
    }

    override fun onBoardNameChanged(name: String) {
        _name.value = name
    }

    fun onSave(name: String) {
        boardNameManager.setBoardName(name)
        navigation.onCloseBoardName()
    }
}