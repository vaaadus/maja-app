package com.maroonbells.maja.screen.home.btconnecting

import com.maroonbells.maja.common.AppViewModel
import com.maroonbells.maja.domain.session.BoardSessionManager

class BluetoothConnectingViewModel(private val sessionManager: BoardSessionManager) : AppViewModel() {

    fun onClose() {
        sessionManager.destroyConfiguration()
    }
}