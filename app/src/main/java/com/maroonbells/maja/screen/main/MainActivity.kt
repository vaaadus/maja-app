package com.maroonbells.maja.screen.main

import android.os.Bundle
import com.maroonbells.maja.common.LifecycleAwareActivity
import com.maroonbells.maja.databinding.ActivityMainBinding
import com.maroonbells.maja.di.inject
import com.maroonbells.maja.di.injectViewModel
import org.koin.core.context.loadKoinModules

class MainActivity : LifecycleAwareActivity<MainViewModel>() {

    override val viewModel: MainViewModel by injectViewModel()
    private val navigator: MainNavigator by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator.onAttach(this)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onDestroy() {
        navigator.onDetach(this)
        super.onDestroy()
    }

    companion object {
        init {
            loadKoinModules(mainModule)
        }
    }
}