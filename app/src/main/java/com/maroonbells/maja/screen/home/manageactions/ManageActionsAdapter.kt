package com.maroonbells.maja.screen.home.manageactions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat.getColor
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.maroonbells.maja.R
import com.maroonbells.maja.common.extensions.obtainTintedDrawable
import com.maroonbells.maja.databinding.ItemActionBinding
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.service.resources.toColorId
import com.maroonbells.maja.service.resources.toDrawableId
import com.maroonbells.maja.view.recycler.ListAdapter

class ManageActionsAdapter(
    private val onActionClicked: (ActionId) -> Unit,
    private val onActionRemoved: (ActionId) -> Unit
) : ListAdapter<ManagedAction, ManageActionsAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemActionBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(action: ManagedAction) {
            val res = binding.root.resources

            binding.root.setOnClickListener { onActionClicked(action.id) }
            binding.removeButton.setOnClickListener { onActionRemoved(action.id) }

            binding.text.text = action.name
            binding.icon.setImageResource(action.icon.toDrawableId())
            binding.removeButton.isVisible = action.isRemoving

            binding.iconContainer.background = res.obtainTintedDrawable(
                R.drawable.bg_gray_rounded_left,
                getColor(res, action.color.toColorId(), null))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemActionBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun areItemsTheSame(oldItem: ManagedAction, newItem: ManagedAction): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ManagedAction, newItem: ManagedAction): Boolean {
        return oldItem == newItem
    }
}