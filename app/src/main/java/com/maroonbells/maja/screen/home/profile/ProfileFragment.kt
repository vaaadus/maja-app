package com.maroonbells.maja.screen.home.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maroonbells.maja.BuildConfig
import com.maroonbells.maja.R
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.databinding.FragmentProfileBinding
import com.maroonbells.maja.di.injectViewModel

class ProfileFragment : AppFragment() {

    private val viewModel: ProfileViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentProfileBinding.inflate(inflater, container, false)

        binding.premiumLearnMore.setOnClickListener { viewModel.onPremiumLearnMore() }
        binding.loginTitle.setOnClickListener { viewModel.onLogin() }
        binding.rememberBoardTitle.setOnClickListener { viewModel.onRememberLastBoardChanged() }
        binding.rateInStore.setOnClickListener { viewModel.onRateInStore() }
        binding.shareWithFriends.setOnClickListener { viewModel.onShareWithFriends() }
        binding.contactUs.setOnClickListener { viewModel.onContactSelected() }
        binding.help.setOnClickListener { viewModel.onHelpSelected() }
        binding.terms.setOnClickListener { viewModel.onTermsAndPrivacySelected() }
        binding.licenses.setOnClickListener { viewModel.onLicensesSelected() }
        binding.credits.setOnClickListener { viewModel.onCreditsSelected() }

        binding.version.text = resources.getString(R.string.profile_version, BuildConfig.VERSION_NAME)

        return binding.root
    }
}