package com.maroonbells.maja.screen.home.editaction

interface EditActionNavigation {

    fun onCloseEditAction()
}