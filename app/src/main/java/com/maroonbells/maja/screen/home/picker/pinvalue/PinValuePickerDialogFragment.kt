package com.maroonbells.maja.screen.home.picker.pinvalue

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maroonbells.maja.R
import com.maroonbells.maja.common.AppSheetFragment
import com.maroonbells.maja.databinding.FragmentPickerPinValueBinding
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.screen.home.picker.pinvalue.PinValuePickerDialogFragment.OnPinValueSelectedListener
import com.maroonbells.maja.service.resources.formatAsPercentage

/**
 * A bottom sheet dialog fragment which allows to pick a pin value (0 - 255).
 *
 * Remember to set target fragment which implements [OnPinValueSelectedListener].
 */
class PinValuePickerDialogFragment : AppSheetFragment(R.drawable.bg_bottom_dialog_gray_dark) {

    interface OnPinValueSelectedListener {

        /**
         * Called when user selected the value. Possible values are 0..255.
         */
        fun onPinValueSelected(requestCode: Int, value: PinValue)
    }

    companion object {

        private const val ARG_INITIAL_VALUE = "arg_initial_value"

        fun newInstance(initialValue: PinValue): PinValuePickerDialogFragment {
            return PinValuePickerDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_INITIAL_VALUE, initialValue.raw)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPickerPinValueBinding.inflate(inflater, container, false)

        binding.seekbar.progress = requireArguments().getInt(ARG_INITIAL_VALUE)
        binding.seekbar.valueFormatter = { PinValue(it).dutyCycle.formatAsPercentage() }

        binding.closeButton.setOnClickListener { dismiss() }

        binding.saveButton.setOnClickListener {
            val listener: OnPinValueSelectedListener? = findListener()
            listener?.onPinValueSelected(targetRequestCode, PinValue(binding.seekbar.progress))
            dismiss()
        }

        return binding.root
    }
}