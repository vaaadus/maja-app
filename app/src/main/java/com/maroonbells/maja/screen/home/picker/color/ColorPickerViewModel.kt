package com.maroonbells.maja.screen.home.picker.color

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.value.Color

class ColorPickerViewModel(private val customizationManager: CustomizationManager) : LifecycleViewModel() {

    private val _availableColors: MutableLiveData<List<Color>> = OptimizedLiveData()

    val availableColors: LiveData<List<Color>> = _availableColors

    override fun onStart() {
        _availableColors.value = customizationManager.availableColors
    }
}