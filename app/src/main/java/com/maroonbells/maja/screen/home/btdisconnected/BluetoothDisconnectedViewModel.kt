package com.maroonbells.maja.screen.home.btdisconnected

import com.maroonbells.maja.common.AppViewModel
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.session.BoardSessionManager

class BluetoothDisconnectedViewModel(
    private val sessionManager: BoardSessionManager,
    private val boardManager: BoardManager) : AppViewModel() {

    fun tryAgain() {
        boardManager.reconnect()
    }

    fun onClose() {
        sessionManager.destroyConfiguration()
    }
}