package com.maroonbells.maja.screen.home

import com.maroonbells.maja.screen.home.accessiblepins.AccessiblePinsNavigation
import com.maroonbells.maja.screen.home.accessiblepins.AccessiblePinsViewModel
import com.maroonbells.maja.screen.home.boardname.BoardNameNavigation
import com.maroonbells.maja.screen.home.boardname.BoardNameViewModel
import com.maroonbells.maja.screen.home.boardsettings.BoardSettingsNavigation
import com.maroonbells.maja.screen.home.boardsettings.BoardSettingsViewModel
import com.maroonbells.maja.screen.home.btconnecting.BluetoothConnectingViewModel
import com.maroonbells.maja.screen.home.btdisabled.BluetoothDisabledNavigation
import com.maroonbells.maja.screen.home.btdisabled.BluetoothDisabledViewModel
import com.maroonbells.maja.screen.home.btdisconnected.BluetoothDisconnectedViewModel
import com.maroonbells.maja.screen.home.btdiscovery.BluetoothDiscoveryNavigation
import com.maroonbells.maja.screen.home.btdiscovery.BluetoothDiscoveryViewModel
import com.maroonbells.maja.screen.home.dashboard.DashboardNavigation
import com.maroonbells.maja.screen.home.dashboard.DashboardViewModel
import com.maroonbells.maja.screen.home.editaction.EditActionNavigation
import com.maroonbells.maja.screen.home.editaction.EditActionViewModel
import com.maroonbells.maja.screen.home.manageactions.ManageActionsNavigation
import com.maroonbells.maja.screen.home.manageactions.ManageActionsViewModel
import com.maroonbells.maja.screen.home.manualcontrol.ManualControlNavigation
import com.maroonbells.maja.screen.home.manualcontrol.ManualControlViewModel
import com.maroonbells.maja.screen.home.personalize.PersonalizePinoutNavigation
import com.maroonbells.maja.screen.home.personalize.PersonalizePinoutViewModel
import com.maroonbells.maja.screen.home.picker.actionstep.ActionStepPickerViewModel
import com.maroonbells.maja.screen.home.picker.color.ColorPickerViewModel
import com.maroonbells.maja.screen.home.picker.icon.IconPickerViewModel
import com.maroonbells.maja.screen.home.premium.PremiumViewModel
import com.maroonbells.maja.screen.home.profile.ProfileViewModel
import com.maroonbells.maja.screen.home.realtimeclock.RealTimeClockNavigation
import com.maroonbells.maja.screen.home.realtimeclock.RealTimeClockViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module

val homeModule = module {
    single { HomeNavigator() }
        .bind(BluetoothDisabledNavigation::class)
        .bind(BluetoothDiscoveryNavigation::class)
        .bind(DashboardNavigation::class)
        .bind(BoardSettingsNavigation::class)
        .bind(ManualControlNavigation::class)
        .bind(PersonalizePinoutNavigation::class)
        .bind(AccessiblePinsNavigation::class)
        .bind(ManageActionsNavigation::class)
        .bind(EditActionNavigation::class)
        .bind(BoardNameNavigation::class)
        .bind(RealTimeClockNavigation::class)

    scope(named<HomeActivity>()) {
        viewModel { HomeViewModel(get(), get()) }
        viewModel { BluetoothConnectingViewModel(get()) }
        viewModel { BluetoothDisabledViewModel(get(), get(), get()) }
        viewModel { BluetoothDisconnectedViewModel(get(), get()) }
        viewModel { BluetoothDiscoveryViewModel(get(), get(), get()) }
        viewModel { DashboardViewModel(get(), get(), get()) }
        viewModel { BoardSettingsViewModel(get(), get()) }
        viewModel { ProfileViewModel() }
        viewModel { PremiumViewModel() }
        viewModel { ManualControlViewModel(get(), get()) }
        viewModel { PersonalizePinoutViewModel(get(), get()) }
        viewModel { AccessiblePinsViewModel(get(), get()) }
        viewModel { ManageActionsViewModel(get(), get()) }
        viewModel { EditActionViewModel(get(), get()) }
        viewModel { BoardNameViewModel(get(), get()) }
        viewModel { RealTimeClockViewModel(get(), get()) }
        viewModel { IconPickerViewModel(get()) }
        viewModel { ColorPickerViewModel(get()) }
        viewModel { ActionStepPickerViewModel(get()) }
    }
}