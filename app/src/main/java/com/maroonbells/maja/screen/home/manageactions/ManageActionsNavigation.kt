package com.maroonbells.maja.screen.home.manageactions

import com.maroonbells.maja.domain.board.value.ActionId

interface ManageActionsNavigation {

    fun onCloseManageActions()

    fun onAddAction()

    fun onEditAction(id: ActionId)
}