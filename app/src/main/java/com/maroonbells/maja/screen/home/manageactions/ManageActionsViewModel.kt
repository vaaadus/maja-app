package com.maroonbells.maja.screen.home.manageactions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.extensions.boolValue
import com.maroonbells.maja.common.extensions.toggleValue
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.board.logic.ActionManager
import com.maroonbells.maja.domain.board.logic.ActionManager.CustomizedActionsListener
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.CustomizedAction
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon

class ManageActionsViewModel(
    private val navigation: ManageActionsNavigation,
    private val actionManager: ActionManager
) : LifecycleViewModel(), CustomizedActionsListener {

    private val _actions: MutableLiveData<List<ManagedAction>> = OptimizedLiveData()
    private val _canAddActions: MutableLiveData<Boolean> = OptimizedLiveData()
    private val _isRemoving: MutableLiveData<Boolean> = OptimizedLiveData()

    val actions: LiveData<List<ManagedAction>> = _actions
    val canAddActions: LiveData<Boolean> = _canAddActions
    val isRemoving: LiveData<Boolean> = _isRemoving

    override fun onStart() {
        actionManager.addActionsListener(this)
    }

    override fun onStop() {
        actionManager.removeActionsListener(this)
    }

    override fun onCustomizedActionsChanged(newActions: List<CustomizedAction>) {
        refreshActions(newActions, _isRemoving.boolValue)
    }

    fun onRemove() {
        _isRemoving.toggleValue()
        refreshActions(actionManager.list, _isRemoving.boolValue)
    }

    fun onClose() {
        navigation.onCloseManageActions()
    }

    fun onAddAction() {
        navigation.onAddAction()
    }

    fun onEditAction(id: ActionId) {
        navigation.onEditAction(id)
    }

    fun onRemoveAction(id: ActionId) {
        actionManager.remove(id)
    }

    private fun refreshActions(actions: List<CustomizedAction>, isRemoving: Boolean) {
        _actions.value = actions.map { ManagedAction(it, isRemoving) }
        _canAddActions.value = actionManager.canAddActions
    }
}

data class ManagedAction(
    val id: ActionId,
    val name: String,
    val icon: Icon,
    val color: Color,
    val isRemoving: Boolean) {

    constructor(action: CustomizedAction, removing: Boolean) : this(
        action.id,
        action.name,
        action.icon,
        action.color,
        removing)
}