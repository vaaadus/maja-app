package com.maroonbells.maja.screen.home.btdiscovery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.common.livedata.OptimizedLiveData
import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice
import com.maroonbells.maja.domain.session.BoardSessionManager

class BluetoothDiscoveryViewModel(
    private val navigation: BluetoothDiscoveryNavigation,
    private val bluetoothManager: BluetoothManager,
    private val sessionManager: BoardSessionManager
) : LifecycleViewModel(), BluetoothManager.DiscoveryListener {

    private val _discovering: MutableLiveData<Boolean> = OptimizedLiveData()
    private val _discoveredDevices: MutableLiveData<List<BluetoothDevice>> = OptimizedLiveData()

    val discovering: LiveData<Boolean> = _discovering
    val discoveredDevices: LiveData<List<BluetoothDevice>> = _discoveredDevices

    override fun onStart() {
        postDiscoveredDevices(bluetoothManager.pairedDevices)
    }

    override fun onStop() {
        bluetoothManager.stopDiscovery()
        _discovering.value = false
    }

    override fun onBluetoothDiscoveryStarted() {
        _discovering.value = true
    }

    override fun onBluetoothDevicesDiscovered(devices: List<BluetoothDevice>) {
        postDiscoveredDevices(bluetoothManager.pairedDevices + devices)
    }

    override fun onBluetoothDiscoveryFinished() {
        _discovering.value = false
    }

    fun onDiscoveryStart() {
        bluetoothManager.startDiscovery(this)
        _discovering.value = true
        postDiscoveredDevices(bluetoothManager.pairedDevices)
    }

    fun onDiscoveryStop() {
        bluetoothManager.stopDiscovery()
        _discovering.value = false
    }

    fun onSelectDevice(device: BluetoothDevice) {
        sessionManager.configure(device.address)
    }

    fun onClose() {
        navigation.onCloseBluetoothDiscovery()
    }

    private fun postDiscoveredDevices(devices: Collection<BluetoothDevice>) {
        _discoveredDevices.value = devices.filterNot { it.name.isNullOrBlank() }.distinct()
    }
}