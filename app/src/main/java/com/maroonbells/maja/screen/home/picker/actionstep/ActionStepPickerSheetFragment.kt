package com.maroonbells.maja.screen.home.picker.actionstep

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareSheetFragment
import com.maroonbells.maja.databinding.FragmentPickerActionStepBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.screen.home.picker.actionstep.ActionStepPickerSheetFragment.OnActionStepSelectedListener
import com.maroonbells.maja.service.resources.formatAsPercentage
import com.maroonbells.maja.view.ItemPicker

/**
 * A bottom sheet dialog fragment which allows to pick an action step.
 *
 * Remember to set target fragment which implements [OnActionStepSelectedListener].
 */
class ActionStepPickerSheetFragment : LifecycleAwareSheetFragment<ActionStepPickerViewModel>
    (R.drawable.bg_bottom_dialog_gray_dark) {

    interface OnActionStepSelectedListener {

        fun onActionStepSelected(requestCode: Int, step: Action.Step)
    }

    companion object {

        private const val DEFAULT_DIGITAL_PWM_VALUE = 192 // 75%
        private const val DEFAULT_DIGITAL_VALUE = true

        private const val ARG_PIN_ID = "arg_pin_id"
        private const val ARG_PIN_VALUE = "arg_pin_value"

        fun newInstance() = ActionStepPickerSheetFragment()

        fun newInstance(step: Action.Step): ActionStepPickerSheetFragment {
            val fragment = newInstance()
            fragment.obtainArguments().putInt(ARG_PIN_ID, step.pinId.raw)
            fragment.obtainArguments().putInt(ARG_PIN_VALUE, step.pinValue.raw)
            return fragment
        }
    }

    override val viewModel: ActionStepPickerViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPickerActionStepBinding.inflate(inflater, container, false)

        binding.closeButton.setOnClickListener { dismiss() }

        binding.saveButton.setOnClickListener {
            val item = binding.pinPicker.getSelectedItem() as NamedPinItem
            val value = when (item.type) {
                Pin.Type.Digital -> when (binding.toggleButton.isChecked) {
                    true -> PinValue.High
                    false -> PinValue.Low
                }
                Pin.Type.DigitalPwm -> PinValue(binding.seekbar.progress)
            }

            val listener: OnActionStepSelectedListener? = findListener()
            listener?.onActionStepSelected(targetRequestCode, Action.Step(item.pinId, value))
            dismiss()
        }

        binding.pinPicker.listener = { item ->
            val namedPin = item as NamedPinItem

            when (namedPin.type) {
                Pin.Type.Digital -> {
                    binding.toggleButton.visibility = VISIBLE
                    binding.seekbar.visibility = INVISIBLE
                }
                Pin.Type.DigitalPwm -> {
                    binding.toggleButton.visibility = GONE
                    binding.seekbar.visibility = VISIBLE
                }
            }

            if (namedPin.defaultValue != null) {
                when (namedPin.type) {
                    Pin.Type.Digital -> binding.toggleButton.isChecked = namedPin.defaultValue.isOn
                    Pin.Type.DigitalPwm -> binding.seekbar.progress = namedPin.defaultValue.raw
                }
            }
        }

        binding.seekbar.valueFormatter = { PinValue(it).dutyCycle.formatAsPercentage() }
        binding.seekbar.progress = DEFAULT_DIGITAL_PWM_VALUE
        binding.toggleButton.isChecked = DEFAULT_DIGITAL_VALUE

        viewModel.pins.observe(viewLifecycleOwner, Observer { pins: List<NamedPin>? ->
            val items = pins ?: emptyList()
            binding.pinPicker.setItems(items.map(::NamedPinItem))
        })

        viewModel.selectedPin.observe(viewLifecycleOwner, Observer { pin: NamedPin? ->
            if (pin != null) {
                binding.pinPicker.setSelectedItem(NamedPinItem(pin))
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arguments = obtainArguments()
        if (arguments.containsKey(ARG_PIN_ID) && arguments.containsKey(ARG_PIN_VALUE)) {
            val pinId = arguments.getInt(ARG_PIN_ID)
            val pinValue = arguments.getInt(ARG_PIN_VALUE)
            viewModel.onConfigureDefaultPin(PinId(pinId), PinValue(pinValue))
        }
    }
}

data class NamedPinItem(
    val pinId: PinId,
    val name: String,
    val type: Pin.Type,
    val defaultValue: PinValue?) : ItemPicker.Item {

    constructor(pin: NamedPin) : this(
        pin.pinId,
        pin.name,
        pin.type,
        pin.defaultValue)

    override fun toString() = name
}