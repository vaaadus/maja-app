package com.maroonbells.maja.screen.main

import com.maroonbells.maja.common.LifecycleViewModel
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener

class MainViewModel(
    private val navigator: MainNavigator,
    private val sessionManager: BoardSessionManager
) : LifecycleViewModel(), BoardSessionListener {

    override fun onStart() {
        super.onStart()
        sessionManager.addBoardSessionListener(this)
    }

    override fun onStop() {
        sessionManager.removeBoardSessionListener(this)
        super.onStop()
    }

    override fun onBoardSessionCreated(address: BoardAddress) {
        navigator.showHomeScreen()
    }

    override fun onBoardSessionDestroyed() = Unit
}