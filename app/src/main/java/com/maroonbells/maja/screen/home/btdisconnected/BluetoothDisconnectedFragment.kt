package com.maroonbells.maja.screen.home.btdisconnected

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import com.maroonbells.maja.common.AppFragment
import com.maroonbells.maja.databinding.FragmentBluetoothDisconnectedBinding
import com.maroonbells.maja.di.injectViewModel

class BluetoothDisconnectedFragment : AppFragment() {

    private val viewModel: BluetoothDisconnectedViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentBluetoothDisconnectedBinding.inflate(inflater, container, false)

        binding.tryAgainButton.setOnClickListener { viewModel.tryAgain() }
        binding.backButton.setOnClickListener { viewModel.onClose() }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.onClose()
        }

        return binding.root
    }
}