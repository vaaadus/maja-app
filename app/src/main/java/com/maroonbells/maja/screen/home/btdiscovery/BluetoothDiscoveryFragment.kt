package com.maroonbells.maja.screen.home.btdiscovery

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.maroonbells.maja.R
import com.maroonbells.maja.common.LifecycleAwareFragment
import com.maroonbells.maja.databinding.FragmentBluetoothDiscoveryBinding
import com.maroonbells.maja.di.injectViewModel
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice
import com.maroonbells.maja.view.recycler.SettingsItem

class BluetoothDiscoveryFragment : LifecycleAwareFragment<BluetoothDiscoveryViewModel>() {

    companion object {
        private const val RC_FINE_LOCATION = 1
    }

    override val viewModel: BluetoothDiscoveryViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentBluetoothDiscoveryBinding.inflate(inflater, container, false)

        binding.backButton.setOnClickListener { viewModel.onClose() }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.onClose()
        }

        binding.availableDevices.listener = {
            viewModel.onSelectDevice((it as BluetoothDeviceItem).device)
        }

        binding.pairedDevices.listener = {
            viewModel.onSelectDevice((it as BluetoothDeviceItem).device)
        }

        viewModel.discovering.observe(viewLifecycleOwner, Observer { discovering: Boolean? ->
            binding.loadingSection.isVisible = discovering == true

            if (discovering == true) {
                binding.scanButton.setText(R.string.common_stopScan)
                binding.scanButton.setOnClickListener { viewModel.onDiscoveryStop() }
            } else {
                binding.scanButton.setText(R.string.common_scan)
                binding.scanButton.setOnClickListener { checkLocationPermissions() }
            }
        })

        viewModel.discoveredDevices.observe(viewLifecycleOwner, Observer { devices: List<BluetoothDevice>? ->
            val deviceList = devices ?: emptyList()
            binding.pairedDevices.submitList(deviceList.filter { it.isPaired }.toItems())
            binding.availableDevices.submitList(deviceList.filterNot { it.isPaired }.toItems())
        })

        return binding.root
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == RC_FINE_LOCATION && grantResults.isNotEmpty()) {
            if (grantResults.first() == PERMISSION_GRANTED) {
                viewModel.onDiscoveryStart()
            }
        }
    }

    private fun checkLocationPermissions() {
        if (checkSelfPermission(requireActivity(), ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            viewModel.onDiscoveryStart()
        } else {
            requestPermissions(arrayOf(ACCESS_FINE_LOCATION), RC_FINE_LOCATION)
        }
    }

    private fun Collection<BluetoothDevice>.toItems(): List<BluetoothDeviceItem> {
        return map { BluetoothDeviceItem(it, resources) }
    }
}

data class BluetoothDeviceItem(val device: BluetoothDevice, val resources: Resources) : SettingsItem {

    override val title: String
        get() = device.name ?: ""

    override val label: String
        get() = resources.getString(R.string.bluetoothDiscovery_deviceAddress, device.address.raw)
}