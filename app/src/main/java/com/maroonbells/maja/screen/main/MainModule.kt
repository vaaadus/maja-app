package com.maroonbells.maja.screen.main

import com.maroonbells.maja.screen.main.start.StartNavigation
import com.maroonbells.maja.screen.main.start.StartViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module

val mainModule = module {
    single { MainNavigator() } bind StartNavigation::class

    scope(named<MainActivity>()) {
        viewModel { MainViewModel(get(), get()) }
        viewModel { StartViewModel(get(), get()) }
    }
}