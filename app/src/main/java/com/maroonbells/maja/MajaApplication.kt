package com.maroonbells.maja

import android.app.Application
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES
import android.os.StrictMode
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.maroonbells.maja.common.logger.Logger
import com.maroonbells.maja.di.majaAppComponent
import com.maroonbells.maja.domain.lifecycle.LifecycleManager
import com.maroonbells.maja.framework.logger.CrashlyticsAppender
import com.maroonbells.maja.framework.logger.KoinLogger
import com.maroonbells.maja.framework.logger.LogcatAppender
import io.fabric.sdk.android.Fabric
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MajaApplication : Application(), LifecycleObserver {

    private val lifecycleManager: LifecycleManager by inject()

    override fun onCreate() {
        super.onCreate()

        initLogging()
        initCrashReporting()
        initStrictModePolicies()
        initDependencyInjection()
        initLifecycleObserver()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onLifecycleStart() {
        lifecycleManager.resume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onLifecycleStop() {
        lifecycleManager.pause()
    }

    private fun initLogging() {
        if (BuildConfig.DEBUG) {
            Logger.registerAppender(LogcatAppender())
        } else {
            Logger.registerAppender(CrashlyticsAppender())
        }
    }

    private fun initCrashReporting() {
        // Set up Crashlytics, disabled for debug builds
        val crashlyticsKit = Crashlytics.Builder()
            .core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
            .build()

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit)
    }

    private fun initStrictModePolicies() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyLog().build()
            )

            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                .apply { if (SDK_INT >= VERSION_CODES.M) detectCleartextNetwork() }
                .apply { if (SDK_INT >= VERSION_CODES.O) detectContentUriWithoutPermission() }
                .detectFileUriExposure()
                .detectActivityLeaks()
                .detectLeakedClosableObjects()
                .detectLeakedRegistrationObjects()
                .detectLeakedSqlLiteObjects()
                .penaltyLog()
                .build())
        }
    }

    private fun initDependencyInjection() {
        startKoin {
            logger(KoinLogger())
            androidContext(applicationContext)
            modules(majaAppComponent)
        }
    }

    private fun initLifecycleObserver() {
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }
}