package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.FeatureDeserializer
import com.maroonbells.maja.framework.board.feature.repository.serializable.testFeatureDeserialization
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MajaSchedulesFeatureTest {

    @MockK
    private lateinit var commandChannel: FeatureCommandChannel

    private lateinit var deserializer: FeatureDeserializer

    @BeforeEach
    fun setUp() {
        deserializer = FeatureDeserializer(commandChannel)
    }

    @Test
    fun deserialize() {
        testFeatureDeserialization(
            deserializer = deserializer,
            expected = MajaSchedulesFeature(
                commandChannel = commandChannel,
                path = FeaturePath("board.schedules"),
                maxSchedules = 3,
                maxBlocksPerDay = 4,
                list = listOf(
                    ScheduleId(1),
                    ScheduleId(2),
                    ScheduleId(3)
                )
            )
        )
    }
}