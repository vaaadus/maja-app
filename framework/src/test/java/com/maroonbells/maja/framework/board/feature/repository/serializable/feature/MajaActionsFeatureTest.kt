package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.FeatureDeserializer
import com.maroonbells.maja.framework.board.feature.repository.serializable.testFeatureDeserialization
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MajaActionsFeatureTest {

    @MockK
    private lateinit var commandChannel: FeatureCommandChannel

    private lateinit var deserializer: FeatureDeserializer

    @BeforeEach
    fun setUp() {
        deserializer = FeatureDeserializer(commandChannel)
    }

    @Test
    fun deserialize() {
        testFeatureDeserialization(
            deserializer = deserializer,
            expected = MajaActionsFeature(
                commandChannel = commandChannel,
                path = FeaturePath("board.actions"),
                maxActions = 10,
                maxStepsPerAction = 5,
                actions = listOf(
                    Action(
                        id = ActionId(1), steps = listOf(
                            Action.Step(pinId = PinId(1), pinValue = PinValue(1)),
                            Action.Step(pinId = PinId(4), pinValue = PinValue(255)),
                            Action.Step(pinId = PinId(13), pinValue = PinValue(0))
                        )),
                    Action(
                        id = ActionId(2), steps = listOf(
                            Action.Step(pinId = PinId(20), pinValue = PinValue(200))
                        ))
                )))
    }
}