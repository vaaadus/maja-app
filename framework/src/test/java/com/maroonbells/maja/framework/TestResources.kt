package com.maroonbells.maja.framework

import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.nio.charset.Charset

object TestResources {

    private const val RESOURCES_PATH_PREFIX = "src/test/resources/"
    private val CHARSET = Charset.forName("UTF-8")

    fun readFile(relativePath: String): String {
        val path = RESOURCES_PATH_PREFIX + relativePath
        val inputStream = FileInputStream(File(path))
        val inputStreamReader = InputStreamReader(inputStream, CHARSET)
        return BufferedReader(inputStreamReader).use { reader ->
            reader.readLines().joinToString(separator = "\n")
        }
    }
}