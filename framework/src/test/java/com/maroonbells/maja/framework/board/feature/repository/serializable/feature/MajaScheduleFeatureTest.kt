package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.datetime.Time
import com.maroonbells.maja.domain.board.value.schedule.ScheduleBlock
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.FeatureDeserializer
import com.maroonbells.maja.framework.board.feature.repository.serializable.testFeatureDeserialization
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MajaScheduleFeatureTest {

    @MockK
    private lateinit var commandChannel: FeatureCommandChannel

    private lateinit var deserializer: FeatureDeserializer

    @BeforeEach
    fun setUp() {
        deserializer = FeatureDeserializer(commandChannel)
    }

    @Test
    fun deserialize() {
        testFeatureDeserialization(
            deserializer = deserializer,
            expected = MajaScheduleFeature(
                commandChannel = commandChannel,
                path = FeaturePath("board.schedules.1"),
                id = ScheduleId(1),
                monday = listOf(
                    ScheduleBlock(
                        start = Time(10, 0),
                        end = Time(11, 35),
                        actionId = ActionId(2)),
                    ScheduleBlock(
                        start = Time(0, 0),
                        end = Time(13, 15),
                        actionId = ActionId(3)),
                    ScheduleBlock(
                        start = Time(5, 5),
                        end = Time(10, 7),
                        actionId = ActionId(20))
                ),
                tuesday = emptyList(),
                wednesday = emptyList(),
                thursday = emptyList(),
                friday = emptyList(),
                saturday = emptyList(),
                sunday = emptyList()
            )
        )
    }
}