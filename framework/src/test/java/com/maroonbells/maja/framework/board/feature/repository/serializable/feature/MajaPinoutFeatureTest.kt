package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.FeatureDeserializer
import com.maroonbells.maja.framework.board.feature.repository.serializable.testFeatureDeserialization
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MajaPinoutFeatureTest {

    @MockK
    private lateinit var commandChannel: FeatureCommandChannel

    private lateinit var deserializer: FeatureDeserializer

    @BeforeEach
    fun setUp() {
        deserializer = FeatureDeserializer(commandChannel)
    }

    @Test
    fun deserialize() {
        testFeatureDeserialization(
            deserializer = deserializer,
            expected = MajaPinoutFeature(
                commandChannel = commandChannel,
                path = FeaturePath("board.pinout"),
                pins = listOf(
                    Pin(id = PinId(0), name = "0", type = Pin.Type.Digital, value = PinValue(0)),
                    Pin(id = PinId(6), name = "6", type = Pin.Type.DigitalPwm, value = PinValue(255)))))
    }
}