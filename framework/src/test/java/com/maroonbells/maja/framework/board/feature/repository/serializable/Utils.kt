package com.maroonbells.maja.framework.board.feature.repository.serializable

import com.maroonbells.maja.domain.board.feature.registry.Feature
import com.maroonbells.maja.framework.TestResources
import org.assertj.core.api.Assertions.assertThat

@Suppress("UNCHECKED_CAST")
fun <T : Feature> testFeatureDeserialization(deserializer: FeatureDeserializer, expected: T) {
    val json = TestResources.readFile("json/feature/${expected.path.raw}.json")
    val feature = deserializer.deserialize(json) as T

    assertThat(feature).isEqualTo(expected)
}