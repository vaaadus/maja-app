package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.FeatureDeserializer
import com.maroonbells.maja.framework.board.feature.repository.serializable.testFeatureDeserialization
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
class MajaRealTimeClockFeatureTest {

    @MockK
    private lateinit var commandChannel: FeatureCommandChannel

    private lateinit var deserializer: FeatureDeserializer

    @BeforeEach
    fun setUp() {
        deserializer = FeatureDeserializer(commandChannel)
    }

    @Test
    fun deserialize() {
        testFeatureDeserialization(
            deserializer = deserializer,
            expected = MajaRealTimeClockFeature(
                commandChannel = commandChannel,
                path = FeaturePath("board.rtc"),
                datetime = Date(1570480259000L)
            )
        )
    }
}