package com.maroonbells.maja.framework.board.feature.repository.demo.specialized

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.ActionsFeature
import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.ActionId
import kotlinx.coroutines.Job

data class DemoActionsFeature(
    override val path: FeaturePath,
    override val actions: List<Action>,
    override val maxActions: Int,
    override val maxStepsPerAction: Int,
    private val onCreate: (steps: List<Action.Step>) -> Job,
    private val onUpdate: (id: ActionId, steps: List<Action.Step>) -> Job,
    private val onRemove: (id: ActionId) -> Job
) : ActionsFeature {

    override fun create(steps: List<Action.Step>) = onCreate(steps)
    override fun update(id: ActionId, steps: List<Action.Step>) = onUpdate(id, steps)
    override fun remove(id: ActionId) = onRemove(id)
}