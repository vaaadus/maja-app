package com.maroonbells.maja.framework.board.feature.repository.serializable

import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable

@Serializable
abstract class SerializableCommand<T : SerializableCommand<T>> {

    abstract val path: SerializablePath
    abstract val type: SerializableCommandType

    abstract fun serializer(): KSerializer<T>
}