package com.maroonbells.maja.framework.board.feature.repository.serializable.value.scene

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Update
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.action.SerializableActionId
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableDateTime
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableUpdateSceneCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Update,
    @SerialName("id") val id: SerializableSceneId,
    @SerialName("actionId") val actionId: SerializableActionId,
    @SerialName("activeTill") val activeTill: SerializableDateTime
) : SerializableCommand<SerializableUpdateSceneCommand>() {

    override fun serializer(): KSerializer<SerializableUpdateSceneCommand> {
        return SerializableUpdateSceneCommand.serializer()
    }
}