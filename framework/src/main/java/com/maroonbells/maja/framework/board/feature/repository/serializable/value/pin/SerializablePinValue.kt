package com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin

import com.maroonbells.maja.domain.board.value.PinValue
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Suppress("FunctionName")
fun SerializablePinValue(value: PinValue): SerializablePinValue = SerializablePinValue(value.raw)

@Serializable(PinValueSerializer::class)
data class SerializablePinValue(val raw: Int) {
    fun asModel(): PinValue = PinValue(raw)
}

@Serializer(forClass = SerializablePinValue::class)
object PinValueSerializer : KSerializer<SerializablePinValue> {

    override val descriptor: SerialDescriptor = StringDescriptor.withName("PinValue")

    override fun serialize(encoder: Encoder, obj: SerializablePinValue) {
        encoder.encodeInt(obj.raw)
    }

    override fun deserialize(decoder: Decoder): SerializablePinValue {
        return SerializablePinValue(raw = decoder.decodeInt())
    }
}