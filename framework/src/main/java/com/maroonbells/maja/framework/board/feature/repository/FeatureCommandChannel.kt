package com.maroonbells.maja.framework.board.feature.repository

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import kotlinx.coroutines.Job

interface FeatureCommandChannel {

    fun <T : SerializableCommand<T>> sendCommand(command: T): Job
}