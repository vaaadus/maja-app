package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.ActionsFeature
import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.action.*
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin.SerializablePinId
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin.SerializablePinValue
import kotlinx.coroutines.Job

@Suppress("FunctionName")
fun MajaActionsFeature(commandChannel: FeatureCommandChannel, actions: SerializableActions): MajaActionsFeature {
    return MajaActionsFeature(
        commandChannel = commandChannel,
        path = actions.path.asModel(),
        maxActions = actions.maxActions,
        maxStepsPerAction = actions.maxStepsPerAction,
        actions = actions.list.map(SerializableAction::asModel)
    )
}

data class MajaActionsFeature(
    private val commandChannel: FeatureCommandChannel,
    override val path: FeaturePath,
    override val actions: List<Action>,
    override val maxActions: Int,
    override val maxStepsPerAction: Int) : ActionsFeature {

    override fun create(steps: List<Action.Step>): Job {
        val command = SerializableCreateActionCommand(
            path = SerializablePath(path),
            stepsCount = steps.size,
            steps = steps.map {
                SerializableStep(
                    SerializablePinId(it.pinId),
                    SerializablePinValue(it.pinValue))
            })

        return commandChannel.sendCommand(command)
    }

    override fun update(id: ActionId, steps: List<Action.Step>): Job {
        val command = SerializableUpdateActionCommand(
            path = SerializablePath(path),
            id = SerializableActionId(id),
            stepsCount = steps.size,
            steps = steps.map {
                SerializableStep(
                    SerializablePinId(it.pinId.raw),
                    SerializablePinValue(it.pinValue))
            })

        return commandChannel.sendCommand(command)
    }

    override fun remove(id: ActionId): Job {
        val command = SerializableRemoveActionCommand(
            path = SerializablePath(path),
            id = SerializableActionId(id))

        return commandChannel.sendCommand(command)
    }
}