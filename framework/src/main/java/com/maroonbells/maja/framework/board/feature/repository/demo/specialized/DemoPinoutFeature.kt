package com.maroonbells.maja.framework.board.feature.repository.demo.specialized

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.PinoutFeature
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import kotlinx.coroutines.Job

data class DemoPinoutFeature(
    override val pins: List<Pin>,
    override val path: FeaturePath,
    val setPinState: (id: PinId, value: PinValue) -> Job
) : PinoutFeature {

    override fun set(id: PinId, value: PinValue) = setPinState(id, value)
}