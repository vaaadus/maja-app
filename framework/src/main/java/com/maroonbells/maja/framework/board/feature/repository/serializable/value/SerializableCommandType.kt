package com.maroonbells.maja.framework.board.feature.repository.serializable.value

import com.maroonbells.maja.framework.serialization.EnumSerializer
import com.maroonbells.maja.framework.serialization.SerializableEnum
import kotlinx.serialization.Serializable

@Serializable(CommandTypeSerializer::class)
enum class SerializableCommandType(override val serialName: String) : SerializableEnum {
    Get("get"),
    Create("create"),
    Update("update"),
    Remove("remove")
}

object CommandTypeSerializer : EnumSerializer<SerializableCommandType>(
    "CommandType", SerializableCommandType.values())