package com.maroonbells.maja.framework.logger

import android.util.Log
import com.maroonbells.maja.common.logger.Logger

class LogcatAppender : Logger.Appender {

    override fun append(priority: Logger.Priority, tag: String, message: String, vararg args: Any?) {
        try {
            Log.println(priority.mapToLogcatScheme(), tag, message.format(args))
            findLastThrowable(args)?.printStackTrace()
        } catch (exception: Exception) {
            Log.println(Log.ERROR, tag, "Failed to append message: [$message] with args: [$args]")
        }
    }

    private fun Logger.Priority.mapToLogcatScheme(): Int = when (this) {
        Logger.Priority.Verbose -> Log.VERBOSE
        Logger.Priority.Debug -> Log.DEBUG
        Logger.Priority.Info -> Log.INFO
        Logger.Priority.Warning -> Log.WARN
        Logger.Priority.Error -> Log.ERROR
    }
}