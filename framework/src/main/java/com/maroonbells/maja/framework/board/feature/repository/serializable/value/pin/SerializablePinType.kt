package com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin

import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.framework.serialization.EnumSerializer
import com.maroonbells.maja.framework.serialization.SerializableEnum
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer

@Serializable(PinTypeSerializer::class)
enum class SerializablePinType(
    override val serialName: String,
    private val model: Pin.Type) : SerializableEnum {

    Digital("digital", Pin.Type.Digital),
    DigitalPwm("digitalPwm", Pin.Type.DigitalPwm);

    fun asModel(): Pin.Type = model
}

@Serializer(forClass = SerializablePinType::class)
object PinTypeSerializer : EnumSerializer<SerializablePinType>(
    "PinType", SerializablePinType.values())