package com.maroonbells.maja.framework.logger


fun findLastThrowable(array: Array<out Any?>): Throwable? {
    return array.findLast { it is Throwable } as Throwable?
}