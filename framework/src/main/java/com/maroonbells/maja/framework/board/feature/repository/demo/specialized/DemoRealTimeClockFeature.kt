package com.maroonbells.maja.framework.board.feature.repository.demo.specialized

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.RealTimeClockFeature
import kotlinx.coroutines.Job
import java.util.*

data class DemoRealTimeClockFeature(
    override val path: FeaturePath,
    override val datetime: Date,
    private val onSetDatetime: (date: Date) -> Job
) : RealTimeClockFeature {

    override fun setDateTime(date: Date): Job = onSetDatetime(date)
}