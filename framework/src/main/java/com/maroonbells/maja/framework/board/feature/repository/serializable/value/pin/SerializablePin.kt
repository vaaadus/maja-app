package com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin

import com.maroonbells.maja.domain.board.value.Pin
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializablePin(
    @SerialName("id") val id: SerializablePinId,
    @SerialName("name") val name: String,
    @SerialName("type") val type: SerializablePinType,
    @SerialName("value") val value: SerializablePinValue) {

    fun asModel(): Pin {
        return Pin(
            id = id.asModel(),
            name = name,
            value = value.asModel(),
            type = type.asModel())
    }
}



