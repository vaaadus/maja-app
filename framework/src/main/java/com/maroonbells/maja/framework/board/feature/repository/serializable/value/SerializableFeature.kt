package com.maroonbells.maja.framework.board.feature.repository.serializable.value

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableFeature(@SerialName("path") val path: SerializablePath)