package com.maroonbells.maja.framework.board.feature.repository.demo.specialized

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.SchedulesFeature
import com.maroonbells.maja.domain.board.model.ScheduleId
import kotlinx.coroutines.Job

data class DemoSchedulesFeature(
    override val path: FeaturePath,
    override val list: List<ScheduleId>,
    override val maxSchedules: Int,
    override val maxBlocksPerDay: Int,
    private val onCreateSchedule: () -> Job,
    private val onRemoveSchedule: () -> Job
) : SchedulesFeature {

    override fun createSchedule(): Job = onCreateSchedule()

    override fun removeSchedule(id: ScheduleId): Job = onRemoveSchedule()
}