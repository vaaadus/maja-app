package com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

private val TIME_ZONE = TimeZone.getDefault()

@Suppress("FunctionName")
fun SerializableDateTime(date: Date): SerializableDateTime {
    val calendar = Calendar.getInstance(TIME_ZONE)
    calendar.time = date

    return SerializableDateTime(
        year = calendar[Calendar.YEAR],
        month = calendar[Calendar.MONTH] + 1, // 1..12 board months
        day = calendar[Calendar.DAY_OF_MONTH],
        dayOfWeek = calendar[Calendar.DAY_OF_WEEK],
        hour = calendar[Calendar.HOUR_OF_DAY],
        minute = calendar[Calendar.MINUTE],
        second = calendar[Calendar.SECOND])
}

@Serializable
data class SerializableDateTime(
    @SerialName("year") val year: Int,
    @SerialName("month") val month: Int,
    @SerialName("day") val day: Int,
    @SerialName("dayOfWeek") val dayOfWeek: Int,
    @SerialName("hour") val hour: Int,
    @SerialName("minute") val minute: Int,
    @SerialName("second") val second: Int) {

    fun asDate(): Date {
        val calendar = Calendar.getInstance(TIME_ZONE).apply {
            this[Calendar.YEAR] = year
            this[Calendar.MONTH] = month - 1 // 0..11 java.util.Date months
            this[Calendar.DAY_OF_MONTH] = day
            this[Calendar.HOUR_OF_DAY] = hour
            this[Calendar.MINUTE] = minute
            this[Calendar.SECOND] = second
            this[Calendar.MILLISECOND] = 0
        }

        return calendar.time
    }
}