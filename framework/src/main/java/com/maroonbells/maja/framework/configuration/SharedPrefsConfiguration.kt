package com.maroonbells.maja.framework.configuration

import android.content.SharedPreferences
import androidx.core.content.edit
import com.maroonbells.maja.domain.configuration.repository.Configuration

class SharedPrefsConfiguration(private val prefs: SharedPreferences) :
    Configuration {

    override fun putString(key: String, value: String) = prefs.edit { putString(key, value) }
    override fun getString(key: String, default: String): String = prefs.getString(key, default) ?: default
    override fun getString(key: String): String? = prefs.getString(key, null)

    override fun putLong(key: String, value: Long) = prefs.edit { putLong(key, value) }
    override fun getLong(key: String, default: Long): Long = prefs.getLong(key, default)
    override fun getLong(key: String): Long? {
        return if (prefs.contains(key)) prefs.getLong(key, 0)
        else null
    }

    override fun putInt(key: String, value: Int) = prefs.edit { putInt(key, value) }
    override fun getInt(key: String, default: Int): Int = prefs.getInt(key, default)
    override fun getInt(key: String): Int? {
        return if (prefs.contains(key)) prefs.getInt(key, 0)
        else null
    }

    override fun putBoolean(key: String, value: Boolean) = prefs.edit { putBoolean(key, value) }
    override fun getBoolean(key: String, default: Boolean): Boolean = prefs.getBoolean(key, default)
    override fun getBoolean(key: String): Boolean? {
        return if (prefs.contains(key)) prefs.getBoolean(key, false)
        else null
    }

    override fun putFloat(key: String, value: Float) = prefs.edit { putFloat(key, value) }
    override fun getFloat(key: String, default: Float): Float = prefs.getFloat(key, default)
    override fun getFloat(key: String): Float? {
        return if (prefs.contains(key)) prefs.getFloat(key, 0f)
        else null
    }

    override fun containsKey(key: String): Boolean = prefs.contains(key)
    override fun clear(key: String) = prefs.edit { remove(key) }
    override fun clearAll() = prefs.edit { clear() }
}