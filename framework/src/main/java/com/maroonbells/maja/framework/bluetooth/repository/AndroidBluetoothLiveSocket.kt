package com.maroonbells.maja.framework.bluetooth.repository

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import com.maroonbells.maja.common.logger.Logger
import com.maroonbells.maja.domain.bluetooth.repository.BluetoothLiveSocket
import com.maroonbells.maja.domain.bluetooth.repository.BluetoothLiveSocket.BluetoothSocketListener
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Semaphore
import java.io.IOException
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

// TODO unit tests
class AndroidBluetoothLiveSocket(private val bluetoothDevice: BluetoothDevice) : BluetoothLiveSocket {

    companion object {
        private val LOG = Logger.forClass(AndroidBluetoothLiveSocket::class.java)
        private val SERVICE_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        private val CHARSET = Charsets.US_ASCII
        private const val MESSAGE_TERMINATOR = '\n'.toByte()
        private const val CONNECT_TIMEOUT = 10000L
        private const val MESSAGE_ACK = "ACK"
        private const val MESSAGE_LOG = "<--"
    }

    private val listeners = CopyOnWriteArrayList<BluetoothSocketListener>()

    private val socketScope = CoroutineScope(Dispatchers.IO + SupervisorJob())
    private val writeLock = WriteLock()

    private var socket: BluetoothSocket? = null

    override var isOpen: Boolean = false
        private set

    override var isConnected: Boolean = false
        private set

    override var isClosed: Boolean = false
        private set

    override val address: BluetoothAddress = bluetoothDevice.asAddress()

    override fun addListener(listener: BluetoothSocketListener) {
        if (listeners.addIfAbsent(listener)) {
            if (isConnected) listener.onSocketOpen(this)
            if (isClosed) listener.onSocketClose(this, null)
        }
    }

    override fun removeListener(listener: BluetoothSocketListener?) {
        listeners.remove(listener)
    }

    override fun open() {
        if (!isOpen) {
            isOpen = true
            isClosed = false
            openSocketAndStartListening()
        }
    }

    override fun send(message: String): Job {
        return sendMessage(message)
    }

    override fun send(bytes: ByteArray): Job {
        return sendBytes(bytes)
    }

    override fun close() {
        closeWithCause(null)
    }

    private fun openSocketAndStartListening() = socketScope.launch {
        var socket: BluetoothSocket? = null
        try {
            socket = openSocketConnection()

            listenForMessagesBlocking(socket)

            closeSocketConnection(socket)
            onSocketClose()

        } catch (exception: IOException) {
            closeSocketConnection(socket)
            onSocketError(exception)
        }
    }

    private suspend fun openSocketConnection(): BluetoothSocket = withTimeout(CONNECT_TIMEOUT) {
        val socket = obtainSocket()
        socket.connect()
        onSocketConnected()
        return@withTimeout socket
    }

    private suspend fun obtainSocket(): BluetoothSocket = withContext(Dispatchers.Main) {
        val obtainedSocket = socket ?: bluetoothDevice.createRfcommSocketToServiceRecord(SERVICE_UUID)
        socket = obtainedSocket
        return@withContext obtainedSocket
    }

    private suspend fun CoroutineScope.listenForMessagesBlocking(socket: BluetoothSocket) {
        val inputStream = socket.inputStream
        val buffer = ByteArray(10240)
        var totalBytesRead = 0
        var bytesRead: Int
        var bufferSizeLeft: Int

        while (isActive) {

            if (totalBytesRead >= buffer.size) {
                LOG.w("Message terminator not received, cleanup")
                // message terminator not received, clean up
                totalBytesRead = 0
            }

            bufferSizeLeft = buffer.size - totalBytesRead
            bytesRead = inputStream.read(buffer, totalBytesRead, bufferSizeLeft)
            totalBytesRead += bytesRead

            if (bytesRead > 0) {
                var lastTerminatorIndex = -1
                var index = 0
                while (index < totalBytesRead) {
                    // search for any messages in the buffer
                    if (buffer[index] == MESSAGE_TERMINATOR) {
                        val offset = lastTerminatorIndex + 1
                        val messageLength = index - lastTerminatorIndex - 1

                        if (messageLength > 0) {
                            val message = String(buffer, offset, messageLength, CHARSET)
                            onSocketMessage(message)
                        }

                        lastTerminatorIndex = index
                    }
                    index++
                }

                // copy the remaining part of the next message to the beginning of the buffer
                if (lastTerminatorIndex >= 0) {
                    totalBytesRead -= (lastTerminatorIndex + 1)
                    var newIndex = 0
                    repeat(times = totalBytesRead) {
                        buffer[newIndex] = buffer[lastTerminatorIndex + 1]
                        newIndex++
                        lastTerminatorIndex++
                    }
                }
            }
        }
    }

    private suspend fun closeSocketConnection(socket: BluetoothSocket?) = withContext(Dispatchers.Main) {
        try {
            socket?.close()
        } catch (exception: Exception) {
            closeWithCause(exception)
        }
    }

    private suspend fun onSocketConnected() = withContext(Dispatchers.Main) {
        isConnected = true
        listeners.forEach { it.onSocketOpen(this@AndroidBluetoothLiveSocket) }
    }

    private suspend fun onSocketMessage(message: String) = withContext(Dispatchers.Main) {
        when {
            message == MESSAGE_ACK -> writeLock.unlock()
            message.startsWith(MESSAGE_LOG) -> return@withContext
            else -> listeners.forEach { it.onSocketMessage(this@AndroidBluetoothLiveSocket, message) }
        }
    }

    private suspend fun onSocketError(error: IOException) = withContext(Dispatchers.Main) {
        closeWithCause(error)
    }

    private suspend fun onSocketClose() = withContext(Dispatchers.Main) {
        closeWithCause(null)
    }

    private fun sendMessage(message: String) = sendBytes(message.toByteArray(CHARSET))

    private fun sendBytes(bytes: ByteArray) = socketScope.launch {
        val socket = socket
        if (socket != null && socket.isConnected) {
            try {
                writeLock.lock()
                socket.outputStream.write(bytes)
                socket.outputStream.write(MESSAGE_TERMINATOR.toInt())
                socket.outputStream.flush()
            } catch (exception: IOException) {
                onSocketError(exception)
            }
        } else {
            LOG.w("Bluetooth socket is not connected")
        }
    }

    private fun closeWithCause(cause: Throwable?) {
        if (isClosed) {
            return
        }

        if (cause != null) {
            LOG.e("closeWithCause: %s", cause.message, cause)
        } else {
            LOG.d("closeWithCause: no cause")
        }

        isOpen = false
        isConnected = false
        isClosed = true

        socketScope.coroutineContext.cancelChildren()
        writeLock.reset()

        socket?.safeClose()
        socket = null

        listeners.forEach { it.onSocketClose(this, cause) }
    }

    private fun BluetoothSocket.safeClose() {
        try {
            close()
        } catch (exception: IOException) {
            LOG.e("BluetoothSocket couldn't be closed", exception)
        }
    }
}

private class WriteLock {

    private val semaphore = Semaphore(permits = 1)

    suspend fun lock() {
        semaphore.acquire()
    }

    fun unlock() {
        if (semaphore.availablePermits == 0) {
            semaphore.release()
        }
    }

    fun reset() {
        unlock()
    }
}