package com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin

import com.maroonbells.maja.domain.board.value.PinId
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Suppress("FunctionName")
fun SerializablePinId(id: PinId): SerializablePinId = SerializablePinId(id.raw)

@Serializable(PinIdSerializer::class)
data class SerializablePinId(val raw: Int) {
    fun asModel(): PinId = PinId(raw)
}

@Serializer(forClass = SerializablePinId::class)
object PinIdSerializer : KSerializer<SerializablePinId> {

    override val descriptor: SerialDescriptor = StringDescriptor.withName("PinId")

    override fun serialize(encoder: Encoder, obj: SerializablePinId) {
        encoder.encodeInt(obj.raw)
    }

    override fun deserialize(decoder: Decoder): SerializablePinId {
        return SerializablePinId(raw = decoder.decodeInt())
    }
}