package com.maroonbells.maja.framework.logger

import com.crashlytics.android.Crashlytics
import com.maroonbells.maja.common.logger.Logger

class CrashlyticsAppender : Logger.Appender {

    override fun append(priority: Logger.Priority, tag: String, message: String, vararg args: Any?) {
        try {
            val formattedMessage = message.format(args)
            Crashlytics.log("%s/%s: %s".format(priority, tag, formattedMessage))

            findLastThrowable(args)?.let {
                Crashlytics.logException(it)
            }
        } catch (error: Throwable) {
            Crashlytics.log("Throwable when trying to log: %s with args %s".format(message, args.toString()))
            Crashlytics.logException(error)
        }
    }
}