package com.maroonbells.maja.framework.board.feature.repository.bluetooth

import com.maroonbells.maja.common.consumable.Consumable
import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.feature.exception.FeatureException
import com.maroonbells.maja.domain.board.feature.name.FeatureClassPath
import com.maroonbells.maja.domain.board.feature.registry.Feature
import com.maroonbells.maja.domain.board.feature.repository.FeatureChannel
import com.maroonbells.maja.domain.board.feature.repository.FeatureChannel.ChannelListener
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.FeatureDeserializer
import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableGetFeatureCommand
import com.maroonbells.maja.framework.serialization.JsonParserFactory
import kotlinx.coroutines.*
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

// TODO unit tests
class BluetoothFeatureChannel(private val bluetooth: BluetoothManager) : FeatureChannel<BluetoothAddress>,
    BluetoothManager.ConnectionListener, BluetoothManager.LiveUpdateListener, FeatureCommandChannel {

    companion object {
        private const val TIMEOUT_PULL_FEATURE = 10000L
    }

    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    private val listeners = CopyOnWriteArrayList<ChannelListener>()
    private val deserializer = FeatureDeserializer(this)
    private val resume = Consumable()
    private val pause = Consumable()
    private var isResumed = false

    private val awaitingFeatureRequests = mutableListOf<FeatureRequest>()

    override fun addChannelListener(listener: ChannelListener) {
        if (listeners.addIfAbsent(listener)) {
            if (isResumed) listener.onFeatureChannelResumed()
        }
    }

    override fun removeChannelListener(listener: ChannelListener?) {
        listeners.remove(listener)
    }

    override fun resume(address: BluetoothAddress) {
        if (resume.consume()) {
            bluetooth.connect(address)
            bluetooth.addConnectionListener(this)
            bluetooth.addLiveUpdateListener(this)
        } else {
            pauseWithCause(FeatureException("FeatureChannel was already resumed"))
        }
    }

    override fun pause() {
        pauseWithCause(null)
    }

    override fun onBluetoothConnecting(address: BluetoothAddress) = Unit

    override fun onBluetoothConnected(address: BluetoothAddress) {
        isResumed = true
        listeners.forEach { it.onFeatureChannelResumed() }
    }

    override fun onBluetoothDisconnected(address: BluetoothAddress?, cause: Throwable?) {
        pauseWithCause(cause)
    }

    override fun onBluetoothMessage(address: BluetoothAddress, message: String) {
        scope.launch {
            try {
                val feature = withContext(Dispatchers.Default) { deserializer.deserialize(message) }
                if (feature != null) {
                    notifyFeatureRequests(feature)
                }
            } catch (e: Exception) {
                pauseWithCause(e)
            }
        }
    }

    private fun pauseWithCause(cause: Throwable?) {
        if (resume.isConsumed && pause.consume()) {
            bluetooth.disconnect()
            bluetooth.removeConnectionListener(this)
            bluetooth.removeLiveUpdateListener(this)

            scope.coroutineContext.cancelChildren()
            awaitingFeatureRequests.clear()

            isResumed = false
            listeners.forEach { it.onFeatureChannelPaused(cause) }

            resume.reset()
            pause.reset()
        }
    }

    override suspend fun <T : Feature> pullFeature(featureClass: FeatureClassPath<T>): T {
        sendCommand(SerializableGetFeatureCommand(featureClass.path))
        return awaitFeature(featureClass)
    }

    override fun <T : SerializableCommand<T>> sendCommand(command: T): Job = scope.launch {
        val message = withContext(Dispatchers.Default) {
            JsonParserFactory
                .createJson()
                .stringify(command.serializer(), command)
        }

        bluetooth.sendMessage(message).join()
    }

    @Suppress("UNCHECKED_CAST")
    private suspend fun <T : Feature> awaitFeature(featureClass: FeatureClassPath<T>): T {
        val featureRef = AtomicReference<T?>()
        val latch = CountDownLatch(1)

        enqueueFeatureRequest(featureClass) { feature: Feature ->
            featureRef.set(feature as T?)
            latch.countDown()
        }

        withContext(Dispatchers.Default) {
            latch.await(TIMEOUT_PULL_FEATURE, TimeUnit.MILLISECONDS)
        }

        return featureRef.get() ?: throw FeatureException("Feature $featureClass wasn't available")
    }

    private fun <T : Feature> enqueueFeatureRequest(
        featureClass: FeatureClassPath<T>,
        callback: (Feature) -> Unit) {

        awaitingFeatureRequests.add(FeatureRequest(featureClass, callback))
    }

    private fun notifyFeatureRequests(feature: Feature) {
        val requests = awaitingFeatureRequests.filter { it.featureClass.path == feature.path }
        try {
            requests.forEach { it.callback(feature) }
        } finally {
            awaitingFeatureRequests.removeAll(requests)
        }
    }
}

data class FeatureRequest(
    val featureClass: FeatureClassPath<out Feature>,
    val callback: (Feature) -> Unit
)