package com.maroonbells.maja.framework.board.feature.repository.demo.specialized

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.ScenesFeature
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.Scene
import com.maroonbells.maja.domain.board.value.SceneId
import kotlinx.coroutines.Job

data class DemoScenesFeature(
    override val path: FeaturePath,
    override val scenes: List<Scene>,
    override val maxScenes: Int,
    private val onCreate: (actionId: ActionId) -> Job,
    private val onUpdate: (scene: Scene) -> Job,
    private val onRemove: (id: SceneId) -> Job
) : ScenesFeature {

    override fun create(actionId: ActionId): Job = onCreate(actionId)

    override fun update(scene: Scene): Job = onUpdate(scene)

    override fun remove(id: SceneId): Job = onRemove(id)
}