package com.maroonbells.maja.framework.board.feature.repository.serializable

import com.maroonbells.maja.common.logger.Logger
import com.maroonbells.maja.domain.board.feature.FeatureRegistry
import com.maroonbells.maja.domain.board.feature.registry.*
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.feature.*
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableFeature
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.action.SerializableActions
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin.SerializablePinout
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableRealTimeClock
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.scene.SerializableScenes
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableSchedule
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableSchedules
import com.maroonbells.maja.framework.serialization.JsonParserFactory
import kotlinx.serialization.json.Json
import kotlin.reflect.KClass

typealias FeatureParser = (Json, String) -> Feature

class FeatureDeserializer(private val commandChannel: FeatureCommandChannel) {

    companion object {
        private val LOG = Logger.forClass(FeatureDeserializer::class.java)
    }

    private val featureParsers: Map<KClass<out Feature>, FeatureParser> = mapOf(
        PinoutFeature::class to ::parsePinoutFeature,
        RealTimeClockFeature::class to ::parseRealTimeClockFeature,
        ActionsFeature::class to ::parseActionsFeature,
        SchedulesFeature::class to ::parseSchedulesFeature,
        ScheduleFeature::class to ::parseScheduleFeature,
        ScenesFeature::class to ::parseScenesFeature
    )

    fun deserialize(string: String): Feature? {
        val json = JsonParserFactory.createJson()
        val feature = parseFeature(json, string)
        val pattern = FeatureRegistry.toFeatureClass(feature.path.asModel()) ?: return null
        val parser = featureParsers[pattern.raw] ?: return null
        return parser.invoke(json, string)
    }

    private fun parseFeature(json: Json, string: String): SerializableFeature {
        val path = json.parse(SerializableFeature.serializer(), string)
        LOG.d("deserialize: ${path.path}")
        return path
    }

    private fun parsePinoutFeature(json: Json, string: String): PinoutFeature {
        val pinout = json.parse(SerializablePinout.serializer(), string)
        return MajaPinoutFeature(commandChannel, pinout)
    }

    private fun parseRealTimeClockFeature(json: Json, string: String): RealTimeClockFeature {
        val realTimeClock = json.parse(SerializableRealTimeClock.serializer(), string)
        return MajaRealTimeClockFeature(commandChannel, realTimeClock)
    }

    private fun parseActionsFeature(json: Json, string: String): ActionsFeature {
        val actions = json.parse(SerializableActions.serializer(), string)
        return MajaActionsFeature(commandChannel, actions)
    }

    private fun parseSchedulesFeature(json: Json, string: String): SchedulesFeature {
        val schedules = json.parse(SerializableSchedules.serializer(), string)
        return MajaSchedulesFeature(commandChannel, schedules)
    }

    private fun parseScheduleFeature(json: Json, string: String): ScheduleFeature {
        val schedule = json.parse(SerializableSchedule.serializer(), string)
        return MajaScheduleFeature(commandChannel, schedule)
    }

    private fun parseScenesFeature(json: Json, string: String): ScenesFeature {
        val scenes = json.parse(SerializableScenes.serializer(), string)
        return MajaScenesFeature(commandChannel, scenes)
    }
}