package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.ScheduleFeature
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.domain.board.value.datetime.Weekday
import com.maroonbells.maja.domain.board.value.schedule.ScheduleBlock
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableSchedule
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableScheduleBlock
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableSetEntriesCommand
import kotlinx.coroutines.Job
import java.util.regex.Pattern

private val pattern = Pattern.compile("board\\.schedules\\.([0-9]+)")

@Suppress("FunctionName")
fun MajaScheduleFeature(commandChannel: FeatureCommandChannel, schedule: SerializableSchedule): MajaScheduleFeature {
    val matcher = pattern.matcher(schedule.path.raw)
    require(matcher.find()) { "${schedule.path} is invalid schedule path" }

    return MajaScheduleFeature(
        commandChannel = commandChannel,
        path = schedule.path.asModel(),
        id = ScheduleId(matcher.group(1).toInt()),
        monday = schedule.mon.map(SerializableScheduleBlock::asModel),
        tuesday = schedule.tue.map(SerializableScheduleBlock::asModel),
        wednesday = schedule.wed.map(SerializableScheduleBlock::asModel),
        thursday = schedule.thu.map(SerializableScheduleBlock::asModel),
        friday = schedule.fri.map(SerializableScheduleBlock::asModel),
        saturday = schedule.sat.map(SerializableScheduleBlock::asModel),
        sunday = schedule.sun.map(SerializableScheduleBlock::asModel)
    )
}

data class MajaScheduleFeature(
    private val commandChannel: FeatureCommandChannel,
    override val path: FeaturePath,
    override val id: ScheduleId,
    override val monday: List<ScheduleBlock>,
    override val tuesday: List<ScheduleBlock>,
    override val wednesday: List<ScheduleBlock>,
    override val thursday: List<ScheduleBlock>,
    override val friday: List<ScheduleBlock>,
    override val saturday: List<ScheduleBlock>,
    override val sunday: List<ScheduleBlock>) : ScheduleFeature {

    override fun setEntries(weekday: Weekday, entries: List<ScheduleBlock>): Job {
        val command = SerializableSetEntriesCommand(
            path = SerializablePath(path.raw + "." + weekday.raw),
            blocksCount = entries.size,
            blocks = entries.map { SerializableScheduleBlock(it) })

        return commandChannel.sendCommand(command)
    }
}
