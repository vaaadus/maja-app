package com.maroonbells.maja.framework.board.feature.repository.serializable

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Get
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Suppress("FunctionName")
fun SerializableGetFeatureCommand(path: FeaturePath): SerializableGetFeatureCommand {
    return SerializableGetFeatureCommand(SerializablePath(path))
}

@Serializable
data class SerializableGetFeatureCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Get
) : SerializableCommand<SerializableGetFeatureCommand>() {

    override fun serializer(): KSerializer<SerializableGetFeatureCommand> {
        return SerializableGetFeatureCommand.serializer()
    }
}