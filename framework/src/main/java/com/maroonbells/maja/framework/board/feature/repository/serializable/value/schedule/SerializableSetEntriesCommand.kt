package com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Update
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableSetEntriesCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Update,
    @SerialName("blocksCount") val blocksCount: Int,
    @SerialName("blocks") val blocks: List<SerializableScheduleBlock>
) : SerializableCommand<SerializableSetEntriesCommand>() {

    override fun serializer(): KSerializer<SerializableSetEntriesCommand> {
        return SerializableSetEntriesCommand.serializer()
    }
}