package com.maroonbells.maja.framework.board.feature.repository.serializable.value.action

import com.maroonbells.maja.domain.board.value.ActionId
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Suppress("FunctionName")
fun SerializableActionId(id: ActionId): SerializableActionId = SerializableActionId(id.raw)

@Serializable(ActionIdSerializer::class)
data class SerializableActionId(val raw: Int) {
    fun asModel(): ActionId = ActionId(raw)
}

@Serializer(forClass = SerializableActionId::class)
object ActionIdSerializer : KSerializer<SerializableActionId> {

    override val descriptor: SerialDescriptor = StringDescriptor.withName("ActionId")

    override fun serialize(encoder: Encoder, obj: SerializableActionId) {
        encoder.encodeInt(obj.raw)
    }

    override fun deserialize(decoder: Decoder): SerializableActionId {
        return SerializableActionId(raw = decoder.decodeInt())
    }
}