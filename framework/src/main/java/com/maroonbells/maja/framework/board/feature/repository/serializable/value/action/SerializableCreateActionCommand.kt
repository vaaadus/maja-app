package com.maroonbells.maja.framework.board.feature.repository.serializable.value.action

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Create
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableCreateActionCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Create,
    @SerialName("stepsCount") val stepsCount: Int,
    @SerialName("steps") val steps: List<SerializableStep>
) : SerializableCommand<SerializableCreateActionCommand>() {

    override fun serializer(): KSerializer<SerializableCreateActionCommand> {
        return SerializableCreateActionCommand.serializer()
    }
}