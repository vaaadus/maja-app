package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.RealTimeClockFeature
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableDateTime
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableRealTimeClock
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableSetDateTimeCommand
import kotlinx.coroutines.Job
import java.util.*

@Suppress("FunctionName")
fun MajaRealTimeClockFeature(
    commandChannel: FeatureCommandChannel,
    realTimeClock: SerializableRealTimeClock): MajaRealTimeClockFeature {

    return MajaRealTimeClockFeature(
        commandChannel = commandChannel,
        path = realTimeClock.path.asModel(),
        datetime = realTimeClock.datetime.asDate())
}

data class MajaRealTimeClockFeature(
    private val commandChannel: FeatureCommandChannel,
    override val path: FeaturePath,
    override val datetime: Date) : RealTimeClockFeature {

    override fun setDateTime(date: Date): Job {
        val command = SerializableSetDateTimeCommand(
            path = SerializablePath(path),
            datetime = SerializableDateTime(date))

        return commandChannel.sendCommand(command)
    }
}