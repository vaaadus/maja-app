package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.PinoutFeature
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin.*
import kotlinx.coroutines.Job

@Suppress("FunctionName")
fun MajaPinoutFeature(commandChannel: FeatureCommandChannel, pinout: SerializablePinout): MajaPinoutFeature {
    return MajaPinoutFeature(
        commandChannel = commandChannel,
        path = pinout.path.asModel(),
        pins = pinout.pins.map(SerializablePin::asModel)
    )
}

data class MajaPinoutFeature(
    private val commandChannel: FeatureCommandChannel,
    override val path: FeaturePath,
    override val pins: List<Pin>) : PinoutFeature {

    override fun set(id: PinId, value: PinValue): Job {
        val command = SerializableSetPinStateCommand(
            path = SerializablePath(path),
            id = SerializablePinId(id),
            value = SerializablePinValue(value))

        return commandChannel.sendCommand(command)
    }
}