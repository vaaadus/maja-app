package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.SchedulesFeature
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableCreateScheduleCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableRemoveScheduleCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableScheduleId
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule.SerializableSchedules
import kotlinx.coroutines.Job

@Suppress("FunctionName")
fun MajaSchedulesFeature(
    commandChannel: FeatureCommandChannel,
    schedules: SerializableSchedules): MajaSchedulesFeature {
    return MajaSchedulesFeature(
        commandChannel = commandChannel,
        path = schedules.path.asModel(),
        maxSchedules = schedules.maxSchedules,
        maxBlocksPerDay = schedules.maxBlocksPerDay,
        list = schedules.list.map(SerializableScheduleId::asModel)
    )
}

data class MajaSchedulesFeature(
    private val commandChannel: FeatureCommandChannel,
    override val path: FeaturePath,
    override val maxSchedules: Int,
    override val maxBlocksPerDay: Int,
    override val list: List<ScheduleId>) : SchedulesFeature {

    override fun createSchedule(): Job {
        val command = SerializableCreateScheduleCommand(SerializablePath(path))
        return commandChannel.sendCommand(command)
    }

    override fun removeSchedule(id: ScheduleId): Job {
        val command = SerializableRemoveScheduleCommand(
            path = SerializablePath(path),
            id = SerializableScheduleId(id))
        return commandChannel.sendCommand(command)
    }
}