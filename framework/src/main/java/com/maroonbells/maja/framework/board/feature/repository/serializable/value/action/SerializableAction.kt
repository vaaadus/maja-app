package com.maroonbells.maja.framework.board.feature.repository.serializable.value.action

import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin.SerializablePinId
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin.SerializablePinValue
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableAction(
    @SerialName("id") val id: SerializableActionId,
    @SerialName("steps") val steps: List<SerializableStep>) {

    fun asModel(): Action {
        return Action(
            id = id.asModel(),
            steps = steps.map(SerializableStep::asModel)
        )
    }
}

@Serializable
data class SerializableStep(
    @SerialName("pin") val pinId: SerializablePinId,
    @SerialName("value") val value: SerializablePinValue) {

    fun asModel(): Action.Step = Action.Step(pinId.asModel(), value.asModel())
}