package com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin

import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializablePinout(
    @SerialName("path") val path: SerializablePath,
    @SerialName("pins") val pins: List<SerializablePin>)