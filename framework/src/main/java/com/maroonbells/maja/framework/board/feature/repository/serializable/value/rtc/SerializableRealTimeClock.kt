package com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc

import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableRealTimeClock(
    @SerialName("path") val path: SerializablePath,
    @SerialName("datetime") val datetime: SerializableDateTime)