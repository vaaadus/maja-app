package com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule

import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableSchedule(
    @SerialName("path") val path: SerializablePath,
    @SerialName("mon") val mon: List<SerializableScheduleBlock>,
    @SerialName("tue") val tue: List<SerializableScheduleBlock>,
    @SerialName("wed") val wed: List<SerializableScheduleBlock>,
    @SerialName("thu") val thu: List<SerializableScheduleBlock>,
    @SerialName("fri") val fri: List<SerializableScheduleBlock>,
    @SerialName("sat") val sat: List<SerializableScheduleBlock>,
    @SerialName("sun") val sun: List<SerializableScheduleBlock>)