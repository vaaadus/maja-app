package com.maroonbells.maja.framework.bluetooth.repository

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED
import android.bluetooth.BluetoothAdapter.ACTION_STATE_CHANGED
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.BluetoothManager.BluetoothStatus
import com.maroonbells.maja.domain.bluetooth.repository.Bluetooth
import com.maroonbells.maja.domain.bluetooth.repository.BluetoothLiveSocket
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice
import java.util.concurrent.CopyOnWriteArrayList

typealias AndroidBluetoothDevice = android.bluetooth.BluetoothDevice

// TODO unit tests
class AndroidBluetooth(
    private val context: Context,
    private val adapter: BluetoothAdapter) : Bluetooth, BroadcastReceiver() {

    private val listeners = CopyOnWriteArrayList<BluetoothManager.StatusListener>()

    override var status: BluetoothStatus = calculateStatus(adapter.state)
        private set

    override val pairedDevices: List<BluetoothDevice>
        get() = adapter.bondedDevices.map { it.asModel() }

    init {
        context.registerReceiver(this, IntentFilter(ACTION_STATE_CHANGED))
        context.registerReceiver(this, IntentFilter(ACTION_CONNECTION_STATE_CHANGED))
    }

    override fun addBluetoothListener(listener: BluetoothManager.StatusListener) {
        if (listeners.addIfAbsent(listener)) {
            listener.onBluetoothStatusChanged(status)
        }
    }

    override fun removeBluetoothListener(listener: BluetoothManager.StatusListener?) {
        listeners.remove(listener)
    }

    override fun enable() {
        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

    override fun disable() {
        adapter.disable()
    }

    override fun startDiscovery(listener: BluetoothManager.DiscoveryListener) {
        if (adapter.isDiscovering) {
            adapter.cancelDiscovery()
        }

        adapter.startDiscovery()

        val receiver = BluetoothDevicesDiscoveryReceiver(listener)
        context.registerReceiver(receiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED))
        context.registerReceiver(receiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
        context.registerReceiver(receiver, IntentFilter(AndroidBluetoothDevice.ACTION_FOUND))
    }

    override fun stopDiscovery() {
        if (adapter.isDiscovering) {
            adapter.cancelDiscovery()
        }
    }

    override fun createLiveSocket(address: BluetoothAddress): BluetoothLiveSocket {
        val bluetoothDevice = adapter.getRemoteDevice(address.raw)
        return AndroidBluetoothLiveSocket(bluetoothDevice)
    }

    override fun onReceive(context: Context, intent: Intent?) {
        when (intent?.action) {
            ACTION_STATE_CHANGED -> {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)
                dispatchStatusChanged(calculateStatus(state))
            }
            ACTION_CONNECTION_STATE_CHANGED -> {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_CONNECTION_STATE, BluetoothAdapter.STATE_OFF)
                dispatchStatusChanged(calculateStatus(state))
            }
        }
    }

    private fun dispatchStatusChanged(newStatus: BluetoothStatus) {
        if (status != newStatus) {
            status = newStatus
            listeners.forEach { it.onBluetoothStatusChanged(newStatus) }
        }
    }

    private fun calculateStatus(adapterState: Int): BluetoothStatus = when (adapterState) {
        BluetoothAdapter.STATE_ON,
        BluetoothAdapter.STATE_CONNECTING,
        BluetoothAdapter.STATE_CONNECTED,
        BluetoothAdapter.STATE_DISCONNECTING,
        BluetoothAdapter.STATE_DISCONNECTED -> BluetoothStatus.Enabled

        BluetoothAdapter.STATE_OFF,
        BluetoothAdapter.STATE_TURNING_ON,
        BluetoothAdapter.STATE_TURNING_OFF -> BluetoothStatus.Disabled

        else -> BluetoothStatus.Disabled
    }
}

internal class BluetoothDevicesDiscoveryReceiver(
    private val listener: BluetoothManager.DiscoveryListener
) : BroadcastReceiver() {

    private val devices = mutableListOf<BluetoothDevice>()

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                listener.onBluetoothDiscoveryStarted()
            }
            BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                context.unregisterReceiver(this)
                listener.onBluetoothDiscoveryFinished()
            }
            AndroidBluetoothDevice.ACTION_FOUND -> {
                val device: AndroidBluetoothDevice = intent.getParcelableExtra(AndroidBluetoothDevice.EXTRA_DEVICE)
                if (!device.isPaired()) {
                    onDeviceDiscovered(device)
                }
            }
        }
    }

    private fun onDeviceDiscovered(device: AndroidBluetoothDevice) {
        val model = device.asModel()
        val existing = devices.find { it.address == model.address }
        if (model != existing) {
            devices.remove(existing)
            devices.add(model)
            listener.onBluetoothDevicesDiscovered(devices)
        }
    }
}

internal fun AndroidBluetoothDevice.asModel(): BluetoothDevice {
    return BluetoothDevice(
        address = asAddress(),
        name = name ?: "",
        isPaired = isPaired()
    )
}

internal fun AndroidBluetoothDevice.asAddress(): BluetoothAddress {
    return BluetoothAddress(raw = address)
}

internal fun AndroidBluetoothDevice.isPaired(): Boolean {
    return bondState == AndroidBluetoothDevice.BOND_BONDED
}