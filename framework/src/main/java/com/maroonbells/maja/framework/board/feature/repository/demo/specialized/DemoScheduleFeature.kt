package com.maroonbells.maja.framework.board.feature.repository.demo.specialized

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.ScheduleFeature
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.domain.board.value.datetime.Weekday
import com.maroonbells.maja.domain.board.value.schedule.ScheduleBlock
import kotlinx.coroutines.Job

data class DemoScheduleFeature(
    override val path: FeaturePath,
    override val id: ScheduleId,
    override val monday: List<ScheduleBlock>,
    override val tuesday: List<ScheduleBlock>,
    override val wednesday: List<ScheduleBlock>,
    override val thursday: List<ScheduleBlock>,
    override val friday: List<ScheduleBlock>,
    override val saturday: List<ScheduleBlock>,
    override val sunday: List<ScheduleBlock>,
    private val onSetEntries: (id: ScheduleId, weekday: Weekday, entries: List<ScheduleBlock>) -> Job
) : ScheduleFeature {

    override fun setEntries(weekday: Weekday, entries: List<ScheduleBlock>): Job {
        return onSetEntries(id, weekday, entries)
    }
}