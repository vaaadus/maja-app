package com.maroonbells.maja.framework.board.feature.repository.serializable.value.pin

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Update
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableSetPinStateCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Update,
    @SerialName("id") val id: SerializablePinId,
    @SerialName("value") val value: SerializablePinValue
) : SerializableCommand<SerializableSetPinStateCommand>() {

    override fun serializer(): KSerializer<SerializableSetPinStateCommand> {
        return SerializableSetPinStateCommand.serializer()
    }
}