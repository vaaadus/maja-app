package com.maroonbells.maja.framework.logger

import com.maroonbells.maja.common.logger.TaggedLogger
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE

class KoinLogger : Logger() {

    companion object {
        private val LOG = TaggedLogger("KoinLogger")
    }

    override fun log(level: Level, msg: MESSAGE) {
        when (level) {
            Level.DEBUG -> LOG.d(msg)
            Level.INFO -> LOG.i(msg)
            Level.ERROR -> LOG.e(msg)
        }
    }
}