package com.maroonbells.maja.framework.board.feature.repository.serializable.value.action

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Update
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableUpdateActionCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Update,
    @SerialName("id") val id: SerializableActionId,
    @SerialName("stepsCount") val stepsCount: Int,
    @SerialName("steps") val steps: List<SerializableStep>
) : SerializableCommand<SerializableUpdateActionCommand>() {

    override fun serializer(): KSerializer<SerializableUpdateActionCommand> {
        return SerializableUpdateActionCommand.serializer()
    }
}