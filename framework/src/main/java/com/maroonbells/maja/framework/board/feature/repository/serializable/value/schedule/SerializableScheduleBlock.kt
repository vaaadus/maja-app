package com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule

import com.maroonbells.maja.domain.board.value.schedule.ScheduleBlock
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.action.SerializableActionId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Suppress("FunctionName")
fun SerializableScheduleBlock(block: ScheduleBlock): SerializableScheduleBlock {
    return SerializableScheduleBlock(
        start = SerializableTime(block.start),
        end = SerializableTime(block.end),
        actionId = SerializableActionId(block.actionId))
}

@Serializable
data class SerializableScheduleBlock(
    @SerialName("start") val start: SerializableTime,
    @SerialName("end") val end: SerializableTime,
    @SerialName("actionId") val actionId: SerializableActionId) {

    fun asModel(): ScheduleBlock {
        return ScheduleBlock(start.asModel(), end.asModel(), actionId.asModel())
    }
}