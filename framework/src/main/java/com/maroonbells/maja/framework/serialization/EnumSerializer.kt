package com.maroonbells.maja.framework.serialization

import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

interface SerializableEnum {
    val serialName: String
}

open class EnumSerializer<T>(name: String, private val values: Array<T>) : KSerializer<T>
        where T : SerializableEnum, T : Enum<T> {

    override val descriptor: SerialDescriptor = StringDescriptor.withName(name)

    override fun deserialize(decoder: Decoder): T {
        val string = decoder.decodeString()
        return values.find { it.serialName == string } ?: throw SerializationException("Unsupported enum: $string")
    }

    override fun serialize(encoder: Encoder, obj: T) {
        encoder.encodeString(obj.serialName)
    }
}