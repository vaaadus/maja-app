package com.maroonbells.maja.framework.board.feature.repository.serializable.value.action

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Remove
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableRemoveActionCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Remove,
    @SerialName("id") val id: SerializableActionId
) : SerializableCommand<SerializableRemoveActionCommand>() {

    override fun serializer(): KSerializer<SerializableRemoveActionCommand> {
        return SerializableRemoveActionCommand.serializer()
    }
}