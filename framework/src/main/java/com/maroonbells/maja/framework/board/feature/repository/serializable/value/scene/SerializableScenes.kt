package com.maroonbells.maja.framework.board.feature.repository.serializable.value.scene

import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableScenes(
    @SerialName("path") val path: SerializablePath,
    @SerialName("maxScenes") val maxScenes: Int,
    @SerialName("scenes") val list: List<SerializableScene>)