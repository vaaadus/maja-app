package com.maroonbells.maja.framework.board.feature.repository.serializable.value

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Suppress("FunctionName")
fun SerializablePath(path: FeaturePath) = SerializablePath(path.raw)

@Serializable(PathSerializer::class)
data class SerializablePath(val raw: String) {
    fun asModel(): FeaturePath = FeaturePath(raw)
}

@Serializer(forClass = SerializablePath::class)
object PathSerializer : KSerializer<SerializablePath> {

    override val descriptor: SerialDescriptor = StringDescriptor.withName("Path")

    override fun serialize(encoder: Encoder, obj: SerializablePath) {
        encoder.encodeString(obj.raw)
    }

    override fun deserialize(decoder: Decoder): SerializablePath {
        return SerializablePath(raw = decoder.decodeString())
    }
}