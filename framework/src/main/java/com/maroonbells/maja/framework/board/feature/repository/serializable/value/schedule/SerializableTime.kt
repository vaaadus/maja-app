package com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule

import com.maroonbells.maja.domain.board.value.datetime.Time
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Suppress("FunctionName")
fun SerializableTime(time: Time): SerializableTime = SerializableTime(time.toString())

@Serializable(TimeSerializer::class)
data class SerializableTime(val raw: String) {
    fun asModel(): Time = Time.parse(raw)
}

@Serializer(forClass = SerializableTime::class)
object TimeSerializer : KSerializer<SerializableTime> {
    override val descriptor: SerialDescriptor = StringDescriptor.withName("Time")

    override fun deserialize(decoder: Decoder): SerializableTime {
        return SerializableTime(raw = decoder.decodeString())
    }

    override fun serialize(encoder: Encoder, obj: SerializableTime) {
        encoder.encodeString(obj.raw)
    }
}