package com.maroonbells.maja.framework.serialization

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

object JsonParserFactory {

    fun createJson(): Json {
        return Json(configuration = JsonConfiguration(strictMode = false))
    }
}