package com.maroonbells.maja.framework.board.feature.repository.serializable.value.scene

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Create
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.action.SerializableActionId
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableDateTime
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableCreateSceneCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Create,
    @SerialName("actionId") val actionId: SerializableActionId,
    @SerialName("activeTill") val activeTill: SerializableDateTime
) : SerializableCommand<SerializableCreateSceneCommand>() {

    override fun serializer(): KSerializer<SerializableCreateSceneCommand> {
        return SerializableCreateSceneCommand.serializer()
    }
}