package com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc

import com.maroonbells.maja.framework.board.feature.repository.serializable.SerializableCommand
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializableCommandType.Update
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableSetDateTimeCommand(
    @SerialName("path") override val path: SerializablePath,
    @SerialName("type") override val type: SerializableCommandType = Update,
    @SerialName("datetime") val datetime: SerializableDateTime
) : SerializableCommand<SerializableSetDateTimeCommand>() {

    override fun serializer(): KSerializer<SerializableSetDateTimeCommand> {
        return SerializableSetDateTimeCommand.serializer()
    }
}