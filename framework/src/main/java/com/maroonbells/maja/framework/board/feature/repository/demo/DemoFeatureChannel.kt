package com.maroonbells.maja.framework.board.feature.repository.demo

import com.maroonbells.maja.domain.board.feature.name.FeatureClassPath
import com.maroonbells.maja.domain.board.feature.registry.*
import com.maroonbells.maja.domain.board.feature.repository.FeatureChannel
import com.maroonbells.maja.domain.board.feature.repository.FeatureChannel.ChannelListener
import com.maroonbells.maja.domain.board.value.BoardAddress
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import java.util.concurrent.CopyOnWriteArrayList

class DemoFeatureChannel<Address : BoardAddress> : FeatureChannel<Address> {

    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())
    private val demoFeatures = DemoFeatures(scope)

    private val listeners = CopyOnWriteArrayList<ChannelListener>()
    private var isResumed = false

    override fun addChannelListener(listener: ChannelListener) {
        if (listeners.addIfAbsent(listener)) {
            if (isResumed) listener.onFeatureChannelResumed()
            else listener.onFeatureChannelPaused(null)
        }
    }

    override fun removeChannelListener(listener: ChannelListener?) {
        listeners.remove(listener)
    }

    override fun resume(address: Address) {
        isResumed = true

        for (listener in listeners) {
            listener.onFeatureChannelResumed()
        }
    }

    override fun pause() {
        isResumed = false

        for (listener in listeners) {
            listener.onFeatureChannelPaused(null)
        }
    }

    @Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")
    override suspend fun <T : Feature> pullFeature(featureClass: FeatureClassPath<T>): T {
        return when (featureClass.raw) {
            PinoutFeature::class -> demoFeatures.pinoutFeature(featureClass.path)
            ActionsFeature::class -> demoFeatures.actionsFeature(featureClass.path)
            ScenesFeature::class -> demoFeatures.scenesFeature(featureClass.path)
            SchedulesFeature::class -> demoFeatures.schedulesFeature(featureClass.path)
            ScheduleFeature::class -> demoFeatures.scheduleFeature(featureClass.path)
            RealTimeClockFeature::class -> demoFeatures.realTimeClockFeature(featureClass.path)

            else -> throw IllegalArgumentException("Unsupported case: ${featureClass.raw}")
        } as T
    }
}