package com.maroonbells.maja.framework.board.feature.repository.serializable.value.scene

import com.maroonbells.maja.domain.board.value.Scene
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.action.SerializableActionId
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableDateTime
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableScene(
    @SerialName("id") val id: SerializableSceneId,
    @SerialName("actionId") val actionId: SerializableActionId,
    @SerialName("activeTill") val activeTill: SerializableDateTime) {

    fun asModel(): Scene {
        return Scene(
            id = id.asModel(),
            actionId = actionId.asModel(),
            activeTill = activeTill.asDate())
    }
}