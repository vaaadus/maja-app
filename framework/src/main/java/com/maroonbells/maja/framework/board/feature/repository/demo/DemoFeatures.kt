package com.maroonbells.maja.framework.board.feature.repository.demo

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.*
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.board.value.datetime.Weekday
import com.maroonbells.maja.domain.board.value.schedule.ScheduleBlock
import com.maroonbells.maja.domain.demo.DemoResources
import com.maroonbells.maja.framework.board.feature.repository.demo.specialized.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*


class DemoFeatures(private val scope: CoroutineScope) {

    private val demo = DemoResources()
    private var pinList: List<Pin> = demo.pinout.map(CustomizedPin::asPin)
    private var actionList: List<Action> = demo.actions.map(CustomizedAction::asAction)

    fun pinoutFeature(path: FeaturePath): PinoutFeature {
        return DemoPinoutFeature(
            path = path,
            pins = pinList,
            setPinState = this::setPinState
        )
    }

    fun actionsFeature(path: FeaturePath): ActionsFeature {
        return DemoActionsFeature(
            path = path,
            actions = actionList,
            maxActions = 3,
            maxStepsPerAction = 4,
            onCreate = this::createAction,
            onUpdate = this::updateAction,
            onRemove = this::removeAction
        )
    }

    fun scenesFeature(path: FeaturePath): ScenesFeature {
        return DemoScenesFeature(
            path = path,
            scenes = emptyList(),
            maxScenes = 4,
            onCreate = this::createScene,
            onUpdate = this::updateScene,
            onRemove = this::removeScene
        )
    }

    fun schedulesFeature(path: FeaturePath): SchedulesFeature {
        return DemoSchedulesFeature(
            path = path,
            list = emptyList(),
            maxSchedules = 4,
            maxBlocksPerDay = 4,
            onCreateSchedule = this::createSchedule,
            onRemoveSchedule = this::removeSchedule
        )
    }

    fun scheduleFeature(path: FeaturePath): ScheduleFeature {
        return DemoScheduleFeature(
            path = path,
            id = ScheduleId(1), // TODO
            monday = emptyList(),
            tuesday = emptyList(),
            wednesday = emptyList(),
            thursday = emptyList(),
            friday = emptyList(),
            saturday = emptyList(),
            sunday = emptyList(),
            onSetEntries = this::setEntries
        )
    }

    fun realTimeClockFeature(path: FeaturePath): RealTimeClockFeature {
        return DemoRealTimeClockFeature(
            path = path,
            datetime = Date(),
            onSetDatetime = this::setDatetime
        )
    }

    private fun setPinState(id: PinId, value: PinValue): Job = scope.launch {
        val newPins = pinList.toMutableList()
        val index = newPins.indexOfFirst { it.id == id }
        newPins[index] = newPins[index].copy(value = value)
        pinList = newPins
    }

    private fun createAction(steps: List<Action.Step>): Job = scope.launch {
        val action = Action(findFreeActionId(actionList), steps)
        val newActions = actionList + action
        actionList = newActions
    }

    private fun updateAction(id: ActionId, steps: List<Action.Step>): Job = scope.launch {
        val newActions = actionList.toMutableList()
        val index = newActions.indexOfFirst { it.id == id }
        newActions[index] = newActions[index].copy(steps = steps)
        actionList = newActions
    }

    private fun removeAction(id: ActionId): Job = scope.launch {
        val newActions = actionList.toMutableList()
        newActions.removeAll { it.id == id }
        actionList = newActions
    }

    private fun findFreeActionId(actions: List<Action>): ActionId {
        val random = Random()
        var num: Int

        do {
            num = random.nextInt()
        } while (actions.any { it.id.raw == num })

        return ActionId(num)
    }

    private fun createScene(id: ActionId): Job = scope.launch {
        // TODO
    }

    private fun updateScene(scene: Scene): Job = scope.launch {
        // TODO
    }

    private fun removeScene(id: SceneId): Job = scope.launch {
        // TODO
    }

    private fun createSchedule(): Job = scope.launch {
        // TODO
    }

    private fun removeSchedule(): Job = scope.launch {
        // TODO
    }

    private fun setEntries(id: ScheduleId, weekday: Weekday, entries: List<ScheduleBlock>): Job = scope.launch {
        // TODO
    }

    private fun setDatetime(date: Date): Job = scope.launch {
        // empty
    }
}