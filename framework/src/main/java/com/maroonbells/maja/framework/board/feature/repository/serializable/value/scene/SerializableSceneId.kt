package com.maroonbells.maja.framework.board.feature.repository.serializable.value.scene

import com.maroonbells.maja.domain.board.value.SceneId
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Suppress("FunctionName")
fun SerializableSceneId(id: SceneId): SerializableSceneId = SerializableSceneId(id.raw)

@Serializable(SceneIdSerializer::class)
data class SerializableSceneId(val raw: Int) {
    fun asModel(): SceneId = SceneId(raw)
}

@Serializer(forClass = SerializableSceneId::class)
object SceneIdSerializer : KSerializer<SerializableSceneId> {

    override val descriptor: SerialDescriptor = StringDescriptor.withName("SceneId")

    override fun serialize(encoder: Encoder, obj: SerializableSceneId) {
        encoder.encodeInt(obj.raw)
    }

    override fun deserialize(decoder: Decoder): SerializableSceneId {
        return SerializableSceneId(raw = decoder.decodeInt())
    }
}