package com.maroonbells.maja.framework.board.feature.repository.serializable.value.action

import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableActions(
    @SerialName("path") val path: SerializablePath,
    @SerialName("maxActions") val maxActions: Int,
    @SerialName("maxStepsPerAction") val maxStepsPerAction: Int,
    @SerialName("actions") val list: List<SerializableAction>)
