package com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule

import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SerializableSchedules(
    @SerialName("path") val path: SerializablePath,
    @SerialName("maxSchedules") val maxSchedules: Int,
    @SerialName("maxBlocksPerDay") val maxBlocksPerDay: Int,
    @SerialName("schedules") val list: List<SerializableScheduleId>)
