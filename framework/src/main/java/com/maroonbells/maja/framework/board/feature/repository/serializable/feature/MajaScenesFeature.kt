package com.maroonbells.maja.framework.board.feature.repository.serializable.feature

import com.maroonbells.maja.domain.board.feature.name.FeaturePath
import com.maroonbells.maja.domain.board.feature.registry.ScenesFeature
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.Scene
import com.maroonbells.maja.domain.board.value.SceneId
import com.maroonbells.maja.framework.board.feature.repository.FeatureCommandChannel
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.SerializablePath
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.action.SerializableActionId
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.rtc.SerializableDateTime
import com.maroonbells.maja.framework.board.feature.repository.serializable.value.scene.*
import kotlinx.coroutines.Job
import java.util.*

@Suppress("FunctionName")
fun MajaScenesFeature(commandChannel: FeatureCommandChannel, scenes: SerializableScenes): MajaScenesFeature {
    return MajaScenesFeature(
        commandChannel = commandChannel,
        path = scenes.path.asModel(),
        maxScenes = scenes.maxScenes,
        scenes = scenes.list.map(SerializableScene::asModel))
}

data class MajaScenesFeature(
    private val commandChannel: FeatureCommandChannel,
    override val path: FeaturePath,
    override val maxScenes: Int,
    override val scenes: List<Scene>) : ScenesFeature {

    override fun create(actionId: ActionId): Job {
        val command = SerializableCreateSceneCommand(
            path = SerializablePath(path),
            actionId = SerializableActionId(actionId),
            activeTill = SerializableDateTime(Date()))

        return commandChannel.sendCommand(command)
    }

    override fun update(scene: Scene): Job {
        val command = SerializableUpdateSceneCommand(
            path = SerializablePath(path),
            id = SerializableSceneId(scene.id),
            actionId = SerializableActionId(scene.actionId),
            activeTill = SerializableDateTime(scene.activeTill))

        return commandChannel.sendCommand(command)
    }

    override fun remove(id: SceneId): Job {
        val command = SerializableRemoveSceneCommand(
            path = SerializablePath(path),
            id = SerializableSceneId(id))

        return commandChannel.sendCommand(command)
    }
}