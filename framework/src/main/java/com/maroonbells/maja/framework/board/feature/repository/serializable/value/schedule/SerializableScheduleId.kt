package com.maroonbells.maja.framework.board.feature.repository.serializable.value.schedule

import com.maroonbells.maja.domain.board.model.ScheduleId
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor


@Suppress("FunctionName")
fun SerializableScheduleId(id: ScheduleId): SerializableScheduleId = SerializableScheduleId(id.raw)

@Serializable(ScheduleIdSerializer::class)
data class SerializableScheduleId(val raw: Int) {
    fun asModel(): ScheduleId = ScheduleId(raw)
}

@Serializer(forClass = SerializableScheduleId::class)
object ScheduleIdSerializer : KSerializer<SerializableScheduleId> {

    override val descriptor: SerialDescriptor = StringDescriptor.withName("ScheduleId")

    override fun serialize(encoder: Encoder, obj: SerializableScheduleId) {
        encoder.encodeInt(obj.raw)
    }

    override fun deserialize(decoder: Decoder): SerializableScheduleId {
        return SerializableScheduleId(raw = decoder.decodeInt())
    }
}