package com.maroonbells.maja.common.observable

import java.lang.ref.WeakReference
import java.util.concurrent.CopyOnWriteArrayList


typealias ObservableChangeListener<T> = (previous: T, next: T) -> Unit
typealias ObservableListener<T> = (next: T) -> Unit

interface Observable<T> {

    val value: T

    fun onChange(listener: ObservableChangeListener<T>): Disposable

    fun onChanged(listener: ObservableListener<T>): Disposable
}

class MutableObservable<T>(initialElement: T) : Observable<T> {

    private val changeListeners = CopyOnWriteArrayList<ObservableChangeListener<T>>()
    private val listeners = CopyOnWriteArrayList<ObservableListener<T>>()

    override var value: T = initialElement
        set(nextElement) {
            val previousElement = field
            field = nextElement

            changeListeners.forEach { it(previousElement, nextElement) }
            listeners.forEach { it(nextElement) }
        }

    override fun onChange(listener: ObservableChangeListener<T>): Disposable {
        if (changeListeners.addIfAbsent(listener)) {
            listener(value, value)
        }
        return ObservableChangeDisposable(this, listener)
    }

    override fun onChanged(listener: ObservableListener<T>): Disposable {
        if (listeners.addIfAbsent(listener)) {
            listener(value)
        }
        return ObservableDisposable(this, listener)
    }

    fun removeChangeListener(listener: ObservableChangeListener<T>?) {
        changeListeners.remove(listener)
    }

    fun removeListener(listener: ObservableListener<T>?) {
        listeners.remove(listener)
    }
}

internal class ObservableChangeDisposable<T>(
    observable: MutableObservable<T>,
    listener: ObservableChangeListener<T>
) : Disposable {

    private val observableRef = WeakReference(observable)
    private val listenerRef = WeakReference(listener)

    override fun dispose() {
        val observable = observableRef.get()
        val listener = listenerRef.get()

        if (observable != null && listener != null) {
            observable.removeChangeListener(listener)
        }

        observableRef.clear()
        listenerRef.clear()
    }

    override fun isDisposed(): Boolean {
        return observableRef.get() == null && listenerRef.get() == null
    }

}

internal class ObservableDisposable<T>(
    observable: MutableObservable<T>,
    listener: ObservableListener<T>
) : Disposable {

    private val observableRef = WeakReference(observable)
    private val listenerRef = WeakReference(listener)

    override fun dispose() {
        val observable = observableRef.get()
        val listener = listenerRef.get()

        if (observable != null && listener != null) {
            observable.removeListener(listener)
        }

        observableRef.clear()
        listenerRef.clear()
    }

    override fun isDisposed(): Boolean {
        return observableRef.get() == null && listenerRef.get() == null
    }
}