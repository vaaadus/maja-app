package com.maroonbells.maja.common.logger

object Logger {

    enum class Priority {
        Verbose, Debug, Info, Warning, Error
    }

    interface Appender {

        fun append(priority: Priority, tag: String, message: String, vararg args: Any?)
    }

    private val appenders = mutableListOf<Appender>()

    fun registerAppender(appender: Appender) {
        appenders.add(appender)
    }

    fun unregisterAppender(appender: Appender?) {
        appenders.remove(appender)
    }

    fun forClass(any: Class<*>): TaggedLogger =
        TaggedLogger(any.simpleName)

    internal fun log(priority: Priority, tag: String, message: String, vararg args: Any?) {
        appenders.forEach { it.append(priority, tag, message, *args) }
    }
}