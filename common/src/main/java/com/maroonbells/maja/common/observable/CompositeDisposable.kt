package com.maroonbells.maja.common.observable

import java.util.concurrent.CopyOnWriteArrayList

class CompositeDisposable : Disposable {

    private val disposables = CopyOnWriteArrayList<Disposable>()

    fun add(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun dispose() {
        disposables.forEach { it.dispose() }
        disposables.clear()
    }

    override fun isDisposed(): Boolean {
        return disposables.all { it.isDisposed() }
    }

    operator fun plusAssign(disposable: Disposable) = add(disposable)
}