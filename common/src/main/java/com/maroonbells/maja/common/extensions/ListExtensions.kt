package com.maroonbells.maja.common.extensions

/**
 * Returns a copy of this list.
 */
fun <T> List<T>.copy(): List<T> = ArrayList(this)