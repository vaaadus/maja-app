package com.maroonbells.maja.common.observable

interface Disposable {

    fun dispose()

    fun isDisposed(): Boolean
}