package com.maroonbells.maja.common.consumable

class Consumable {

    var isConsumed: Boolean = false
        private set

    /**
     * @return true if consumed, false if already consumed
     */
    @Synchronized
    fun consume(): Boolean {
        if (!isConsumed) {
            isConsumed = true
            return true
        }
        return false
    }

    /**
     * @return true if was consumed before resetting, false if wasn't
     */
    @Synchronized
    fun reset(): Boolean {
        if (isConsumed) {
            isConsumed = false
            return true
        }
        return false
    }
}