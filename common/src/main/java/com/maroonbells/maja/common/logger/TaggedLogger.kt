package com.maroonbells.maja.common.logger

class TaggedLogger(private val tag: String) {

    fun v(message: String, vararg args: Any?) {
        Logger.log(Logger.Priority.Verbose, tag, message, *args)
    }

    fun d(message: String, vararg args: Any?) {
        Logger.log(Logger.Priority.Debug, tag, message, *args)
    }

    fun i(message: String, vararg args: Any?) {
        Logger.log(Logger.Priority.Info, tag, message, *args)
    }

    fun w(message: String, vararg args: Any?) {
        Logger.log(Logger.Priority.Warning, tag, message, *args)
    }

    fun e(message: String, vararg args: Any?) {
        Logger.log(Logger.Priority.Error, tag, message, *args)
    }
}