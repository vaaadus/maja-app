package com.maroonbells.maja.common.observable

import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class CompositeDisposableTest {

    @RelaxedMockK
    private lateinit var first: Disposable

    @RelaxedMockK
    private lateinit var second: Disposable

    private lateinit var composite: CompositeDisposable

    @BeforeEach
    fun setUp() {
        composite = CompositeDisposable()
    }

    @Test
    fun emptyCompositeDisposable_isDisposed() {
        assertThat(composite.isDisposed()).isEqualTo(true)
    }

    @Test
    fun compositeDisposable_withDisposedDisposable_isDisposed() {
        every { first.isDisposed() } returns true
        every { second.isDisposed() } returns true

        composite.add(first)
        composite.add(second)

        assertThat(composite.isDisposed()).isEqualTo(true)
    }

    @Test
    fun compositeDisposable_withNotDisposedDisposable_isNotDisposed() {
        every { first.isDisposed() } returns true
        every { second.isDisposed() } returns false

        composite.add(first)
        composite.add(second)

        assertThat(composite.isDisposed()).isEqualTo(false)
    }

    @Test
    fun dispose_disposesAll() {
        composite.add(first)
        composite.add(second)

        composite.dispose()

        verify {
            first.dispose()
            second.dispose()
        }
    }
}