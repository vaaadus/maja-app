package com.maroonbells.maja.common.observable

import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertEquals

@ExtendWith(MockKExtension::class)
class MutableObservableTest {

    companion object {
        private const val INITIAL = "initial"
        private const val PREVIOUS = "previous"
        private const val NEXT = "NEXT"
    }

    private lateinit var observable: MutableObservable<String>

    @RelaxedMockK
    private lateinit var listener: ObservableListener<String>

    @RelaxedMockK
    private lateinit var changeListener: ObservableChangeListener<String>

    private lateinit var listenerDisposable: Disposable

    private lateinit var changeListenerDisposable: Disposable

    @BeforeEach
    fun setUp() {
        observable = MutableObservable(INITIAL)
        listenerDisposable = observable.onChanged(listener)
        changeListenerDisposable = observable.onChange(changeListener)
    }

    @Test
    fun initialValue_isPresent() {
        assertEquals(INITIAL, observable.value)
    }

    @Test
    fun initialValue_notifiesListeners() {
        verify {
            listener(INITIAL)
            changeListener(INITIAL, INITIAL)
        }
    }

    @Test
    fun singleChange_setsValue_andNotifiesListeners() {
        observable.value = NEXT

        verify {
            listener(NEXT)
            changeListener(INITIAL, NEXT)
        }
    }

    @Test
    fun doubleChange_setsValues_andNotifiesListeners() {
        observable.value = PREVIOUS
        observable.value = NEXT

        verify {
            listener(NEXT)
            changeListener(PREVIOUS, NEXT)
        }
    }

    @Test
    fun removedListener_isNotNotified() {
        assertThat(listenerDisposable.isDisposed()).isEqualTo(false)
        assertThat(changeListenerDisposable.isDisposed()).isEqualTo(false)

        listenerDisposable.dispose()
        changeListenerDisposable.dispose()

        observable.value = NEXT

        assertThat(listenerDisposable.isDisposed()).isEqualTo(true)
        assertThat(changeListenerDisposable.isDisposed()).isEqualTo(true)

        verify(exactly = 0) { listener(NEXT) }
        verify(exactly = 0) { changeListener(INITIAL, NEXT) }
    }
}