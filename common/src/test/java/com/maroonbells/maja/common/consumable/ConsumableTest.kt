package com.maroonbells.maja.common.consumable

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ConsumableTest {

    private lateinit var consumable: Consumable

    @BeforeEach
    fun setUp() {
        consumable = Consumable()
    }

    @Test
    fun newConsumable_isNotConsumed() {
        assertThat(consumable.isConsumed).isEqualTo(false)
    }

    @Test
    fun newConsumable_canBeConsumed() {
        assertThat(consumable.consume()).isEqualTo(true)
        assertThat(consumable.isConsumed).isEqualTo(true)
    }

    @Test
    fun newConsumable_cannotBeReset() {
        assertThat(consumable.reset()).isEqualTo(false)
        assertThat(consumable.isConsumed).isEqualTo(false)
    }

    @Test
    fun consumedConsumable_cannotBeConsumed() {
        consumable.consume()

        assertThat(consumable.consume()).isEqualTo(false)
        assertThat(consumable.isConsumed).isEqualTo(true)
    }

    @Test
    fun consumedConsumable_canBeReset() {
        consumable.consume()

        assertThat(consumable.reset()).isEqualTo(true)
        assertThat(consumable.isConsumed).isEqualTo(false)
    }

    @Test
    fun resetConsumable_cannotBeReset() {
        consumable.consume()
        consumable.reset()

        assertThat(consumable.reset()).isEqualTo(false)
        assertThat(consumable.isConsumed).isEqualTo(false)
    }
}