package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.logic.PinoutManager.CustomizedPinoutListener
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.model.Pinout
import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.utils.mockNode
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException

@ExtendWith(MockKExtension::class)
class MajaPinoutManagerTest {

    companion object {
        private const val accessibleFromHome = true
        private const val pinName = "Water valve"
        private val pinIcon = Icon.Light

        private val pin = Pin(
            id = PinId(3),
            name = "3",
            value = PinValue.High,
            type = Pin.Type.Digital)

        private val customizedPin = CustomizedPin(
            id = pin.id,
            customName = pinName,
            defaultName = pin.name,
            value = pin.value,
            type = pin.type,
            icon = pinIcon,
            accessibleFromHome = accessibleFromHome)
    }

    private val dispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var boardManager: BoardManager

    @RelaxedMockK
    private lateinit var customizationManager: CustomizationManager

    @RelaxedMockK
    private lateinit var pinoutListener: CustomizedPinoutListener

    private lateinit var board: Board
    private lateinit var pinout: Pinout

    private lateinit var manager: MajaPinoutManager

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(dispatcher)

        board = mockNode(Board::class)
        pinout = mockNode(Pinout::class)

        every { boardManager.board } returns board
        every { board.pinout } returns pinout

        every { customizationManager.getCustomName(board.address, pin.id) } returns pinName
        every { customizationManager.getIcon(board.address, pin.id) } returns pinIcon
        every { customizationManager.isAccessibleFromHome(board.address, pin.id) } returns accessibleFromHome

        manager = MajaPinoutManager(sessionManager, boardManager, customizationManager)
        manager.addPinoutListener(pinoutListener)
        clearMocks(pinoutListener)
    }

    @AfterEach
    fun tearDown() {
        dispatcher.cleanupTestCoroutines()
        Dispatchers.resetMain()
    }

    @Test
    fun listAndGetPinout() {
        every { pinout.pins } returns listOf(pin)
        every { pinout[pin.id] } returns pin

        assertThat(manager.pins).isEqualTo(listOf(customizedPin))
        assertThat(manager[pin.id]).isEqualTo(customizedPin)
    }

    @Test
    fun listAndGetPinout_whenDisconnected() {
        every { boardManager.board } returns null

        assertThat(manager.pins).isEmpty()
        assertThat(manager[pin.id]).isNull()
    }

    @Test
    fun setState() {
        manager.setState(pin.id, PinValue.Low)

        verify { pinout.setState(pin.id, PinValue.Low) }
    }


    @Test
    fun setName() {
        manager.setName(pin.id, pinName)

        verify { customizationManager.setCustomName(board.address, pin.id, pinName) }
    }

    @Test
    fun setIcon() {
        manager.setIcon(pin.id, Icon.Bell)

        verify { customizationManager.setCustomIcon(board.address, pin.id, Icon.Bell) }
    }

    @Test
    fun setAccessibleFromHome() {
        manager.setAccessibleFromHome(pin.id, false)

        verify { customizationManager.setAccessibleFromHome(board.address, pin.id, false) }
    }

    @Test
    fun onConnected_updatesPinout() {
        every { pinout.pins } returns listOf(pin)
        every { pinout[pin.id] } returns pin

        manager.onConnected(board)

        verify { pinoutListener.onCustomizedPinoutChanged(listOf(customizedPin)) }
    }

    @Test
    fun onDisconnected_updatesPinout() {
        every { boardManager.board } returns null

        manager.onDisconnected(null, IOException())

        verify { pinoutListener.onCustomizedPinoutChanged(emptyList()) }
    }

    @Test
    fun onCustomizationsChanged_updatesPinout() {
        every { pinout.pins } returns listOf(pin)
        every { pinout[pin.id] } returns pin

        manager.onCustomizationsChanged()

        verify { pinoutListener.onCustomizedPinoutChanged(listOf(customizedPin)) }
    }
}