package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.logic.RealTimeClockManager.DatetimeListener
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.model.RealTimeClock
import com.maroonbells.maja.domain.utils.mockNode
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException
import java.util.*

@ExtendWith(MockKExtension::class)
class MajaRealTimeClockManagerTest {

    private val dispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    private lateinit var boardManager: BoardManager

    @RelaxedMockK
    private lateinit var datetimeListener: DatetimeListener

    private lateinit var board: Board

    private lateinit var realTimeClock: RealTimeClock

    private lateinit var manager: MajaRealTimeClockManager

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(dispatcher)

        board = mockNode(Board::class)
        realTimeClock = mockNode(RealTimeClock::class)

        every { boardManager.board } returns board
        every { board.realTimeClock } returns realTimeClock

        manager = MajaRealTimeClockManager(boardManager)
        manager.addDatetimeListener(datetimeListener)
        clearMocks(datetimeListener)
    }

    @AfterEach
    fun tearDown() {
        dispatcher.cleanupTestCoroutines()
        Dispatchers.resetMain()
    }

    @Test
    fun listDatetime() {
        val date = Date()
        every { realTimeClock.datetime } returns date
        assertThat(manager.datetime).isEqualTo(date)
    }

    @Test
    fun synchronize() {
        manager.synchronize()

        verify { realTimeClock.synchronize() }
    }

    @Test
    fun onConnected_updatesDatetime() {
        val date = Date()
        every { realTimeClock.datetime } returns date

        manager.onConnected(board)

        verify { datetimeListener.onDatetimeChanged(date) }
    }

    @Test
    fun onDisconnected_updatesDatetime() {
        manager.onDisconnected(null, IOException())

        verify { datetimeListener.onDatetimeChanged(null) }
    }
}