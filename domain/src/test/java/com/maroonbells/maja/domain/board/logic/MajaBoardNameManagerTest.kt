package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.logic.BoardNameManager.BoardNameListener
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.utils.mockNode
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MajaBoardNameManagerTest {

    private val dispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var customizationManager: CustomizationManager

    @RelaxedMockK
    private lateinit var boardManager: BoardManager

    @RelaxedMockK
    private lateinit var nameListener: BoardNameListener

    private lateinit var board: Board

    private lateinit var manager: MajaBoardNameManager

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(dispatcher)

        board = mockNode(Board::class)
        every { boardManager.board } returns board

        manager = MajaBoardNameManager(sessionManager, customizationManager, boardManager)
        manager.addBoardNameListener(nameListener)
        clearMocks(nameListener)
    }

    @AfterEach
    fun tearDown() {
        dispatcher.cleanupTestCoroutines()
        Dispatchers.resetMain()
    }

    @Test
    fun listName() {
        val name = "Hello"
        every { customizationManager.getName(board.address) } returns name
        assertThat(manager.name).isEqualTo(name)
    }

    @Test
    fun setBoardName() {
        val newName = "I'm changed"
        manager.setBoardName(newName)

        verify { customizationManager.setCustomName(board.address, newName) }
    }

    @Test
    fun onCustomizationsChanged_updatesListener() {
        val name = "Test 1234"
        every { customizationManager.getName(board.address) } returns name

        manager.onCustomizationsChanged()

        verify { nameListener.onBoardNameChanged(name) }
    }
}