package com.maroonbells.maja.domain.board.bluetooth

import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.BoardManager.Status
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.lifecycle.LifecycleManager
import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.mockk.verifyOrder
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MajaBluetoothBoardManagerTest {

    private val address = BluetoothAddress("84:DA:EF:EE:22:19")

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var bluetooth: BluetoothManager

    @RelaxedMockK
    private lateinit var lifecycle: LifecycleManager

    @RelaxedMockK
    private lateinit var featureManager: FeatureManager<BluetoothAddress>

    @RelaxedMockK
    private lateinit var boardListener: BoardManager.BoardListener

    private lateinit var manager: MajaBluetoothBoardManager

    @BeforeEach
    fun setUp() {
        manager = MajaBluetoothBoardManager(sessionManager, bluetooth, lifecycle, featureManager)
        manager.addBoardListener(boardListener)
    }

    @Test
    fun onBoardSessionCreated_willPauseAndResume() {
        manager.onBoardSessionCreated(address)
        manager.onFeatureManagerResumed()
        manager.onLifecycleResume()

        val otherAddress = BluetoothAddress("65:DB:DF:CC:11:22")
        every { lifecycle.isResumed } returns true

        manager.onBoardSessionCreated(otherAddress)
        manager.onFeatureManagerResumed()

        verifyOrder {
            boardListener.onDisconnected(any(), null)
            featureManager.pause()

            boardListener.onConnecting()
            featureManager.resume(otherAddress)
            featureManager.addLifecycleListener(manager)

            boardListener.onConnected(any())
        }
    }

    @Test
    fun onBoardSessionDestroyed_willPause() {
        manager.onBoardSessionCreated(address)
        manager.onLifecycleResume()
        manager.onFeatureManagerResumed()
        manager.onBoardSessionDestroyed()

        assertThat(manager.status).isEqualTo(Status.Disconnected)
        assertThat(manager.board).isNull()

        verify {
            boardListener.onDisconnected(any(), null)
            featureManager.pause()
        }
    }

    @Test
    fun resume_willResumeFeatureManager() {
        manager.onBoardSessionCreated(address)
        manager.onLifecycleResume()

        assertThat(manager.status).isEqualTo(Status.Connecting)
        assertThat(manager.board).isNull()

        verify {
            boardListener.onConnecting()
            featureManager.resume(address)
            featureManager.addLifecycleListener(manager)
        }

        manager.onFeatureManagerResumed()

        assertThat(manager.status).isEqualTo(Status.Connected)
        assertThat(manager.board).isNotNull

        verify { boardListener.onConnected(any()) }
    }

    @Test
    fun resume_willDoNothing_whenNotConfigured() {
        manager.onLifecycleResume()

        assertThat(manager.status).isEqualTo(Status.Disconnected)
        assertThat(manager.board).isNull()

        verify(exactly = 0) {
            boardListener.onConnecting()
            boardListener.onConnected(any())
            featureManager.resume(any())
        }
    }

    @Test
    fun pause_willPauseFeatureManager() {
        manager.onBoardSessionCreated(address)
        manager.onLifecycleResume()
        manager.onFeatureManagerResumed()
        manager.onLifecyclePause()

        assertThat(manager.status).isEqualTo(Status.Disconnected)
        assertThat(manager.board).isNull()

        verify { boardListener.onDisconnected(any(), null) }
    }

    @Test
    fun reconnect_willPauseAndResume() {
        manager.onBoardSessionCreated(address)
        manager.onLifecycleResume()
        manager.onFeatureManagerResumed()

        every { lifecycle.isResumed } returns true

        manager.reconnect()
        manager.onFeatureManagerResumed()

        verifyOrder {
            boardListener.onDisconnected(any(), null)
            featureManager.pause()

            boardListener.onConnecting()
            featureManager.resume(address)
            featureManager.addLifecycleListener(manager)

            boardListener.onConnected(any())
        }
    }
}