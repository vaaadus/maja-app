package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.common.observable.MutableObservable
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.feature.exception.FeatureNotDefinedException
import com.maroonbells.maja.domain.board.feature.registry.RealTimeClockFeature
import com.maroonbells.maja.domain.board.model.Node
import com.maroonbells.maja.domain.utils.assertThrows
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class MajaFeatureNodeTest {

    @RelaxedMockK
    private lateinit var listener: (Node<*>) -> Unit

    @MockK
    private lateinit var features: FeatureManager<*>

    @MockK
    private lateinit var rtcFeature: RealTimeClockFeature

    private lateinit var scope: CoroutineScope

    @BeforeEach
    fun setUp() {
        scope = CoroutineScope(TestCoroutineDispatcher())
        every { features.scope } returns scope
        every { features.getFeature(board.rtc) } returns MutableObservable(rtcFeature)
        every { features.getFeature(board.pinout) } returns MutableObservable(null)
        every { features.refreshFeature(board.rtc) } returns Job()
    }

    @Test
    fun onChanged_callsImmediately() = withFeatureNode {
        onChanged(listener)

        verify { listener(this@withFeatureNode) }
    }

    @Test
    fun onChanged_canBeDisposed() = withFeatureNode {
        val disposable = onChanged(listener)
        clearMocks(listener)

        disposable.dispose()
        dispatchChange()

        verify(exactly = 0) { listener(this@withFeatureNode) }
    }

    @Test
    fun require_returnsFeature() = withFeatureNode {
        assertThat(board.rtc.require()).isEqualTo(rtcFeature)
    }

    @Test
    fun require_throwsException_whenFeatureMissing() = withFeatureNode {
        assertThrows<FeatureNotDefinedException> {
            board.pinout.require()
        }
    }

    @Test
    fun get_returnsOptionalFeature() = withFeatureNode {
        assertThat(board.pinout.get()).isNull()
    }

    @Test
    fun get_returnsFeature() = withFeatureNode {
        assertThat(board.rtc.get()).isEqualTo(rtcFeature)
    }

    @Test
    fun isSupported_forPresentFeature() = withFeatureNode {
        assertThat(board.rtc.isSupported()).isEqualTo(true)
    }

    @Test
    fun isSupported_forAbsentFeature() = withFeatureNode {
        assertThat(board.pinout.isSupported()).isEqualTo(false)
    }

    @Test
    fun onFeatureChanged_callsImmediately() = withFeatureNode {
        var called = false
        board.rtc.onChanged {
            assertThat(it).isEqualTo(rtcFeature)
            called = true
        }
        assertThat(called).isEqualTo(true)
    }

    @Test
    fun onFeatureNext_doesNotCallWithNulls() = withFeatureNode {
        board.pinout.onNext {
            fail("onNext must not be called when feature is not present")
        }
    }

    @Test
    fun onFeatureNext_callsImmediately() = withFeatureNode {
        var called = false
        board.rtc.onNext {
            assertThat(it).isEqualTo(rtcFeature)
            called = true
        }
        assertThat(called).isEqualTo(true)
    }

    @Test
    fun refresh_willDelegateCall() = withFeatureNode {
        board.rtc.refresh()

        verify { features.refreshFeature(board.rtc) }
    }

    private fun withFeatureNode(block: FeatureNode<Node<*>>.() -> Unit) {
        object : FeatureNode<Node<*>>(features) {
            init {
                block()
            }
        }
    }
}