package com.maroonbells.maja.domain.configuration.repository

class InMemoryConfiguration : Configuration {

    private val map = mutableMapOf<String, Any?>()

    override fun putString(key: String, value: String) {
        map[key] = value
    }

    override fun getString(key: String, default: String): String {
        return map[key] as String? ?: default
    }

    override fun getString(key: String): String? {
        return map[key] as String?
    }

    override fun putLong(key: String, value: Long) {
        map[key] = value
    }

    override fun getLong(key: String, default: Long): Long {
        return map[key] as Long? ?: default
    }

    override fun getLong(key: String): Long? {
        return map[key] as Long?
    }

    override fun putInt(key: String, value: Int) {
        map[key] = value
    }

    override fun getInt(key: String, default: Int): Int {
        return map[key] as Int? ?: default
    }

    override fun getInt(key: String): Int? {
        return map[key] as Int?
    }

    override fun putBoolean(key: String, value: Boolean) {
        map[key] = value
    }

    override fun getBoolean(key: String, default: Boolean): Boolean {
        return map[key] as Boolean? ?: default
    }

    override fun getBoolean(key: String): Boolean? {
        return map[key] as Boolean?
    }

    override fun putFloat(key: String, value: Float) {
        map[key] = value
    }

    override fun getFloat(key: String, default: Float): Float {
        return map[key] as Float? ?: default
    }

    override fun getFloat(key: String): Float? {
        return map[key] as Float?
    }

    override fun containsKey(key: String): Boolean {
        return map.containsKey(key)
    }

    override fun clear(key: String) {
        map.remove(key)
    }

    override fun clearAll() {
        map.clear()
    }
}