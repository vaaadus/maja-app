package com.maroonbells.maja.domain.lifecycle

import io.mockk.clearMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertTrue

@ExtendWith(MockKExtension::class)
class MajaLifecycleManagerTest {

    private val dispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    private lateinit var listener: LifecycleManager.LifecycleListener

    private lateinit var manager: MajaLifecycleManager

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(dispatcher)

        manager = MajaLifecycleManager()
        manager.addLifecycleListener(listener)

        clearMocks(listener)
    }

    @AfterEach
    fun tearDown() {
        dispatcher.cleanupTestCoroutines()
        Dispatchers.resetMain()
    }

    @Test
    fun manager_initially_isPaused() {
        assertTrue(!manager.isResumed)
    }

    @Test
    fun resume_resumesImmediately() {
        manager.resume()

        verify { listener.onLifecycleResume() }
    }

    @Test
    fun doubleResume_doesNotResumeTwice() {
        manager.resume()
        manager.resume()

        verify(exactly = 1) { listener.onLifecycleResume() }
    }

    @Test
    fun pause_pausesWithDelay() = runBlockingTest(dispatcher) {
        manager.resume()
        manager.pause()

        verify(exactly = 0) { listener.onLifecyclePause() }

        advanceTimeBy(MajaLifecycleManager.DELAYED_PAUSE)

        verify(exactly = 1) { listener.onLifecyclePause() }
    }

    @Test
    fun numberOfResumes_mustBeEqualToNumberOfPauses() = runBlockingTest(dispatcher) {
        manager.resume()
        manager.resume()
        manager.pause()

        advanceTimeBy(MajaLifecycleManager.DELAYED_PAUSE)
        verify(exactly = 0) { listener.onLifecyclePause() }

        manager.pause()

        advanceTimeBy(MajaLifecycleManager.DELAYED_PAUSE)
        verify(exactly = 1) { listener.onLifecyclePause() }
    }

    @Test
    fun postResume_unschedulesDelayedPause() = runBlockingTest(dispatcher) {
        manager.resume()
        manager.pause()

        advanceTimeBy(MajaLifecycleManager.DELAYED_PAUSE / 2)
        manager.resume()

        advanceTimeBy(MajaLifecycleManager.DELAYED_PAUSE)
        verify(exactly = 0) { listener.onLifecyclePause() }
    }
}