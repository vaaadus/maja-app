package com.maroonbells.maja.domain.customization

import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.configuration.repository.Configuration
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_COLOR
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_ICON
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_PIN_ICON
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.domain.resources.ResourceLocator
import com.maroonbells.maja.domain.resources.ResourceLocator.Text.BoardName
import com.maroonbells.maja.domain.resources.ResourceLocator.Text.UnknownActionName
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class MajaCustomizationManagerTest {

    companion object {
        private val BOARD_ADDRESS = BluetoothAddress("11:22:33:44")

        private const val KEY_BOARD_NAME = "11:22:33:44-board_name"
        private const val CUSTOM_BOARD_NAME = "I'm changed"
        private const val DEFAULT_BOARD_NAME = "Your Board"

        private val PIN_ID = PinId(10)
        private val PIN = Pin(PIN_ID, "test", PinValue(0), Pin.Type.Digital)
        private const val KEY_PIN_NAME = "11:22:33:44-pin-10-pin_name"
        private const val KEY_PIN_ICON = "11:22:33:44-pin-10-pin_icon"
        private const val KEY_PIN_HOME = "11:22:33:44-pin-10-pin_home"

        private const val CUSTOM_PIN_NAME = "I'm changed pin"
        private val CUSTOM_PIN_ICON = Icon.Bell

        private val ACTION_ID = ActionId(30)
        private const val KEY_ACTION_NAME = "11:22:33:44-action-30-action_name"
        private const val KEY_ACTION_ICON = "11:22:33:44-action-30-action_icon"
        private const val KEY_ACTION_COLOR = "11:22:33:44-action-30-action_color"
        private const val KEY_ACTION_SCENE = "11:22:33:44-action-30-action_scene"

        private const val CUSTOM_ACTION_NAME = "I'm changed action"
        private const val UNKNOWN_ACTION_NAME = "???"
        private val CUSTOM_ACTION_ICON = Icon.Computer
        private val CUSTOM_ACTION_COLOR = Color.Blue
    }

    @RelaxedMockK
    private lateinit var configuration: Configuration

    @RelaxedMockK
    private lateinit var resourceLocator: ResourceLocator

    private lateinit var manager: MajaCustomizationManager

    @BeforeEach
    fun setUp() {
        manager = MajaCustomizationManager(configuration, resourceLocator)
    }

    @Test
    fun availableIcons_returnsAllIcons() {
        assertThat(manager.availableIcons).isEqualTo(Icon.values().toList())
    }

    @Test
    fun availableColors_returnsAllColors() {
        assertThat(manager.availableColors).isEqualTo(Color.values().toList())
    }

    @Test
    fun board_getName_returnsStoredOrDefaultName() {
        every { configuration.getString(KEY_BOARD_NAME) } returns CUSTOM_BOARD_NAME
        assertThat(manager.getName(BOARD_ADDRESS)).isEqualTo(CUSTOM_BOARD_NAME)

        every { resourceLocator.getString(BoardName) } returns DEFAULT_BOARD_NAME
        every { configuration.getString(KEY_BOARD_NAME) } returns null
        assertThat(manager.getName(BOARD_ADDRESS)).isEqualTo(DEFAULT_BOARD_NAME)
    }

    @Test
    fun board_getCustomName_returnsStoredNameOrNull() {
        every { configuration.getString(KEY_BOARD_NAME) } returns CUSTOM_BOARD_NAME
        assertThat(manager.getCustomName(BOARD_ADDRESS)).isEqualTo(CUSTOM_BOARD_NAME)

        every { configuration.getString(KEY_BOARD_NAME) } returns null
        assertThat(manager.getCustomName(BOARD_ADDRESS)).isNull()
    }

    @Test
    fun board_setCustomName_setsIt() {
        manager.setCustomName(BOARD_ADDRESS, CUSTOM_BOARD_NAME)
        verify { configuration.putString(KEY_BOARD_NAME, CUSTOM_BOARD_NAME) }

        clearMocks(configuration)

        manager.setCustomName(BOARD_ADDRESS, null)
        verify { configuration.clear(KEY_BOARD_NAME) }
    }

    @Test
    fun pin_getName_returnsStoredOrDefaultName() {
        every { configuration.getString(KEY_PIN_NAME, PIN.name) } returns CUSTOM_PIN_NAME
        assertThat(manager.getName(BOARD_ADDRESS, PIN)).isEqualTo(CUSTOM_PIN_NAME)

        every { configuration.getString(KEY_PIN_NAME, PIN.name) } returns PIN.name
        assertThat(manager.getName(BOARD_ADDRESS, PIN)).isEqualTo(PIN.name)
    }

    @Test
    fun pin_getCustomName_returnsStoredNameOrNull() {
        every { configuration.getString(KEY_PIN_NAME) } returns CUSTOM_PIN_NAME
        assertThat(manager.getCustomName(BOARD_ADDRESS, PIN_ID)).isEqualTo(CUSTOM_PIN_NAME)

        every { configuration.getString(KEY_PIN_NAME) } returns null
        assertThat(manager.getCustomName(BOARD_ADDRESS, PIN_ID)).isNull()
    }

    @Test
    fun pin_setCustomName_setsIt() {
        manager.setCustomName(BOARD_ADDRESS, PIN_ID, CUSTOM_PIN_NAME)
        verify { configuration.putString(KEY_PIN_NAME, CUSTOM_PIN_NAME) }

        clearMocks(configuration)

        manager.setCustomName(BOARD_ADDRESS, PIN_ID, null)
        verify { configuration.clear(KEY_PIN_NAME) }
    }

    @Test
    fun pin_getIcon_returnsStoredOrDefaultIcon() {
        every { configuration.getInt(KEY_PIN_ICON) } returns CUSTOM_PIN_ICON.ordinal
        assertThat(manager.getIcon(BOARD_ADDRESS, PIN_ID)).isEqualTo(CUSTOM_PIN_ICON)

        every { configuration.getInt(KEY_PIN_ICON) } returns null
        assertThat(manager.getIcon(BOARD_ADDRESS, PIN_ID)).isEqualTo(DEFAULT_PIN_ICON)
    }

    @Test
    fun pin_getCustomIcon_returnsStoredIconOrNull() {
        every { configuration.getInt(KEY_PIN_ICON) } returns CUSTOM_PIN_ICON.ordinal
        assertThat(manager.getCustomIcon(BOARD_ADDRESS, PIN_ID)).isEqualTo(CUSTOM_PIN_ICON)

        every { configuration.getInt(KEY_PIN_ICON) } returns null
        assertThat(manager.getCustomIcon(BOARD_ADDRESS, PIN_ID)).isNull()
    }

    @Test
    fun pin_setCustomIcon_setsIt() {
        manager.setCustomIcon(BOARD_ADDRESS, PIN_ID, CUSTOM_PIN_ICON)

        verify { configuration.putInt(KEY_PIN_ICON, CUSTOM_PIN_ICON.ordinal) }
    }

    @Test
    fun pin_isAccessibleFromHome_returnsValue() {
        every { configuration.getBoolean(KEY_PIN_HOME, false) } returns true

        assertThat(manager.isAccessibleFromHome(BOARD_ADDRESS, PIN_ID)).isEqualTo(true)
    }

    @Test
    fun pin_setAccessibleFromHome_setsIt() {
        manager.setAccessibleFromHome(BOARD_ADDRESS, PIN_ID, true)

        verify { configuration.putBoolean(KEY_PIN_HOME, true) }
    }

    @Test
    fun action_getName_returnsStoredOrDefaultName() {
        every { configuration.getString(KEY_ACTION_NAME) } returns CUSTOM_ACTION_NAME
        assertThat(manager.getName(BOARD_ADDRESS, ACTION_ID)).isEqualTo(CUSTOM_ACTION_NAME)

        every { resourceLocator.getString(UnknownActionName) } returns UNKNOWN_ACTION_NAME
        every { configuration.getString(KEY_ACTION_NAME) } returns null
        assertThat(manager.getName(BOARD_ADDRESS, ACTION_ID)).isEqualTo(UNKNOWN_ACTION_NAME)
    }

    @Test
    fun action_getCustomName_returnsStoredNameOrNull() {
        every { configuration.getString(KEY_ACTION_NAME) } returns CUSTOM_ACTION_NAME
        assertThat(manager.getCustomName(BOARD_ADDRESS, ACTION_ID)).isEqualTo(CUSTOM_ACTION_NAME)

        every { configuration.getString(KEY_ACTION_NAME) } returns null
        assertThat(manager.getCustomName(BOARD_ADDRESS, ACTION_ID)).isNull()
    }

    @Test
    fun action_setCustomName_setsIt() {
        manager.setCustomName(BOARD_ADDRESS, ACTION_ID, CUSTOM_ACTION_NAME)
        verify { configuration.putString(KEY_ACTION_NAME, CUSTOM_ACTION_NAME) }

        clearMocks(configuration)

        manager.setCustomName(BOARD_ADDRESS, ACTION_ID, null)
        verify { configuration.clear(KEY_ACTION_NAME) }
    }

    @Test
    fun action_getIcon_returnsStoredOrDefaultIcon() {
        every { configuration.getInt(KEY_ACTION_ICON) } returns CUSTOM_ACTION_ICON.ordinal
        assertThat(manager.getIcon(BOARD_ADDRESS, ACTION_ID)).isEqualTo(CUSTOM_ACTION_ICON)

        every { configuration.getInt(KEY_ACTION_ICON) } returns null
        assertThat(manager.getIcon(BOARD_ADDRESS, ACTION_ID)).isEqualTo(DEFAULT_ACTION_ICON)
    }

    @Test
    fun action_getCustomIcon_returnsStoredIconOrNull() {
        every { configuration.getInt(KEY_ACTION_ICON) } returns CUSTOM_ACTION_ICON.ordinal
        assertThat(manager.getCustomIcon(BOARD_ADDRESS, ACTION_ID)).isEqualTo(CUSTOM_ACTION_ICON)

        every { configuration.getInt(KEY_ACTION_ICON) } returns null
        assertThat(manager.getCustomIcon(BOARD_ADDRESS, ACTION_ID)).isNull()
    }

    @Test
    fun action_setCustomIcon_setsIt() {
        manager.setCustomIcon(BOARD_ADDRESS, ACTION_ID, CUSTOM_ACTION_ICON)

        verify { configuration.putInt(KEY_ACTION_ICON, CUSTOM_ACTION_ICON.ordinal) }
    }

    @Test
    fun action_getColor_returnsStoredOrDefaultColor() {
        every { configuration.getInt(KEY_ACTION_COLOR) } returns CUSTOM_ACTION_COLOR.ordinal
        assertThat(manager.getColor(BOARD_ADDRESS, ACTION_ID)).isEqualTo(CUSTOM_ACTION_COLOR)

        every { configuration.getInt(KEY_ACTION_COLOR) } returns null
        assertThat(manager.getColor(BOARD_ADDRESS, ACTION_ID)).isEqualTo(DEFAULT_ACTION_COLOR)
    }

    @Test
    fun action_getCustomColor_returnsStoredColorOrNull() {
        every { configuration.getInt(KEY_ACTION_COLOR) } returns CUSTOM_ACTION_COLOR.ordinal
        assertThat(manager.getCustomColor(BOARD_ADDRESS, ACTION_ID)).isEqualTo(CUSTOM_ACTION_COLOR)

        every { configuration.getInt(KEY_ACTION_COLOR) } returns null
        assertThat(manager.getCustomColor(BOARD_ADDRESS, ACTION_ID)).isNull()
    }

    @Test
    fun action_setCustomColor_setsIt() {
        manager.setCustomColor(BOARD_ADDRESS, ACTION_ID, CUSTOM_ACTION_COLOR)

        verify { configuration.putInt(KEY_ACTION_COLOR, CUSTOM_ACTION_COLOR.ordinal) }
    }

    @Test
    fun action_isAvailableAsScene_returnsStoredValueOrDefault() {
        every { configuration.getBoolean(KEY_ACTION_SCENE, false) } returns true
        assertThat(manager.isAvailableAsScene(BOARD_ADDRESS, ACTION_ID)).isEqualTo(true)

        every { configuration.getBoolean(KEY_ACTION_SCENE, false) } returns false
        assertThat(manager.isAvailableAsScene(BOARD_ADDRESS, ACTION_ID)).isEqualTo(false)
    }

    @Test
    fun action_setAvailableAsScene_setsIt() {
        manager.setAvailableAsScene(BOARD_ADDRESS, ACTION_ID, true)

        verify { configuration.putBoolean(KEY_ACTION_SCENE, true) }
    }
}