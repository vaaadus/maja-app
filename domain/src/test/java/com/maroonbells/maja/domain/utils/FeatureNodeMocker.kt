package com.maroonbells.maja.domain.utils

import com.maroonbells.maja.domain.board.model.Node
import com.maroonbells.maja.domain.board.model.NodeListener
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkClass
import io.mockk.slot
import kotlin.reflect.KClass

/**
 * Mocks the node to simulate original behavior: invoke listener immediately.
 */
fun <T : Node<T>> mockNode(kClass: KClass<T>): T {
    val node: T = mockkClass(kClass, relaxed = true)
    val lambda = slot<NodeListener<Node<T>>>()

    every { node.onChanged(capture(lambda)) } answers {
        lambda.captured.invoke(node)
        mockk(relaxed = true)
    }

    return node
}