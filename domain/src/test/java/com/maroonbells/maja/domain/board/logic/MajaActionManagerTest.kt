package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.model.Actions
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.model.Pinout
import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_COLOR
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_ICON
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.utils.mockNode
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException

@ExtendWith(MockKExtension::class)
class MajaActionManagerTest {

    companion object {

        private const val pinName = "Valve"
        private const val actionName = "Water the lawn..."
        private const val actionAsScene = true
        private val actionIcon = Icon.Blinds
        private val actionColor = Color.Yellow

        private val pin = Pin(PinId(3), "3", PinValue.High, Pin.Type.Digital)

        private val action = Action(
            id = ActionId(11),
            steps = listOf(Action.Step(pin.id, pin.value)))

        private val customizedAction = CustomizedAction(
            id = action.id,
            steps = listOf(
                CustomizedActionStep(
                    pinId = pin.id,
                    pinValue = pin.value,
                    pinName = pinName,
                    pinType = pin.type)
            ),
            customName = actionName,
            name = actionName,
            icon = actionIcon,
            color = actionColor,
            availableAsScene = actionAsScene)
    }

    private val dispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var boardManager: BoardManager

    @RelaxedMockK
    private lateinit var customizationManager: CustomizationManager

    @RelaxedMockK
    private lateinit var actionsListener: ActionManager.CustomizedActionsListener

    private lateinit var board: Board
    private lateinit var actions: Actions
    private lateinit var pinout: Pinout

    private lateinit var manager: MajaActionManager

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(dispatcher)

        board = mockNode(Board::class)
        actions = mockNode(Actions::class)
        pinout = mockNode(Pinout::class)

        every { boardManager.board } returns board
        every { board.actions } returns actions
        every { board.pinout } returns pinout

        every { customizationManager.getName(board.address, pin) } returns pinName
        every { customizationManager.getName(board.address, action.id) } returns actionName
        every { customizationManager.getCustomName(board.address, action.id) } returns actionName
        every { customizationManager.getIcon(board.address, action.id) } returns actionIcon
        every { customizationManager.getColor(board.address, action.id) } returns actionColor
        every { customizationManager.isAvailableAsScene(board.address, action.id) } returns actionAsScene

        manager = MajaActionManager(sessionManager, boardManager, customizationManager)
        manager.addActionsListener(actionsListener)
        clearMocks(actionsListener)
    }

    @AfterEach
    fun tearDown() {
        dispatcher.cleanupTestCoroutines()
        Dispatchers.resetMain()
    }

    @Test
    fun listAndGetActions() {
        every { actions.list } returns listOf(action)
        every { pinout[pin.id] } returns pin

        assertThat(manager.list).isEqualTo(listOf(customizedAction))
        assertThat(manager[action.id]).isEqualTo(customizedAction)
    }

    @Test
    fun listAndGetActions_whenDisconnected() {
        every { boardManager.board } returns null

        assertThat(manager.list).isEmpty()
        assertThat(manager[action.id]).isNull()
    }

    @Test
    fun maxActions() {
        every { actions.maxActions } returns 5
        assertThat(manager.maxActions).isEqualTo(5)

        every { boardManager.board } returns null
        assertThat(manager.maxActions).isEqualTo(0)
    }

    @Test
    fun maxStepsPerAction() {
        every { actions.maxStepsPerAction } returns 10
        assertThat(manager.maxStepsPerAction).isEqualTo(10)

        every { boardManager.board } returns null
        assertThat(manager.maxStepsPerAction).isEqualTo(0)
    }

    @Test
    fun canAddActions() {
        every { actions.canAddActions } returns true
        assertThat(manager.canAddActions).isTrue()

        every { boardManager.board } returns null
        assertThat(manager.canAddActions).isFalse()
    }

    @Suppress("DeferredResultUnused")
    @Test
    fun create() {
        val actionId = ActionId(10)
        val request = UpdateCustomizedActionRequest(
            steps = emptyList(),
            name = "Name",
            icon = Icon.Heating,
            color = Color.Yellow,
            availableAsScene = true)

        val deferredActionId = mockk<Deferred<ActionId>> {
            coEvery { await() } returns actionId
        }

        every { actions.createAsync(request.steps) } returns deferredActionId

        manager.create(request)

        verify {
            actions.createAsync(request.steps)
            customizationManager.setCustomName(board.address, actionId, request.name)
            customizationManager.setCustomIcon(board.address, actionId, request.icon ?: DEFAULT_ACTION_ICON)
            customizationManager.setCustomColor(board.address, actionId, request.color ?: DEFAULT_ACTION_COLOR)
            customizationManager.setAvailableAsScene(board.address, actionId, request.availableAsScene ?: false)
        }
    }

    @Test
    fun update() {
        val actionId = ActionId(10)
        val request = UpdateCustomizedActionRequest(
            steps = emptyList(),
            name = "Name",
            icon = Icon.Heating,
            color = Color.Yellow,
            availableAsScene = true)

        manager.update(actionId, request)

        verify {
            actions.update(actionId, request.steps)
            customizationManager.setCustomName(board.address, actionId, request.name)
            customizationManager.setCustomIcon(board.address, actionId, request.icon ?: DEFAULT_ACTION_ICON)
            customizationManager.setCustomColor(board.address, actionId, request.color ?: DEFAULT_ACTION_COLOR)
            customizationManager.setAvailableAsScene(board.address, actionId, request.availableAsScene ?: false)
        }
    }

    @Test
    fun remove() {
        val actionId = ActionId(11)
        manager.remove(actionId)
        verify { actions.remove(actionId) }
    }

    @Test
    fun onConnected_updatesActions() {
        every { actions.list } returns listOf(action)
        every { pinout[pin.id] } returns pin

        manager.onConnected(board)

        verify { actionsListener.onCustomizedActionsChanged(listOf(customizedAction)) }
    }

    @Test
    fun onDisconnected_updatesActions() {
        every { boardManager.board } returns null

        manager.onDisconnected(null, IOException())

        verify { actionsListener.onCustomizedActionsChanged(emptyList()) }
    }

    @Test
    fun onCustomizationsChanged_updatesActions() {
        every { actions.list } returns listOf(action)
        every { pinout[pin.id] } returns pin

        manager.onCustomizationsChanged()

        verify { actionsListener.onCustomizedActionsChanged(listOf(customizedAction)) }
    }
}