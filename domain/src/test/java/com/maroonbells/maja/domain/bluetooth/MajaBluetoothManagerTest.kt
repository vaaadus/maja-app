package com.maroonbells.maja.domain.bluetooth

import com.maroonbells.maja.domain.bluetooth.BluetoothManager.BluetoothStatus
import com.maroonbells.maja.domain.bluetooth.BluetoothManager.ConnectionStatus
import com.maroonbells.maja.domain.bluetooth.repository.Bluetooth
import com.maroonbells.maja.domain.bluetooth.repository.BluetoothLiveSocket
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyOrder
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException

@ExtendWith(MockKExtension::class)
class MajaBluetoothManagerTest {

    @RelaxedMockK
    private lateinit var bluetooth: Bluetooth

    private lateinit var manager: MajaBluetoothManager

    @BeforeEach
    fun setUp() {
        manager = MajaBluetoothManager(bluetooth)
    }

    @Nested
    inner class BluetoothState {

        @Test
        fun statusIsTakenFrom_enabledBluetooth() {
            every { bluetooth.status } returns BluetoothStatus.Enabled
            assertThat(manager.status).isEqualTo(BluetoothStatus.Enabled)
        }

        @Test
        fun statusIsTakenFrom_disabledBluetooth() {
            every { bluetooth.status } returns BluetoothStatus.Disabled
            assertThat(manager.status).isEqualTo(BluetoothStatus.Disabled)
        }

        @Test
        fun listenersAreNotified_onChange() {
            every { bluetooth.status } returns BluetoothStatus.Disabled

            val listener: BluetoothManager.StatusListener = mockk(relaxed = true)
            manager.addBluetoothListener(listener)
            manager.onBluetoothStatusChanged(BluetoothStatus.Enabled)

            verifyOrder {
                listener.onBluetoothStatusChanged(BluetoothStatus.Disabled)
                listener.onBluetoothStatusChanged(BluetoothStatus.Enabled)
            }
        }

        @Test
        fun enable_invokesBluetooth() {
            manager.enable()

            verify { bluetooth.enable() }
        }

        @Test
        fun disable_invokesBluetooth() {
            manager.disable()

            verify { bluetooth.disable() }
        }

        @Test
        fun pairedDevices_listsAll() {
            val devices: List<BluetoothDevice> = listOf(mockk(name = "LG"), mockk(name = "Samsung"))
            every { bluetooth.pairedDevices } returns devices
            assertThat(manager.pairedDevices).isEqualTo(devices)
        }
    }

    @Nested
    inner class BluetoothDiscovery {

        @RelaxedMockK
        private lateinit var listener: BluetoothManager.DiscoveryListener

        @Test
        fun startDiscovery_willPassCallback() {
            manager.startDiscovery(listener)

            verify { bluetooth.startDiscovery(any()) }
        }

        @Test
        fun stopDiscovery_willStopIt() {
            manager.stopDiscovery()

            verify { bluetooth.stopDiscovery() }
        }

    }

    @Nested
    inner class Connection {

        private val address = BluetoothAddress(raw = "30:74:96:0A:4B:F2")
        private val message = "Hello, world!"
        private val bytes = message.toByteArray()

        @RelaxedMockK
        private lateinit var connectionListener: BluetoothManager.ConnectionListener

        @RelaxedMockK
        private lateinit var socket: BluetoothLiveSocket

        @BeforeEach
        fun setUp() {
            every { bluetooth.createLiveSocket(address) } returns socket
            every { socket.address } returns address

            manager.addConnectionListener(connectionListener)
        }

        @Test
        fun connectionStatus_isInitiallyDisconnected() {
            assertThat(manager.connectionStatus).isEqualTo(ConnectionStatus.Disconnected)
        }

        @Test
        fun connect_willStopBluetoothDiscovery_andListenToSocket() {
            manager.connect(address)

            verify {
                bluetooth.stopDiscovery()
                socket.addListener(manager)
            }
        }

        @Test
        fun connect_willChangeStatus_toConnecting() {
            manager.connect(address)

            assertThat(manager.connectionStatus).isEqualTo(ConnectionStatus.Connecting)
            verify { connectionListener.onBluetoothConnecting(address) }
        }

        @Test
        fun connect_willChangeStatus_toConnected_whenSocketOpens() {
            manager.connect(address)
            manager.onSocketOpen(socket)

            assertThat(manager.connectionStatus).isEqualTo(ConnectionStatus.Connected)
            verify { connectionListener.onBluetoothConnected(address) }
        }

        @Test
        fun connect_willDisconnect_previousConnection_toOtherBoard() {
            manager.connect(address)
            manager.onSocketOpen(socket)

            val otherAddress = BluetoothAddress("84:C0:EF:CE:36:68")
            val otherSocket: BluetoothLiveSocket = mockk(relaxed = true)

            every { bluetooth.createLiveSocket(otherAddress) } returns otherSocket
            every { otherSocket.address } returns otherAddress

            manager.connect(otherAddress)
            manager.onSocketOpen(otherSocket)

            verifyOrder {
                connectionListener.onBluetoothConnecting(address)
                connectionListener.onBluetoothConnected(address)
                connectionListener.onBluetoothDisconnected(address, null)

                connectionListener.onBluetoothConnecting(otherAddress)
                connectionListener.onBluetoothConnected(otherAddress)
            }

            assertThat(manager.connectionStatus).isEqualTo(ConnectionStatus.Connected)
        }

        @Test
        fun disconnect_willChangeState_andDoCleanup() {
            manager.connect(address)
            manager.onSocketOpen(socket)
            manager.disconnect()

            assertThat(manager.connectionStatus).isEqualTo(ConnectionStatus.Disconnected)
            verify {
                connectionListener.onBluetoothDisconnected(address, null)
                socket.removeListener(manager)
            }
        }

        @Test
        fun closedSocket_willChangeState_andDoCleanup() {
            manager.connect(address)
            manager.onSocketOpen(socket)

            val exception = IOException("Network issues")

            manager.onSocketClose(socket, exception)

            assertThat(manager.connectionStatus).isEqualTo(ConnectionStatus.Disconnected)
            verify {
                connectionListener.onBluetoothDisconnected(address, exception)
                socket.removeListener(manager)
            }
        }

        @Test
        fun sendMessage_willPassMessage() {
            manager.connect(address)
            manager.onSocketOpen(socket)
            manager.sendMessage(message)

            verify { socket.send(message) }
        }

        @Test
        fun sendMessage_wontPassMessage_whenSocketIsClosed() {
            manager.connect(address)
            manager.onSocketOpen(socket)
            manager.onSocketClose(socket, null)
            manager.sendMessage(message)

            verify(exactly = 0) { socket.send(message) }
        }

        @Test
        fun sendBytes_willPassBytes() {
            manager.connect(address)
            manager.onSocketOpen(socket)
            manager.sendBytes(bytes)

            verify { socket.send(bytes) }
        }

        @Test
        fun sendBytes_wontPassBytes_whenSocketIsClosed() {
            manager.connect(address)
            manager.onSocketOpen(socket)
            manager.onSocketClose(socket, null)
            manager.sendBytes(bytes)

            verify(exactly = 0) { socket.send(bytes) }
        }
    }
}