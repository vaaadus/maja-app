package com.maroonbells.maja.domain.utils

import org.junit.jupiter.api.fail

inline fun <reified T : Throwable> assertThrows(block: () -> Unit) {
    try {
        block()
        fail("${T::class.java.simpleName} not thrown")
    } catch (e: Exception) {
        if (e !is T) fail("${e.javaClass.simpleName} thrown, expected ${T::class.java.simpleName}")
    }
}