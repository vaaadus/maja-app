package com.maroonbells.maja.domain.board.feature

import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.feature.repository.FeatureChannel
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.mockk.verifyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException

@ExtendWith(MockKExtension::class)
class MajaFeatureManagerTest {

    private val address = BluetoothAddress("84:C0:EF:CE:36:68")

    @RelaxedMockK
    private lateinit var featureChannel: FeatureChannel<BluetoothAddress>

    @RelaxedMockK
    private lateinit var listener: FeatureManager.LifecycleListener

    private lateinit var manager: MajaFeatureManager<BluetoothAddress>

    @BeforeEach
    fun setUp() {
        manager = MajaFeatureManager(featureChannel)
        manager.addLifecycleListener(listener)
    }

    @Test
    fun resume_willResumeFeatureChannel() {
        manager.resume(address)

        verify {
            featureChannel.resume(address)
            featureChannel.addChannelListener(manager)
        }
    }

    @Test
    fun pause_willPauseFeatureChannel() {
        manager.resume(address)
        manager.pause()

        verify {
            featureChannel.pause()
            featureChannel.removeChannelListener(manager)
        }
    }

    @Test
    fun onFeatureChannelResumed_willNotifyLifecycleListeners() {
        manager.resume(address)
        manager.onFeatureChannelResumed()

        verify { listener.onFeatureManagerResumed() }
    }

    @Test
    fun onFeatureChannelPaused_willNotifyLifecycleListeners() {
        val cause = IOException("Network issues")

        manager.resume(address)
        manager.onFeatureChannelResumed()
        manager.onFeatureChannelPaused(cause)

        verifyOrder {
            listener.onFeatureManagerResumed()
            listener.onFeatureManagerPaused(cause)
        }
    }
}