package com.maroonbells.maja.domain.board.value.schedule

import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.datetime.Time
import com.maroonbells.maja.domain.board.value.datetime.Weekday.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ScheduleEntriesTest {

    companion object {
        private const val maxBlocksPerDay = 4

        private val block = ScheduleBlock(
            start = Time(10, 0),
            end = Time(20, 0),
            actionId = ActionId(3))
    }

    private lateinit var builder: ScheduleEntriesBuilder

    @BeforeEach
    fun setUp() {
        builder = ScheduleEntriesBuilder()
    }

    @Test
    fun emptyBuilder() {
        assertThat(builder.maxBlocksPerDay()).isEqualTo(0)
        assertThat(builder.blocksAt(Monday)).isEmpty()
        assertThat(builder.blocksAt(Tuesday)).isEmpty()
        assertThat(builder.blocksAt(Wednesday)).isEmpty()
        assertThat(builder.blocksAt(Thursday)).isEmpty()
        assertThat(builder.blocksAt(Friday)).isEmpty()
        assertThat(builder.blocksAt(Saturday)).isEmpty()
        assertThat(builder.blocksAt(Sunday)).isEmpty()
    }

    @Test
    fun emptySchedule() {
        val schedule = builder.build()

        assertThat(schedule.maxBlocksPerDay).isEqualTo(0)
        assertThat(schedule.blocksAt(Monday)).isEmpty()
        assertThat(schedule.blocksAt(Tuesday)).isEmpty()
        assertThat(schedule.blocksAt(Wednesday)).isEmpty()
        assertThat(schedule.blocksAt(Thursday)).isEmpty()
        assertThat(schedule.blocksAt(Friday)).isEmpty()
        assertThat(schedule.blocksAt(Saturday)).isEmpty()
        assertThat(schedule.blocksAt(Sunday)).isEmpty()
    }

    @Test
    fun maxBlocksPerDay() {
        builder.maxBlocksPerDay(maxBlocksPerDay)

        assertThat(builder.maxBlocksPerDay()).isEqualTo(maxBlocksPerDay)
    }

    @Test
    fun addBlock() {
        builder.add(Monday, block)

        assertThat(builder.blocksAt(Monday)).contains(block)
        assertThat(builder.blocksAt(Sunday)).isEmpty()
    }

    @Test
    fun addBlock_removeBlock() {
        builder.add(Tuesday, block)
        assertThat(builder.blocksAt(Tuesday)).contains(block)

        builder.remove(Tuesday, block)
        assertThat(builder.blocksAt(Tuesday)).doesNotContain(block)
    }

    @Test
    fun addAllBlocks() {
        builder.addAll(Wednesday, listOf(block, block))
        assertThat(builder.blocksAt(Wednesday)).isEqualTo(listOf(block, block))
    }

    @Test
    fun clearAll() {
        builder.add(Friday, block)
        builder.add(Sunday, block)
        builder.clearAll()

        assertThat(builder.blocksAt(Friday)).isEmpty()
        assertThat(builder.blocksAt(Sunday)).isEmpty()
    }

    @Test
    fun clear() {
        builder.add(Friday, block)
        builder.clear(Friday)

        assertThat(builder.blocksAt(Friday)).isEmpty()
    }

    @Test
    fun sort() {
        val block1 = ScheduleBlock(
            start = Time(22, 30),
            end = Time(23, 0),
            actionId = ActionId(2))

        val block2 = ScheduleBlock(
            start = Time(10, 0),
            end = Time(15, 0),
            actionId = ActionId(1))

        builder.add(Friday, block1)
        builder.add(Friday, block2)
        builder.sort()

        assertThat(builder.blocksAt(Friday)).isEqualTo(listOf(block2, block1))
    }

    @Test
    fun build_reducesBlocks_toLimit() {
        builder.maxBlocksPerDay(2)
        builder.add(Saturday, block)
        builder.add(Saturday, block)
        builder.add(Saturday, block)

        val schedule = builder.build()
        assertThat(schedule.blocksAt(Saturday)).hasSize(2)
    }
}