package com.maroonbells.maja.domain.connection

import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.BluetoothManager.BluetoothStatus
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.connection.ConnectionManager.Status
import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.domain.lifecycle.LifecycleManager
import com.maroonbells.maja.domain.session.BoardSessionManager
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException

@ExtendWith(MockKExtension::class)
class MajaConnectionManagerTest {

    @RelaxedMockK
    private lateinit var lifecycleManager: LifecycleManager

    @RelaxedMockK
    private lateinit var bluetoothManager: BluetoothManager

    @RelaxedMockK
    private lateinit var sessionManager: BoardSessionManager

    @RelaxedMockK
    private lateinit var boardManager: BoardManager

    @RelaxedMockK
    private lateinit var statusListener: ConnectionManager.StatusListener

    private lateinit var manager: MajaConnectionManager

    @BeforeEach
    fun setUp() {
        manager = MajaConnectionManager(lifecycleManager, bluetoothManager, sessionManager)
        manager.addConnectionStatusListener(statusListener)
    }

    @Test
    fun onLifecycleResume_addsListeners() {
        simulateBoardSessionCreated()

        manager.onLifecycleResume()

        verify {
            sessionManager.addBoardSessionListener(manager)
            bluetoothManager.addBluetoothListener(manager)
            boardManager.addBoardListener(manager)
        }
    }

    @Test
    fun onLifecyclePause_removesListeners() {
        simulateBoardSessionCreated()

        manager.onLifecyclePause()

        verify {
            sessionManager.removeBoardSessionListener(manager)
            bluetoothManager.removeBluetoothListener(manager)
            boardManager.removeBoardListener(manager)
        }
    }

    @Test
    fun bluetoothDisabled() {
        simulateBluetoothStatus(BluetoothStatus.Disabled)

        assertConnectionStatus(Status.BluetoothDisabled)
    }

    @Test
    fun connecting() {
        simulateBoardSessionCreated()
        simulateBluetoothStatus(BluetoothStatus.Enabled)
        simulateBoardStatus(BoardManager.Status.Connecting)

        assertConnectionStatus(Status.Connecting)
    }

    @Test
    fun connected() {
        simulateBoardSessionCreated()
        simulateBluetoothStatus(BluetoothStatus.Enabled)
        simulateBoardStatus(BoardManager.Status.Connecting)
        simulateBoardStatus(BoardManager.Status.Connected)

        assertConnectionStatus(Status.Connected)
    }

    @Test
    fun disconnected() {
        simulateBoardSessionCreated()
        simulateBluetoothStatus(BluetoothStatus.Enabled)
        simulateBoardStatus(BoardManager.Status.Connecting)
        simulateBoardStatus(BoardManager.Status.Disconnected)

        assertConnectionStatus(Status.Disconnected)
    }

    @Test
    fun bluetoothEnabled() {
        simulateBluetoothStatus(BluetoothStatus.Enabled)

        assertConnectionStatus(Status.BluetoothEnabled)
    }

    @Test
    fun bluetoothEnabled_afterBeingDestroyed() {
        simulateBoardSessionCreated()
        simulateBluetoothStatus(BluetoothStatus.Enabled)
        simulateBoardStatus(BoardManager.Status.Connected)
        simulateBoardSessionDestroyed()

        assertConnectionStatus(Status.BluetoothEnabled)
    }

    private fun simulateBluetoothStatus(status: BluetoothStatus) {
        every { bluetoothManager.status } returns status
        manager.onBluetoothStatusChanged(status)
    }

    private fun simulateBoardStatus(status: BoardManager.Status) {
        every { boardManager.status } returns status

        when (status) {
            BoardManager.Status.Disconnected -> manager.onDisconnected(mockk(), IOException())
            BoardManager.Status.Connected -> manager.onConnected(mockk())
            BoardManager.Status.Connecting -> manager.onConnecting()
        }
    }

    private fun simulateBoardSessionCreated() {
        every { sessionManager.boardManager } returns boardManager
        manager.onBoardSessionCreated(DemoAddress())
    }

    private fun simulateBoardSessionDestroyed() {
        every { sessionManager.boardManager } returns null
        manager.onBoardSessionDestroyed()
    }

    private fun assertConnectionStatus(status: Status) {
        assertThat(manager.status).isEqualTo(status)
        verify { statusListener.onConnectionStatusChanged(status) }
    }
}