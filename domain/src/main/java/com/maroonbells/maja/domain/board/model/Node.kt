package com.maroonbells.maja.domain.board.model

import com.maroonbells.maja.common.observable.Disposable

typealias NodeListener<T> = (T) -> Unit

interface Node<out T : Node<T>> {

    fun onChanged(listener: NodeListener<T>): Disposable
}