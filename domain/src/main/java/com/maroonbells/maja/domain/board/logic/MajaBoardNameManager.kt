package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.logic.BoardNameManager.BoardNameListener
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.CustomizationManager.CustomizationsListener
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.util.concurrent.CopyOnWriteArrayList

class MajaBoardNameManager(
    private val sessionManager: BoardSessionManager,
    private val customizationManager: CustomizationManager,
    private val boardManager: BoardManager
) : BoardNameManager, BoardSessionListener, CustomizationsListener {

    private val listeners = CopyOnWriteArrayList<BoardNameListener>()
    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    private val board: Board?
        get() = boardManager.board

    init {
        sessionManager.addBoardSessionListener(this)
    }

    override fun onBoardSessionCreated(address: BoardAddress) {
        customizationManager.addCustomizationsListener(this)
    }

    override fun onBoardSessionDestroyed() {
        customizationManager.removeCustomizationsListener(this)
        sessionManager.removeBoardSessionListener(this)
    }

    override fun addBoardNameListener(listener: BoardNameListener) {
        if (listeners.addIfAbsent(listener)) {
            listener.onBoardNameChanged(name)
        }
    }

    override fun removeBoardNameListener(listener: BoardNameListener?) {
        listeners.remove(listener)
    }

    override val name: String
        get() = board?.let { customizationManager.getName(it.address) } ?: ""

    override fun setBoardName(name: String?) = scope.launch {
        val board = requireNotNull(board)

        customizationManager.setCustomName(board.address, name)
    }

    override fun onCustomizationsChanged() {
        for (listener in listeners) {
            listener.onBoardNameChanged(name)
        }
    }
}