package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.common.extensions.copy
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.model.Pinout
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import kotlinx.coroutines.launch

class FeaturePinout(features: FeatureManager<*>) : FeatureNode<Pinout>(features), Pinout {

    override val pins: List<Pin>
        get() = mutableList.copy()

    private val mutableList = mutableListOf<Pin>()

    init {
        board.pinout.onNext { feature ->
            mutableList.clear()
            mutableList.addAll(feature.pins)

            dispatchChange()
        }
    }

    override fun setState(id: PinId, value: PinValue) = scope.launch {
        board.pinout.require().set(id, value).join()
        board.pinout.refresh().join()
    }

    override fun get(id: PinId?): Pin? {
        return pins.find { it.id == id }
    }
}