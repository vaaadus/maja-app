package com.maroonbells.maja.domain.board.value.schedule

import com.maroonbells.maja.domain.board.value.datetime.Weekday

class ScheduleEntries(builder: ScheduleEntriesBuilder) {

    companion object {
        val EMPTY = ScheduleEntriesBuilder().build()
    }

    private val blocks: Map<Weekday, List<ScheduleBlock>>
    val maxBlocksPerDay: Int = builder.maxBlocksPerDay()

    init {
        val map = mutableMapOf<Weekday, List<ScheduleBlock>>()
        for (weekday in Weekday.values()) {
            map[weekday] = builder.blocksAt(weekday)
        }
        blocks = map
    }

    fun blocksAt(weekday: Weekday): List<ScheduleBlock> {
        return blocks[weekday] ?: emptyList()
    }

    fun toBuilder(): ScheduleEntriesBuilder = ScheduleEntriesBuilder(this)
}