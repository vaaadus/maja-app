package com.maroonbells.maja.domain.bluetooth

import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice
import kotlinx.coroutines.Job

interface BluetoothManager {

    enum class BluetoothStatus {
        Disabled, Enabled
    }

    enum class ConnectionStatus {
        Connecting, Connected, Disconnected
    }

    interface StatusListener {

        fun onBluetoothStatusChanged(status: BluetoothStatus)
    }

    interface DiscoveryListener {

        fun onBluetoothDiscoveryStarted()

        fun onBluetoothDevicesDiscovered(devices: List<BluetoothDevice>)

        fun onBluetoothDiscoveryFinished()
    }

    interface ConnectionListener {

        fun onBluetoothConnecting(address: BluetoothAddress)

        fun onBluetoothConnected(address: BluetoothAddress)

        fun onBluetoothDisconnected(address: BluetoothAddress?, cause: Throwable?)
    }

    interface LiveUpdateListener {

        fun onBluetoothMessage(address: BluetoothAddress, message: String)
    }

    val status: BluetoothStatus

    val connectionStatus: ConnectionStatus

    val pairedDevices: List<BluetoothDevice>

    fun addBluetoothListener(listener: StatusListener)

    fun removeBluetoothListener(listener: StatusListener?)

    fun addConnectionListener(listener: ConnectionListener)

    fun removeConnectionListener(listener: ConnectionListener?)

    fun addLiveUpdateListener(listener: LiveUpdateListener)

    fun removeLiveUpdateListener(listener: LiveUpdateListener?)

    fun enable()

    fun disable()

    fun startDiscovery(listener: DiscoveryListener)

    fun stopDiscovery()

    fun connect(address: BluetoothAddress)

    fun disconnect()

    fun sendMessage(message: String): Job

    fun sendBytes(bytes: ByteArray): Job
}