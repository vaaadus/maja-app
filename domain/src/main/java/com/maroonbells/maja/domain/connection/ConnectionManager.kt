package com.maroonbells.maja.domain.connection

/**
 * Renders a final connection state of the board,
 * observing all interesting events, such as bluetooth enabled/disabled, etc...
 */
interface ConnectionManager {

    enum class Status {

        BluetoothDisabled,

        BluetoothEnabled,

        Connecting,

        Connected,

        Disconnected
    }

    interface StatusListener {

        fun onConnectionStatusChanged(status: Status)
    }

    val status: Status

    fun addConnectionStatusListener(listener: StatusListener)

    fun removeConnectionStatusListener(listener: StatusListener?)
}