package com.maroonbells.maja.domain.board

import com.maroonbells.maja.domain.board.model.Board

interface BoardManager {

    enum class Status {
        Disconnected, Connecting, Connected
    }

    interface BoardListener {

        fun onConnecting()

        fun onConnected(board: Board)

        fun onDisconnected(board: Board?, cause: Throwable?)
    }

    val status: Status

    val board: Board?

    fun addBoardListener(listener: BoardListener)

    fun removeBoardListener(listener: BoardListener?)

    fun reconnect()
}