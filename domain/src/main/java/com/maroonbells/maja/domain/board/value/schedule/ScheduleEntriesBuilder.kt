package com.maroonbells.maja.domain.board.value.schedule

import com.maroonbells.maja.domain.board.value.datetime.Weekday

class ScheduleEntriesBuilder {

    private val blocks = mutableMapOf<Weekday, MutableList<ScheduleBlock>>()
    private var maxBlocksPerDay: Int = 0

    constructor() {
        for (weekday in Weekday.values()) {
            blocks[weekday] = mutableListOf()
        }
    }

    constructor(schedule: ScheduleEntries) {
        maxBlocksPerDay = schedule.maxBlocksPerDay
        for (weekday in Weekday.values()) {
            blocks[weekday] = schedule.blocksAt(weekday).toMutableList()
        }
    }

    fun blocksAt(weekday: Weekday): List<ScheduleBlock> {
        return blocks[weekday]?.copy() ?: emptyList()
    }

    fun maxBlocksPerDay(): Int {
        return maxBlocksPerDay
    }

    fun maxBlocksPerDay(limit: Int): ScheduleEntriesBuilder {
        maxBlocksPerDay = limit
        return this
    }

    fun addAll(weekday: Weekday, blocks: List<ScheduleBlock>): ScheduleEntriesBuilder {
        for (block in blocks) {
            add(weekday, block)
        }
        return this
    }

    fun add(weekday: Weekday, block: ScheduleBlock): ScheduleEntriesBuilder {
        blocks[weekday]?.add(block)
        return this
    }

    fun remove(weekday: Weekday, block: ScheduleBlock): ScheduleEntriesBuilder {
        blocks[weekday]?.remove(block)
        return this
    }

    fun clearAll(): ScheduleEntriesBuilder {
        for (weekday in Weekday.values()) {
            clear(weekday)
        }
        return this
    }

    fun clear(weekday: Weekday): ScheduleEntriesBuilder {
        blocks[weekday]?.clear()
        return this
    }

    fun sort(): ScheduleEntriesBuilder {
        for (weekday in Weekday.values()) {
            blocks[weekday]?.sort()
        }
        return this
    }

    fun build(): ScheduleEntries {
        reduceBlocksToMaxLimit()
        return ScheduleEntries(this)
    }

    private fun reduceBlocksToMaxLimit() {
        for (weekday in Weekday.values()) {
            val list = blocks[weekday] ?: continue
            while (list.size > maxBlocksPerDay) {
                list.removeLast()
            }
        }
    }

    private fun <T> MutableList<T>.removeLast(): Boolean {
        if (isNotEmpty()) {
            return remove(last())
        }
        return false
    }

    private fun <T> List<T>.copy(): List<T> {
        return ArrayList(this)
    }
}