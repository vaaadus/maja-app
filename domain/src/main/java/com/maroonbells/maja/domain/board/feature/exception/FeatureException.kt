package com.maroonbells.maja.domain.board.feature.exception

open class FeatureException : Exception {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)

    companion object {
        fun forCause(throwable: Throwable?): FeatureException {
            return if (throwable is FeatureException) throwable
            else FeatureException(throwable)
        }
    }
}