package com.maroonbells.maja.domain.board.feature

import com.maroonbells.maja.domain.board.feature.name.*
import com.maroonbells.maja.domain.board.feature.registry.*

@Suppress("ClassName", "ClassNaming")
object FeatureRegistry {

    private val all = mutableListOf<FeatureClass<out Feature>>()

    object board {
        val pinout = path<PinoutFeature>("board.pinout")
        val rtc = path<RealTimeClockFeature>("board.rtc")
        val actions = path<ActionsFeature>("board.actions")
        val schedules = path<SchedulesFeature>("board.schedules")
        val schedule = pattern<ScheduleFeature>("board.schedules.%s")
        val scenes = path<ScenesFeature>("board.scenes")

        fun loadMappings() = Unit
    }

    init {
        board.loadMappings()
    }

    private inline fun <reified T : Feature> path(raw: String): FeatureClassPath<T> {
        val featureClass = FeatureClassPath<T>(FeaturePath(raw))
        all.add(featureClass)
        return featureClass
    }

    private inline fun <reified T : Feature> pattern(raw: String): FeatureClassPattern<T> {
        val featureClass = FeatureClassPattern<T>(FeaturePattern(raw))
        all.add(featureClass)
        return featureClass
    }

    fun toFeatureClass(path: FeaturePath): FeatureClass<out Feature>? {
        return all.firstOrNull { it.matches(path) }
    }
}