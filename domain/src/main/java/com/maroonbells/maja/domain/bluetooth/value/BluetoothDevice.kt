package com.maroonbells.maja.domain.bluetooth.value

data class BluetoothDevice(val address: BluetoothAddress, val name: String?, val isPaired: Boolean)