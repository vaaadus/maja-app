package com.maroonbells.maja.domain.customization.value

/**
 * Supported icons used to personalize the app.
 */
enum class Icon {
    Ventilation,
    Light,
    Heating,
    Pan,
    Computer,
    Laptop,
    Smartphone,
    GamePad,
    TV,
    Battery,
    Lock,
    Blinds,
    Door,
    Bell,
    Alarm
}