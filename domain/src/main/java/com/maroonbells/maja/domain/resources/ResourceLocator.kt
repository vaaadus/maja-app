package com.maroonbells.maja.domain.resources

interface ResourceLocator {

    enum class Text {
        BoardName,
        UnknownActionName
    }

    fun getString(text: Text): String
}