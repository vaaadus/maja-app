package com.maroonbells.maja.domain.board.feature.registry

import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import kotlinx.coroutines.Job

interface PinoutFeature : Feature {

    val pins: List<Pin>

    fun set(id: PinId, value: PinValue): Job
}