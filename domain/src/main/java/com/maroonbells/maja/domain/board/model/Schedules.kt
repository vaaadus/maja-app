package com.maroonbells.maja.domain.board.model

import kotlinx.coroutines.Job

interface Schedules : Node<Schedules> {

    val list: List<Schedule>
    val maxSchedules: Int
    val maxBlocksPerDay: Int

    fun createSchedule(): Job

    fun removeSchedule(id: ScheduleId): Job
}