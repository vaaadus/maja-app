package com.maroonbells.maja.domain.board.value

import com.maroonbells.maja.domain.customization.value.Icon

data class CustomizedPin(
    val id: PinId,
    val customName: String?,
    val defaultName: String,
    val value: PinValue,
    val type: Pin.Type,
    val icon: Icon,
    val accessibleFromHome: Boolean) {

    val name: String = customName ?: defaultName

    fun asPin(): Pin {
        return Pin(id, defaultName, value, type)
    }
}

data class Pin(val id: PinId, val name: String, val value: PinValue, val type: Type) {

    enum class Type {
        Digital, DigitalPwm
    }
}

inline class PinId(val raw: Int)

inline class PinValue(val raw: Int) {

    companion object {
        const val MAX_VALUE = 255
        val High = PinValue(1)
        val Low = PinValue(0)

        fun fromDutyCycle(percentage: Int): PinValue {
            return PinValue(percentage * MAX_VALUE / 100)
        }

        fun fromState(enabled: Boolean): PinValue {
            return if (enabled) High else Low
        }
    }

    val isOn: Boolean
        get() = this != Low

    val isOff: Boolean
        get() = this == Low

    /**
     * Percentage 0.0 - 1.0 how long is the pin on.
     */
    val dutyCycle: Int
        get() = 100 * raw / MAX_VALUE
}