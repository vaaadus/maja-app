package com.maroonbells.maja.domain.board.feature.registry

import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.ActionId
import kotlinx.coroutines.Job

interface ActionsFeature : Feature {

    val actions: List<Action>
    val maxActions: Int
    val maxStepsPerAction: Int

    fun create(steps: List<Action.Step>): Job

    fun update(id: ActionId, steps: List<Action.Step>): Job

    fun remove(id: ActionId): Job
}