package com.maroonbells.maja.domain.bluetooth.repository

import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import kotlinx.coroutines.Job

interface BluetoothLiveSocket {

    interface BluetoothSocketListener {

        fun onSocketOpen(socket: BluetoothLiveSocket)

        fun onSocketMessage(socket: BluetoothLiveSocket, message: String)

        fun onSocketClose(socket: BluetoothLiveSocket, cause: Throwable?)
    }

    val isOpen: Boolean

    val isConnected: Boolean

    val isClosed: Boolean

    val address: BluetoothAddress

    fun addListener(listener: BluetoothSocketListener)

    fun removeListener(listener: BluetoothSocketListener?)

    fun open()

    fun send(message: String): Job

    fun send(bytes: ByteArray): Job

    fun close()
}