package com.maroonbells.maja.domain.board.feature.registry

import com.maroonbells.maja.domain.board.feature.name.FeaturePath

interface Feature {

    val path: FeaturePath
}