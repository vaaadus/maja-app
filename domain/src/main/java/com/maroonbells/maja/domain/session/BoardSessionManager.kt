package com.maroonbells.maja.domain.session

import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.logic.ActionManager
import com.maroonbells.maja.domain.board.logic.BoardNameManager
import com.maroonbells.maja.domain.board.logic.PinoutManager
import com.maroonbells.maja.domain.board.value.BoardAddress

/**
 * Manages the dependencies tied to a board lifecycle.
 * When board is configured (its address is stored)
 * the dependencies are available. Otherwise they are null.
 */
interface BoardSessionManager {

    interface BoardSessionListener {

        fun onBoardSessionCreated(address: BoardAddress)

        fun onBoardSessionDestroyed()
    }

    val boardAddress: BoardAddress?
    val boardManager: BoardManager?
    val boardNameManager: BoardNameManager?
    val pinoutManager: PinoutManager?
    val actionManager: ActionManager?

    fun addBoardSessionListener(listener: BoardSessionListener)

    fun removeBoardSessionListener(listener: BoardSessionListener?)

    fun configure(address: BoardAddress)

    fun destroyConfiguration()
}