package com.maroonbells.maja.domain.connection

import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.BluetoothManager.BluetoothStatus
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.BoardManager.BoardListener
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.connection.ConnectionManager.Status
import com.maroonbells.maja.domain.connection.ConnectionManager.StatusListener
import com.maroonbells.maja.domain.lifecycle.LifecycleManager
import com.maroonbells.maja.domain.lifecycle.LifecycleManager.LifecycleListener
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener
import java.util.concurrent.CopyOnWriteArrayList

class MajaConnectionManager(
    lifecycleManager: LifecycleManager,
    private val bluetooth: BluetoothManager,
    private val sessionManager: BoardSessionManager
) : ConnectionManager, LifecycleListener, BluetoothManager.StatusListener, BoardSessionListener, BoardListener {

    private val statusListeners = CopyOnWriteArrayList<StatusListener>()
    private var bluetoothStatus: BluetoothStatus = BluetoothStatus.Disabled

    private var boardManager: BoardManager? = null
    private var boardStatus: BoardManager.Status? = null
    private var boardException: Throwable? = null

    override var status: Status = Status.BluetoothDisabled

    init {
        lifecycleManager.addLifecycleListener(this)
    }

    override fun onLifecycleResume() {
        sessionManager.addBoardSessionListener(this)
        bluetooth.addBluetoothListener(this)
        boardManager?.addBoardListener(this)
    }

    override fun onLifecyclePause() {
        sessionManager.removeBoardSessionListener(this)
        bluetooth.removeBluetoothListener(this)
        boardManager?.removeBoardListener(this)
    }

    override fun addConnectionStatusListener(listener: StatusListener) {
        if (statusListeners.addIfAbsent(listener)) {
            listener.onConnectionStatusChanged(status)
        }
    }

    override fun removeConnectionStatusListener(listener: StatusListener?) {
        statusListeners.remove(listener)
    }

    override fun onBluetoothStatusChanged(status: BluetoothStatus) {
        bluetoothStatus = status
        recalculateAndDispatchStatus()
    }

    override fun onBoardSessionCreated(address: BoardAddress) {
        boardManager = sessionManager.boardManager
        boardManager?.addBoardListener(this)
    }

    override fun onBoardSessionDestroyed() {
        boardManager?.removeBoardListener(this)
        boardManager = null

        boardStatus = BoardManager.Status.Disconnected
        boardException = null
        recalculateAndDispatchStatus()
    }

    override fun onConnecting() {
        boardStatus = boardManager?.status
        boardException = null
        recalculateAndDispatchStatus()
    }

    override fun onConnected(board: Board) {
        boardStatus = boardManager?.status
        boardException = null
        recalculateAndDispatchStatus()
    }

    override fun onDisconnected(board: Board?, cause: Throwable?) {
        boardStatus = boardManager?.status
        boardException = cause
        recalculateAndDispatchStatus()
    }

    private fun recalculateAndDispatchStatus() {
        val status = calculateStatus(bluetoothStatus, boardStatus, boardException)
        if (this.status != status) {
            this.status = status
            statusListeners.forEach { it.onConnectionStatusChanged(status) }
        }
    }

    private fun calculateStatus(
        bluetoothStatus: BluetoothStatus,
        boardStatus: BoardManager.Status?,
        boardException: Throwable?): Status {

        if (bluetoothStatus == BluetoothStatus.Disabled) {
            return Status.BluetoothDisabled
        }

        if (boardStatus == BoardManager.Status.Connecting) {
            return Status.Connecting
        }

        if (boardStatus == BoardManager.Status.Connected) {
            return Status.Connected
        }

        if (boardStatus == BoardManager.Status.Disconnected && boardException != null) {
            return Status.Disconnected
        }

        return Status.BluetoothEnabled
    }
}