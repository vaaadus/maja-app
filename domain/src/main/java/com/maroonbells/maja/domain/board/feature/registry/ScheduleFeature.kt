package com.maroonbells.maja.domain.board.feature.registry

import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.domain.board.value.datetime.Weekday
import com.maroonbells.maja.domain.board.value.schedule.ScheduleBlock
import kotlinx.coroutines.Job

interface ScheduleFeature : Feature {

    val id: ScheduleId
    val monday: List<ScheduleBlock>
    val tuesday: List<ScheduleBlock>
    val wednesday: List<ScheduleBlock>
    val thursday: List<ScheduleBlock>
    val friday: List<ScheduleBlock>
    val saturday: List<ScheduleBlock>
    val sunday: List<ScheduleBlock>

    fun setEntries(weekday: Weekday, entries: List<ScheduleBlock>): Job
}