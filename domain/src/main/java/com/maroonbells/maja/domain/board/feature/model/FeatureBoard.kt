package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.model.*
import com.maroonbells.maja.domain.board.value.BoardAddress

@Suppress("JoinDeclarationAndAssignment")
class FeatureBoard(
    features: FeatureManager<*>,
    override val address: BoardAddress) : FeatureNode<Board>(features), Board {

    override val pinout: Pinout
    override val actions: Actions
    override val realTimeClock: RealTimeClock
    override val schedules: Schedules
    override val scenes: Scenes

    init {
        pinout = FeaturePinout(features)
        actions = FeatureActions(features)
        realTimeClock = FeatureRealTimeClock(features)
        schedules = FeatureSchedules(features)
        scenes = FeatureScenes(features)
    }

    override fun toString(): String = "FeatureBoard[address=${address.raw}]"
}