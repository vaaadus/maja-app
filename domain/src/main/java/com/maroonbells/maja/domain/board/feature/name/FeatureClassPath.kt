package com.maroonbells.maja.domain.board.feature.name

import com.maroonbells.maja.domain.board.feature.registry.Feature
import kotlin.reflect.KClass

@Suppress("FunctionName")
inline fun <reified T : Feature> FeatureClassPath(path: FeaturePath): FeatureClassPath<T> {
    return FeatureClassPath(T::class, path)
}

data class FeatureClassPath<T : Feature>(
    override val raw: KClass<T>,
    val path: FeaturePath) : FeatureClass<T> {

    override fun matches(path: FeaturePath): Boolean {
        return this.path == path
    }
}