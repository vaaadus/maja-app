package com.maroonbells.maja.domain.configuration.repository

interface Configuration {

    fun putString(key: String, value: String)
    fun getString(key: String, default: String): String
    fun getString(key: String): String?

    fun putLong(key: String, value: Long)
    fun getLong(key: String, default: Long): Long
    fun getLong(key: String): Long?

    fun putInt(key: String, value: Int)
    fun getInt(key: String, default: Int): Int
    fun getInt(key: String): Int?

    fun putBoolean(key: String, value: Boolean)
    fun getBoolean(key: String, default: Boolean): Boolean
    fun getBoolean(key: String): Boolean?

    fun putFloat(key: String, value: Float)
    fun getFloat(key: String, default: Float): Float
    fun getFloat(key: String): Float?

    fun containsKey(key: String): Boolean
    fun clear(key: String)
    fun clearAll()
}