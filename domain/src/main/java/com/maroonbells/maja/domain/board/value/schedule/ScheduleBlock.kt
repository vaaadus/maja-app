package com.maroonbells.maja.domain.board.value.schedule

import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.datetime.Time

data class ScheduleBlock(val start: Time, val end: Time, val actionId: ActionId) : Comparable<ScheduleBlock> {

    override fun compareTo(other: ScheduleBlock): Int {
        return compareValuesBy(this, other, ScheduleBlock::start, ScheduleBlock::end)
    }
}
