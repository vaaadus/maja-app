package com.maroonbells.maja.domain.board.bluetooth

import com.maroonbells.maja.common.consumable.Consumable
import com.maroonbells.maja.common.logger.Logger
import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.BluetoothManager.BluetoothStatus
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.BoardManager.BoardListener
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.model.FeatureBoard
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.lifecycle.LifecycleManager
import com.maroonbells.maja.domain.lifecycle.LifecycleManager.LifecycleListener
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener
import java.util.concurrent.CopyOnWriteArrayList

class MajaBluetoothBoardManager(
    private val sessionManager: BoardSessionManager,
    private val bluetooth: BluetoothManager,
    private val lifecycle: LifecycleManager,
    private val featureManager: FeatureManager<BluetoothAddress>
) : BluetoothBoardManager, BoardSessionListener, LifecycleListener,
    BluetoothManager.StatusListener, FeatureManager.LifecycleListener {

    companion object {
        private val LOG = Logger.forClass(MajaBluetoothBoardManager::class.java)
    }

    private val listeners = CopyOnWriteArrayList<BoardListener>()
    private val resume = Consumable()
    private val pause = Consumable()
    private var configuredBoardAddress: BluetoothAddress? = null

    override var status: BoardManager.Status = BoardManager.Status.Disconnected
    override var board: FeatureBoard? = null

    init {
        sessionManager.addBoardSessionListener(this)
    }

    override fun onBoardSessionCreated(address: BoardAddress) {
        LOG.d("onBoardSessionCreated: $address")

        configuredBoardAddress = BluetoothAddress(address)

        if (lifecycle.isResumed) {
            pauseWithCause(null)
            performResume()
        }

        lifecycle.addLifecycleListener(this)
        bluetooth.addBluetoothListener(this)
    }

    override fun onBoardSessionDestroyed() {
        LOG.d("onBoardSessionDestroyed")

        configuredBoardAddress = null
        pauseWithCause(null)

        lifecycle.removeLifecycleListener(this)
        bluetooth.removeBluetoothListener(this)
        sessionManager.removeBoardSessionListener(this)
    }

    override fun addBoardListener(listener: BoardListener) {
        if (listeners.addIfAbsent(listener)) {
            when (status) {
                BoardManager.Status.Connecting -> listener.onConnecting()
                BoardManager.Status.Connected -> board?.let { listener.onConnected(it) }
                BoardManager.Status.Disconnected -> listener.onDisconnected(board, null)
            }
        }
    }

    override fun removeBoardListener(listener: BoardListener?) {
        listeners.remove(listener)
    }

    override fun reconnect() {
        LOG.d("reconnect")

        if (lifecycle.isResumed) {
            pauseWithCause(null)
            performResume()
        }
    }

    override fun onLifecycleResume() {
        performResume()
    }

    override fun onLifecyclePause() {
        pauseWithCause(null)
    }

    override fun onBluetoothStatusChanged(status: BluetoothStatus) {
        if (status == BluetoothStatus.Enabled) {
            performResume()
        }
    }

    override fun onFeatureManagerResumed() {
        LOG.d("onFeatureManagerResumed")

        try {
            val address = configuredBoardAddress ?: throw IllegalStateException("Board not configured")
            val newBoard = FeatureBoard(featureManager, address)
            board = newBoard
            status = BoardManager.Status.Connected
            listeners.forEach { it.onConnected(newBoard) }
        } catch (exception: Exception) {
            pauseWithCause(exception)
        }
    }

    override fun onFeatureManagerPaused(cause: Throwable?) {
        pauseWithCause(cause)
    }

    private fun performResume() {
        val address = configuredBoardAddress ?: return

        if (resume.consume()) {
            LOG.d("performResume: resuming...")

            status = BoardManager.Status.Connecting
            listeners.forEach { it.onConnecting() }

            featureManager.resume(address)
            featureManager.addLifecycleListener(this)
        } else {
            LOG.d("performResume: already resumed")
        }
    }

    private fun pauseWithCause(cause: Throwable?) {
        if (cause != null) LOG.e("pauseWithCause: ${cause.message}", cause)
        else LOG.d("pauseWithCause: no cause")

        if (resume.isConsumed && pause.consume()) {

            val previousBoard = board

            board = null
            status = BoardManager.Status.Disconnected
            listeners.forEach { it.onDisconnected(previousBoard, cause) }

            featureManager.pause()
            featureManager.removeLifecycleListener(this)

            resume.reset()
            pause.reset()
        }
    }
}