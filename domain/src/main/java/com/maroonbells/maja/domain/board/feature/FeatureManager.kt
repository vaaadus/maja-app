package com.maroonbells.maja.domain.board.feature

import com.maroonbells.maja.common.observable.Observable
import com.maroonbells.maja.domain.board.feature.name.FeatureClassPath
import com.maroonbells.maja.domain.board.feature.registry.Feature
import com.maroonbells.maja.domain.board.value.BoardAddress
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

interface FeatureManager<Address : BoardAddress> {

    interface LifecycleListener {

        fun onFeatureManagerResumed()

        fun onFeatureManagerPaused(cause: Throwable?)
    }

    val scope: CoroutineScope

    fun resume(address: Address)

    fun pause()

    fun addLifecycleListener(listener: LifecycleListener)

    fun removeLifecycleListener(listener: LifecycleListener?)

    fun <T : Feature> getFeature(path: FeatureClassPath<T>): Observable<T?>

    fun <T : Feature> refreshFeature(path: FeatureClassPath<T>): Job
}