package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.common.observable.CompositeDisposable
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.BoardManager.BoardListener
import com.maroonbells.maja.domain.board.logic.RealTimeClockManager.DatetimeListener
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.model.RealTimeClock
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

class MajaRealTimeClockManager(private val boardManager: BoardManager) : RealTimeClockManager, BoardListener {

    private val listeners = CopyOnWriteArrayList<DatetimeListener>()
    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())
    private val disposeOnDisconnected = CompositeDisposable()

    private val board: Board?
        get() = boardManager.board

    private val realTimeClock: RealTimeClock?
        get() = board?.realTimeClock

    override val datetime: Date?
        get() = realTimeClock?.datetime

    init {
        boardManager.addBoardListener(this)
    }

    override fun addDatetimeListener(listener: DatetimeListener) {
        if (listeners.addIfAbsent(listener)) {
            listener.onDatetimeChanged(datetime)
        }
    }

    override fun removeDatetimeListener(listener: DatetimeListener?) {
        listeners.remove(listener)
    }

    override fun synchronize(): Job = scope.launch {
        val rtc = requireNotNull(realTimeClock)
        rtc.synchronize().join()
    }

    override fun onConnecting() = Unit

    override fun onConnected(board: Board) {
        disposeOnDisconnected += board.realTimeClock.onChanged {
            dispatchDatetimeChanged(it.datetime)
        }
    }

    override fun onDisconnected(board: Board?, cause: Throwable?) {
        scope.coroutineContext.cancelChildren()
        disposeOnDisconnected.dispose()
        dispatchDatetimeChanged(null)
    }

    private fun dispatchDatetimeChanged(datetime: Date?) {
        for (listener in listeners) {
            listener.onDatetimeChanged(datetime)
        }
    }
}