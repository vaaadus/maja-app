package com.maroonbells.maja.domain.demo

import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon

class DemoResources {

    val boardName = "Demo Mode"

    val pinout = listOf(
        CustomizedPin(
            id = PinId(0),
            customName = "Heating",
            defaultName = "0",
            type = Pin.Type.Digital,
            value = PinValue.High,
            icon = Icon.Heating,
            accessibleFromHome = true
        ),
        CustomizedPin(
            id = PinId(1),
            customName = "Bell",
            defaultName = "1",
            type = Pin.Type.Digital,
            value = PinValue.Low,
            icon = Icon.Bell,
            accessibleFromHome = true
        ),
        CustomizedPin(
            id = PinId(2),
            customName = "Alarm",
            defaultName = "2",
            type = Pin.Type.Digital,
            value = PinValue.Low,
            icon = Icon.Alarm,
            accessibleFromHome = false
        ),
        CustomizedPin(
            id = PinId(3),
            customName = "Lights",
            defaultName = "3",
            type = Pin.Type.DigitalPwm,
            value = PinValue.fromDutyCycle(77),
            icon = Icon.Light,
            accessibleFromHome = true
        ),
        CustomizedPin(
            id = PinId(4),
            customName = "GamePad",
            defaultName = "4",
            type = Pin.Type.DigitalPwm,
            value = PinValue.Low,
            icon = Icon.GamePad,
            accessibleFromHome = false
        )
    )

    val actions = listOf(
        CustomizedAction(
            id = ActionId(1),
            steps = listOf(
                CustomizedActionStep(
                    pinId = PinId(1),
                    pinValue = PinValue.High,
                    pinName = "1",
                    pinType = Pin.Type.Digital
                ),
                CustomizedActionStep(
                    pinId = PinId(3),
                    pinValue = PinValue.fromDutyCycle(69),
                    pinName = "3",
                    pinType = Pin.Type.DigitalPwm
                )
            ),
            name = "Water the lawn",
            customName = "Water the lawn",
            icon = Icon.Pan,
            color = Color.Blue,
            availableAsScene = true
        )
    )
}