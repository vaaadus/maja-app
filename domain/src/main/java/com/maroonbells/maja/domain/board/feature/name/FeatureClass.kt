package com.maroonbells.maja.domain.board.feature.name

import com.maroonbells.maja.domain.board.feature.registry.Feature
import kotlin.reflect.KClass

interface FeatureClass<T : Feature> {

    val raw: KClass<T>

    fun matches(path: FeaturePath): Boolean
}