package com.maroonbells.maja.domain.board.feature.registry

import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.Scene
import com.maroonbells.maja.domain.board.value.SceneId
import kotlinx.coroutines.Job
import java.util.*

interface ScenesFeature : Feature {

    val scenes: List<Scene>
    val maxScenes: Int

    fun create(actionId: ActionId): Job

    fun update(scene: Scene): Job

    fun remove(id: SceneId): Job
}