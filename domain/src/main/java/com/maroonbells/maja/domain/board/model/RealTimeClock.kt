package com.maroonbells.maja.domain.board.model

import kotlinx.coroutines.Job
import java.util.*

interface RealTimeClock : Node<RealTimeClock> {

    val datetime: Date

    fun synchronize(): Job
}