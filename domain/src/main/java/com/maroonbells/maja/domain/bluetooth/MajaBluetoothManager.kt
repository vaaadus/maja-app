package com.maroonbells.maja.domain.bluetooth

import com.maroonbells.maja.common.logger.Logger
import com.maroonbells.maja.domain.bluetooth.BluetoothManager.*
import com.maroonbells.maja.domain.bluetooth.repository.Bluetooth
import com.maroonbells.maja.domain.bluetooth.repository.BluetoothLiveSocket
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice
import kotlinx.coroutines.Job
import java.util.concurrent.CopyOnWriteArrayList

class MajaBluetoothManager(private val bluetooth: Bluetooth) : BluetoothManager,
    StatusListener, BluetoothLiveSocket.BluetoothSocketListener {

    companion object {
        private val LOG = Logger.forClass(MajaBluetoothManager::class.java)
    }

    private val bluetoothListeners = CopyOnWriteArrayList<StatusListener>()
    private val connectionListeners = CopyOnWriteArrayList<ConnectionListener>()
    private val liveUpdateListeners = CopyOnWriteArrayList<LiveUpdateListener>()
    private var connection: BluetoothLiveSocket? = null

    init {
        bluetooth.addBluetoothListener(this)
    }

    override val status: BluetoothStatus
        get() = bluetooth.status

    override var connectionStatus: ConnectionStatus = ConnectionStatus.Disconnected
        private set

    override val pairedDevices: List<BluetoothDevice>
        get() = bluetooth.pairedDevices

    override fun addBluetoothListener(listener: StatusListener) {
        if (bluetoothListeners.addIfAbsent(listener)) {
            listener.onBluetoothStatusChanged(status)
        }
    }

    override fun removeBluetoothListener(listener: StatusListener?) {
        bluetoothListeners.remove(listener)
    }

    override fun addConnectionListener(listener: ConnectionListener) {
        if (connectionListeners.addIfAbsent(listener)) {
            when (connectionStatus) {
                ConnectionStatus.Connecting -> {
                    connection?.address?.let {
                        listener.onBluetoothConnecting(it)
                    }
                }
                ConnectionStatus.Connected -> {
                    connection?.let {
                        listener.onBluetoothConnected(it.address)
                    }
                }
                ConnectionStatus.Disconnected -> {
                    listener.onBluetoothDisconnected(connection?.address, null)
                }
            }
        }
    }

    override fun removeConnectionListener(listener: ConnectionListener?) {
        connectionListeners.remove(listener)
    }

    override fun addLiveUpdateListener(listener: LiveUpdateListener) {
        liveUpdateListeners.addIfAbsent(listener)
    }

    override fun removeLiveUpdateListener(listener: LiveUpdateListener?) {
        liveUpdateListeners.remove(listener)
    }

    override fun enable() {
        bluetooth.enable()
    }

    override fun disable() {
        bluetooth.disable()
    }

    override fun startDiscovery(listener: DiscoveryListener) {
        bluetooth.startDiscovery(listener)
    }

    override fun stopDiscovery() {
        bluetooth.stopDiscovery()
    }

    override fun connect(address: BluetoothAddress) {
        stopDiscovery()

        when (connectionStatus) {
            ConnectionStatus.Connected, ConnectionStatus.Connecting -> {
                if (connection?.address != address) {
                    disconnectWithCause(null)
                    initConnection(address)
                }
            }
            ConnectionStatus.Disconnected -> {
                initConnection(address)
            }
        }
    }

    override fun disconnect() {
        disconnectWithCause(null)
    }

    override fun sendMessage(message: String): Job {
        if (connectionStatus != ConnectionStatus.Connected) {
            LOG.w("Bluetooth not connected, skipping message: $message")
            return Job()
        }

        LOG.d("sendMessage: $message")

        return connection?.send(message) ?: Job()
    }

    override fun sendBytes(bytes: ByteArray): Job {
        if (connectionStatus != ConnectionStatus.Connected) {
            LOG.w("Bluetooth not connected, skipping bytes.size(): ${bytes.size}")
            return Job()
        }

        LOG.d("sendBytes: $bytes")

        return connection?.send(bytes) ?: Job()
    }

    override fun onBluetoothStatusChanged(status: BluetoothStatus) {
        bluetoothListeners.forEach { it.onBluetoothStatusChanged(status) }
    }

    override fun onSocketOpen(socket: BluetoothLiveSocket) {
        LOG.d("onSocketOpen: ${socket.address}")

        connectionStatus = ConnectionStatus.Connected
        dispatchConnected(socket.address)
    }

    override fun onSocketMessage(socket: BluetoothLiveSocket, message: String) {
        LOG.d("onSocketMessage: $message")

        dispatchMessage(socket.address, message)
    }

    override fun onSocketClose(socket: BluetoothLiveSocket, cause: Throwable?) {
        LOG.d("onSocketClose: ${socket.address} with cause: $cause")

        disconnectWithCause(cause)
    }

    private fun initConnection(address: BluetoothAddress) {
        connectionStatus = ConnectionStatus.Connecting
        dispatchConnecting(address)

        connection = bluetooth.createLiveSocket(address)
        connection?.open()
        connection?.addListener(this)
    }

    private fun disconnectWithCause(cause: Throwable?) {
        val previousConnection = connection

        connection = null
        connectionStatus = ConnectionStatus.Disconnected

        previousConnection?.let { connection ->
            connection.removeListener(this)
            connection.close()
            dispatchDisconnected(connection.address, cause)
        }
    }

    private fun dispatchConnecting(address: BluetoothAddress) {
        connectionListeners.forEach { it.onBluetoothConnecting(address) }
    }

    private fun dispatchConnected(address: BluetoothAddress) {
        connectionListeners.forEach { it.onBluetoothConnected(address) }
    }

    private fun dispatchMessage(address: BluetoothAddress, message: String) {
        liveUpdateListeners.forEach { it.onBluetoothMessage(address, message) }
    }

    private fun dispatchDisconnected(address: BluetoothAddress, cause: Throwable?) {
        connectionListeners.forEach { it.onBluetoothDisconnected(address, cause) }
    }
}