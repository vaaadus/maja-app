package com.maroonbells.maja.domain.bluetooth.repository

import com.maroonbells.maja.domain.bluetooth.BluetoothManager
import com.maroonbells.maja.domain.bluetooth.value.BluetoothAddress
import com.maroonbells.maja.domain.bluetooth.value.BluetoothDevice

interface Bluetooth {

    val status: BluetoothManager.BluetoothStatus

    val pairedDevices: List<BluetoothDevice>

    fun addBluetoothListener(listener: BluetoothManager.StatusListener)

    fun removeBluetoothListener(listener: BluetoothManager.StatusListener?)

    fun enable()

    fun disable()

    fun startDiscovery(listener: BluetoothManager.DiscoveryListener)

    fun stopDiscovery()

    /**
     * Creates a new live socket channel or returns already created if available.
     */
    fun createLiveSocket(address: BluetoothAddress): BluetoothLiveSocket
}