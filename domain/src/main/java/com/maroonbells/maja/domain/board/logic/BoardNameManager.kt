package com.maroonbells.maja.domain.board.logic

import kotlinx.coroutines.Job

/**
 * Manages the board name.
 */
interface BoardNameManager {

    interface BoardNameListener {

        fun onBoardNameChanged(name: String)
    }

    fun addBoardNameListener(listener: BoardNameListener)

    fun removeBoardNameListener(listener: BoardNameListener?)

    val name: String

    fun setBoardName(name: String?): Job
}