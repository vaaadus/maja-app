package com.maroonbells.maja.domain.board.value

import java.util.*

data class Scene(val id: SceneId, val actionId: ActionId, val activeTill: Date)

inline class SceneId(val raw: Int) {
    override fun toString(): String = raw.toString()
}