package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import kotlinx.coroutines.Job

/**
 * Manages customizable [Action]s.
 *
 * Clients of the model should prefer to use this overlay instead of raw Actions node.
 */
interface ActionManager {

    interface CustomizedActionsListener {

        fun onCustomizedActionsChanged(newActions: List<CustomizedAction>)
    }

    val list: List<CustomizedAction>
    val maxActions: Int
    val maxStepsPerAction: Int
    val canAddActions: Boolean
    val defaultIcon: Icon
    val defaultColor: Color

    val availableIcons: List<Icon>
    val availableColors: List<Color>

    fun addActionsListener(listener: CustomizedActionsListener)

    fun removeActionsListener(listener: CustomizedActionsListener?)

    fun create(request: UpdateCustomizedActionRequest): Job

    fun update(id: ActionId, request: UpdateCustomizedActionRequest): Job

    fun remove(id: ActionId): Job

    operator fun get(actionId: ActionId?): CustomizedAction?

    fun asCustomizedActionStep(step: Action.Step): CustomizedActionStep?
}