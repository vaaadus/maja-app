package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.domain.board.value.CustomizedPin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import com.maroonbells.maja.domain.customization.value.Icon
import kotlinx.coroutines.Job

interface PinoutManager {

    interface CustomizedPinoutListener {

        fun onCustomizedPinoutChanged(newPins: List<CustomizedPin>)
    }

    val pins: List<CustomizedPin>

    fun addPinoutListener(listener: CustomizedPinoutListener)

    fun removePinoutListener(listener: CustomizedPinoutListener?)

    fun setState(id: PinId, value: PinValue): Job

    fun setName(id: PinId, name: String?): Job

    fun setIcon(id: PinId, icon: Icon): Job

    fun setAccessibleFromHome(id: PinId, accessible: Boolean): Job

    operator fun get(id: PinId?): CustomizedPin?
}