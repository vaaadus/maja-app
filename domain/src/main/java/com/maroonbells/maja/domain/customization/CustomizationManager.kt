package com.maroonbells.maja.domain.customization

import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon

/**
 * Stores/restores user personal settings, such as icons, names, etc...
 */
interface CustomizationManager {

    companion object {
        val DEFAULT_PIN_ICON = Icon.Blinds
        val DEFAULT_ACTION_ICON = Icon.Blinds
        val DEFAULT_ACTION_COLOR = Color.Red
    }

    interface CustomizationsListener {

        /**
         * Called when personal settings are changed.
         */
        fun onCustomizationsChanged()
    }

    fun addCustomizationsListener(listener: CustomizationsListener)

    fun removeCustomizationsListener(listener: CustomizationsListener?)

    /**
     * Lists all available icons.
     */
    val availableIcons: List<Icon>

    /**
     * Lists all available colors.
     */
    val availableColors: List<Color>

    /**
     * Returns a board name (if present) or default value.
     */
    fun getName(address: BoardAddress): String

    /**
     * Returns a custom name (if present) or null for board.
     */
    fun getCustomName(address: BoardAddress): String?

    /**
     * Sets a custom name for board.
     */
    fun setCustomName(address: BoardAddress, name: String?)

    /**
     * Returns a custom name (if present) or default name for pin.
     */
    fun getName(address: BoardAddress, pin: Pin): String

    /**
     * Returns a custom name or null for pin.
     */
    fun getCustomName(address: BoardAddress, pinId: PinId): String?

    /**
     * Sets a custom name for pin.
     */
    fun setCustomName(address: BoardAddress, pinId: PinId, name: String?)

    /**
     * Returns a custom icon (if present) or default icon for pin.
     */
    fun getIcon(address: BoardAddress, pinId: PinId): Icon

    /**
     * Returns a custom icon or null for pin.
     */
    fun getCustomIcon(address: BoardAddress, pinId: PinId): Icon?

    /**
     * Sets a custom icon for pin.
     */
    fun setCustomIcon(address: BoardAddress, pinId: PinId, icon: Icon)

    /**
     * Whether a pin is configured to be accessible from home.
     */
    fun isAccessibleFromHome(address: BoardAddress, pinId: PinId): Boolean

    /**
     * Sets whether a pin is accessible from home.
     */
    fun setAccessibleFromHome(address: BoardAddress, pinId: PinId, accessible: Boolean)

    /**
     * Returns a custom or default name for action.
     */
    fun getName(address: BoardAddress, actionId: ActionId): String

    /**
     * Returns a custom name or null for action.
     */
    fun getCustomName(address: BoardAddress, actionId: ActionId): String?

    /**
     * Sets a custom name for action.
     */
    fun setCustomName(address: BoardAddress, actionId: ActionId, name: String?)

    /**
     * Returns a custom icon (if present) or default icon for action.
     */
    fun getIcon(address: BoardAddress, actionId: ActionId): Icon

    /**
     * Returns a custom icon for action.
     */
    fun getCustomIcon(address: BoardAddress, actionId: ActionId): Icon?

    /**
     * Sets a custom icon for action.
     */
    fun setCustomIcon(address: BoardAddress, actionId: ActionId, icon: Icon)

    /**
     * Returns a custom color (if present) or default color for action.
     */
    fun getColor(address: BoardAddress, actionId: ActionId): Color

    /**
     * Returns a custom color for action.
     */
    fun getCustomColor(address: BoardAddress, actionId: ActionId): Color?

    /**
     * Sets a custom color for action.
     */
    fun setCustomColor(address: BoardAddress, actionId: ActionId, color: Color)

    /**
     * Whether action is available as scene.
     */
    fun isAvailableAsScene(address: BoardAddress, actionId: ActionId): Boolean

    /**
     * Sets whether action is available as scene.
     */
    fun setAvailableAsScene(address: BoardAddress, actionId: ActionId, available: Boolean)
}