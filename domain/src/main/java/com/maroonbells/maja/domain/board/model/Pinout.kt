package com.maroonbells.maja.domain.board.model

import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.board.value.PinValue
import kotlinx.coroutines.Job

interface Pinout : Node<Pinout> {

    val pins: List<Pin>

    fun setState(id: PinId, value: PinValue): Job

    operator fun get(id: PinId?): Pin?
}