package com.maroonbells.maja.domain.board.value.datetime

import java.util.regex.Pattern

data class Time(val hour: Int, val minute: Int) : Comparable<Time> {

    override fun compareTo(other: Time): Int {
        return compareValuesBy(this, other, Time::hour, Time::minute)
    }

    override fun toString(): String = String.format("%d:%02d", hour, minute)

    companion object {
        val MIN = Time(0, 0)
        val MAX = Time(24, 0)
        private val pattern = Pattern.compile("([0-9]{1,2}):([0-9]{1,2})")

        fun parse(string: String): Time {
            val matcher = pattern.matcher(string)

            require(matcher.find()) { "Cannot parse $string as Time" }

            return Time(
                hour = matcher.group(1).toInt(),
                minute = matcher.group(2).toInt())
        }
    }
}