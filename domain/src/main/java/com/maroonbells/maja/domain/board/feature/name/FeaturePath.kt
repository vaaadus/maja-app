package com.maroonbells.maja.domain.board.feature.name

inline class FeaturePath(val raw: String) {

    fun matches(regex: Regex): Boolean {
        return regex.matches(raw)
    }
}