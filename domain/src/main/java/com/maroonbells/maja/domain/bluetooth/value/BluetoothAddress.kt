package com.maroonbells.maja.domain.bluetooth.value

import com.maroonbells.maja.domain.board.value.BoardAddress

@Suppress("FunctionName")
fun BluetoothAddress(address: BoardAddress): BluetoothAddress {
    return if (address is BluetoothAddress) address
    else BluetoothAddress(address.raw)
}

data class BluetoothAddress(override val raw: String) : BoardAddress