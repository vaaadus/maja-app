package com.maroonbells.maja.domain.board.model

import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.ActionId
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job

interface Actions : Node<Actions> {

    val list: List<Action>
    val maxActions: Int
    val maxStepsPerAction: Int
    val canAddActions: Boolean

    fun createAsync(steps: List<Action.Step>): Deferred<ActionId>

    fun update(id: ActionId, steps: List<Action.Step>): Job

    fun remove(id: ActionId): Job

    operator fun get(actionId: ActionId?): Action?
}