package com.maroonbells.maja.domain.board.model

import com.maroonbells.maja.domain.board.value.schedule.ScheduleEntries
import kotlinx.coroutines.Job

interface Schedule : Node<Schedule> {

    val id: ScheduleId

    val entries: ScheduleEntries

    fun setEntries(entries: ScheduleEntries): Job
}

inline class ScheduleId(val raw: Int) {
    override fun toString(): String = raw.toString()
}