package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.common.extensions.copy
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.model.Scenes
import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.Scene
import com.maroonbells.maja.domain.board.value.SceneId
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class FeatureScenes(features: FeatureManager<*>) : FeatureNode<Scenes>(features), Scenes {

    override val list: List<Scene>
        get() = mutableList.copy()

    override var maxScenes: Int = 0

    private val mutableList = mutableListOf<Scene>()

    init {
        board.scenes.onNext {
            maxScenes = it.maxScenes

            mutableList.clear()
            mutableList.addAll(it.scenes)

            dispatchChange()
        }
    }

    override fun create(actionId: ActionId): Job = scope.launch {
        board.scenes.require().create(actionId).join()
        board.scenes.refresh().join()
    }

    override fun update(scene: Scene): Job = scope.launch {
        board.scenes.require().update(scene).join()
        board.scenes.refresh().join()
    }

    override fun remove(id: SceneId): Job = scope.launch {
        board.scenes.require().remove(id).join()
        board.scenes.refresh().join()
    }

}