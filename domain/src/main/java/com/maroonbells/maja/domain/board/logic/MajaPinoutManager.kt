package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.common.observable.CompositeDisposable
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.BoardManager.BoardListener
import com.maroonbells.maja.domain.board.logic.PinoutManager.CustomizedPinoutListener
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.model.Pinout
import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.CustomizationManager.CustomizationsListener
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener
import kotlinx.coroutines.*
import java.util.concurrent.CopyOnWriteArrayList

class MajaPinoutManager(
    private val sessionManager: BoardSessionManager,
    private val boardManager: BoardManager,
    private val customizationManager: CustomizationManager
) : PinoutManager, BoardSessionListener, BoardListener, CustomizationsListener {

    private val listeners = CopyOnWriteArrayList<CustomizedPinoutListener>()
    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())
    private val disposeOnDisconnected = CompositeDisposable()

    private val board: Board?
        get() = boardManager.board

    private val pinout: Pinout?
        get() = board?.pinout

    override val pins: List<CustomizedPin>
        get() = asCustomizedPins(pinout?.pins ?: emptyList())

    init {
        sessionManager.addBoardSessionListener(this)
    }

    override fun onBoardSessionCreated(address: BoardAddress) {
        boardManager.addBoardListener(this)
        customizationManager.addCustomizationsListener(this)
    }

    override fun onBoardSessionDestroyed() {
        boardManager.removeBoardListener(this)
        customizationManager.removeCustomizationsListener(this)
        sessionManager.removeBoardSessionListener(this)
    }

    override fun addPinoutListener(listener: CustomizedPinoutListener) {
        if (listeners.addIfAbsent(listener)) {
            listener.onCustomizedPinoutChanged(pins)
        }
    }

    override fun removePinoutListener(listener: CustomizedPinoutListener?) {
        listeners.remove(listener)
    }

    override fun setState(id: PinId, value: PinValue): Job = scope.launch {
        val pinout = requireNotNull(pinout)
        pinout.setState(id, value).join()
    }

    override fun setName(id: PinId, name: String?): Job = scope.launch {
        val board = requireNotNull(board)
        customizationManager.setCustomName(board.address, id, name)
    }

    override fun setIcon(id: PinId, icon: Icon): Job = scope.launch {
        val board = requireNotNull(board)
        customizationManager.setCustomIcon(board.address, id, icon)
    }

    override fun setAccessibleFromHome(id: PinId, accessible: Boolean) = scope.launch {
        val board = requireNotNull(board)
        customizationManager.setAccessibleFromHome(board.address, id, accessible)
    }

    override fun get(id: PinId?): CustomizedPin? {
        return pins.firstOrNull { it.id == id }
    }

    override fun onConnecting() = Unit

    override fun onConnected(board: Board) {
        disposeOnDisconnected += board.pinout.onChanged {
            dispatchPinsChanged(pins)
        }
    }

    override fun onDisconnected(board: Board?, cause: Throwable?) {
        scope.coroutineContext.cancelChildren()
        disposeOnDisconnected.dispose()
        dispatchPinsChanged(pins)
    }

    override fun onCustomizationsChanged() {
        dispatchPinsChanged(pins)
    }

    private fun asCustomizedPins(pins: List<Pin>): List<CustomizedPin> {
        val board = board ?: return emptyList()

        return pins.map { pin ->
            CustomizedPin(
                id = pin.id,
                value = pin.value,
                type = pin.type,
                defaultName = pin.name,
                customName = customizationManager.getCustomName(board.address, pin.id),
                icon = customizationManager.getIcon(board.address, pin.id),
                accessibleFromHome = customizationManager.isAccessibleFromHome(board.address, pin.id))
        }
    }


    private fun dispatchPinsChanged(pins: List<CustomizedPin>) {
        for (listener in listeners) {
            listener.onCustomizedPinoutChanged(pins)
        }
    }
}