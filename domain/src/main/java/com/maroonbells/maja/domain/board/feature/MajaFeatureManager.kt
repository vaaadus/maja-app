package com.maroonbells.maja.domain.board.feature

import com.maroonbells.maja.common.consumable.Consumable
import com.maroonbells.maja.common.observable.MutableObservable
import com.maroonbells.maja.common.observable.Observable
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.feature.exception.FeatureNotDefinedException
import com.maroonbells.maja.domain.board.feature.name.FeatureClassPath
import com.maroonbells.maja.domain.board.feature.registry.Feature
import com.maroonbells.maja.domain.board.feature.repository.FeatureChannel
import com.maroonbells.maja.domain.board.value.BoardAddress
import kotlinx.coroutines.*
import java.util.concurrent.CopyOnWriteArrayList

class MajaFeatureManager<Address : BoardAddress>(
    private val featureChannel: FeatureChannel<Address>
) : FeatureManager<Address>, FeatureChannel.ChannelListener {

    private val observableFeatures = mutableMapOf<FeatureClassPath<*>, MutableObservable<out Feature?>>()
    private val listeners = CopyOnWriteArrayList<FeatureManager.LifecycleListener>()
    private val resume = Consumable()
    private val pause = Consumable()
    private var isResumed = false

    override val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    override fun addLifecycleListener(listener: FeatureManager.LifecycleListener) {
        if (listeners.addIfAbsent(listener)) {
            if (isResumed) listener.onFeatureManagerResumed()
        }
    }

    override fun removeLifecycleListener(listener: FeatureManager.LifecycleListener?) {
        listeners.remove(listener)
    }

    override fun resume(address: Address) {
        if (resume.consume()) {
            featureChannel.resume(address)
            featureChannel.addChannelListener(this)
        } else {
            pauseWithCause(IllegalStateException("FeatureManager was already resumed"))
        }
    }

    override fun pause() {
        pauseWithCause(null)
    }

    override fun <T : Feature> getFeature(path: FeatureClassPath<T>): Observable<T?> {
        return path.obtainObservable()
    }

    override fun <T : Feature> refreshFeature(path: FeatureClassPath<T>): Job {
        return scope.launch {
            try {
                path.pull()
            } catch (e: Exception) {
                pauseWithCause(e)
            }
        }
    }

    override fun onFeatureChannelResumed() {
        scope.launch {
            try {
                pullAllFeatures()
                dispatchResumed()
                refreshFeaturesPeriodicallyBlocking()
            } catch (e: Exception) {
                pauseWithCause(e)
            }
        }
    }

    override fun onFeatureChannelPaused(cause: Throwable?) {
        pauseWithCause(cause)
    }

    private fun pauseWithCause(cause: Throwable?) {
        if (resume.isConsumed && pause.consume()) {
            featureChannel.pause()
            featureChannel.removeChannelListener(this)

            scope.coroutineContext.cancelChildren()
            observableFeatures.clear()

            isResumed = false
            listeners.forEach { it.onFeatureManagerPaused(cause) }

            resume.reset()
            pause.reset()
        }
    }

    @Suppress("UNCHECKED_CAST")
    @Synchronized
    private fun <T : Feature> FeatureClassPath<T>.obtainObservable(): MutableObservable<T?> {
        val observable = (observableFeatures[this] as? MutableObservable<T?>) ?: MutableObservable<T?>(null)
        observableFeatures[this] = observable
        return observable
    }

    private fun <T : Feature> FeatureClassPath<T>.require(): T {
        return obtainObservable().value ?: throw FeatureNotDefinedException("$path feature was not defined")
    }

    private suspend fun <T : Feature> FeatureClassPath<T>.pull(): T {
        val feature = featureChannel.pullFeature(this)
        obtainObservable().value = feature
        return feature
    }

    private suspend fun pullAllFeatures() {
        board.pinout.pull()
        board.rtc.pull()
        board.actions.pull()
        board.scenes.pull()

        val schedules = board.schedules.pull()
        for (schedule in schedules.list) {
            board.schedule.of(schedule).pull()
        }
    }

    private fun dispatchResumed() {
        isResumed = true
        listeners.forEach { it.onFeatureManagerResumed() }
    }

    private suspend fun CoroutineScope.refreshFeaturesPeriodicallyBlocking() {
        while (isActive) {
            delay(timeMillis = 1000L) // FIXME hardcoded
            board.rtc.pull()
        }
    }
}