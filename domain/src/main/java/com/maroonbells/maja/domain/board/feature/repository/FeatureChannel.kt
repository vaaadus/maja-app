package com.maroonbells.maja.domain.board.feature.repository

import com.maroonbells.maja.domain.board.feature.name.FeatureClassPath
import com.maroonbells.maja.domain.board.feature.registry.Feature
import com.maroonbells.maja.domain.board.value.BoardAddress

interface FeatureChannel<Address : BoardAddress> {

    interface ChannelListener {

        fun onFeatureChannelResumed()

        fun onFeatureChannelPaused(cause: Throwable?)
    }

    fun addChannelListener(listener: ChannelListener)

    fun removeChannelListener(listener: ChannelListener?)

    fun resume(address: Address)

    fun pause()

    suspend fun <T : Feature> pullFeature(featureClass: FeatureClassPath<T>): T
}