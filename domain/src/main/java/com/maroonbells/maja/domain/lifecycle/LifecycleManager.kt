package com.maroonbells.maja.domain.lifecycle

/**
 * Manages the app lifecycle. Notifies other managers, when to resume and when to pause.
 */
interface LifecycleManager {

    interface LifecycleListener {

        fun onLifecycleResume()

        fun onLifecyclePause()
    }

    val isResumed: Boolean

    fun addLifecycleListener(listener: LifecycleListener)

    fun removeLifecycleListener(listener: LifecycleListener?)

    fun resume()

    fun pause()
}