package com.maroonbells.maja.domain.demo.value

import com.maroonbells.maja.domain.board.value.BoardAddress

/**
 * Represents an address of a board in the demo mode.
 */
data class DemoAddress(override val raw: String = "Demo") : BoardAddress