package com.maroonbells.maja.domain.board.value

interface BoardAddress {
    val raw: String
}
