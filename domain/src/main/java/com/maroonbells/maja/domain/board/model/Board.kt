package com.maroonbells.maja.domain.board.model

import com.maroonbells.maja.domain.board.value.BoardAddress

interface Board : Node<Board> {

    val address: BoardAddress
    val pinout: Pinout
    val actions: Actions
    val realTimeClock: RealTimeClock
    val schedules: Schedules
    val scenes: Scenes
}