package com.maroonbells.maja.domain.lifecycle

import com.maroonbells.maja.domain.lifecycle.LifecycleManager.LifecycleListener
import kotlinx.coroutines.*
import java.util.concurrent.CopyOnWriteArrayList

class MajaLifecycleManager : LifecycleManager {

    companion object {
        const val DELAYED_PAUSE = 5000L
    }

    private val listeners = CopyOnWriteArrayList<LifecycleListener>()
    private var resumeCount = 0
    private var delayedPause: Job? = null

    override var isResumed: Boolean = false
        private set

    override fun resume() {
        unscheduleDelayedPause()

        if (incrementResumeCount() && !isResumed) {
            performResume()
        }
    }

    override fun pause() {
        if (decrementResumeCount() && isResumed) {
            scheduleDelayedPause()
        }
    }

    override fun addLifecycleListener(listener: LifecycleListener) {
        if (listeners.addIfAbsent(listener)) {
            notifyListener(listener, isResumed)
        }
    }

    override fun removeLifecycleListener(listener: LifecycleListener?) {
        listeners.remove(listener)
    }

    private fun notifyListener(listener: LifecycleListener, isResumed: Boolean) {
        if (isResumed) {
            listener.onLifecycleResume()
        } else {
            listener.onLifecyclePause()
        }
    }

    private fun incrementResumeCount(): Boolean {
        resumeCount++
        return resumeCount == 1
    }

    private fun decrementResumeCount(): Boolean {
        resumeCount--
        return resumeCount == 0
    }

    private fun scheduleDelayedPause() {
        if (delayedPause == null) {
            delayedPause = GlobalScope.launch(Dispatchers.Main) {
                delay(DELAYED_PAUSE)
                performPause()
            }
        }
    }

    private fun unscheduleDelayedPause() {
        delayedPause?.cancel()
        delayedPause = null
    }

    private fun performResume() {
        unscheduleDelayedPause()
        isResumed = true
        notifyResumed()
    }

    private fun performPause() {
        unscheduleDelayedPause()
        isResumed = false
        notifyPaused()
    }

    private fun notifyResumed() {
        listeners.forEach { it.onLifecycleResume() }
    }

    private fun notifyPaused() {
        listeners.forEach { it.onLifecyclePause() }
    }
}