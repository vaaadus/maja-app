package com.maroonbells.maja.domain.board.value

import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon

data class CustomizedAction(
    val id: ActionId,
    val steps: List<CustomizedActionStep>,
    val name: String,
    val customName: String?,
    val icon: Icon,
    val color: Color,
    val availableAsScene: Boolean) {

    fun asAction(): Action {
        return Action(
            id = id,
            steps = steps.map(CustomizedActionStep::asStep)
        )
    }
}

data class CustomizedActionStep(
    val pinId: PinId,
    val pinValue: PinValue,
    val pinName: String,
    val pinType: Pin.Type) {

    fun matches(actionStep: Action.Step): Boolean {
        return pinId == actionStep.pinId && pinValue == actionStep.pinValue
    }

    fun asStep(): Action.Step {
        return Action.Step(pinId, pinValue)
    }
}

data class UpdateCustomizedActionRequest(
    val steps: List<Action.Step>,
    val name: String?,
    val icon: Icon?,
    val color: Color?,
    val availableAsScene: Boolean?)

data class Action(val id: ActionId, val steps: List<Step>) {

    data class Step(val pinId: PinId, val pinValue: PinValue)
}


inline class ActionId(val raw: Int)