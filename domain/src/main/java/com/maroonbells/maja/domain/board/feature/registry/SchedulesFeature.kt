package com.maroonbells.maja.domain.board.feature.registry

import com.maroonbells.maja.domain.board.model.ScheduleId
import kotlinx.coroutines.Job

interface SchedulesFeature : Feature {

    val list: List<ScheduleId>
    val maxSchedules: Int
    val maxBlocksPerDay: Int

    fun createSchedule(): Job

    fun removeSchedule(id: ScheduleId): Job
}