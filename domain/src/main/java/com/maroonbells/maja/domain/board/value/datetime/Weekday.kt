package com.maroonbells.maja.domain.board.value.datetime

enum class Weekday(val raw: String) {

    Monday("mon"),
    Tuesday("tue"),
    Wednesday("wed"),
    Thursday("thu"),
    Friday("fri"),
    Saturday("sat"),
    Sunday("sun");

    override fun toString(): String = raw

    companion object {
        fun parse(string: String): Weekday {
            return values()
                .find { it.raw == string }
                ?: throw IllegalArgumentException("Cannot parse weekday from $string")
        }
    }
}