package com.maroonbells.maja.domain.board.feature.registry

import kotlinx.coroutines.Job
import java.util.*

interface RealTimeClockFeature : Feature {

    val datetime: Date

    fun setDateTime(date: Date): Job
}