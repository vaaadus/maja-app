package com.maroonbells.maja.domain.board.logic

import kotlinx.coroutines.Job
import java.util.*

/**
 * Manages the real time clock settings.
 */
interface RealTimeClockManager {

    interface DatetimeListener {

        fun onDatetimeChanged(date: Date?)
    }

    val datetime: Date?

    fun addDatetimeListener(listener: DatetimeListener)

    fun removeDatetimeListener(listener: DatetimeListener?)

    fun synchronize(): Job
}