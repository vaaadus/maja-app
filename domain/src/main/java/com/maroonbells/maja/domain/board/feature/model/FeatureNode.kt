package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.common.observable.Disposable
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.exception.FeatureNotDefinedException
import com.maroonbells.maja.domain.board.feature.name.FeatureClassPath
import com.maroonbells.maja.domain.board.feature.registry.Feature
import com.maroonbells.maja.domain.board.model.Node
import com.maroonbells.maja.domain.board.model.NodeListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import java.lang.ref.WeakReference
import java.util.concurrent.CopyOnWriteArrayList

class FeatureNodeDisposable<T : Node<T>>(
    node: FeatureNode<T>,
    listener: NodeListener<T>) : Disposable {

    private val nodeRef = WeakReference(node)
    private val listenerRef = WeakReference(listener)

    override fun dispose() {
        val node = nodeRef.get()
        val listener = listenerRef.get()

        if (node != null && listener != null) {
            node.removeListener(listener)
        }

        nodeRef.clear()
        listenerRef.clear()
    }

    override fun isDisposed(): Boolean {
        return nodeRef.get() == null && listenerRef.get() == null
    }
}

abstract class FeatureNode<T : Node<T>>(private val features: FeatureManager<*>) : Node<T> {

    private val listeners = CopyOnWriteArrayList<NodeListener<T>>()
    protected val scope: CoroutineScope = features.scope

    @Suppress("UNCHECKED_CAST")
    override fun onChanged(listener: NodeListener<T>): Disposable {
        if (listeners.addIfAbsent(listener)) {
            listener(this as T)
        }
        return FeatureNodeDisposable(this, listener)
    }

    fun <T : Feature> FeatureClassPath<T>.require(): T {
        return get() ?: throw FeatureNotDefinedException(toString())
    }

    fun <T : Feature> FeatureClassPath<T>.get(): T? {
        return features.getFeature(this).value
    }

    fun <T : Feature> FeatureClassPath<T>.isSupported(): Boolean {
        return get() != null
    }

    fun <T : Feature> FeatureClassPath<T>.onChanged(listener: (T?) -> Unit): Disposable {
        return features.getFeature(this).onChanged(listener)
    }

    fun <T : Feature> FeatureClassPath<T>.onNext(listener: (T) -> Unit): Disposable {
        return features.getFeature(this).onChanged {
            if (it != null) listener(it)
        }
    }

    fun <T : Feature> FeatureClassPath<T>.refresh(): Job {
        return features.refreshFeature(this)
    }

    fun removeListener(listener: NodeListener<T>?) {
        listeners.remove(listener)
    }

    @Suppress("UNCHECKED_CAST")
    fun dispatchChange() {
        listeners.forEach { it(this as T) }
    }
}