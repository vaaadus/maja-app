package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.model.RealTimeClock
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

class FeatureRealTimeClock(features: FeatureManager<*>) : FeatureNode<RealTimeClock>(features), RealTimeClock {

    override var datetime: Date = Date(0L)

    init {
        board.rtc.onNext {
            datetime = it.datetime
            dispatchChange()
        }
    }

    override fun synchronize(): Job = scope.launch {
        board.rtc.require().setDateTime(Date()).join()
    }
}