package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.common.extensions.copy
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.model.Schedule
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.domain.board.model.Schedules
import kotlinx.coroutines.launch

class FeatureSchedules(features: FeatureManager<*>) : FeatureNode<Schedules>(features), Schedules {

    override val list: List<Schedule>
        get() = mutableList.copy()

    override var maxSchedules: Int = 0
    override var maxBlocksPerDay: Int = 0

    private val mutableList = mutableListOf<Schedule>()

    init {
        board.schedules.onNext { feature ->
            maxSchedules = feature.maxSchedules
            maxBlocksPerDay = feature.maxBlocksPerDay

            mutableList.clear()
            mutableList.addAll(feature.list.map { FeatureSchedule(features, it) })

            dispatchChange()
        }
    }

    override fun createSchedule() = scope.launch {
        board.schedules.require().createSchedule().join()
        refreshSchedules()
    }

    override fun removeSchedule(id: ScheduleId) = scope.launch {
        board.schedules.require().removeSchedule(id).join()
        refreshSchedules()
    }

    private suspend fun refreshSchedules() {
        board.schedules.refresh().join()
        val schedules = board.schedules.require()
        for (id in schedules.list) {
            board.schedule.of(id).refresh().join()
        }
    }
}