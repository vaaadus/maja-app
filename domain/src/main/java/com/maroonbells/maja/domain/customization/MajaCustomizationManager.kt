package com.maroonbells.maja.domain.customization

import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.BoardAddress
import com.maroonbells.maja.domain.board.value.Pin
import com.maroonbells.maja.domain.board.value.PinId
import com.maroonbells.maja.domain.configuration.repository.Configuration
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_COLOR
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_ICON
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_PIN_ICON
import com.maroonbells.maja.domain.customization.CustomizationManager.CustomizationsListener
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.domain.demo.DemoResources
import com.maroonbells.maja.domain.demo.value.DemoAddress
import com.maroonbells.maja.domain.resources.ResourceLocator
import com.maroonbells.maja.domain.resources.ResourceLocator.Text
import java.util.concurrent.CopyOnWriteArrayList

class MajaCustomizationManager(
    private val configuration: Configuration,
    private val resourceLocator: ResourceLocator) : CustomizationManager {

    companion object {
        private const val PIN_NAME = "pin_name"
        private const val PIN_ICON = "pin_icon"
        private const val PIN_HOME = "pin_home"

        private const val BOARD_NAME = "board_name"
        private const val ACTION_NAME = "action_name"
        private const val ACTION_ICON = "action_icon"
        private const val ACTION_COLOR = "action_color"
        private const val ACTION_SCENE = "action_scene"
    }

    private val listeners = CopyOnWriteArrayList<CustomizationsListener>()

    init {
        loadDemoCustomizations()
    }

    override fun addCustomizationsListener(listener: CustomizationsListener) {
        if (listeners.addIfAbsent(listener)) {
            listener.onCustomizationsChanged()
        }
    }

    override fun removeCustomizationsListener(listener: CustomizationsListener?) {
        listeners.remove(listener)
    }

    override val availableIcons: List<Icon>
        get() = Icon.values().toList()

    override val availableColors: List<Color>
        get() = Color.values().toList()

    override fun getName(address: BoardAddress): String {
        return configuration.getString(keyOf(address, BOARD_NAME))
            ?: resourceLocator.getString(Text.BoardName)
    }

    override fun getCustomName(address: BoardAddress): String? {
        return configuration.getString(keyOf(address, BOARD_NAME))
    }

    override fun setCustomName(address: BoardAddress, name: String?) {
        if (name.isNullOrBlank()) configuration.clear(keyOf(address, BOARD_NAME))
        else configuration.putString(keyOf(address, BOARD_NAME), name)

        notifyCustomizationsChanged()
    }

    override fun getName(address: BoardAddress, pin: Pin): String {
        return configuration.getString(keyOf(address, pin.id, PIN_NAME), pin.name)
    }

    override fun getCustomName(address: BoardAddress, pinId: PinId): String? {
        return configuration.getString(keyOf(address, pinId, PIN_NAME))
    }

    override fun setCustomName(address: BoardAddress, pinId: PinId, name: String?) {
        if (name.isNullOrBlank()) configuration.clear(keyOf(address, pinId, PIN_NAME))
        else configuration.putString(keyOf(address, pinId, PIN_NAME), name)

        notifyCustomizationsChanged()
    }

    override fun getIcon(address: BoardAddress, pinId: PinId): Icon {
        val index = configuration.getInt(keyOf(address, pinId, PIN_ICON)) ?: return DEFAULT_PIN_ICON
        return Icon.values()[index]
    }

    override fun getCustomIcon(address: BoardAddress, pinId: PinId): Icon? {
        val index = configuration.getInt(keyOf(address, pinId, PIN_ICON)) ?: return null
        return Icon.values()[index]
    }

    override fun setCustomIcon(address: BoardAddress, pinId: PinId, icon: Icon) {
        configuration.putInt(keyOf(address, pinId, PIN_ICON), icon.ordinal)

        notifyCustomizationsChanged()
    }

    override fun isAccessibleFromHome(address: BoardAddress, pinId: PinId): Boolean {
        return configuration.getBoolean(keyOf(address, pinId, PIN_HOME), false)
    }

    override fun setAccessibleFromHome(address: BoardAddress, pinId: PinId, accessible: Boolean) {
        configuration.putBoolean(keyOf(address, pinId, PIN_HOME), accessible)

        notifyCustomizationsChanged()
    }

    override fun getName(address: BoardAddress, actionId: ActionId): String {
        return configuration.getString(keyOf(address, actionId, ACTION_NAME))
            ?: resourceLocator.getString(Text.UnknownActionName)
    }

    override fun getCustomName(address: BoardAddress, actionId: ActionId): String? {
        return configuration.getString(keyOf(address, actionId, ACTION_NAME))
    }

    override fun setCustomName(address: BoardAddress, actionId: ActionId, name: String?) {
        if (name.isNullOrBlank()) configuration.clear(keyOf(address, actionId, ACTION_NAME))
        else configuration.putString(keyOf(address, actionId, ACTION_NAME), name)

        notifyCustomizationsChanged()
    }

    override fun getIcon(address: BoardAddress, actionId: ActionId): Icon {
        val index = configuration.getInt(keyOf(address, actionId, ACTION_ICON)) ?: return DEFAULT_ACTION_ICON
        return Icon.values()[index]
    }

    override fun getCustomIcon(address: BoardAddress, actionId: ActionId): Icon? {
        val index = configuration.getInt(keyOf(address, actionId, ACTION_ICON)) ?: return null
        return Icon.values()[index]
    }

    override fun setCustomIcon(address: BoardAddress, actionId: ActionId, icon: Icon) {
        configuration.putInt(keyOf(address, actionId, ACTION_ICON), icon.ordinal)

        notifyCustomizationsChanged()
    }

    override fun getColor(address: BoardAddress, actionId: ActionId): Color {
        val index = configuration.getInt(keyOf(address, actionId, ACTION_COLOR)) ?: return DEFAULT_ACTION_COLOR
        return Color.values()[index]
    }

    override fun getCustomColor(address: BoardAddress, actionId: ActionId): Color? {
        val index = configuration.getInt(keyOf(address, actionId, ACTION_COLOR)) ?: return null
        return Color.values()[index]
    }

    override fun setCustomColor(address: BoardAddress, actionId: ActionId, color: Color) {
        configuration.putInt(keyOf(address, actionId, ACTION_COLOR), color.ordinal)

        notifyCustomizationsChanged()
    }

    override fun isAvailableAsScene(address: BoardAddress, actionId: ActionId): Boolean {
        return configuration.getBoolean(keyOf(address, actionId, ACTION_SCENE), false)
    }

    override fun setAvailableAsScene(address: BoardAddress, actionId: ActionId, available: Boolean) {
        configuration.putBoolean(keyOf(address, actionId, ACTION_SCENE), available)

        notifyCustomizationsChanged()
    }

    private fun loadDemoCustomizations() {
        val demo = DemoResources()
        val address = DemoAddress()

        setCustomName(address, demo.boardName)

        for (pin in demo.pinout) {
            setCustomName(address, pin.id, pin.customName)
            setCustomIcon(address, pin.id, pin.icon)
            setAccessibleFromHome(address, pin.id, pin.accessibleFromHome)
        }

        for (action in demo.actions) {
            setCustomName(address, action.id, action.customName)
            setCustomIcon(address, action.id, action.icon)
            setCustomColor(address, action.id, action.color)
            setAvailableAsScene(address, action.id, action.availableAsScene)
        }
    }

    private fun keyOf(address: BoardAddress, settingName: String): String {
        return "${address.raw}-$settingName"
    }

    private fun keyOf(address: BoardAddress, pinId: PinId, settingName: String): String {
        return "${address.raw}-pin-${pinId.raw}-$settingName"
    }

    private fun keyOf(address: BoardAddress, actionId: ActionId, settingName: String): String {
        return "${address.raw}-action-${actionId.raw}-$settingName"
    }

    private fun notifyCustomizationsChanged() {
        listeners.forEach { it.onCustomizationsChanged() }
    }
}