package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.common.extensions.copy
import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.model.Actions
import com.maroonbells.maja.domain.board.value.Action
import com.maroonbells.maja.domain.board.value.ActionId
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class FeatureActions(features: FeatureManager<*>) : FeatureNode<Actions>(features), Actions {

    override var maxActions: Int = 0
    override var maxStepsPerAction: Int = 0

    override val list: List<Action>
        get() = mutableList.copy()

    override val canAddActions: Boolean
        get() = list.size < maxActions

    private val mutableList = mutableListOf<Action>()

    init {
        board.actions.onNext {
            maxActions = it.maxActions
            maxStepsPerAction = it.maxStepsPerAction

            mutableList.clear()
            mutableList.addAll(it.actions)

            dispatchChange()
        }
    }

    override fun createAsync(steps: List<Action.Step>): Deferred<ActionId> = scope.async {
        val previousList = list
        board.actions.require().create(steps).join()
        board.actions.refresh().join()

        return@async requireNewAction(previousList, list).id
    }

    override fun update(id: ActionId, steps: List<Action.Step>): Job = scope.launch {
        board.actions.require().update(id, steps).join()
        board.actions.refresh().join()
    }

    override fun remove(id: ActionId): Job = scope.launch {
        board.actions.require().remove(id).join()
        board.actions.refresh().join()
    }

    override fun get(actionId: ActionId?): Action? {
        return list.find { it.id == actionId }
    }

    private fun findNewAction(prev: List<Action>, next: List<Action>): Action? {
        return next.lastOrNull { it !in prev }
    }

    private fun requireNewAction(prev: List<Action>, next: List<Action>): Action {
        return requireNotNull(findNewAction(prev, next))
    }
}