package com.maroonbells.maja.domain.board.logic

import com.maroonbells.maja.common.observable.CompositeDisposable
import com.maroonbells.maja.domain.board.BoardManager
import com.maroonbells.maja.domain.board.BoardManager.BoardListener
import com.maroonbells.maja.domain.board.logic.ActionManager.CustomizedActionsListener
import com.maroonbells.maja.domain.board.model.Actions
import com.maroonbells.maja.domain.board.model.Board
import com.maroonbells.maja.domain.board.value.*
import com.maroonbells.maja.domain.customization.CustomizationManager
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_COLOR
import com.maroonbells.maja.domain.customization.CustomizationManager.Companion.DEFAULT_ACTION_ICON
import com.maroonbells.maja.domain.customization.CustomizationManager.CustomizationsListener
import com.maroonbells.maja.domain.customization.value.Color
import com.maroonbells.maja.domain.customization.value.Icon
import com.maroonbells.maja.domain.session.BoardSessionManager
import com.maroonbells.maja.domain.session.BoardSessionManager.BoardSessionListener
import kotlinx.coroutines.*
import java.util.concurrent.CopyOnWriteArrayList

class MajaActionManager(
    private val sessionManager: BoardSessionManager,
    private val boardManager: BoardManager,
    private val customizationManager: CustomizationManager
) : ActionManager, BoardSessionListener, BoardListener, CustomizationsListener {

    private val listeners = CopyOnWriteArrayList<CustomizedActionsListener>()
    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())
    private val disposeOnDisconnected = CompositeDisposable()

    private val board: Board?
        get() = boardManager.board

    private val actions: Actions?
        get() = board?.actions

    override val list: List<CustomizedAction>
        get() = asCustomizedActions(actions?.list ?: emptyList())

    override val maxActions: Int
        get() = actions?.maxActions ?: 0

    override val maxStepsPerAction: Int
        get() = actions?.maxStepsPerAction ?: 0

    override val canAddActions: Boolean
        get() = actions?.canAddActions ?: false

    override val defaultIcon: Icon
        get() = DEFAULT_ACTION_ICON

    override val defaultColor: Color
        get() = DEFAULT_ACTION_COLOR

    override val availableIcons: List<Icon>
        get() = customizationManager.availableIcons

    override val availableColors: List<Color>
        get() = customizationManager.availableColors

    init {
        sessionManager.addBoardSessionListener(this)
    }

    override fun onBoardSessionCreated(address: BoardAddress) {
        boardManager.addBoardListener(this)
        customizationManager.addCustomizationsListener(this)
    }

    override fun onBoardSessionDestroyed() {
        boardManager.removeBoardListener(this)
        customizationManager.removeCustomizationsListener(this)
        sessionManager.removeBoardSessionListener(this)
    }

    override fun addActionsListener(listener: CustomizedActionsListener) {
        if (listeners.addIfAbsent(listener)) {
            listener.onCustomizedActionsChanged(list)
        }
    }

    override fun removeActionsListener(listener: CustomizedActionsListener?) {
        listeners.remove(listener)
    }

    override fun create(request: UpdateCustomizedActionRequest): Job = scope.launch {
        val board = requireNotNull(board)
        val actions = requireNotNull(actions)

        val actionId = actions.createAsync(request.steps).await()

        customizationManager.setCustomName(board.address, actionId, request.name)
        customizationManager.setCustomIcon(board.address, actionId, request.icon ?: DEFAULT_ACTION_ICON)
        customizationManager.setCustomColor(board.address, actionId, request.color ?: DEFAULT_ACTION_COLOR)
        customizationManager.setAvailableAsScene(board.address, actionId, request.availableAsScene ?: false)
    }

    override fun update(id: ActionId, request: UpdateCustomizedActionRequest): Job = scope.launch {
        val board = requireNotNull(board)
        val actions = requireNotNull(actions)

        actions.update(id, request.steps).join()

        customizationManager.setCustomName(board.address, id, request.name)
        customizationManager.setCustomIcon(board.address, id, request.icon ?: DEFAULT_ACTION_ICON)
        customizationManager.setCustomColor(board.address, id, request.color ?: DEFAULT_ACTION_COLOR)
        customizationManager.setAvailableAsScene(board.address, id, request.availableAsScene ?: false)
    }

    override fun remove(id: ActionId): Job = scope.launch {
        val actions = requireNotNull(actions)
        actions.remove(id).join()
    }

    override fun get(actionId: ActionId?): CustomizedAction? {
        return list.firstOrNull { it.id == actionId }
    }

    override fun asCustomizedActionStep(step: Action.Step): CustomizedActionStep? {
        val board = board ?: return null
        val pin = board.pinout[step.pinId] ?: return null

        return CustomizedActionStep(
            pinId = step.pinId,
            pinValue = step.pinValue,
            pinName = customizationManager.getName(board.address, pin),
            pinType = pin.type)
    }

    override fun onConnecting() = Unit

    override fun onConnected(board: Board) {
        disposeOnDisconnected += board.actions.onChanged {
            dispatchActionsChanged(list)
        }
    }

    override fun onDisconnected(board: Board?, cause: Throwable?) {
        scope.coroutineContext.cancelChildren()
        disposeOnDisconnected.dispose()
        dispatchActionsChanged(list)
    }

    override fun onCustomizationsChanged() {
        dispatchActionsChanged(list)
    }

    private fun asCustomizedActions(actions: List<Action>): List<CustomizedAction> {
        val board = board ?: return emptyList()

        return actions.map { action ->
            CustomizedAction(
                id = action.id,
                steps = action.steps.mapNotNull(::asCustomizedActionStep),
                name = customizationManager.getName(board.address, action.id),
                customName = customizationManager.getCustomName(board.address, action.id),
                icon = customizationManager.getIcon(board.address, action.id),
                color = customizationManager.getColor(board.address, action.id),
                availableAsScene = customizationManager.isAvailableAsScene(board.address, action.id))
        }
    }

    private fun dispatchActionsChanged(actions: List<CustomizedAction>) {
        for (listener in listeners) {
            listener.onCustomizedActionsChanged(actions)
        }
    }
}