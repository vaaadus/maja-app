package com.maroonbells.maja.domain.board.feature.model

import com.maroonbells.maja.domain.board.feature.FeatureManager
import com.maroonbells.maja.domain.board.feature.FeatureRegistry.board
import com.maroonbells.maja.domain.board.model.Schedule
import com.maroonbells.maja.domain.board.model.ScheduleId
import com.maroonbells.maja.domain.board.value.datetime.Weekday
import com.maroonbells.maja.domain.board.value.schedule.ScheduleEntries
import kotlinx.coroutines.launch

class FeatureSchedule(features: FeatureManager<*>, override val id: ScheduleId)
    : FeatureNode<Schedule>(features), Schedule {

    override var entries: ScheduleEntries = ScheduleEntries.EMPTY

    init {
        board.schedules.require().let {
            entries = entries.toBuilder()
                .maxBlocksPerDay(it.maxBlocksPerDay)
                .build()
        }

        board.schedule.of(id).onNext {
            entries = entries.toBuilder()
                .clearAll()
                .addAll(Weekday.Monday, it.monday)
                .addAll(Weekday.Tuesday, it.tuesday)
                .addAll(Weekday.Wednesday, it.wednesday)
                .addAll(Weekday.Thursday, it.thursday)
                .addAll(Weekday.Friday, it.friday)
                .addAll(Weekday.Saturday, it.saturday)
                .addAll(Weekday.Sunday, it.sunday)
                .build()

            dispatchChange()
        }
    }

    override fun setEntries(entries: ScheduleEntries) = scope.launch {
        for (weekday in Weekday.values()) {
            if (detectChanges(entries, weekday)) {
                board.schedule.of(id).require()
                    .setEntries(weekday, entries.blocksAt(weekday))
                    .join()
            }
        }

        board.schedule.of(id).refresh()
            .join()
    }

    private fun detectChanges(newEntries: ScheduleEntries, day: Weekday): Boolean {
        return entries.blocksAt(day) != newEntries.blocksAt(day)
    }
}