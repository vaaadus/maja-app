package com.maroonbells.maja.domain.board.feature.name

import com.maroonbells.maja.domain.board.feature.registry.Feature
import kotlin.reflect.KClass

@Suppress("FunctionName")
inline fun <reified T : Feature> FeatureClassPattern(pattern: FeaturePattern): FeatureClassPattern<T> {
    return FeatureClassPattern(T::class, pattern)
}

data class FeatureClassPattern<T : Feature>(
    override val raw: KClass<T>,
    val pattern: FeaturePattern) : FeatureClass<T> {

    private val regex by lazy { pattern.regex() }

    override fun matches(path: FeaturePath): Boolean {
        return path.matches(regex)
    }

    fun of(vararg args: Any): FeatureClassPath<T> {
        return FeatureClassPath(raw, pattern.of(*args))
    }
}