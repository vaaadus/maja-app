package com.maroonbells.maja.domain.board.feature.name


inline class FeaturePattern(val raw: String) {

    fun of(vararg args: Any): FeaturePath {
        return FeaturePath(raw.format(*args))
    }

    fun regex(): Regex {
        val pattern = raw
            .replace("%s", "[a-zA-Z0-9:]+")
            .replace(".", "\\.")

        return Regex(pattern)
    }
}