package com.maroonbells.maja.domain.board.model

import com.maroonbells.maja.domain.board.value.ActionId
import com.maroonbells.maja.domain.board.value.Scene
import com.maroonbells.maja.domain.board.value.SceneId
import kotlinx.coroutines.Job
import java.util.*

interface Scenes : Node<Scenes> {

    val list: List<Scene>
    val maxScenes: Int

    fun create(actionId: ActionId): Job

    fun update(scene: Scene): Job

    fun remove(id: SceneId): Job
}